import { defineStore } from 'pinia'

export const userStore = defineStore('user', {
  state: () => {
    return {
      userInfo: {},
      supplier_pay_enable: true,
    }
  },
  getters: {},
  actions: {},
})
