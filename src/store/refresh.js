import { defineStore } from 'pinia'

export const refreshStore = defineStore('refresh', {
  state: () => {
    return {
      disabled: false,
    }
  },
  getters: {},
  actions: {},
})
