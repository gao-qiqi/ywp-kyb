import { createApp } from 'vue'
import Toast from './toast.vue'

export default {
  instance: null,
  parent: null,
  open(options) {
    this.instance = createApp(Toast, options)
    this.parent = document.createElement('div')
    let appDom = document.getElementById('app')
    appDom.appendChild(this.parent)
    this.instance.mount(this.parent)
  },
  close() {
    this.instance.unmount()
  },
}
