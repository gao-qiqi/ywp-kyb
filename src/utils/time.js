/**
 * 目录
 * 1、 getRemaining === 根据剩余秒数 获取 剩余 天 时 分 秒
 * 2、 dateParse    === 获取时间戳  兼容ios
 * 3、 date2Obj     === 时间转对象 可转化 自己想的格式
 * */

/**
 * 根据剩余秒数 获取 剩余 天 时 分 秒
 * 入参：str-秒数
 * */
export function getRemaining(str) {
  const second = str % 60 > 9 ? str % 60 : '0' + (str % 60)
  const minute =
    Math.floor((str / 60) % 60) > 9
      ? Math.floor((str / 60) % 60)
      : '0' + Math.floor((str / 60) % 60)
  const hour =
    Math.floor((str / 3600) % 24) > 9
      ? Math.floor((str / 3600) % 24)
      : '0' + Math.floor((str / 3600) % 24)
  const day =
    Math.floor(str / 3600 / 24) > 9
      ? Math.floor(str / 3600 / 24)
      : '0' + Math.floor(str / 3600 / 24)
  const totalHour =
    +hour + +day * 24 > 9 ? +hour + +day * 24 : '0' + (+hour + +day * 24)
  const symbolStr = `${day}天${hour}时${minute}分${second}秒`
  const symbolTime = `${hour}:${minute}:${second}`
  return {
    second,
    minute,
    hour,
    day,
    totalHour,
    symbolStr,
    symbolTime,
  }
}

/**
 * 获取时间戳  兼容ios
 * 入参：time-时间格式
 * */
export function dateParse(time = new Date()) {
  if (typeof time === 'string') time = time.replace(/-/g, '/')
  return Date.parse(new Date(time))
}

/**
 * 时间转对象 可转化 自己想的格式
 * 入参：time-时间格式 symbol-转化年月日的间隔符号
 * */
export function date2Obj(time, symbol = '-') {
  if (typeof time === 'string') time = time.replace(/-/g, '/')
  time = new Date(time.replace(/-/g, '/')) || new Date()
  const year = time.getFullYear().toString()
  const month = (
    time.getMonth() + 1 < 10 ? '0' + (time.getMonth() + 1) : time.getMonth() + 1
  ).toString()
  const day = (
    time.getDate() < 10 ? '0' + time.getDate() : time.getDate()
  ).toString()
  const hour = time.getHours() < 10 ? '0' + time.getHours() : time.getHours()
  const minute =
    time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes()
  const second =
    time.getSeconds() < 10 ? '0' + time.getSeconds() : time.getSeconds()
  const symbolTime =
    year +
    symbol +
    month +
    symbol +
    day +
    ' ' +
    hour +
    ':' +
    minute +
    ':' +
    second
  const symbolTime2 =
    month + symbol + day + ' ' + hour + ':' + minute + ':' + second
  const justTime = hour + ':' + minute
  const symbolDate = year + symbol + month + symbol + day
  const symbolCn = `${year}年${month}月${day}日 ${hour}:${minute}:${second}`
  const symbolMonth = `${month}月${day}日 ${hour}:${minute}`
  return {
    year,
    month,
    day,
    hour,
    minute,
    second,
    symbolTime,
    symbolTime2,
    justTime,
    symbolDate,
    symbolCn,
    symbolMonth,
  }
}

export function arrayToTime(list, symbol = '-') {
  if (!list || !list.length) return ''
  return date2Obj(`${list[0]}-${list[1]}-${list[2]}`, symbol).symbolDate
}

export function timeToArray(time) {
  const timeObj = date2Obj(time)
  return [timeObj.year, timeObj.month, timeObj.day]
}

export function formatTime(time, type = 'time') {
  if (!time) return ''
  time = String(time)
  if (type === 'date') {
    return time.slice(0, 4) + '-' + time.slice(4, 6) + '-' + time.slice(6, 8)
  } else {
    return (
      time.slice(0, 4) +
      '-' +
      time.slice(4, 6) +
      '-' +
      time.slice(6, 8) +
      ' ' +
      time.slice(8, 10) +
      ':' +
      time.slice(10, 12) +
      ':' +
      time.slice(12, 14)
    )
  }
}
