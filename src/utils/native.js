import dsBridge from 'dsbridge'

function getTokenBySync() {
  return dsBridge.call('getTokenSync')
}

function exitWeb() {
  return dsBridge.call('exitWeb', {}, () => {})
}

function jumpExternalLink(params = {}) {
  return dsBridge.call('jumpExternalLink', params, () => {})
}

function getToLogin() {
  dsBridge.call('getToLogin')
}

export default {
  getTokenBySync,
  exitWeb,
  getToLogin,
  jumpExternalLink,
}
