function enNameValidator(value, length) {
  if (value.length > length) return false
  const chineseReg =
    /^(?:[\u3400-\u4DB5\u4E00-\u9FEA\uFA0E\uFA0F\uFA11\uFA13\uFA14\uFA1F\uFA21\uFA23\uFA24\uFA27-\uFA29]|[\uD840-\uD868\uD86A-\uD86C\uD86F-\uD872\uD874-\uD879][\uDC00-\uDFFF]|\uD869[\uDC00-\uDED6\uDF00-\uDFFF]|\uD86D[\uDC00-\uDF34\uDF40-\uDFFF]|\uD86E[\uDC00-\uDC1D\uDC20-\uDFFF]|\uD873[\uDC00-\uDEA1\uDEB0-\uDFFF]|\uD87A[\uDC00-\uDFE0])+$/
  return !chineseReg.test(value)
}

// 不含空格，不超过64
const contractNoValidator = (val) => {
  if (val.length > 64) return false
  return !/\s+/g.test(val)
}

// 不含中文，不超过64
const buyerNameValidator = (val) => {
  return enNameValidator(val, 64)
}

const amountValidator = (val) => {
  if (val <= 0) return false
  if (val.length > 15) return false
  const numReg = /(^[0-9]+$)|(^[0-9]+[\.]{1}[0-9]{1,2}$)/
  return numReg.test(val)
}

const goodsEnNameValidator = (val) => {
  return enNameValidator(val, 32)
}

const goodsCnNameValidator = (val) => {
  if (val.length > 32) return false
}

const goodsQuantityValidator = (val) => {
  if (val <= 0) return false
  if (val > 2147483647) return false
  if (val.length > 13) return false
  const numReg = /(^[0-9]+$)|(^[0-9]+[\.]{1}[0-9]{1,4}$)/
  return numReg.test(val)
}

const forwarderAgentNameValidator = (val) => {
  if (val.length > 64) return false
}

const logisticsCopNameValidator = (val) => {
  if (val.length > 64) return false
}

const logisticsBillNoValidator = (val) => {
  if (val.length > 512) return false
}

const declVoucherNoValidator = (val) => {
  if (val.length > 32) return false
}

const fileTypeValidator = (val, type) => {
  const reg = new RegExp(`.(${type.join('|')})$`, 'i')
  return reg.test(val)
}

const trimObject = (obj) => {
  try {
    const data = {}
    Object.keys(obj).forEach((key) => {
      if (obj[key] && typeof obj[key] === 'string') {
        data[key] = obj[key].trim()
      } else {
        data[key] = obj[key]
      }
    })
    return data
  } catch (e) {
    return obj
  }
}
export {
  contractNoValidator,
  buyerNameValidator,
  amountValidator,
  goodsEnNameValidator,
  goodsCnNameValidator,
  goodsQuantityValidator,
  forwarderAgentNameValidator,
  logisticsCopNameValidator,
  logisticsBillNoValidator,
  declVoucherNoValidator,
  fileTypeValidator,
  trimObject,
}
