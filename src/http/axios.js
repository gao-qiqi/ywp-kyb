import axios from 'axios'
import nativeApi from '../utils/native'
import { showToast } from 'vant'
const KJT_ENV = import.meta.env.VITE_REQUEST_BASE_URL
// 正在进行中的请求列表
// let reqList = []

/**
 * 阻止重复请求
 * @param {array} reqList - 请求缓存列表
 * @param {function} cancel - 请求中断函数
 */
// const stopRepeatRequest = function (req, index) {
//   req.cancel()
//   reqList.splice(index, 1)
// }

// 请求之前
axios.interceptors.request.use(
  (config) => {
    // let cancel
    // 设置cancelToken对象
    // config.cancelToken = new axios.CancelToken(function (c) {
    //   cancel = c
    // })
    // for (let i = 0, len = reqList.length; i < len; i++) {
    //   // 阻止重复请求
    //   if (reqList[i].url === config.url) {
    //     stopRepeatRequest(reqList[i], i, config.url)
    //     break
    //   }
    // }
    // reqList.push({
    //   url: config.url,
    //   date: Date.parse(),
    //   cancel,
    // })
    config.timeout = 20000
    config.headers.authorization =
      localStorage.cg_kyb_token || nativeApi.getTokenBySync()
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

// 请求回来
axios.interceptors.response.use(
  (res) => {
    if (res.request.responseURL.includes(KJT_ENV)) {
      // 走我们的网管
      if (res.data.code === 'S0001') {
        return res.data.bizContent
      } else {
        if(!(res.config && res.config.noToastErrorMsg)){
          showToast(res.data.subMsg || res.data.msg)
        }
        return Promise.reject(res.data)
      }
    } else {
      // 增加延迟，相同请求不得在短时间内重复发送
      if (res.data.code !== 20000) {
        if (!res.config.noCheck) {
          showToast(res.data.message)
          return Promise.reject(res)
        } else {
          return res.data
        }
      } else {
        return res.data
      }
    }
  },
  (error) => {
    const { response } = error
    // console.log(error, `this is error`)
    if (response) {
      if (response.status === 401) {
        nativeApi.getToLogin()
      } else {
        showToast(response.data.message)
      }
    }
    return Promise.reject(error)
  }
)

// 声明请求方法
const request = (options = {}) => {
  return axios(options)
}

export default request
