import Model from '@/http/index.js'
class userModel extends Model {
  getUser(options = {}) {
    const params = {
      url: `/kyb/cust/getByUserId`,
      params: options,
    }
    return this.get(params)
  }

  getUserCoin(options = {}) {
    const params = {
      url: `/kyb/cust-bal-detail/get`,
      params: options,
    }
    return this.get(params)
  }
}

export default new userModel()
