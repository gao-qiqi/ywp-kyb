import Model from '@/http/index.js'
class couponModel extends Model {
  // 提现时 获取优惠券列表
  getCouponInWithdrawing(options = {}) {
    const params = {
      url: `/kyb/withdrawal/couponQuery`,
      params: options,
    }
    return this.get(params)
  }

  // 获取入账详情
  getEnterDetail(options = {}) {
    const params = {
      url: `/kyb/trade/getWithDetail`,
      params: options,
    }
    return this.get(params)
  }
}

export default new couponModel()
