import Model from '@/http/index.js'

class tradeModel extends Model {
  // 获取入账列表
  getEnterList(options = {}) {
    const params = {
      url: `/kyb/trade/listEntitysByPage`,
      data: options,
    }
    return this.post(params)
  }

  // 获取入账详情
  getEnterDetail(options = {}) {
    const params = {
      url: `/kyb/trade/getWithDetail`,
      params: options,
    }
    return this.get(params)
  }

  // 获取入账列表
  getWithdrawList(options = {}) {
    const params = {
      url: `/kyb/withdrawal/listEntitysByPage`,
      data: options,
    }
    return this.post(params)
  }

  // 获取提现详情
  getWithdrawDetail(options = {}) {
    const params = {
      url: `/kyb/withdrawal/get`,
      params: options,
    }
    return this.get(params)
  }

  // 获取提现费率
  getWithdrawFee(options = {}) {
    const params = {
      url: `/kyb/withdrawal/initQuery`,
      params: options,
    }
    return this.get(params)
  }

  // 计算提现费用
  getCalcFee(options = {}) {
    const params = {
      url: `/kyb/withdrawal/calculateRefresh`,
      params: options,
      cancelParames: Date.parse(),
    }
    return this.get(params)
  }

  // 提现申请
  submitApply(options = {}) {
    const params = {
      url: `/kyb/withdrawal/submit`,
      data: options,
    }
    return this.post(params)
  }

  // 提现确认
  submitConfirm(options = {}) {
    const params = {
      url: `/kyb/withdrawal/confirm`,
      params: options,
      noCheck: true,
    }
    return this.get(params)
  }

  // 关联合同订单
  connectOrder(options = {}) {
    const params = {
      url: `/kyb/trade-contract/add`,
      data: options,
      noCheck: true,
    }
    return this.post(params)
  }

  // 关联合同订单
  updateConnectOrder(options = {}) {
    const params = {
      url: `/kyb/trade-contract/addWithUpdate`,
      data: options,
      noCheck: true,
    }
    return this.post(params)
  }
}

export default new tradeModel()
