import Model from '@/http/index.js'
import axios from 'axios'
import { getAxioCommonBody } from '@/utils/utility'
class commonModel extends Model {
  regionList(data) {
    return this.post({
      url: `/kyb/country-code/listEntitysByPage`,
      data,
    })
  }
  uploadFile(data) {
    return this.post({
      url: `/kyb/file/upload`,
      data,
    })
  }

  getEnumsByName(params) {
    return this.get({
      url: `/kyb/enums/getEnums`,
      params,
    })
  }

  // GATEWA_API  data:业务参数
  gatewayApi(data = {}) {
    // options为业务参数
    let gateOpt = getAxioCommonBody()
    let gatewayOpt = { ...gateOpt, ...data }
    //请求参数 格式化
    gatewayOpt.bizContent = JSON.stringify(data.bizContent)
    return this.post({
      baseURL: import.meta.env.VITE_REQUEST_BASE_URL,
      url: `/api`,
      data: JSON.stringify(gatewayOpt),
      noToastErrorMsg:data.noToastErrorMsg,//不toast错误提醒，catch自己捕获获取
      headers: {
        'Content-Type': 'application/json',
      },
    })
  }

}

export default new commonModel()
