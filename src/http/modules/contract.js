import Model from '@/http/index.js'
class contractModel extends Model {
  // 获取合同列表
  getList(options = {}) {
    const params = {
      url: `/kyb/contract/listEntitysByPage`,
      data: options,
    }
    return this.post(params)
  }
  // 新增合同
  addContract(data) {
    return this.post({
      url: `/kyb/contract/add`,
      data,
      noCheck: true,
    })
  }
  // 新增合同订单商品信息
  addContractGoods(data) {
    return this.post({
      url: `/kyb/contract-goods/adds`,
      data,
      noCheck: true,
    })
  }
  // 新增合同订单物流信息
  addContractShip(data) {
    return this.post({
      url: `/kyb/contract-ship/add`,
      data,
      noCheck: true,
    })
  }
  // 提交合同
  addContractSubmit(params) {
    return this.get({
      url: `/kyb/contract/submit`,
      params,
      noCheck: true,
    })
  }
  // 编辑完提交合同
  submitUpdate(params) {
    return this.get({
      url: `/kyb/contract/submitUpdate`,
      params,
      noCheck: true,
    })
  }
  // 合同详情
  getContractDetail(params) {
    return this.get({
      url: `/kyb/contract/getWithDetail`,
      params,
    })
  }
  // 交易流水
  getTradeContractDetail(params) {
    return this.get({
      url: `/kyb/trade-contract/getByContractNo`,
      params,
    })
  }

  // 关闭合同
  closeContract(params) {
    return this.get({
      url: `/kyb/contract/close`,
      params,
    })
  }
  // 完成合同
  doneContract(params) {
    return this.get({
      url: `/kyb/contract/complete`,
      params,
    })
  }

  // 删除合同
  deleteContract(params) {
    return this.delete({
      url: `/kyb/contract/delete`,
      params,
    })
  }

  // 获取合同订单详情
  getContractOrderById(params) {
    return this.get({
      url: `/kyb/contract/get`,
      params,
    })
  }

  // 编辑合同订单详情
  editContractOrder(data) {
    return this.put({
      url: `/kyb/contract/edit`,
      data,
      noCheck: true,
    })
  }

  // 获取合同订单商品详情
  getContractGoodsById(params) {
    return this.get({
      url: `/kyb/contract-goods/getByContractId`,
      params,
    })
  }

  // 编辑合同订单商品详情
  editContractGoods(data) {
    return this.post({
      url: `/kyb/contract-goods/edits`,
      data,
      noCheck: true,
    })
  }

  // 获取合同订单物流详情
  getContractShipById(params) {
    return this.get({
      url: `/kyb/contract-ship/getByContractId`,
      params,
    })
  }

  // 编辑合同订单物流详情
  editContractShip(data) {
    return this.post({
      url: `/kyb/contract/editContractShip`,
      data,
      noCheck: true,
    })
  }

  editSelfContractShip(data) {
    return this.put({
      url: `/kyb/contract-ship/edit`,
      data,
      noCheck: true,
    })
  }

  updateAmount(data) {
    return this.post({
      url: `/kyb/contract/editContractAmount`,
      data,
      noCheck: true,
    })
  }
}

export default new contractModel()
