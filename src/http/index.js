import request from './axios.js'
const baseUrl = import.meta.env.VITE_BASE_API
class Model {
  constructor(baseURL, options = {}) {
    this.options = {
      baseURL: baseURL || baseUrl,
      // withCredentials: true,
      ...options,
    }
  }
  get(options = {}) {
    return request({
      method: 'get',
      ...this.options,
      ...options,
    })
  }

  post(options = {}) {
    return request({
      method: 'post',
      ...this.options,
      ...options,
    })
  }

  put(options = {}) {
    return request({
      method: 'put',
      ...this.options,
      ...options,
    })
  }

  delete(options = {}) {
    return request({
      method: 'delete',
      ...this.options,
      ...options,
    })
  }
}

export default Model
