export const enumDslList = [
  {
    dslDef: 'APOLLO|enums.NationalityEnums',
    fieldName: 'Nationality',
  },
  {
    dslDef: 'ENUM|com.kjtpay.common.enums.payment.CurrencyType',
    fieldName: 'CurrencyType',
  },
  {
    dslDef: 'ENUM|com.kjtpay.trade.util.enums.YesNoEnum',
    fieldName: 'SetExec',
  },
  {
    dslDef: 'ENUM|com.kjtpay.trade.util.enums.BuyerTypeEnum|getType|getDesc',
    fieldName: 'BuyerType',
  },
  {
    dslDef: 'ENUM|com.kjtpay.trade.util.enums.YesNoEnum',
    fieldName: 'DeclTypeEnum',
  },
  {
    dslDef: 'ENUM|com.kjtpay.trade.util.enums.YesNoEnum',
    fieldName: 'WhetherShip',
  },
  {
    dslDef: 'ENUM|com.kjtpay.trade.util.enums.DealTypeEnum|getCode|getDesc',
    fieldName: 'DealType',
  },
  {
    dslDef:
      'ENUM|com.kjtpay.trade.util.enums.LogisticsTypeEnum|getCode|getDesc',
    fieldName: 'LogisticsType',
  },
  {
    dslDef: 'ENUM|com.kjtpay.trade.util.enums.PayTypeEnum|getCode|getDesc',
    fieldName: 'PayType',
  },
  {
    dslDef: 'ENUM|com.kjtpay.trade.util.enums.ShipModeEnum|getCode|getDesc',
    fieldName: 'ShipMode',
  },
  {
    dslDef: 'ENUM|com.kjtpay.trade.util.enums.GoodsUnitEnum|getCode|getDesc',
    fieldName: 'GoodsUnit',
  },
  {
    dslDef:
      'ENUM|com.kjtpay.trade.util.enums.WithdrawStatusEnum|getCode|getMessageWrap',
    fieldName: 'WithdrawStatus',
  },
  {
    dslDef:
      'ENUM|com.kjtpay.trade.util.enums.ContractOrderStatus|getStatus|getCgName',
    fieldName: 'ContractOrderStatus',
  },
  {
    dslDef:
      'ENUM|com.kjtpay.trade.util.enums.IncomingAuditStatus|getStatus|getCgName',
    fieldName: 'IncomingAuditStatus',
  },
  {
    dslDef:
      'ENUM|com.kjtpay.trade.util.enums.IncomingEntryStatus|getStatus|getMessage',
    fieldName: 'EntryAuditStatus',
  },
  {
    dslDef:
      'ENUM|com.kjtpay.trade.util.enums.GoodsCategoryEnum|getCode|getDesc',
    fieldName: 'GoodsCategoryEnum',
  },
]
