const coinList = [
  { name: 'HKD港币', value: 'HKD', remark: '港币' },
  { name: 'USD美元', value: 'USD', remark: '美元' },
  { name: 'EUR欧元', value: 'EUR', remark: '欧元' },
  { name: 'JPY日元', value: 'JPY', remark: '日元' },
  { name: 'GBP英镑', value: 'GBP', remark: '英镑' },
  { name: 'AUD澳元', value: 'AUD', remark: '澳元' },
  { name: 'CAD加拿大元', value: 'CAD', remark: '加拿大元' },
  { name: 'SGD新加坡币', value: 'SGD', remark: '新加坡币' },
  { name: 'DKK丹麦克朗', value: 'DKK', remark: '丹麦克朗' },
  { name: 'NOK挪威克朗', value: 'NOK', remark: '挪威克朗' },
  { name: 'SEK瑞典克朗', value: 'SEK', remark: '瑞典克朗' },
  { name: 'CHF瑞士法郎', value: 'CHF', remark: '瑞士法郎' },
  { name: 'NZD新西兰元', value: 'NZD', remark: '新西兰元' },
  { name: 'ZAR南非兰特', value: 'ZAR', remark: '南非兰特' },
  { name: 'TRY土耳其新里拉', value: 'TRY', remark: '土耳其新里拉' },
  { name: 'CNH离岸人民币', value: 'CNH', remark: '离岸人民币' },
]

const withdrawStatusList = [
  { name: '提现处理中', value: 'TRADE_WAIT_PAY', remark: '提现处理中' },
  { name: '提现失败', value: 'TRADE_FAILED', remark: '提现失败' },
  { name: '提现成功', value: 'TRADE_SUCCESS', remark: '提现成功' },
]
const payStatusList = [
  { name: '付款处理中', value: 'TRADE_WAIT_PAY', remark: '付款处理中' },
  { name: '付款失败', value: 'TRADE_FAILED', remark: '付款失败' },
  { name: '付款成功', value: 'TRADE_SUCCESS', remark: '付款成功' },
]
const enterStatusList = [
  //TODO
  {
    name: '待关联订单',
    value: 'Initial',
    remark: '待关联订单',
  },
  { name: '审核中', value: 'Pending', remark: '审核中' },
  { name: '待跟进', value: 'Returned', remark: '待跟进' },
  { name: '已入帐', value: 'Approved', remark: '已入帐' },
  {
    name: '入帐拒绝-已退款',
    value: 'Refunded',
    remark: '入帐拒绝-已退款',
  },
  {
    name: '入账拒绝-未退款',
    value: 'Rejected',
    remark: '入账拒绝-未退款',
  },
]

export { coinList, enterStatusList, withdrawStatusList, payStatusList }
