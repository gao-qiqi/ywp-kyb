const statusList = [
  { name: '待跟进', value: 'Returned' },
  { name: '草稿订单', value: 'Init,Draft,SubmitFailed,SubmitProcessing' },
  { name: '已关闭', value: 'Closed' },
  { name: '收款中', value: 'Pending,Risking,Approved' },
  { name: '收款成功', value: 'Completed' },
  { name: ' 待关联入帐流水', value: 'SubmitSuccess,Initial' },
]

const orderStatusList = [
  { name: '已发货', value: 'Y' },
  { name: '未发货', value: 'N' },
]

export { statusList, orderStatusList }
