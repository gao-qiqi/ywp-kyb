import { createRouter, createWebHistory } from 'vue-router'

const modules = import.meta.globEager('./modules/*.js')

function setupRoutes() {
  let routeModule = []
  for (var i in modules) {
    routeModule = routeModule.concat(
      Array.isArray(modules[i].default)
        ? modules[i].default
        : [modules[i].default]
    )
  }
  return routeModule
}
const routes = [
  {
    path: '/',
    component: () => import('@/layout/index.vue'),
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: () => import('@/pages/index.vue'),
        meta: {
          title: 'YiwuPay义支付跨境',
          exitWeb: true,
          keep: true,
        },
      },
      {
        path: 'trade',
        component: () => import('@/pages/trade/index.vue'),
        meta: {
          title: '交易记录',
          hideBack: true,
          keep: true,
        },
      },
      {
        path: 'order',
        component: () => import('@/pages/order/index.vue'),
        meta: {
          title: '合同订单',
          hideBack: true,
          keep: true,
        },
      },
      {
        path: 'payment',
        component: () => import('@/pages/payment/index.vue'),
        meta: {
          title: '供应商付款',
          hideBack: true,
        },
      },
    ],
  },
  {
    path: '/mine',
    component: () => import('@/pages/mine/index.vue'),
    meta: {
      title: '我的',
      hideNav: true,
    },
  },
  ...setupRoutes(),
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})
// router.beforeResolve((to) => {
// console.log(to)
// })
export default router
