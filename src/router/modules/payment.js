export default [
  {
    path: '/payment/pay/:id',
    component: () => import('@/pages/payment/pay.vue'),
    meta: {
      title: '供应商付款',
      noRefresh: true,
      appointUrl: '/payment',
    },
  },
  {
    path: '/payment/detail/:id',
    component: () => import('@/pages/payment/payDetail.vue'),
    meta: {
      title: '付款详情',
    },
  },
]
