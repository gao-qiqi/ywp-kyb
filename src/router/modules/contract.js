export default [
  {
    path: '/contract/list',
    component: () => import('@/pages/contract/index.vue'),
    meta: {
      title: '合同列表',
    },
  },
  {
    path: '/contract/create',
    component: () => import('@/pages/contract/create/index.vue'),
    meta: {
      title: '创建合同',
      hideNav: true,
      noRefresh: true,
    },
  },
  {
    path: '/contract/connection/:id',
    component: () => import('@/pages/contract/connection.vue'),
    meta: {
      title: '关联合同订单',
    },
  },
  {
    path: '/contract/detail/:id',
    component: () => import('@/pages/contract/detail/index.vue'),
    meta: {
      title: '合同详情',
    },
  },
  {
    path: '/contract/detail/:id/flow',
    component: () => import('@/pages/contract/detail/transactionHistory.vue'),
    meta: {
      title: '合同关联流水',
    },
  },
]
