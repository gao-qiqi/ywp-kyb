export default [
  {
    path: '/prize',
    component: () => import('@/pages/prize/index.vue'),
    meta: {
      title: '幸运抽奖',
      noRefresh: true,
    },
  },
  {
    path: '/myPrize',
    component: () => import('@/pages/prize/myPrize.vue'),
    meta: {
      title: '我的奖品',
      noRefresh: true,
    },
  },
  {
    path: '/prizeRule',
    component: () => import('@/pages/prize/prizeRule.vue'),
    meta: {
      title: '活动规则',
      noRefresh: true,
    },
  },
]
