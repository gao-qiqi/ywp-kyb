export default [
  {
    path: '/trade/enter/:id',
    component: () => import('@/pages/trade/enterDetail.vue'),
    meta: {
      title: '入账详情',
    },
  },
  {
    path: '/trade/withdraw/:id',
    component: () => import('@/pages/trade/withdrawDetail.vue'),
    meta: {
      title: '提现详情',
    },
  },
  {
    path: '/trade/withdrawing',
    component: () => import('@/pages/trade/withdrawing.vue'),
    meta: {
      title: '提现',
      appointUrl: '/home',
      noRefresh: true,
    },
  },
]
