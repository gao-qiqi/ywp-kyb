import { createApp } from 'vue'
import router from '@/router'
import setupFilter from '@/filters/index.js'
import store from '@/store/index.js'
import 'vant/lib/index.css'
import './style.css'
import App from './App.vue'
import Vconsole from 'vconsole'
import Utility from '@/utils/utility'

const app = createApp(App)

// filter
setupFilter(app)

// 路由
app.use(router)

// 状态管理器
app.use(store)

//测试环境日志
if (Utility.isTestEnv()) {
  let vconsole = new Vconsole()
  app.use(vconsole)
}

app.mount('#app')
