window.onload = () => {
  const docEl = document.documentElement
  const resizeEvt =
    'orientationchange' in window ? 'orientationchange' : 'resize'
  const recalc = function () {
    const clientWidth = docEl.clientWidth
    if (!clientWidth) return
    if (clientWidth >= 750) docEl.style.fontSize = '200px'
    else docEl.style.fontSize = `${200 * (clientWidth / 750)}px`
  }
  if (!document.addEventListener) return

  recalc()
  window.addEventListener(resizeEvt, recalc, false)
  document.addEventListener('DOMContentLoaded', recalc, false)
}
