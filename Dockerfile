FROM nginx:1.15.7-alpine

COPY nginx/kyb.conf /etc/nginx/conf.d/default.conf
RUN mkdir -p /usr/src/app/dist
COPY dist /usr/src/app/dist

EXPOSE 80/tcp