
/********************************************************************/
/*                                                                  */
/*  Copyright (c) 2014 Genesis Mobile                               */
/*                                                                  */
/*  This obfuscated code was created by Jasob 4.1 Trial Version.    */
/*  The code may be used for evaluation purposes only.              */
/*  To obtain full rights to the obfuscated code you have to        */
/*  purchase the license key (http://www.jasob.com/Purchase.html).  */
/*                                                                  */
/********************************************************************/

var AM = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
var iI = new Array(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1);
var aE, eW, kM;
var justic3, Tim = 0,
    M = [],
    MM = [],
    T = [],
    miss = null,
    M3, Hid, Show = 0,
    Show2;
var BL = {
    "SV": "",
    "ad": "",
    "KK": "",
    "lol": "0",
    "olo": "0",
    "sh": "0",
    "T": "",
    "passGuard": "",
    "kb": "",
    "FD": "",
    "WH": "",
    "double": "0",
    "key2": "",
    "once": "0",
    "Mov": "0",
    "fixed": ""
};
var PH = {
    "Id": "",
    "s_n": "",
    "pm": "",
    "cs": "0",
    "add": "1",
    "arrPlace": new Array(),
    "L": [],
    "arrId": [],
    "arrPGD": []
};

function aB(bS) {
    var jj, hF, fG, eR;
    var i, aG, out;
    aG = bS.length;
    i = 0;
    out = "";
    while(i < aG) {
        do {
            jj = iI[bS.charCodeAt(i++) & 0xff];
        } while (i < aG && jj == -1);
        if(jj == -1) break;
        do {
            hF = iI[bS.charCodeAt(i++) & 0xff];
        } while (i < aG && hF == -1);
        if(hF == -1) break;
        out += String.fromCharCode((jj << 2) | ((hF & 0x30) >> 4));
        do {
            fG = bS.charCodeAt(i++) & 0xff;
            if(fG == 61) return out;
            fG = iI[fG];
        } while (i < aG && fG == -1);
        if(fG == -1) break;
        out += String.fromCharCode(((hF & 0XF) << 4) | ((fG & 0x3C) >> 2));
        do {
            eR = bS.charCodeAt(i++) & 0xff;
            if(eR == 61) return out;
            eR = iI[eR];
        } while (i < aG && eR == -1);
        if(eR == -1) break;
        out += String.fromCharCode(((fG & 0x03) << 6) | eR);
    }
    return out;
};
var u = navigator.userAgent;
var lA = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1;
var nD = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
var hM = function() {
    var r = true;
    if(/MediaPad/i.test(u)) return false;
    r = (/Mobile/i.test(u) && !/ipad/i.test(u));
    return r;
};

function keyBoard(options) {
    this.settings = {
        "id": "",
        "chaosMode": 0,
        "pressStatus": 1,
        "kbType": 0,
        "aQ": {},
        "odd": 1,
        "reset": 0,
        "svg": ""
    };
    this.gg = undefined;
    if(options != undefined) {
        this.settings = options;
    };
    this.caps = false;
    this.shift = false;
    this.dt = false;
    this.aV = false;
    BL.SV = this.settings.svg;
    var bA = this;
    this.cf = function() {
        var aY = "#" + bA.settings.id;
        $(aY).hide();
        document.querySelector(aY).removeEventListener("webkitAnimationEnd", bA.cf, false);
        document.querySelector(aY).removeEventListener("animationEnd", bA.cf, false);
        document.querySelector(aY).removeEventListener("webkitAnimationEnd", bA.ht, false);
        document.querySelector(aY).removeEventListener("animationEnd", bA.ht, false);
    };
    this.orientation = function() {
        var b = u.indexOf('SM-T800') > -1;
        if(!b) {
            return window.orientation;
        }
        var bk = 0;
        switch(window.orientation) {
            case 0:
                bk = 90;
                break;
            case 90:
                bk = 180;
                break;
            case 180:
                bk = -90;
                break;
            case -90:
                bk = 180;
                break;
        }
        return bk;
    };
    this.ma = function() {
        var r = 0;
        if((this.orientation() == 90) || (this.orientation() == -90)) {
            if(nD) {
                if(hM()) r = 0.50;
                else r = 0.40;
            } else if(lA) {
                if(hM()) r = 0.50;
                else r = 0.40;
            } else {
                r = 0.50;
            }
        } else {
            if(nD) {
                if(hM()) r = 0.45;
                else r = 0.40;
            } else if(lA) {
                if(hM()) r = 0.45;
                else r = 0.40;
            } else {
                r = 0.45;
            }
        }
        return r;
    };
    this.ht = function() {
        var aY = "#" + bA.settings.id;
        if((bA.orientation() == 90) || (bA.orientation() == -90)) {
            if(bA.caps) {
                $("#kb_c_CAP").css({
                    background: "url(" + BL.SV + "/shift_DS_H.svg) ",
                    backgroundSize: "100% 100%"
                });
            } else if(bA.shift) {
                $("#kb_c_CAP").css({
                    background: "url(" + BL.SV + "/shift_D_H.svg) ",
                    backgroundSize: "100% 100%"
                });
            } else {
                $("#kb_c_CAP").css({
                    background: "url(" + BL.SV + "/shift_H.svg) ",
                    backgroundSize: "100% 100%"
                });
            }
        } else {
            if(bA.caps) {
                $("#kb_c_CAP").css({
                    background: "url(" + BL.SV + "/shift_DS.svg) ",
                    backgroundSize: "100% 100%"
                });
            } else if(bA.shift) {
                $("#kb_c_CAP").css({
                    background: "url(" + BL.SV + "/shift_D.svg) ",
                    backgroundSize: "100% 100%"
                });
            } else {
                $("#kb_c_CAP").css({
                    background: "url(" + BL.SV + "/shift.svg) ",
                    backgroundSize: "100% 100%"
                });
            }
        }
        if(BL.jianpan == 1) {
            $(".Bank").css({
                background: "url(" + BL.SV + "/logo.svg) ",
                backgroundSize: "100% 100%"
            });
            $(".sure").css({
                background: "#F63"
            });
        } else {
            $(".Bank").css({
                background: "url(" + BL.SV + "/logo2.svg) ",
                backgroundSize: "100% 100%"
            });
            $(".sure").css({
                background: "#36A6F8"
            });
        };
        $(".row1pwd,.row2pwd,.row3pwdb").css({
            background: "url(" + BL.SV + "/anjian.svg)",
            backgroundSize: "100% 100%"
        });
        $(".row123").css({
            background: "url(" + BL.SV + "/123.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".rowdelet,#kb_s_N,#kb_n_S,.row4pwdd,#kb_s_N1").css({
            background: "url(" + BL.SV + "/quanDEL.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".row3pwdd").css({
            background: "url(" + BL.SV + "/quanDEL_2.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".rowspace").css({
            background: "url(" + BL.SV + "/space.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".rowclose").css({
            background: "url(" + BL.SV + "/123.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".pwd").css({
            background: "url(" + BL.SV + "/shuzi_1.svg) ",
            backgroundSize: "100% 100%"
        });
        $("#kb_p_D,#kb_p_CLOSE1").css({
            background: "url(" + BL.SV + "/shuzi_2.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".pwd2").css({
            background: "url(" + BL.SV + "/shuzi_3.svg) ",
            backgroundSize: "100% 100%"
        });
        $("#kb_p_D>div").css({
            background: "url(" + BL.SV + "/shuzi_delete1.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".rowdelet>div").css({
            background: "url(" + BL.SV + "/DEL.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".rowclose>div,#kb_p_CLOSE>div,#kb_p_CLOSE1>div").css({
            background: "url(" + BL.SV + "/done_1.svg) ",
            backgroundSize: "100% 100%"
        });
        document.querySelector(aY).removeEventListener("webkitAnimationEnd", bA.cf, false);
        document.querySelector(aY).removeEventListener("animationEnd", bA.cf, false);
        document.querySelector(aY).removeEventListener("webkitAnimationEnd", bA.ht, false);
        document.querySelector(aY).removeEventListener("animationEnd", bA.ht, false);
    }
};
keyBoard.prototype.generate = function(jianpan) {
    this.settings.id = "testkbid";
    var wO = '<div id=' + this.settings.id,
        wb = 'IGNsYXNzPSJwd2RrZXlib2FyZG91dCIgc3R5bGU9ImRpc3BsYXk6IG5vbmU7IGJvdHRvbTogMHB4OyBtYXJnaW4tbGVmdDogMHB4OyIgPjxkaXYgaWQ9ImNoYXJfa2V5Ym9hcmQiIHN0eWxlPSJ3aWR0aDoxMDAlOyBoZWlnaHQ6MTAwJSA7IGRpc3BsYXk6bm9uZTsiPjxkaXYgY2xhc3M9ImxvZ28iPjxkaXYgY2xhc3M9IkJhbmsiPjwvZGl2PjxkaXYgaWQ9ImtiX2NfU1VSRSIgY2xhc3M9InN1cmUiPjwvZGl2PjwvZGl2PjxkaXYgY2xhc3M9InJvdzEiPg==',
        wo = "PGRpdiBpZD0ia2JfY18wIiBjbGFzcz0icm93MXB3ZCByb3dsZWZ0IiBkYXRhLW5hbWU9ImNfMCI+cTwvZGl2Pg==",
        vL = "PGRpdiBpZD0ia2JfY18xIiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJjXzEiPnc8L2Rpdj4=",
        wk = "PGRpdiBpZD0ia2JfY18yIiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJjXzIiPmU8L2Rpdj4=",
        vU = "PGRpdiBpZD0ia2JfY18zIiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJjXzMiPnI8L2Rpdj4=",
        vl = "PGRpdiBpZD0ia2JfY180IiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJjXzQiPnQ8L2Rpdj4=",
        vI = "PGRpdiBpZD0ia2JfY181IiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJjXzUiPnk8L2Rpdj4=",
        xc = "PGRpdiBpZD0ia2JfY182IiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJjXzYiPnU8L2Rpdj4=",
        wI = "PGRpdiBpZD0ia2JfY183IiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJjXzciPmk8L2Rpdj4=",
        wj = "PGRpdiBpZD0ia2JfY184IiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJjXzgiPm88L2Rpdj4=",
        vZ = "PGRpdiBpZD0ia2JfY185IiBjbGFzcz0icm93MXB3ZCByb3dyaWdodCIgZGF0YS1uYW1lPSJjXzkiPnA8L2Rpdj4=",
        vT = "PC9kaXY+PGRpdiBjbGFzcz0icm93MiI+PGRpdiBpZD0ia2JfY18xMCIgY2xhc3M9InJvdzJwd2Qgcm93bGVmdCIgZGF0YS1uYW1lPSJjXzEwIj5hPC9kaXY+",
        wE = "PGRpdiBpZD0ia2JfY18xMSIgY2xhc3M9InJvdzJwd2QiIGRhdGEtbmFtZT0iY18xMSI+czwvZGl2Pg==",
        uj = "PGRpdiBpZD0ia2JfY18xMiIgY2xhc3M9InJvdzJwd2QiIGRhdGEtbmFtZT0iY18xMiI+ZDwvZGl2Pg==",
        tY = "PGRpdiBpZD0ia2JfY18xMyIgY2xhc3M9InJvdzJwd2QiIGRhdGEtbmFtZT0iY18xMyI+ZjwvZGl2Pg==",
        ut = "PGRpdiBpZD0ia2JfY18xNCIgY2xhc3M9InJvdzJwd2QiIGRhdGEtbmFtZT0iY18xNCI+ZzwvZGl2Pg==",
        tq = "PGRpdiBpZD0ia2JfY18xNSIgY2xhc3M9InJvdzJwd2QiIGRhdGEtbmFtZT0iY18xNSI+aDwvZGl2Pg==",
        tF = "PGRpdiBpZD0ia2JfY18xNiIgY2xhc3M9InJvdzJwd2QiIGRhdGEtbmFtZT0iY18xNiI+ajwvZGl2Pg==",
        uw = "PGRpdiBpZD0ia2JfY18xNyIgY2xhc3M9InJvdzJwd2QiIGRhdGEtbmFtZT0iY18xNyI+azwvZGl2Pg==",
        tQ = "PGRpdiBpZD0ia2JfY18xOCIgY2xhc3M9InJvdzJwd2Qgcm93cmlnaHQiIGRhdGEtbmFtZT0iY18xOCI+bDwvZGl2Pg==",
        tu = "PC9kaXY+PGRpdiBjbGFzcz0icm93MyI+PGRpdiBpZD0ia2JfY19DQVAiIGNsYXNzPSJyb3czcHdkYSIgZGF0YS1uYW1lPSJjX0NBUCI+PC9kaXY+",
        uB = "PGRpdiBpZD0ia2JfY18yMCIgY2xhc3M9InJvdzNwd2RiIiBkYXRhLW5hbWU9ImNfMjAiPno8L2Rpdj4=",
        vd = "PGRpdiBpZD0ia2JfY18yMSIgY2xhc3M9InJvdzNwd2RiIiBkYXRhLW5hbWU9ImNfMjEiPng8L2Rpdj4=",
        vh = "PGRpdiBpZD0ia2JfY18yMiIgY2xhc3M9InJvdzNwd2RiIiBkYXRhLW5hbWU9ImNfMjIiPmM8L2Rpdj4=",
        tU = "PGRpdiBpZD0ia2JfY18yMyIgY2xhc3M9InJvdzNwd2RiIiBkYXRhLW5hbWU9ImNfMjMiPnY8L2Rpdj4=",
        tP = "PGRpdiBpZD0ia2JfY18yNCIgY2xhc3M9InJvdzNwd2RiIiBkYXRhLW5hbWU9ImNfMjQiPmI8L2Rpdj4=",
        ud = "PGRpdiBpZD0ia2JfY18yNSIgY2xhc3M9InJvdzNwd2RiIiBkYXRhLW5hbWU9ImNfMjUiPm48L2Rpdj4=",
        tN = "PGRpdiBpZD0ia2JfY18yNiIgY2xhc3M9InJvdzNwd2RiIiBkYXRhLW5hbWU9ImNfMjYiPm08L2Rpdj4=",
        uW = "PGRpdiBpZD0ia2JfY19EIiBjbGFzcz0icm93M3B3ZGMgcm93ZGVsZXQiIGRhdGEtbmFtZT0iY19EIj48ZGl2PjwvZGl2PjwvZGl2Pg==",
        tJ = "PC9kaXY+PGRpdiBjbGFzcz0icm93NCI+PGRpdiBpZD0ia2JfY19OMSIgY2xhc3M9InJvdzRwd2RhIHJvdzEyMyIgZGF0YS1uYW1lPSJjX04xIj48c3Bhbj4uPzEyMzwvc3Bhbj48L2Rpdj48ZGl2IGlkPSJrYl9jX1AiIGNsYXNzPSJyb3c0cHdkZCIgZGF0YS1uYW1lPSJjX1AiPjxzcGFuPjEyMzwvc3Bhbj48L2Rpdj48ZGl2IGlkPSJrYl9jX04iIGNsYXNzPSJyb3c0cHdkZCIgZGF0YS1uYW1lPSJjX04iPjxzcGFuPjwvc3Bhbj48L2Rpdj4=",
        ub = "PGRpdiBpZD0ia2JfY19TUEFDRSIgY2xhc3M9InJvdzRwd2RiIHJvd3NwYWNlIiBkYXRhLW5hbWU9ImNfU1BBQ0UiPjxzcGFuPjwvc3Bhbj48L2Rpdj4=",
        uh = "PGRpdiBpZD0ia2JfY19DTE9TRSIgY2xhc3M9InJvdzRwd2RhIHJvd2Nsb3NlIiBkYXRhLW5hbWU9ImNfQ0xPU0UiPjxkaXY+PC9kaXY+PC9kaXY+",
        ur = "PC9kaXY+PC9kaXY+PGRpdiBpZD0ibnVtYmVyX2tleWJvYXJkIj48ZGl2IGNsYXNzPSJsb2dvIj48ZGl2IGNsYXNzPSJCYW5rIj48L2Rpdj48ZGl2IGlkPSJrYl9uX1NVUkUiIGNsYXNzPSJzdXJlIj48L2Rpdj48L2Rpdj4=",
        tn = "PGRpdiBjbGFzcz0icm93MSI+PGRpdiBpZD0ia2Jfbl8wIiBjbGFzcz0icm93MXB3ZCByb3dsZWZ0IGNoZyIgZGF0YS1uYW1lPSJuXzAiPjE8L2Rpdj4=",
        uH = "PGRpdiBpZD0ia2Jfbl8xIiBjbGFzcz0icm93MXB3ZCBjaGciIGRhdGEtbmFtZT0ibl8xIj4yPC9kaXY+",
        vf = "PGRpdiBpZD0ia2Jfbl8yIiBjbGFzcz0icm93MXB3ZCBjaGciIGRhdGEtbmFtZT0ibl8yIj4zPC9kaXY+",
        tL = "PGRpdiBpZD0ia2Jfbl8zIiBjbGFzcz0icm93MXB3ZCBjaGciIGRhdGEtbmFtZT0ibl8zIj40PC9kaXY+",
        uN = "PGRpdiBpZD0ia2Jfbl80IiBjbGFzcz0icm93MXB3ZCBjaGciIGRhdGEtbmFtZT0ibl80Ij41PC9kaXY+",
        uF = "PGRpdiBpZD0ia2Jfbl81IiBjbGFzcz0icm93MXB3ZCBjaGciIGRhdGEtbmFtZT0ibl81Ij42PC9kaXY+",
        uG = "PGRpdiBpZD0ia2Jfbl82IiBjbGFzcz0icm93MXB3ZCBjaGciIGRhdGEtbmFtZT0ibl82Ij43PC9kaXY+",
        tx = "PGRpdiBpZD0ia2Jfbl83IiBjbGFzcz0icm93MXB3ZCBjaGciIGRhdGEtbmFtZT0ibl83Ij44PC9kaXY+",
        tZ = "PGRpdiBpZD0ia2Jfbl84IiBjbGFzcz0icm93MXB3ZCBjaGciIGRhdGEtbmFtZT0ibl84Ij45PC9kaXY+",
        uC = "PGRpdiBpZD0ia2Jfbl85IiBjbGFzcz0icm93MXB3ZCByb3dyaWdodCBjaGciIGRhdGEtbmFtZT0ibl85Ij4wPC9kaXY+PC9kaXY+PGRpdiBjbGFzcz0icm93MyI+",
        uY = "PGRpdiBpZD0ia2Jfbl8xMCIgY2xhc3M9InJvdzFwd2Qgcm93bGVmdCIgZGF0YS1uYW1lPSJuXzEwIj4tPC9kaXY+",
        tK = "PGRpdiBpZD0ia2Jfbl8xMSIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ibl8xMSI+LzwvZGl2Pg==",
        ug = "PGRpdiBpZD0ia2Jfbl8xMiIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ibl8xMiI+OjwvZGl2Pg==",
        uD = "PGRpdiBpZD0ia2Jfbl8xMyIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ibl8xMyI+OzwvZGl2Pg==",
        vg = "PGRpdiBpZD0ia2Jfbl8xNCIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ibl8xNCI+KDwvZGl2Pg==",
        vc = "PGRpdiBpZD0ia2Jfbl8xNSIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ibl8xNSI+KTwvZGl2Pg==",
        uU = "PGRpdiBpZD0ia2Jfbl8xNiIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ibl8xNiI+JDwvZGl2Pg==",
        tm = "PGRpdiBpZD0ia2Jfbl8xNyIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ibl8xNyI+JjwvZGl2Pg==",
        tw = "PGRpdiBpZD0ia2Jfbl8xOCIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ibl8xOCI+QDwvZGl2Pg==",
        ty = "PGRpdiBpZD0ia2Jfbl8xOSIgY2xhc3M9InJvdzFwd2Qgcm93cmlnaHQgcm93YWxvbmUiIGRhdGEtbmFtZT0ibl8xOSI+IjwvZGl2Pg==",
        uR = "PC9kaXY+PGRpdiBjbGFzcz0icm93MyI+PGRpdiBpZD0ia2Jfbl9TIiBjbGFzcz0icm93M3B3ZGEiIGRhdGEtbmFtZT0ibl9TIj48c3Bhbj4jKz08L3NwYW4+PC9kaXY+",
        tM = "PGRpdiBpZD0ia2Jfbl8yMSIgY2xhc3M9InJvdzNwd2RkIiBkYXRhLW5hbWU9Im5fMjEiPi48L2Rpdj4=",
        uV = "PGRpdiBpZD0ia2Jfbl8yMiIgY2xhc3M9InJvdzNwd2RkIiBkYXRhLW5hbWU9Im5fMjIiPiw8L2Rpdj4=",
        tC = "PGRpdiBpZD0ia2Jfbl8yMyIgY2xhc3M9InJvdzNwd2RkIiBkYXRhLW5hbWU9Im5fMjMiPj88L2Rpdj4=",
        tE = "PGRpdiBpZD0ia2Jfbl8yNCIgY2xhc3M9InJvdzNwd2RkIiBkYXRhLW5hbWU9Im5fMjQiPiE8L2Rpdj4=",
        uQ = "PGRpdiBpZD0na2Jfbl8yNScgY2xhc3M9J3JvdzNwd2RkJyBkYXRhLW5hbWU9J25fMjUnPic8L2Rpdj4=",
        uk = "PGRpdiBpZD0ia2Jfbl9EIiBjbGFzcz0icm93M3B3ZGMgcm93ZGVsZXQiIGRhdGEtbmFtZT0ibl9EIj48ZGl2PjwvZGl2PjwvZGl2Pg==",
        uz = "PC9kaXY+PGRpdiBjbGFzcz0icm93NCI+PGRpdiBpZD0ia2Jfbl9DMSIgY2xhc3M9InJvdzRwd2RhIHJvdzEyMyIgZGF0YS1uYW1lPSJuX0MxIj48c3Bhbj5BQkM8L3NwYW4+PC9kaXY+PGRpdiBpZD0ia2Jfbl9QIiBjbGFzcz0icm93NHB3ZGQiIGRhdGEtbmFtZT0ibl9QIj48c3Bhbj4xMjM8L3NwYW4+PC9kaXY+PGRpdiBpZD0ia2Jfbl9DIiBjbGFzcz0icm93NHB3ZGQiIGRhdGEtbmFtZT0ibl9DIj48c3Bhbj5BQkM8L3NwYW4+PC9kaXY+",
        uE = "PGRpdiBpZD0ia2Jfbl9TUEFDRSIgY2xhc3M9InJvdzRwd2RiIHJvd3NwYWNlIiBkYXRhLW5hbWU9Im5fU1BBQ0UiPjxzcGFuPjwvc3Bhbj48L2Rpdj4=",
        uc = "PGRpdiBpZD0ia2Jfbl9DTE9TRSIgY2xhc3M9InJvdzRwd2RhIHJvd2Nsb3NlIiBkYXRhLW5hbWU9Im5fQ0xPU0UiPjxkaXY+PC9kaXY+PC9kaXY+",
        uf = "PC9kaXY+PC9kaXY+PGRpdiBpZD0ic3ltYmxlX2tleWJvYXJkIiAgc3R5bGU9IndpZHRoOjEwMCU7IGhlaWdodDoxMDAlIDsgZGlzcGxheTpub25lOyI+PGRpdiBjbGFzcz0ibG9nbyI+PGRpdiBjbGFzcz0iQmFuayI+PC9kaXY+PGRpdiBpZD0ia2Jfc19TVVJFIiBjbGFzcz0ic3VyZSI+PC9kaXY+PC9kaXY+PGRpdiBjbGFzcz0icm93MSI+",
        vb = "PGRpdiBpZD0ia2Jfc18wIiBjbGFzcz0icm93MXB3ZCByb3dsZWZ0IiBkYXRhLW5hbWU9InNfMCI+WzwvZGl2Pg==",
        tW = "PGRpdiBpZD0ia2Jfc18xIiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJzXzEiPl08L2Rpdj4=",
        us = "PGRpdiBpZD0ia2Jfc18yIiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJzXzIiPns8L2Rpdj4=",
        tG = "PGRpdiBpZD0ia2Jfc18zIiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJzXzMiPn08L2Rpdj4=",
        uo = "PGRpdiBpZD0ia2Jfc180IiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJzXzQiPiM8L2Rpdj4=",
        tz = "PGRpdiBpZD0ia2Jfc181IiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJzXzUiPiU8L2Rpdj4=",
        tp = "PGRpdiBpZD0ia2Jfc182IiBjbGFzcz0icm93MXB3ZCByb3dhbG9uZSIgZGF0YS1uYW1lPSJzXzYiPl48L2Rpdj4=",
        va = "PGRpdiBpZD0ia2Jfc183IiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJzXzciPio8L2Rpdj4=",
        tO = "PGRpdiBpZD0ia2Jfc184IiBjbGFzcz0icm93MXB3ZCIgZGF0YS1uYW1lPSJzXzgiPis8L2Rpdj4=",
        tX = "PGRpdiBpZD0ia2Jfc185IiBjbGFzcz0icm93MXB3ZCByb3dyaWdodCIgZGF0YS1uYW1lPSJzXzkiPj08L2Rpdj4=",
        uK = "PC9kaXY+PGRpdiBjbGFzcz0icm93MyI+PGRpdiBpZD0ia2Jfc18xMCIgY2xhc3M9InJvdzFwd2Qgcm93bGVmdCByb3dsb25lbHkiIGRhdGEtbmFtZT0ic18xMCI+XzwvZGl2Pg==",
        uS = "PGRpdiBpZD0ia2Jfc18xMSIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ic18xMSI+XDwvZGl2Pg==",
        uM = "PGRpdiBpZD0ia2Jfc18xMiIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ic18xMiI+fDwvZGl2Pg==",
        uv = "PGRpdiBpZD0ia2Jfc18xMyIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ic18xMyI+fjwvZGl2Pg==",
        vF = "PGRpdiBpZD0ia2Jfc18xNCIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ic18xNCI+PDwvZGl2Pg==",
        yZ = "PGRpdiBpZD0ia2Jfc18xNSIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ic18xNSI+PjwvZGl2Pg==",
        xj = "PGRpdiBpZD0ia2Jfc18xNiIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ic18xNiI+JDwvZGl2Pg==",
        xM = "PGRpdiBpZD0ia2Jfc18xNyIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ic18xNyI+JjwvZGl2Pg==",
        xV = "PGRpdiBpZD0ia2Jfc18xOCIgY2xhc3M9InJvdzFwd2QiIGRhdGEtbmFtZT0ic18xOCI+QDwvZGl2Pg==",
        xH = "PGRpdiBpZD0ia2Jfc18xOSIgY2xhc3M9InJvdzFwd2Qgcm93cmlnaHQgcm93YWxvbmUiIGRhdGEtbmFtZT0ic18xOSI+YDwvZGl2Pg==",
        xN = "PC9kaXY+PGRpdiBjbGFzcz0icm93MyI+PGRpdiBpZD0ia2Jfc19OIiBjbGFzcz0icm93M3B3ZGEiIGRhdGEtbmFtZT0ic19OIj48c3Bhbj48L3NwYW4+PC9kaXY+PGRpdiBpZD0ia2Jfc19OMSIgY2xhc3M9InJvdzNwd2RhIiBkYXRhLW5hbWU9InNfTjEiPjxzcGFuPjEyMzwvc3Bhbj48L2Rpdj4=",
        xK = "PGRpdiBpZD0ia2Jfc18yMSIgY2xhc3M9InJvdzNwd2RkIiBkYXRhLW5hbWU9InNfMjEiPi48L2Rpdj4=",
        xE = "PGRpdiBpZD0ia2Jfc18yMiIgY2xhc3M9InJvdzNwd2RkIiBkYXRhLW5hbWU9InNfMjIiPiw8L2Rpdj4=",
        xo = "PGRpdiBpZD0ia2Jfc18yMyIgY2xhc3M9InJvdzNwd2RkIiBkYXRhLW5hbWU9InNfMjMiPj88L2Rpdj4=",
        yz = "PGRpdiBpZD0ia2Jfc18yNCIgY2xhc3M9InJvdzNwd2RkIiBkYXRhLW5hbWU9InNfMjQiPiE8L2Rpdj4=",
        xw = "PGRpdiBpZD0na2Jfc18yNScgY2xhc3M9J3JvdzNwd2RkJyBkYXRhLW5hbWU9J3NfMjUnPic8L2Rpdj4=",
        yu = "PGRpdiBpZD0ia2Jfc19EIiBjbGFzcz0icm93M3B3ZGMgcm93ZGVsZXQiIGRhdGEtbmFtZT0ic19EIj48ZGl2PjwvZGl2PjwvZGl2Pg==",
        yq = "PC9kaXY+PGRpdiBjbGFzcz0icm93NCI+PGRpdiBpZD0ia2Jfc19DMSIgY2xhc3M9InJvdzRwd2RhIHJvdzEyMyIgZGF0YS1uYW1lPSJzX0MxIj48c3Bhbj5BQkM8L3NwYW4+PC9kaXY+PGRpdiBpZD0ia2Jfc19QIiBjbGFzcz0icm93NHB3ZGQiIGRhdGEtbmFtZT0ic19QIj48c3Bhbj4xMjM8L3NwYW4+PC9kaXY+PGRpdiBpZD0ia2Jfc19DIiBjbGFzcz0icm93NHB3ZGQiIGRhdGEtbmFtZT0ic19DIj48c3Bhbj5BQkM8L3NwYW4+PC9kaXY+",
        xk = "PGRpdiBpZD0ia2Jfc19TUEFDRSIgY2xhc3M9InJvdzRwd2RiIHJvd3NwYWNlIiBkYXRhLW5hbWU9InNfU1BBQ0UiPjxzcGFuPjwvc3Bhbj48L2Rpdj4=",
        ym = "PGRpdiBpZD0ia2Jfc19DTE9TRSIgY2xhc3M9InJvdzRwd2RhIHJvd2Nsb3NlIiBkYXRhLW5hbWU9InNfQ0xPU0UiPjxkaXY+PC9kaXY+PC9kaXY+",
        xJ = "PC9kaXY+PC9kaXY+PGRpdiBpZD0icHVyZW51bWJlcl9rZXlib2FyZCIgc3R5bGU9ImRpc3BsYXk6bm9uZTsiPjxkaXYgY2xhc3M9ImxvZ28iPjxkaXYgY2xhc3M9IkJhbmsiPjwvZGl2PjxkaXYgaWQ9ImtiX3BfU1VSRSIgY2xhc3M9InN1cmUiPjwvZGl2PjwvZGl2PjxkaXYgY2xhc3M9InJvdzYiPg==",
        yQ = "PGRpdiBpZD0ia2JfcF8wIiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfMCI+MTwvZGl2Pg==",
        yi = "PGRpdiBpZD0ia2JfcF8xIiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfMSI+MjwvZGl2Pg==",
        xf = "PGRpdiBpZD0ia2JfcF8yIiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfMiI+MzwvZGl2Pg==",
        yG = "PC9kaXY+PGRpdiBjbGFzcz0icm93NSI+PGRpdiBpZD0ia2JfcF8zIiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfMyI+NDwvZGl2Pg==",
        yH = "PGRpdiBpZD0ia2JfcF80IiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfNCI+NTwvZGl2Pg==",
        ya = "PGRpdiBpZD0ia2JfcF81IiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfNSI+NjwvZGl2Pg==",
        xr = "PC9kaXY+PGRpdiBjbGFzcz0icm93NSI+PGRpdiBpZD0ia2JfcF82IiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfNiI+NzwvZGl2Pg==",
        yx = "PGRpdiBpZD0ia2JfcF83IiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfNyI+ODwvZGl2Pg==",
        xC = "PGRpdiBpZD0ia2JfcF84IiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfOCI+OTwvZGl2Pg==",
        xY = "PC9kaXY+PGRpdiBjbGFzcz0icm93NSI+PGRpdiBpZD0ia2JfcF9DTE9TRTEiIGNsYXNzPSJwd2QiIGRhdGEtbmFtZT0icF9DTE9TRTEiPjxkaXY+PC9kaXY+PC9kaXY+PGRpdiBpZD0ia2JfcF9DIiBjbGFzcz0icHdkMiIgZGF0YS1uYW1lPSJwX0MiPjxzcGFuPkFCQzwvc3Bhbj48L2Rpdj48ZGl2IGlkPSJrYl9wX0NMT1NFIiBjbGFzcz0icHdkMiIgZGF0YS1uYW1lPSJwX0NMT1NFIj48ZGl2PjwvZGl2PjwvZGl2Pg==",
        yf = "PGRpdiBpZD0ia2JfcF8xMCIgY2xhc3M9InB3ZCIgZGF0YS1uYW1lPSJwXzEwIj4wPC9kaXY+",
        kj = "PGRpdiBpZD0ia2JfcF9EIiBjbGFzcz0icHdkIiBkYXRhLW5hbWU9InBfRCI+PGRpdj48L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj4=",
        yb = "",
        yS = "",
        xn = "";
    var ce = wO + aB(wb) + aB(wo) + aB(vL) + aB(wk) + aB(vU) + aB(vl) + aB(vI) + aB(xc) + aB(wI) + aB(wj);
    ce += aB(vZ) + aB(vT) + aB(wE) + aB(uj) + aB(tY) + aB(ut) + aB(tq) + aB(tF) + aB(uw) + aB(tQ);
    ce += aB(tu) + aB(uB) + aB(vd) + aB(vh) + aB(tU) + aB(tP) + aB(ud) + aB(tN) + aB(uW) + aB(tJ);
    ce += aB(ub) + aB(uh) + aB(ur) + aB(tn) + aB(uH) + aB(vf) + aB(tL) + aB(uN) + aB(uF) + aB(uG);
    ce += aB(tx) + aB(tZ) + aB(uC) + aB(uY) + aB(tK) + aB(ug) + aB(uD) + aB(vg) + aB(vc) + aB(uU);
    ce += aB(tm) + aB(tw) + aB(ty) + aB(uR) + aB(tM) + aB(uV) + aB(tC) + aB(tE) + aB(uQ) + aB(uk);
    ce += aB(uz) + aB(uE) + aB(uc) + aB(uf) + aB(vb) + aB(tW) + aB(us) + aB(tG) + aB(uo) + aB(tz);
    ce += aB(tp) + aB(va) + aB(tO) + aB(tX) + aB(uK) + aB(uS) + aB(uM) + aB(uv) + aB(vF) + aB(yZ);
    ce += aB(xj) + aB(xM) + aB(xV) + aB(xH) + aB(xN) + aB(xK) + aB(xE) + aB(xo) + aB(yz) + aB(xw);
    ce += aB(yu) + aB(yq) + aB(xk) + aB(ym) + aB(xJ) + aB(yQ) + aB(yi) + aB(xf) + aB(yG) + aB(yH);
    ce += aB(ya) + aB(xr) + aB(yx) + aB(xC) + aB(xY) + aB(yf) + aB(kj) + aB(yb) + aB(yS) + aB(xn);
    $(document.body).append(ce);
    eW = $(window).height();
    BL.jianpan = jianpan;
    $(".sure").html("完成");
    $(".rowspace span").html("空格");
    $("#kb_c_N span,#kb_s_N span").html("符");
    if(BL.jianpan == 1) {
        $(".Bank").css({
            background: "url(" + BL.SV + "/logo.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".sure").css({
            background: "#F63"
        });
        $(".row4pwdd").hide();
        $("#kb_c_N1").show();
        $("#kb_n_C1").show();
        $("#kb_s_N1").show();
        $("#kb_s_N").hide();
        $("#kb_s_C1").show();
        $("#kb_p_CLOSE1").show();
        $(".pwd2").hide();
    } else {
        $(".Bank").css({
            background: "url(" + BL.SV + "/logo2.svg) ",
            backgroundSize: "100% 100%"
        });
        $(".sure").css({
            background: "#36A6F8"
        });
        $(".row4pwdd").show();
        $("#kb_c_N1").hide();
        $("#kb_n_C1").hide();
        $("#kb_s_N1").hide();
        $("#kb_s_N").show();
        $("#kb_s_C1").hide();
        $("#kb_p_CLOSE1").hide();
        $(".pwd2").show();
    };
    $(".row1pwd,.row2pwd,.row3pwdb").css({
        background: "url(" + BL.SV + "/anjian.svg)",
        backgroundSize: "100% 100%"
    });
    $(".row123").css({
        background: "url(" + BL.SV + "/123.svg) ",
        backgroundSize: "100% 100%"
    });
    $("#kb_c_CAP").css({
        background: "url(" + BL.SV + "/shift.svg) ",
        backgroundSize: "100% 100%"
    });
    $(".rowdelet,#kb_s_N,#kb_n_S,.row4pwdd,#kb_s_N1").css({
        background: "url(" + BL.SV + "/quanDEL.svg) ",
        backgroundSize: "100% 100%"
    });
    $(".row3pwdd").css({
        background: "url(" + BL.SV + "/quanDEL_2.svg) ",
        backgroundSize: "100% 100%"
    });
    $(".rowspace").css({
        background: "url(" + BL.SV + "/space.svg) ",
        backgroundSize: "100% 100%"
    });
    $(".rowclose").css({
        background: "url(" + BL.SV + "/123.svg) ",
        backgroundSize: "100% 100%"
    });
    $(".pwd").css({
        background: "url(" + BL.SV + "/shuzi_1.svg) ",
        backgroundSize: "100% 100%"
    });
    $("#kb_p_D,#kb_p_CLOSE1").css({
        background: "url(" + BL.SV + "/shuzi_2.svg) ",
        backgroundSize: "100% 100%"
    });
    $(".pwd2").css({
        background: "url(" + BL.SV + "/shuzi_3.svg) ",
        backgroundSize: "100% 100%"
    });
    $("#kb_p_D>div").css({
        background: "url(" + BL.SV + "/shuzi_delete1.svg) ",
        backgroundSize: "100% 100%"
    });
    $(".rowdelet>div").css({
        background: "url(" + BL.SV + "/DEL.svg) ",
        backgroundSize: "100% 100%"
    });
    $(".rowclose>div,#kb_p_CLOSE>div,#kb_p_CLOSE1>div").css({
        background: "url(" + BL.SV + "/done_1.svg) ",
        backgroundSize: "100% 100%"
    });
    // if($("body").height() < eW) { //modify by zhuxm  页面内容少页面滚动问题
    //     $("body").css({
    //         height: "100%"
    //     });
    //     $("html").css({
    //         height: "100%"
    //     });
    // }
    if(this.settings.kbType != 0) {
        $("#purenumber_keyboard").css("display", "block");
        $("#char_keyboard").css("display", "none");
        $("#number_keyboard").css("display", "none");
        $("#symble_keyboard").css("display", "none");
    } else {
        $("#purenumber_keyboard").css("display", "none");
        $("#char_keyboard").css("display", "block");
        $("#number_keyboard").css("display", "none");
        $("#symble_keyboard").css("display", "none");
    }
    var R = this;
    var aY = "#" + this.settings.id;
    aE = 0;
    var ii = parseInt($(aY).css("height"));
    kM = function() {
        if((R.orientation() == 90) || (R.orientation() == -90)) {
            var jB = Math.max(window.innerHeight, document.documentElement.clientHeight);
            aE = jB * R.ma();
        } else {
            var jB = Math.max(document.documentElement.clientHeight);
            aE = jB * R.ma();
            if(aE < ii) {
                aE = ii;
            };
        }
        $(aY).css("height", aE);
        $("#purenumber_keyboard").css("height", aE);
        $("#char_keyboard").css("height", aE);
        $("#number_keyboard").css("height", aE);
        $("#symble_keyboard").css("height", aE);
        $(".row1,.row6").css({
            marginTop: aE * 0.02 + "px"
        });
        var r1 = new RegExp("row\\d");
        var r2 = new RegExp("_keyboard");
        $(aY).contents("div").filter(function() {
            if(this.id != "purenumber_keyboard") {
                $("#" + this.id).contents("div").filter(function() {
                    if(r1.test(this.className)) {
                        $(this).contents("div").filter(function() {
                            var h = 0.185 * aE;
                            $("#" + this.id).css({
                                height: h + "px",
                                lineHeight: h + "px"
                            });
                            var a = $(aY).width();
                            $(".row4pwdd span").css({
                                width: 0.125 * a + "px",
                                height: h + "px"
                            });
                            $(".rowspace span").css({
                                width: 0.5 * a + "px",
                                height: h + "px"
                            });
                            $(".row3pwda span").css({
                                width: 0.5 * a + "px",
                                height: h + "px"
                            });
                            $(".rowdelet>div").css({
                                width: h * 0.51154 + "px"
                            });
                            $(".rowclose>div").css({
                                width: h * 0.76521 + "px"
                            });
                            $(".row4pwda span").css({
                                width: 0.25 * a + "px",
                                height: h + "px"
                            });
                        });
                    } else {
                        var h = 0.14 * aE;
                        if(BL.jianpan == 1) {
                            $(".Bank").css({
                                width: parseInt(aE * 0.5292) + "px",
                                height: parseInt(aE * 0.084) + "px"
                            });
                        } else {
                            $(".Bank").css({
                                width: parseInt(aE * 0.5839) + "px",
                                height: parseInt(aE * 0.084) + "px"
                            });
                        };
                        $("." + this.className).css({
                            height: h + "px",
                            lineHeight: h + "px"
                        });
                        $(".sure").css({
                            height: (h * 0.7) + "px",
                            lineHeight: (h * 0.7 + 1) + "px"
                        })
                    }
                });
            } else {
                $("#" + this.id).contents("div").filter(function() {
                    if(r1.test(this.className)) {
                        $(this).contents("div").filter(function() {
                            var h = 0.19 * aE;
                            $("#" + this.id).css({
                                height: h + "px",
                                lineHeight: h + "px"
                            });
                            var a = $(aY).width();
                            $(".pwd2 span").css({
                                width: 0.16666 * a + "px",
                                height: h + "px"
                            });
                            $("#kb_p_D>div").css({
                                width: h * 0.65263 + "px"
                            });
                            $("#kb_p_CLOSE>div,#kb_p_CLOSE1>div").css({
                                width: h * 0.5625 + "px"
                            });
                        });
                    }
                });
            }
        });
    };
    kM();
    justic3 = function() {
        $(aY).css("height", aE);
        $("#purenumber_keyboard").css("height", aE);
        $("#char_keyboard").css("height", aE);
        $("#number_keyboard").css("height", aE);
        $("#symble_keyboard").css("height", aE);
        $(".row1").css({
            marginTop: aE * 0.02 + "px"
        });
        var r1 = new RegExp("row\\d");
        var r2 = new RegExp("_keyboard");
        $(aY).contents("div").filter(function() {
            if(this.id != "purenumber_keyboard") {
                $("#" + this.id).contents("div").filter(function() {
                    if(r1.test(this.className)) {
                        $(this).contents("div").filter(function() {
                            var h = 0.185 * aE;
                            $("#" + this.id).css({
                                height: h + "px",
                                lineHeight: h + "px"
                            });
                            $(".rowdelet>div").css({
                                width: h * 0.51154 + "px"
                            });
                            $(".rowclose>div").css({
                                width: h * 0.76521 + "px"
                            });
                            var a = $(aY).width();
                            $(".row4pwdd span").css({
                                width: 0.125 * a + "px",
                                height: h + "px"
                            });
                            $(".rowspace span").css({
                                width: 0.5 * a + "px",
                                height: h + "px"
                            });
                            $(".row3pwda span").css({
                                width: 0.5 * a + "px",
                                height: h + "px"
                            });
                            $(".row4pwda span").css({
                                width: 0.25 * a + "px",
                                height: h + "px"
                            });
                        });
                    } else {
                        var h = 0.14 * aE;
                        if(BL.jianpan == 1) {
                            $(".Bank").css({
                                width: parseInt(aE * 0.5292) + "px",
                                height: parseInt(aE * 0.084) + "px"
                            });
                        } else {
                            $(".Bank").css({
                                width: parseInt(aE * 0.5839) + "px",
                                height: parseInt(aE * 0.084) + "px"
                            });
                        };
                        $("." + this.className).css({
                            height: h + "px",
                            lineHeight: h + "px"
                        });
                        $(".sure").css({
                            height: (h * 0.7) + "px",
                            lineHeight: (h * 0.7 + 1) + "px"
                        })
                    }
                });
            } else {
                $("#" + this.id).contents("div").filter(function() {
                    if(r1.test(this.className)) {
                        $(this).contents("div").filter(function() {
                            var h = 0.19 * aE;
                            $("#" + this.id).css({
                                height: h + "px",
                                lineHeight: h + "px"
                            });
                            var a = $(aY).width();
                            $(".pwd2 span").css({
                                width: 0.16666 * a + "px",
                                height: h + "px"
                            });
                            $("#kb_p_D>div").css({
                                width: h * 0.65263 + "px"
                            });
                            $("#kb_p_CLOSE>div,#kb_p_CLOSE1>div").css({
                                width: h * 0.5625 + "px"
                            });
                        });
                    }
                });
            }
        });
    };
    if(!this.aV) {
        var pe = new RegExp("_keyboard");
        var sJ = new RegExp("kb_");
        var nQ = new RegExp("row\\d");
        var images = new Array();

        function preload() {
            for(i = 0; i < preload.arguments.length; i++) {
                images[i] = new Image();
                images[i].src = preload.arguments[i];
            }
        };
        preload(BL.SV + "/logo.svg", BL.SV + "/123.svg", BL.SV + "/123_2.svg", BL.SV + "/anjian.svg", BL.SV + "/anjian_2.svg", BL.SV + "/DEL.svg", BL.SV + "/DEL_2.svg", BL.SV + "/shuzi_delete2.svg", BL.SV + "/shuzi_delete1.svg", BL.SV + "/done_1.svg", BL.SV + "/shuzi_1.svg", BL.SV + "/shuzi_2.svg", BL.SV + "/shuzi_3.svg", BL.SV + "/shuzi_4.svg", BL.SV + "/space.svg", BL.SV + "/space_2.svg", BL.SV + "/dianji_left.svg", BL.SV + "/dianji_right.svg", BL.SV + "/dianji.svg", BL.SV + "/quanDEL.svg", BL.SV + "/quanDEL_2.svg", BL.SV + "/shift.svg", BL.SV + "/shift_D.svg", BL.SV + "/shift_DS.svg");
        $(".HBlogo").on("touchstart", function(e) {
            e.preventDefault();
        });
        var kZ = function(aK) {
            function call(url, color) {
                var img = new Image;
                img.onload = function() {
                    if(!color) {
                        $("#" + aK).css({
                            backgroundImage: 'url(' + img.src + ')',
                            backgroundSize: "100% 100%"
                        });
                    } else {
                        $("#" + aK).css({
                            backgroundImage: 'url(' + img.src + ')',
                            backgroundSize: "100% 100%",
                            color: color
                        });
                    }
                };
                img.src = url;
            };

            function call2(url) {
                var img = new Image;
                img.onload = function() {
                    $("#" + aK + ">div").css({
                        backgroundImage: 'url(' + img.src + ')',
                        backgroundSize: "100% 100%"
                    });
                };
                img.src = url;
            };
            var event = 0;
            if(sJ.test(aK)) {
                var nR = /_p_/;
                var oq = /_n_/;
                var pU = /_c_/;
                var nV = /_s_/;
                if(nR.test(aK)) {
                    function del() {
                        if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                            setTimeout(function() {
                                var url = BL.SV + "/shuzi_2.svg";
                                var url2 = BL.SV + "/shuzi_delete1.svg";
                                var img = new Image;
                                img.onload = function() {
                                    $("#kb_p_D").css({
                                        backgroundImage: 'url(' + img.src + ')',
                                        backgroundSize: "100% 100%"
                                    });
                                    $("#kb_p_D>div").css({
                                        backgroundImage: 'url(' + img.src2 + ')',
                                        backgroundSize: "100% 100%"
                                    });
                                };
                                img.src = url;
                                img.src2 = url2;
                            }, 100);
                        }
                        clearInterval(dv);
                        for(var bG = 0; bG < av.length; bG++) {
                            clearInterval(av[bG]);
                        }
                        av = [];
                        //var input = document.getElementById(passGuardThis.settings.id);// modified by landry 修改软键盘删除键this指向错误
                        var input = document.getElementById(R.settings.aQ.settings.id);// modified by landry 修改软键盘删除键this指向错误
                        var dO = "";
                        for(var i = 0; i < R.settings.aQ.bu.length; i++) {
                            dO += "*";
                        };
                        if(R.settings.aQ.settings.displayMode == 1) {
                            input.value = R.settings.aQ.bg;
                        } else {
                            input.value = dO;
                        };
                        BL.double = 0;
                    };
                    if((aK != "kb_p_D") && (aK != "kb_p_CLOSE") && (aK != "kb_p_CLOSE1") && (aK != "kb_p_SURE")) {
                        $("#" + aK).on("touchstart", function(e) {
                            if(BL.double == 0) {
                                BL.once = 0;
                                BL.Mov = 0;
                                var key = this.innerText;
                                BL.key2 = $(this).attr("data-name");
                                BL.double = 1;
                                if(aK == "kb_p_C") {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        call(BL.SV + "/shuzi_4.svg");
                                    }
                                } else {
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        var url = BL.SV + "/shuzi_2.svg";
                                        var img = new Image;
                                        img.src = url;
                                        img.onload = function() {
                                            $(".pwd").removeClass("anxia2"); //modified by landry 按下去软键盘起来较慢
                                            $(".anxia2").css({
                                                background: 'url(' + BL.SV + '/shuzi_1.svg' + ')',
                                                backgroundSize: "100% 100%"
                                            });
                                            //$("#purenumber_keyboard div").removeClass("anxia2");
                                            $("#" + aK).addClass("anxia2");
                                            $("#" + aK).css({
                                                backgroundImage: 'url(' + BL.SV + '/shuzi_2.svg' + ')',
                                                backgroundSize: "100% 100%",
                                                color: "#000"
                                            });
                                        }
                                    };
                                }
                                // modified by wsj 修改6位键盘无法点击问题
                                //if(!R.settings.aQ){
                                //    R.settings.aQ = passGuardThis;
                                //}
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                            }
                            e.preventDefault();
                        });
                        $("#" + aK).on("touchmove", function(e) {
                            var MH = e.originalEvent.changedTouches[0].clientY;
                            if(BL.WH <= MH + 1) {
                                if(aK == "kb_p_C") {
                                    call(BL.SV + "/shuzi_3.svg");
                                } else {
                                    setTimeout(function() {
                                        call(BL.SV + "/shuzi_1.svg");
                                    }, 100);
                                    if(R.settings.chaosMode == 2 && event == 0 && BL.once == 0) {
                                        var aN = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
                                        aN = R.gC(aN);
                                        var au = $("#purenumber_keyboard")[0].querySelectorAll(".pwd");
                                        var bY = [];
                                        for(var j = 0, i = 0; j < au.length; j++) {
                                            if(au[j].id == "kb_p_CLOSE" || au[j].id == "kb_p_D") continue;
                                            i++;
                                            bY.push(au[j]);
                                        }
                                        for(var j = 0; j < bY.length; j++) {
                                            bY[j].innerHTML = aN[j];
                                        }
                                    };
                                }
                                BL.double = 0;
                                BL.once = 1;
                                BL.Mov = 1;
                                e.preventDefault();
                            };
                        });
                        $("#" + aK).on("touchend", function(e) {
                            if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                                BL.double = 0;
                                if(aK == "kb_p_C") {
                                    setTimeout(function() {
                                        call(BL.SV + "/shuzi_3.svg");
                                    }, 100);
                                } else {
                                    setTimeout(function() {
                                        call(BL.SV + "/shuzi_1.svg");
                                    }, 100);
                                    if(R.settings.chaosMode == 2 && event == 0) {
                                        var aN = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
                                        aN = R.gC(aN);
                                        var au = $("#purenumber_keyboard")[0].querySelectorAll(".pwd");
                                        var bY = [];
                                        for(var j = 0, i = 0; j < au.length; j++) {
                                            if(au[j].id == "kb_p_CLOSE" || au[j].id == "kb_p_D") continue;
                                            i++;
                                            bY.push(au[j]);
                                        }
                                        for(var j = 0; j < bY.length; j++) {
                                            bY[j].innerHTML = aN[j];
                                        }
                                    }
                                }
                                e.preventDefault();
                            }
                        });
                    } else if(aK == "kb_p_D") {
                        var event = 2;
                        var dv;
                        av = [];
                        $("#" + aK).on("touchstart", function(e) {
                            if(BL.double == 0) {
                                BL.double = 1;
                                BL.once = 0;
                                BL.Mov = 0;
                                var key = this.innerText;
                                BL.key2 = $(this).attr("data-name");
                                $(".anxia2").css({
                                    background: 'url(' + BL.SV + '/shuzi_1.svg' + ')',
                                    backgroundSize: "100% 100%"
                                });
                                $(".pwd").removeClass("anxia2");
                                if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                    var url = BL.SV + "/shuzi_1.svg";
                                    var url2 = BL.SV + "/shuzi_delete2.svg";
                                    var img = new Image;
                                    img.onload = function() {
                                        $("#kb_p_D").css({
                                            backgroundImage: 'url(' + img.src + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                        $("#kb_p_D>div").css({
                                            backgroundImage: 'url(' + img.src2 + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                    };
                                    img.src = url;
                                    img.src2 = url2;
                                }
                                fr = new Date().getTime();
                                dv = window.setInterval(function() {
                                    eO = new Date().getTime();
                                    var x = eO - fr;
                                    if(x > 500) {
                                        //var input = document.getElementById(passGuardThis.settings.id);// modified by landry 修改软键盘删除键this指向错误
                                    	var input = document.getElementById(R.settings.aQ.settings.id);
                                        var fK = setInterval(function() {
                                            R.settings.aQ.bu = R.settings.aQ.bu.substr(0, R.settings.aQ.bu.length - 1);
                                            R.settings.aQ.bg -= 1;
                                            if(R.settings.aQ.bg < 0) R.settings.aQ.bg = 0;
                                            if(R.settings.aQ.bg == 0) R.settings.aQ.asm = [""];
                                            if(R.settings.aQ.bg == 1) R.settings.aQ.bsm = [""];
                                            if(R.settings.aQ.bg == 2) R.settings.aQ.csm = [""];
                                            if(R.settings.aQ.bg == 3) R.settings.aQ.dsm = [""];
                                            if(R.settings.aQ.bg == 4) R.settings.aQ.esm = [""];
                                            if(R.settings.aQ.bg == 5) R.settings.aQ.fsm = [""];
                                            if(R.settings.aQ.bg == 6) R.settings.aQ.gsm = [""];
                                            if(R.settings.aQ.bg == 7) R.settings.aQ.hsm = [""];
                                            if(R.settings.aQ.bg == 8) R.settings.aQ.ism = [""];
                                            if(R.settings.aQ.bg == 9) R.settings.aQ.jsm = [""];
                                            if(R.settings.aQ.bg == 10) R.settings.aQ.ksm = [""];
                                            if(R.settings.aQ.bg == 11) R.settings.aQ.lsm = [""];
                                            if(R.settings.aQ.bg == 12) R.settings.aQ.msm = [""];
                                            if(R.settings.aQ.bg == 13) R.settings.aQ.nsm = [""];
                                            if(R.settings.aQ.bg == 14) R.settings.aQ.osm = [""];
                                            if(R.settings.aQ.bg == 15) R.settings.aQ.psm = [""];
                                            if(R.settings.aQ.bg == 16) R.settings.aQ.qsm = [""];
                                            if(R.settings.aQ.bg == 17) R.settings.aQ.rsm = [""];
                                            if(R.settings.aQ.bg == 18) R.settings.aQ.ssm = [""];
                                            if(R.settings.aQ.bg == 19) R.settings.aQ.tsm = [""];
                                            if(R.settings.aQ.bg == 20) R.settings.aQ.usm = [""];
                                            if(R.settings.aQ.bg == 21) R.settings.aQ.vsm = [""];
                                            if(R.settings.aQ.bg == 22) R.settings.aQ.wsm = [""];
                                            if(R.settings.aQ.bg == 23) R.settings.aQ.xsm = [""];
                                            if(R.settings.aQ.bg == 24) R.settings.aQ.ysm = [""];
                                            if(R.settings.aQ.bg == 25) R.settings.aQ.zsm = [""];
                                            if(R.settings.aQ.bg == 26) R.settings.aQ.aasm = [""];
                                            if(R.settings.aQ.bg == 27) R.settings.aQ.bbsm = [""];
                                            if(R.settings.aQ.bg == 28) R.settings.aQ.ccsm = [""];
                                            if(R.settings.aQ.bg == 29) R.settings.aQ.ddsm = [""];
                                            input.value = input.value.substr(0, input.value.length - 1);
                                            if(R.settings.aQ.settings.jump == 1) {
                                                R.settings.aQ.settings.del(R.settings.aQ.bg);
                                            }
                                        }, 50);
                                        av.push(fK);
                                        clearInterval(fK)
                                        clearInterval(dv);
                                    }
                                }, 1);
                                e.preventDefault();
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                                return false;
                            }
                        });
                        $("#" + aK).on("touchend", function(e) {
                            if(BL.key2 == $(this).attr("data-name")) {
                                del();
                                e.preventDefault();
                                return false;
                            }
                        });
                        $("#" + aK).on("touchmove", function(e) {
                            var MH = e.originalEvent.changedTouches[0].clientY;
                            if(BL.WH <= MH + 1) {
                                del();
                                e.preventDefault();
                                return false;
                            }
                        });
                    };
                    $("#" + aK).on("touchend", function(e) {
                        if((aK == "kb_p_CLOSE" || aK == "kb_p_CLOSE1") && (R.settings.pressStatus == 1 || R.settings.pressStatus == 2)) {
                            setTimeout(function() {
                                call(BL.SV + "/shuzi_2.svg");
                            }, 100);
                        };
                        if(aK == "kb_p_C") {
                            setTimeout(function() {
                                $("#char_keyboard").css("display", "block");
                                $("#number_keyboard").css("display", "none");
                                $("#symble_keyboard").css("display", "none");
                                $("#purenumber_keyboard").css("display", "none");
                                justic3();
                            }, 80)
                        };
                    });
                    $("#" + aK).on("touchstart", function(e) {
                        var key = this.innerText;
                        BL.WH = $(window).height();
                        if(aK != "kb_p_D") {
                            if((aK == "kb_p_CLOSE" || aK == "kb_p_CLOSE1" || aK == "kb_p_SURE") && BL.double == 0) {
                                if((R.settings.pressStatus == 1 || R.settings.pressStatus == 2) && aK == "kb_p_CLOSE") {
                                    var url = BL.SV + "/shuzi_4.svg";
                                    var img = new Image;
                                    img.onload = function() {
                                        $("#kb_p_CLOSE").css({
                                            backgroundImage: 'url(' + img.src + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                        $("#kb_p_CLOSE1").css({
                                            backgroundImage: 'url(' + img.src + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                    };
                                    img.src = url;
                                };
                                if(BL.fixed == "") {
                                    $("body").css({
                                        position: "relative",
                                        left: 0,
                                        top: 0,
                                        transition: "all 0.3s"
                                    });
                                } else {
                                    $("body").css({
                                        position: "relative",
                                        left: 0,
                                        top: 0,
                                        transition: "all 0.3s"
                                    });
                                    $("#" + BL.fixed).css({
                                        bottom: 0 + "px",
                                        transition: "all 0.3s"
                                    });
                                };
                                setTimeout(function() {
                                    PH.s_n();
                                    PH.add++;
                                    PH.cs = 1;
                                    R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                                }, 50);
                                $(aY).removeClass("pwdkeyboard").addClass("pwdkeyboardout");
                                document.querySelector(aY).addEventListener("webkitAnimationEnd", R.cf, false);
                                document.querySelector(aY).addEventListener("animationEnd", R.cf, false);
                                event = 1;
                                e.preventDefault();
                            };
                        };
                    });
                }
                if(pU.test(aK)) {
                    function del2() {
                        if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                            setTimeout(function() {
                                var url = BL.SV + "/quanDEL.svg";
                                var img = new Image;
                                img.src = url;
                                img.onload = function() {
                                    $("#kb_c_D").css({
                                        backgroundImage: 'url(' + BL.SV + '/quanDEL.svg' + ')',
                                        backgroundSize: "100% 100%"
                                    });
                                    $("#kb_c_D>div").css({
                                        backgroundImage: 'url(' + BL.SV + '/DEL.svg' + ')',
                                        backgroundSize: "100% 100%"
                                    });
                                }
                            }, 100);
                        }
                        clearInterval(dv);
                        for(var bG = 0; bG < av.length; bG++) {
                            clearInterval(av[bG]);
                        }
                        av = [];
                        //var input = document.getElementById(passGuardThis.settings.id);// modified by landry 修改软键盘删除键this指向错误
                        var input = document.getElementById(R.settings.aQ.settings.id);
                        var dO = "";
                        //for(var i = 0; i < passGuardThis.bu.length; i++) {// modified by wsj 修改删除按钮密码length错误
                        for(var i = 0; i < R.settings.aQ.bu.length; i++) {
                            dO += "*";
                        };
                        if(R.settings.aQ.settings.displayMode == 1) {
                            input.value = R.settings.aQ.bg;
                        } else {
                            input.value = dO;
                        };
                        BL.double = 0;
                    };
                    if((aK != "kb_c_D") && (aK != "kb_c_SURE") && (aK != "kb_c_CLOSE") && (aK != "kb_c_CAP")) {
                        var zJ = 1;
                        $("#" + aK).on("touchstart", function(e) {
                            if(BL.double == 0) {
                                var key = this.innerText;
                                clearTimeout(BL.FD);
                                BL.once = 0;
                                BL.Mov = 0;
                                BL.key2 = $(this).attr("data-name");
                                BL.double = 1;
                                $(".fd").eq(0).remove();
                                if(/_SPACE/i.test(aK)) {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        var url = BL.SV + "/space_2.svg";
                                        var img = new Image;
                                        img.src = url;
                                        img.onload = function() {
                                            $(".anxia").css({
                                                background: "url(" + BL.SV + "/anjian.svg) ",
                                                backgroundSize: "100% 100%"
                                            });
                                            $("#char_keyboard div").removeClass("anxia");
                                            $("#kb_c_SPACE").css({
                                                backgroundImage: 'url(' + BL.SV + '/space_2.svg' + ')',
                                                backgroundSize: "100% 100%"
                                            });
                                        }
                                    }
                                } else if(aK == "kb_c_N1") {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        call(BL.SV + "/123_2.svg");
                                    }
                                } else if(aK == "kb_c_N" || aK == "kb_c_P") {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        call(BL.SV + "/quanDEL_2.svg");
                                    }
                                } else {
                                    if(R.settings.pressStatus == 1) {
                                        var cc = $(this);
                                        var Html = $(this).html();
                                        cc.append($("<div class='fd'></div>"));
                                        cc.css({
                                            position: "relative"
                                        });
                                        if(cc.hasClass("rowleft")) {
                                            var img = new Image;
                                            img.src = BL.SV + '/dianji_left.svg';
                                            img.onload = function() {
                                                $(".fd").eq(0).html(Html);
                                                $(".fd").eq(0).css({
                                                    backgroundImage: 'url(' + img.src + ')',
                                                    backgroundSize: "100% 100%",
                                                    position: "absolute",
                                                    width: "140%",
                                                    height: aE * 0.4 + "px",
                                                    left: 0,
                                                    bottom: "0px",
                                                    textAlign: "center",
                                                    fontSize: "1.5em",
                                                    zIndex: "999",
                                                    lineHeight: aE * 0.23 + "px"
                                                });
                                            }
                                        } else if(cc.hasClass("rowright")) {
                                            var img = new Image;
                                            img.src = BL.SV + '/dianji_right.svg';
                                            img.onload = function() {
                                                $(".fd").eq(0).html(Html);
                                                $(".fd").eq(0).css({
                                                    backgroundImage: 'url(' + img.src + ')',
                                                    backgroundSize: "100% 100%",
                                                    position: "absolute",
                                                    width: "140%",
                                                    height: aE * 0.4 + "px",
                                                    right: 0,
                                                    bottom: "0px",
                                                    textAlign: "center",
                                                    fontSize: "1.5em",
                                                    zIndex: "999",
                                                    lineHeight: aE * 0.23 + "px"
                                                });
                                            }
                                        } else {
                                            var img = new Image;
                                            img.src = BL.SV + '/dianji.svg';
                                            img.onload = function() {
                                                $(".fd").eq(0).html(Html);
                                                $(".fd").eq(0).css({
                                                    backgroundImage: 'url(' + img.src + ')',
                                                    backgroundSize: "100% 100%",
                                                    position: "absolute",
                                                    width: "140%",
                                                    height: aE * 0.4 + "px",
                                                    left: "-20%",
                                                    bottom: "0px",
                                                    textAlign: "center",
                                                    fontSize: "1.5em",
                                                    zIndex: "999",
                                                    lineHeight: aE * 0.23 + "px"
                                                });
                                            }
                                        }
                                    } else if(R.settings.pressStatus == 2) {
                                        var url = BL.SV + "/anjian_2.svg";
                                        var img = new Image;
                                        img.src = url;
                                        img.onload = function() {
                                            $(".anxia").css({
                                                background: "url(" + BL.SV + "/anjian.svg) ",
                                                backgroundSize: "100% 100%"
                                            });
                                            $("#char_keyboard div").removeClass("anxia");
                                            $("#" + aK).addClass("anxia");
                                            $("#" + aK).css({
                                                backgroundImage: 'url(' + BL.SV + '/anjian_2.svg' + ')',
                                                backgroundSize: "100% 100%"
                                            });
                                        }
                                    }
                                }
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                                e.preventDefault();
                            }
                        });
                        $("#" + aK).on("touchmove", function(e) {
                            var MH = e.originalEvent.changedTouches[0].clientY;
                            if(BL.WH <= MH + 1 && BL.Mov == 0) {
                                if(/_SPACE/i.test(aK)) {
                                    setTimeout(function() {
                                        call(BL.SV + "/space.svg");
                                    }, 100);
                                } else if(aK == "kb_c_N1") {
                                    call(BL.SV + "/123.svg");
                                } else if(aK == "kb_c_N" || aK == "kb_c_P") {
                                    call(BL.SV + "/quanDEL.svg");
                                } else {
                                    setTimeout(function() {
                                        $(".fd").eq(0).remove();
                                    }, 100);
                                    call(BL.SV + "/anjian.svg");
                                    if(!R.caps && R.shift) {
                                        setTimeout(function() {
                                            $("#char_keyboard").contents("div").filter(function() {
                                                $(this).contents("div").filter(function() {
                                                    var c = $(this).text();
                                                    if(c.length == 1) {
                                                        this.innerText = c.toLowerCase();
                                                    } else if(2 <= c.length && c.length <= 3 && (c != '完成' && c != '空格' && c != '123')) {
                                                        this.innerText = c.substring(0, 1).toLowerCase();
                                                    }
                                                });
                                            });
                                        }, 100);
                                        if((R.orientation() == 90) || (R.orientation() == -90)) {
                                            $("#kb_c_CAP").css({
                                                background: "url(" + BL.SV + "/shift_H.svg) ",
                                                backgroundSize: "100% 100%"
                                            });
                                        } else {
                                            $("#kb_c_CAP").css({
                                                background: "url(" + BL.SV + "/shift.svg) ",
                                                backgroundSize: "100% 100%"
                                            });
                                        }
                                    };
                                    R.shift = false;
                                    if(R.settings.chaosMode == 2 && event == 0 && BL.Mov == 0) {
                                        var aN = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
                                        aN = R.gC(aN, R.caps);
                                        var au = $("#char_keyboard")[0].querySelectorAll(".row1pwd,.row2pwd,.row3pwdb");
                                        setTimeout(function() {
                                            for(var j = 0; j < au.length; j++) {
                                                au[j].innerHTML = aN[j];
                                            }
                                        }, 100)
                                    }
                                };
                                BL.once = 1;
                                BL.Mov = 1;
                                BL.double = 0;
                                e.preventDefault();
                            }
                        });
                        $("#" + aK).on("touchend", function(e) {
                            if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                                BL.double = 0;
                                if(/_SPACE/i.test(aK)) {
                                    setTimeout(function() {
                                        call(BL.SV + "/space.svg");
                                    }, 100);
                                } else if(aK == "kb_c_N1") {
                                    setTimeout(function() {
                                        call(BL.SV + "/123.svg");
                                    }, 100);
                                } else if(aK == "kb_c_N" || aK == "kb_c_P") {
                                    setTimeout(function() {
                                        call(BL.SV + "/quanDEL.svg");
                                    }, 100);
                                } else {
                                    BL.FD = setTimeout(function() {
                                        $(".fd").eq(0).remove();
                                    }, 100);
                                    setTimeout(function() {
                                        call(BL.SV + "/anjian.svg");
                                    }, 100);
                                };
                            }
                            e.preventDefault();
                        });
                    } else if(aK == "kb_c_D") {
                        var key = this.innerText;
                        var dv;
                        var av = [];
                        var event = 2;
                        $("#" + aK).on("touchstart", function(e) {
                            if(BL.double == 0) {
                                BL.double = 1;
                                BL.once = 0;
                                BL.Mov = 0;
                                BL.key2 = $(this).attr("data-name");
                                var key = this.innerText;
                                if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                    $(".anxia").css({
                                        background: "url(" + BL.SV + "/anjian.svg) ",
                                        backgroundSize: "100% 100%"
                                    });
                                    $("#char_keyboard div").removeClass("anxia");
                                    var url = BL.SV + "/quanDEL_2.svg";
                                    var img = new Image;
                                    img.src = url;
                                    img.onload = function() {
                                        $("#kb_c_D").css({
                                            backgroundImage: 'url(' + BL.SV + '/quanDEL_2.svg' + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                        $("#kb_c_D>div").css({
                                            backgroundImage: 'url(' + BL.SV + '/DEL_2.svg' + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                    }
                                }
                                fr = new Date().getTime();
                                dv = window.setInterval(function() {
                                    eO = new Date().getTime();
                                    var x = eO - fr;
                                    if(x > 500) {
                                        //var input = document.getElementById(passGuardThis.settings.id);// modified by landry 修改软键盘删除键this指向错误
                                    	var input = document.getElementById(R.settings.aQ.settings.id);
                                    	var fK = setInterval(function() {
                                            R.settings.aQ.bu = R.settings.aQ.bu.substr(0, R.settings.aQ.bu.length - 1);
                                            R.settings.aQ.bg -= 1;
                                            if(R.settings.aQ.bg < 0) R.settings.aQ.bg = 0;
                                            if(R.settings.aQ.bg == 0) R.settings.aQ.asm = [""];
                                            if(R.settings.aQ.bg == 1) R.settings.aQ.bsm = [""];
                                            if(R.settings.aQ.bg == 2) R.settings.aQ.csm = [""];
                                            if(R.settings.aQ.bg == 3) R.settings.aQ.dsm = [""];
                                            if(R.settings.aQ.bg == 4) R.settings.aQ.esm = [""];
                                            if(R.settings.aQ.bg == 5) R.settings.aQ.fsm = [""];
                                            if(R.settings.aQ.bg == 6) R.settings.aQ.gsm = [""];
                                            if(R.settings.aQ.bg == 7) R.settings.aQ.hsm = [""];
                                            if(R.settings.aQ.bg == 8) R.settings.aQ.ism = [""];
                                            if(R.settings.aQ.bg == 9) R.settings.aQ.jsm = [""];
                                            if(R.settings.aQ.bg == 10) R.settings.aQ.ksm = [""];
                                            if(R.settings.aQ.bg == 11) R.settings.aQ.lsm = [""];
                                            if(R.settings.aQ.bg == 12) R.settings.aQ.msm = [""];
                                            if(R.settings.aQ.bg == 13) R.settings.aQ.nsm = [""];
                                            if(R.settings.aQ.bg == 14) R.settings.aQ.osm = [""];
                                            if(R.settings.aQ.bg == 15) R.settings.aQ.psm = [""];
                                            if(R.settings.aQ.bg == 16) R.settings.aQ.qsm = [""];
                                            if(R.settings.aQ.bg == 17) R.settings.aQ.rsm = [""];
                                            if(R.settings.aQ.bg == 18) R.settings.aQ.ssm = [""];
                                            if(R.settings.aQ.bg == 19) R.settings.aQ.tsm = [""];
                                            if(R.settings.aQ.bg == 20) R.settings.aQ.usm = [""];
                                            if(R.settings.aQ.bg == 21) R.settings.aQ.vsm = [""];
                                            if(R.settings.aQ.bg == 22) R.settings.aQ.wsm = [""];
                                            if(R.settings.aQ.bg == 23) R.settings.aQ.xsm = [""];
                                            if(R.settings.aQ.bg == 24) R.settings.aQ.ysm = [""];
                                            if(R.settings.aQ.bg == 25) R.settings.aQ.zsm = [""];
                                            if(R.settings.aQ.bg == 26) R.settings.aQ.aasm = [""];
                                            if(R.settings.aQ.bg == 27) R.settings.aQ.bbsm = [""];
                                            if(R.settings.aQ.bg == 28) R.settings.aQ.ccsm = [""];
                                            if(R.settings.aQ.bg == 29) R.settings.aQ.ddsm = [""];
                                            input.value = input.value.substr(0, input.value.length - 1);
                                            if(R.settings.aQ.settings.jump == 1) {
                                                R.settings.aQ.settings.del(R.settings.aQ.bg);
                                            }
                                        }, 50);
                                        av.push(fK);
                                        clearInterval(fK)
                                        clearInterval(dv);
                                    }
                                }, 1);
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                                e.preventDefault();
                                return false;
                            }
                        });
                        $("#" + aK).on("touchmove", function(e) {
                            var MH = e.originalEvent.changedTouches[0].clientY;
                            if(BL.WH <= MH) {
                                del2();
                            }
                            e.preventDefault();
                            return false;
                        });
                        $("#" + aK).on("touchend", function(e) {
                            if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                                del2();
                                BL.Mov = 1;
                                e.preventDefault();
                                return false;
                            }
                        });
                    }
                    if(aK == "kb_c_CAP") {
                        function CAPtap() {
                            if(!R.dt) {
                                if(R.caps) {
                                    R.caps = false;
                                    R.shift = false;
                                    if((R.orientation() == 90) || (R.orientation() == -90)) {
                                        call(BL.SV + "/shift_H.svg");
                                    } else {
                                        call(BL.SV + "/shift.svg");
                                    }
                                } else {
                                    if(R.shift) {
                                        R.shift = false;
                                        if((R.orientation() == 90) || (R.orientation() == -90)) {
                                            call(BL.SV + "/shift_H.svg");
                                        } else {
                                            call(BL.SV + "/shift.svg");
                                        }
                                    } else {
                                        R.shift = true;
                                        if((R.orientation() == 90) || (R.orientation() == -90)) {
                                            call(BL.SV + "/shift_D_H.svg");
                                        } else {
                                            call(BL.SV + "/shift_D.svg");
                                        }
                                    }
                                    if(R.caps) {
                                        R.caps = false;
                                        if((R.orientation() == 90) || (R.orientation() == -90)) {
                                            call(BL.SV + "/shift_H.svg");
                                        } else {
                                            call(BL.SV + "/shift.svg");
                                        }
                                    }
                                }
                            } else {
                                R.dt = false;
                            }
                            if(R.caps || R.shift) {
                                $("#char_keyboard").contents("div").filter(function() {
                                    $(this).contents("div").filter(function() {
                                        var c = this.innerText;
                                        if(c.length == 1) {
                                            this.innerText = c.toUpperCase();
                                        } else if(2 <= c.length && c.length <= 3 && (c != '完成' && c != '空格' && c != '123')) {
                                            this.innerText = c.substring(0, 1).toUpperCase();
                                        }
                                    });
                                });
                            } else {
                                $("#char_keyboard").contents("div").filter(function() {
                                    $(this).contents("div").filter(function() {
                                        var c = this.innerText;
                                        if(c.length == 1) {
                                            this.innerText = c.toLowerCase();
                                        } else if(2 <= c.length && c.length <= 3 && (c != '完成' && c != '空格' && c != '123')) {
                                            this.innerText = c.substring(0, 1).toLowerCase();
                                        }
                                    });
                                });
                            }
                            $(".fd").remove();
                            var now = new Date().getTime();
                            var lv = $(this).data('lastTouch') || now + 1;
                            var nr = now - lv;
                            if(nr < 600 && nr > 0) {
                                R.caps = true;
                                if((R.orientation() == 90) || (R.orientation() == -90)) {
                                    call(BL.SV + "/shift_DS_H.svg");
                                } else {
                                    call(BL.SV + "/shift_DS.svg");
                                }
                                $("#char_keyboard").contents("div").filter(function() {
                                    $(this).contents("div").filter(function() {
                                        var c = this.innerText;
                                        if(c.length == 1) {
                                            this.innerText = c.toUpperCase();
                                        }
                                    });
                                });
                            } else {
                                $(this).data('lastTouch', now);
                            }
                            BL.double = 0;
                        };
                        $("#" + aK).on('touchstart', function(event) {
                            if(BL.double == 0) {
                                BL.double = 1;
                                BL.once = 0;
                                BL.Mov = 0;
                                var key = this.innerText;
                                BL.key2 = $(this).attr("data-name");
                            }
                        });
                        $("#" + aK).on("touchmove", function(e) {
                            var MH = e.originalEvent.changedTouches[0].clientY;
                            if(BL.WH <= MH && BL.Mov == 0) {
                                if(!R.dt) {
                                    if(R.caps) {
                                        R.caps = false;
                                        R.shift = false;
                                        if((R.orientation() == 90) || (R.orientation() == -90)) {
                                            call(BL.SV + "/shift_H.svg");
                                        } else {
                                            call(BL.SV + "/shift.svg");
                                        }
                                    } else {
                                        if(R.shift) {
                                            R.shift = false;
                                            if((R.orientation() == 90) || (R.orientation() == -90)) {
                                                call(BL.SV + "/shift_H.svg");
                                            } else {
                                                call(BL.SV + "/shift.svg");
                                            }
                                        } else {
                                            R.shift = true;
                                            if((R.orientation() == 90) || (R.orientation() == -90)) {
                                                call(BL.SV + "/shift_D_H.svg");
                                            } else {
                                                call(BL.SV + "/shift_D.svg");
                                            }
                                        }
                                        if(R.caps) {
                                            R.caps = false;
                                            if((R.orientation() == 90) || (R.orientation() == -90)) {
                                                call(BL.SV + "/shift_H.svg");
                                            } else {
                                                call(BL.SV + "/shift.svg");
                                            }
                                        }
                                    }
                                } else {
                                    R.dt = false;
                                }
                                if(R.caps || R.shift) {
                                    $("#char_keyboard").contents("div").filter(function() {
                                        $(this).contents("div").filter(function() {
                                            var c = this.innerText;
                                            if(c.length == 1) {
                                                this.innerText = c.toUpperCase();
                                            } else if(2 <= c.length && c.length <= 3 && (c != '完成' && c != '空格' && c != '123')) {
                                                this.innerText = c.substring(0, 1).toUpperCase();
                                            }
                                        });
                                    });
                                } else {
                                    $("#char_keyboard").contents("div").filter(function() {
                                        $(this).contents("div").filter(function() {
                                            var c = this.innerText;
                                            if(c.length == 1) {
                                                this.innerText = c.toLowerCase();
                                            } else if(2 <= c.length && c.length <= 3 && (c != '完成' && c != '空格' && c != '123')) {
                                                this.innerText = c.substring(0, 1).toLowerCase();
                                            }
                                        });
                                    });
                                }
                                BL.double = 0;
                                BL.once = 1;
                                BL.Mov = 1;
                            }
                            e.preventDefault();
                            return false;
                        });
                        $("#" + aK).on('touchend', function(event) {
                            if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                                CAPtap();
                                BL.Mov = 1;
                            }
                        });
                        event = 7;
                    }
                    $("#" + aK).on("touchend", function(e) {
                        if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                            if(aK != "kb_c_D" && aK != "kb_c_SPACE" && aK != "kb_c_CLOSE" && aK != "kb_c_CAP" && aK != "kb_c_N" && aK != "kb_c_N1" && aK != "kb_c_P") {
                                if(!R.caps && R.shift) {
                                    setTimeout(function() {
                                        $("#char_keyboard").contents("div").filter(function() {
                                            $(this).contents("div").filter(function() {
                                                var c = $(this).text();
                                                if(c.length == 1) {
                                                    this.innerText = c.toLowerCase();
                                                } else if(2 <= c.length && c.length <= 3 && (c != '完成' && c != '空格' && c != '123')) {
                                                    this.innerText = c.substring(0, 1).toLowerCase();
                                                }
                                            });
                                        });
                                    }, 100);
                                    if((R.orientation() == 90) || (R.orientation() == -90)) {
                                        $("#kb_c_CAP").css({
                                            background: "url(" + BL.SV + "/shift_H.svg) ",
                                            backgroundSize: "100% 100%"
                                        });
                                    } else {
                                        $("#kb_c_CAP").css({
                                            background: "url(" + BL.SV + "/shift.svg) ",
                                            backgroundSize: "100% 100%"
                                        });
                                    }
                                };
                                R.shift = false;
                            }
                            if(aK == "kb_c_P") {
                                setTimeout(function() {
                                    $("#char_keyboard").css("display", "none");
                                    $("#number_keyboard").css("display", "none");
                                    $("#symble_keyboard").css("display", "none");
                                    $("#purenumber_keyboard").css("display", "block");
                                    justic3();
                                }, 80)
                            };
                            if(aK == "kb_c_N" || aK == "kb_c_N1") {
                                setTimeout(function() {
                                    $("#char_keyboard").css("display", "none");
                                    $("#number_keyboard").css("display", "block");
                                    $("#symble_keyboard").css("display", "none");
                                    $("#purenumber_keyboard").css("display", "none");
                                    justic3();
                                }, 80)
                            };
                            if(aK == "kb_c_CLOSE" && (R.settings.pressStatus == 1 || R.settings.pressStatus == 2)) {
                                setTimeout(function() {
                                    call(BL.SV + "/123.svg");
                                }, 100);
                            };
                            if(R.settings.chaosMode == 2 && event == 0 && BL.Mov == 0) {
                                var aN = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
                                aN = R.gC(aN, R.caps);
                                var au = $("#char_keyboard")[0].querySelectorAll(".row1pwd,.row2pwd,.row3pwdb");
                                setTimeout(function() {
                                    for(var j = 0; j < au.length; j++) {
                                        au[j].innerHTML = aN[j];
                                    }
                                }, 100)
                            }
                            BL.Mov = 1;
                        }
                    });
                    $("#" + aK).on("touchstart", function(e) {
                        BL.WH = $(window).height();
                        var key = this.innerText;
                        if(aK != "kb_c_D") {
                            if(aK == "kb_c_SPACE") {
                                return;
                            } else if((aK == "kb_c_SURE" || aK == "kb_c_CLOSE") && BL.double == 0) {
                                if(aK == "kb_c_CLOSE" && (R.settings.pressStatus == 1 || R.settings.pressStatus == 2)) {
                                    call(BL.SV + "/123_2.svg");
                                };
                                if(BL.fixed == "") {
                                    $("body").css({
                                        position: "relative",
                                        left: 0,
                                        top: 0,
                                        transition: "all 0.3s"
                                    });
                                } else {
                                    $("body").css({
                                        position: "relative",
                                        left: 0,
                                        top: 0,
                                        transition: "all 0.3s"
                                    });
                                    $("#" + BL.fixed).css({
                                        bottom: 0 + "px",
                                        transition: "all 0.3s"
                                    });
                                };
                                $(aY).removeClass("pwdkeyboard").addClass("pwdkeyboardout");
                                document.querySelector(aY).addEventListener("webkitAnimationEnd", R.cf, false);
                                document.querySelector(aY).addEventListener("animationEnd", R.cf, false);
                                event = 1;
                                setTimeout(function() {
                                    PH.s_n();
                                    PH.add++;
                                    PH.cs = 1;
                                    R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                                }, 80)
                            }
                            e.preventDefault();
                        }
                    });
                }
                if(oq.test(aK)) {
                    function del3() {
                        if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                            setTimeout(function() {
                                var url = BL.SV + "/quanDEL.svg";
                                var img = new Image;
                                img.src = url;
                                img.onload = function() {
                                    $("#kb_n_D").css({
                                        backgroundImage: 'url(' + BL.SV + '/quanDEL.svg' + ')',
                                        backgroundSize: "100% 100%"
                                    });
                                    $("#kb_n_D>div").css({
                                        backgroundImage: 'url(' + BL.SV + '/DEL.svg' + ')',
                                        backgroundSize: "100% 100%"
                                    });
                                }
                            }, 80);
                        }
                        clearInterval(dv);
                        for(var bG = 0; bG < av.length; bG++) {
                            clearInterval(av[bG]);
                        }
                        av = [];
                        //var input = document.getElementById(passGuardThis.settings.id);// modified by landry 修改软键盘删除键this指向错误
                        var input = document.getElementById(R.settings.aQ.settings.id);
                        var dO = "";
                        for(var i = 0; i < R.settings.aQ.bu.length; i++) {
                            dO += "*";
                        };
                        if(R.settings.aQ.settings.displayMode == 1) {
                            input.value = R.settings.aQ.bg;
                        } else {
                            input.value = dO;
                        };
                        BL.double = 0;
                    };
                    if((aK != "kb_n_D") && (aK != "kb_n_CLOSE") && (aK != "kb_n_SURE")) {
                        $("#" + aK).on("touchstart", function(e) {
                            if(BL.double == 0) {
                                var key = this.innerText;
                                clearTimeout(BL.FD);
                                BL.once = 0;
                                BL.Mov = 0;
                                BL.key2 = $(this).attr("data-name");
                                BL.double = 1;
                                $(".fd").eq(0).remove();
                                if(/_SPACE/i.test(aK)) {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        var url = BL.SV + "/space_2.svg";
                                        var img = new Image;
                                        img.src = url;
                                        img.onload = function() {
                                            $(".anxia").css({
                                                background: "url(" + BL.SV + "/anjian.svg) ",
                                                backgroundSize: "100% 100%"
                                            });
                                            $(".anxia5").css({
                                                background: "url(" + BL.SV + "/quanDEL_2.svg) ",
                                                backgroundSize: "100% 100%"
                                            });
                                            $("#number_keyboard div").removeClass("anxia");
                                            $("#kb_n_SPACE").css({
                                                backgroundImage: 'url(' + BL.SV + '/space_2.svg' + ')',
                                                backgroundSize: "100% 100%"
                                            });
                                        }
                                    }
                                } else if(aK == "kb_n_C1") {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        call(BL.SV + "/123_2.svg");
                                    }
                                } else if(aK == "kb_n_C" || aK == "kb_n_P") {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        call(BL.SV + "/quanDEL_2.svg");
                                    }
                                } else if(/_S/.test(aK)) {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        call(BL.SV + "/quanDEL_2.svg");
                                    }
                                } else {
                                    if(R.settings.pressStatus == 1) {
                                        var cc = $(this);
                                        var Html = $(this).html();
                                        cc.append($("<div class='fd'></div>"));
                                        cc.css({
                                            position: "relative"
                                        });
                                        if(cc.hasClass("rowleft")) {
                                            var img = new Image;
                                            img.src = BL.SV + '/dianji_left.svg';
                                            img.onload = function() {
                                                $(".fd").eq(0).html(Html);
                                                $(".fd").eq(0).css({
                                                    backgroundImage: 'url(' + img.src + ')',
                                                    backgroundSize: "100% 100%",
                                                    position: "absolute",
                                                    width: "140%",
                                                    height: aE * 0.4 + "px",
                                                    left: 0,
                                                    bottom: "0px",
                                                    textAlign: "center",
                                                    fontSize: "1.5em",
                                                    zIndex: "999",
                                                    lineHeight: aE * 0.19 + "px"
                                                });
                                            }
                                        } else if(cc.hasClass("rowright")) {
                                            var img = new Image;
                                            img.src = BL.SV + '/dianji_right.svg';
                                            img.onload = function() {
                                                $(".fd").eq(0).html(Html);
                                                $(".fd").eq(0).css({
                                                    backgroundImage: 'url(' + img.src + ')',
                                                    backgroundSize: "100% 100%",
                                                    position: "absolute",
                                                    width: "140%",
                                                    height: aE * 0.4 + "px",
                                                    right: 0,
                                                    bottom: "0px",
                                                    textAlign: "center",
                                                    fontSize: "1.5em",
                                                    zIndex: "999",
                                                    lineHeight: aE * 0.19 + "px"
                                                });
                                            }
                                        } else {
                                            if($("#" + aK).hasClass("row3pwdd")) {
                                                var img = new Image;
                                                img.src = BL.SV + '/dianji.svg';
                                                img.onload = function() {
                                                    $(".fd").eq(0).html(Html);
                                                    $(".fd").eq(0).css({
                                                        backgroundImage: 'url(' + img.src + ')',
                                                        position: "absolute",
                                                        width: "150%",
                                                        height: aE * 0.4 + "px",
                                                        left: "-25%",
                                                        bottom: "0px",
                                                        textAlign: "center",
                                                        fontSize: "1.5em",
                                                        zIndex: "999",
                                                        lineHeight: aE * 0.19 + "px",
                                                        backgroundSize: "100% 100%"
                                                    });
                                                }
                                            } else {
                                                var img = new Image;
                                                img.src = BL.SV + '/dianji.svg';
                                                img.onload = function() {
                                                    $(".fd").eq(0).html(Html);
                                                    $(".fd").eq(0).css({
                                                        backgroundImage: 'url(' + img.src + ')',
                                                        position: "absolute",
                                                        width: "140%",
                                                        height: aE * 0.4 + "px",
                                                        left: "-20%",
                                                        bottom: "0px",
                                                        textAlign: "center",
                                                        fontSize: "1.5em",
                                                        zIndex: "999",
                                                        lineHeight: aE * 0.19 + "px",
                                                        backgroundSize: "100% 100%"
                                                    });
                                                }
                                            }
                                        }
                                    } else if(R.settings.pressStatus == 2) {
                                        if($("#" + aK).hasClass("row3pwdd")) {
                                            var url = BL.SV + "/quanDEL.svg";
                                            var img = new Image;
                                            img.src = url;
                                            img.onload = function() {
                                                $(".anxia").css({
                                                    background: "url(" + BL.SV + "/anjian.svg) ",
                                                    backgroundSize: "100% 100%"
                                                });
                                                $(".anxia5").css({
                                                    background: "url(" + BL.SV + "/quanDEL_2.svg) ",
                                                    backgroundSize: "100% 100%"
                                                });
                                                $("#number_keyboard div").removeClass("anxia");
                                                $("#number_keyboard div").removeClass("anxia5");
                                                $("#" + aK).addClass("anxia5");
                                                $("#" + aK).css({
                                                    backgroundImage: 'url(' + BL.SV + '/quanDEL.svg' + ')',
                                                    backgroundSize: "100% 100%"
                                                });
                                            }
                                        } else {
                                            var url = BL.SV + "/anjian_2.svg";
                                            var img = new Image;
                                            img.src = url;
                                            img.onload = function() {
                                                $(".anxia").css({
                                                    background: "url(" + BL.SV + "/anjian.svg) ",
                                                    backgroundSize: "100% 100%"
                                                });
                                                $(".anxia5").css({
                                                    background: "url(" + BL.SV + "/quanDEL_2.svg) ",
                                                    backgroundSize: "100% 100%"
                                                });
                                                $("#number_keyboard div").removeClass("anxia");
                                                $("#number_keyboard div").removeClass("anxia5");
                                                $("#" + aK).addClass("anxia");
                                                $("#" + aK).css({
                                                    backgroundImage: 'url(' + BL.SV + '/anjian_2.svg' + ')',
                                                    backgroundSize: "100% 100%"
                                                });
                                            }
                                        }
                                    }
                                }
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                                e.preventDefault();
                            }
                        });
                        $("#" + aK).on("touchmove", function(e) {
                            var MH = e.originalEvent.changedTouches[0].clientY;
                            if(BL.WH <= MH + 1 && BL.Mov == 0) {
                                if(/_SPACE/i.test(aK)) {
                                    setTimeout(function() {
                                        call(BL.SV + "/space.svg");
                                    }, 100);
                                } else if(aK == "kb_n_C1") {
                                    setTimeout(function() {
                                        call(BL.SV + "/123.svg");
                                    }, 80);
                                } else if(aK == "kb_n_C" || aK == "kb_n_P") {
                                    setTimeout(function() {
                                        call(BL.SV + "/quanDEL.svg");
                                    }, 80);
                                } else if(/_S/.test(aK)) {
                                    setTimeout(function() {
                                        call(BL.SV + "/quanDEL.svg");
                                    }, 80);
                                } else {
                                    if($("#" + aK).hasClass("row3pwdd")) {
                                        call(BL.SV + "/quanDEL_2.svg");
                                    } else {
                                        call(BL.SV + "/anjian.svg");
                                    }
                                    BL.FD = setTimeout(function() {
                                        $(".fd").remove();
                                    }, 100);
                                };
                                if(e.target.className.indexOf("chg") > -1) {
                                    if(R.settings.chaosMode == 2 && event == 0) {
                                        var aN = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
                                        aN = R.gC(aN);
                                        var bY = [];
                                        var au = $("#number_keyboard > .row1")[0].querySelectorAll(".row1pwd");
                                        setTimeout(function() {
                                            for(var j = 0, i = 0; j < au.length; j++) {
                                                if(au[j].id == "kb_n_CLOSE" || au[j].id == "kb_n_D") continue;
                                                i++;
                                                bY.push(au[j]);
                                            }
                                            for(var j = 0; j < bY.length; j++) {
                                                bY[j].innerHTML = aN[j];
                                            }
                                        }, 100)
                                    }
                                };
                                BL.once = 1;
                                BL.Mov = 1;
                                BL.double = 0;
                                e.preventDefault();
                            }
                        });
                        $("#" + aK).on("touchend", function(e) {
                            if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                                BL.double = 0;
                                if(/_SPACE/i.test(aK)) {
                                    setTimeout(function() {
                                        call(BL.SV + "/space.svg");
                                    }, 100);
                                } else if(aK == "kb_n_C1") {
                                    setTimeout(function() {
                                        call(BL.SV + "/123.svg");
                                    }, 80);
                                } else if(aK == "kb_n_C" || aK == "kb_n_P") {
                                    setTimeout(function() {
                                        call(BL.SV + "/quanDEL.svg");
                                    }, 80);
                                } else if(/_S/.test(aK)) {
                                    setTimeout(function() {
                                        call(BL.SV + "/quanDEL.svg");
                                    }, 80);
                                } else {
                                    setTimeout(function() {
                                        if($("#" + aK).hasClass("row3pwdd")) {
                                            call(BL.SV + "/quanDEL_2.svg");
                                        } else {
                                            call(BL.SV + "/anjian.svg");
                                        }
                                    }, 100);
                                    BL.FD = setTimeout(function() {
                                        $(".fd").remove();
                                    }, 100);
                                };
                                e.preventDefault();
                            }
                        });
                    } else if(aK == "kb_n_D") {
                        var key = this.innerText;
                        var event = 2;
                        var dv;
                        var av = [];
                        $("#" + aK).on("touchstart", function(e) {
                            if(BL.double == 0) {
                                BL.double = 1;
                                BL.once = 0;
                                BL.Mov = 0;
                                BL.key2 = $(this).attr("data-name");
                                var key = this.innerText;
                                if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                    $(".anxia").css({
                                        background: "url(" + BL.SV + "/anjian.svg) ",
                                        backgroundSize: "100% 100%"
                                    });
                                    $(".anxia5").css({
                                        background: "url(" + BL.SV + "/quanDEL_2.svg) ",
                                        backgroundSize: "100% 100%"
                                    });
                                    $("#number_keyboard div").removeClass("anxia");
                                    var url = BL.SV + "/quanDEL_2.svg";
                                    var img = new Image;
                                    img.src = url;
                                    img.onload = function() {
                                        $("#kb_n_D").css({
                                            backgroundImage: 'url(' + BL.SV + '/quanDEL_2.svg' + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                        $("#kb_n_D>div").css({
                                            backgroundImage: 'url(' + BL.SV + '/DEL_2.svg' + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                    }
                                }
                                fr = new Date().getTime();
                                dv = window.setInterval(function() {
                                    eO = new Date().getTime();
                                    var x = eO - fr;
                                    if(x > 500) {
                                        //var input = document.getElementById(passGuardThis.settings.id);// modified by landry 修改软键盘删除键this指向错误
                                    	var input = document.getElementById(R.settings.aQ.settings.id);
                                    	var fK = setInterval(function() {
                                            R.settings.aQ.bu = R.settings.aQ.bu.substr(0, R.settings.aQ.bu.length - 1);
                                            R.settings.aQ.bg -= 1;
                                            if(R.settings.aQ.bg < 0) R.settings.aQ.bg = 0;
                                            if(R.settings.aQ.bg == 0) R.settings.aQ.asm = [""];
                                            if(R.settings.aQ.bg == 1) R.settings.aQ.bsm = [""];
                                            if(R.settings.aQ.bg == 2) R.settings.aQ.csm = [""];
                                            if(R.settings.aQ.bg == 3) R.settings.aQ.dsm = [""];
                                            if(R.settings.aQ.bg == 4) R.settings.aQ.esm = [""];
                                            if(R.settings.aQ.bg == 5) R.settings.aQ.fsm = [""];
                                            if(R.settings.aQ.bg == 6) R.settings.aQ.gsm = [""];
                                            if(R.settings.aQ.bg == 7) R.settings.aQ.hsm = [""];
                                            if(R.settings.aQ.bg == 8) R.settings.aQ.ism = [""];
                                            if(R.settings.aQ.bg == 9) R.settings.aQ.jsm = [""];
                                            if(R.settings.aQ.bg == 10) R.settings.aQ.ksm = [""];
                                            if(R.settings.aQ.bg == 11) R.settings.aQ.lsm = [""];
                                            if(R.settings.aQ.bg == 12) R.settings.aQ.msm = [""];
                                            if(R.settings.aQ.bg == 13) R.settings.aQ.nsm = [""];
                                            if(R.settings.aQ.bg == 14) R.settings.aQ.osm = [""];
                                            if(R.settings.aQ.bg == 15) R.settings.aQ.psm = [""];
                                            if(R.settings.aQ.bg == 16) R.settings.aQ.qsm = [""];
                                            if(R.settings.aQ.bg == 17) R.settings.aQ.rsm = [""];
                                            if(R.settings.aQ.bg == 18) R.settings.aQ.ssm = [""];
                                            if(R.settings.aQ.bg == 19) R.settings.aQ.tsm = [""];
                                            if(R.settings.aQ.bg == 20) R.settings.aQ.usm = [""];
                                            if(R.settings.aQ.bg == 21) R.settings.aQ.vsm = [""];
                                            if(R.settings.aQ.bg == 22) R.settings.aQ.wsm = [""];
                                            if(R.settings.aQ.bg == 23) R.settings.aQ.xsm = [""];
                                            if(R.settings.aQ.bg == 24) R.settings.aQ.ysm = [""];
                                            if(R.settings.aQ.bg == 25) R.settings.aQ.zsm = [""];
                                            if(R.settings.aQ.bg == 26) R.settings.aQ.aasm = [""];
                                            if(R.settings.aQ.bg == 27) R.settings.aQ.bbsm = [""];
                                            if(R.settings.aQ.bg == 28) R.settings.aQ.ccsm = [""];
                                            if(R.settings.aQ.bg == 29) R.settings.aQ.ddsm = [""];
                                            input.value = input.value.substr(0, input.value.length - 1);
                                            if(R.settings.aQ.settings.jump == 1) {
                                                R.settings.aQ.settings.del(R.settings.aQ.bg);
                                            }
                                        }, 50);
                                        av.push(fK);
                                        clearInterval(fK)
                                        clearInterval(dv);
                                    }
                                }, 1);
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                                e.preventDefault();
                                return false;
                            }
                        });
                        $("#" + aK).on("touchmove", function(e) {
                            var MH = e.originalEvent.changedTouches[0].clientY;
                            if(BL.WH <= MH) {
                                del3();
                            }
                            e.preventDefault();
                            return false;
                        });
                        $("#" + aK).on("touchend", function(e) {
                            if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                                del3();
                                BL.Mov = 1;
                                e.preventDefault();
                                return false;
                            }
                        });
                    };
                    $("#" + aK).on("touchend", function(e) {
                        if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                            if(aK == "kb_n_CLOSE" && (R.settings.pressStatus == 1 || R.settings.pressStatus == 2)) {
                                setTimeout(function() {
                                    call(BL.SV + "/123.svg");
                                }, 80);
                            };
                            if(aK == "kb_n_S") {
                                setTimeout(function() {
                                    $("#char_keyboard").css("display", "none");
                                    $("#number_keyboard").css("display", "none");
                                    $("#symble_keyboard").css("display", "block");
                                    $("#purenumber_keyboard").css("display", "none");
                                    justic3();
                                }, 80);
                            };
                            if(aK == "kb_n_P") {
                                setTimeout(function() {
                                    $("#char_keyboard").css("display", "none");
                                    $("#number_keyboard").css("display", "none");
                                    $("#symble_keyboard").css("display", "none");
                                    $("#purenumber_keyboard").css("display", "block");
                                    justic3();
                                }, 80);
                            };
                            if(aK == "kb_n_C" || aK == "kb_n_C1") {
                                setTimeout(function() {
                                    $("#char_keyboard").css("display", "block");
                                    $("#number_keyboard").css("display", "none");
                                    $("#symble_keyboard").css("display", "none");
                                    $("#purenumber_keyboard").css("display", "none");
                                    justic3();
                                }, 80);
                            };
                            if(e.target.className.indexOf("chg") > -1) {
                                if(R.settings.chaosMode == 2 && event == 0) {
                                    var aN = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
                                    aN = R.gC(aN);
                                    var bY = [];
                                    var au = $("#number_keyboard > .row1")[0].querySelectorAll(".row1pwd");
                                    setTimeout(function() {
                                        for(var j = 0, i = 0; j < au.length; j++) {
                                            if(au[j].id == "kb_n_CLOSE" || au[j].id == "kb_n_D") continue;
                                            i++;
                                            bY.push(au[j]);
                                        }
                                        for(var j = 0; j < bY.length; j++) {
                                            bY[j].innerHTML = aN[j];
                                        }
                                    }, 100)
                                }
                            };
                            BL.Mov = 1;
                        }
                    });
                    $("#" + aK).on("touchstart", function(e) {
                        BL.WH = $(window).height();
                        var key = this.innerText;
                        if(aK == "kb_n_SPACE") {
                            return;
                        } else if(aK == "kb_n_D") {
                            event = 2;
                        } else if((aK == "kb_n_SURE" || aK == "kb_n_CLOSE") && BL.double == 0) {
                            if(aK == "kb_n_CLOSE" && (R.settings.pressStatus == 1 || R.settings.pressStatus == 2)) {
                                call(BL.SV + "/123_2.svg");
                            };
                            if(BL.fixed == "") {
                                $("body").css({
                                    position: "relative",
                                    left: 0,
                                    top: 0,
                                    transition: "all 0.3s"
                                });
                            } else {
                                $("body").css({
                                    position: "relative",
                                    left: 0,
                                    top: 0,
                                    transition: "all 0.3s"
                                });
                                $("#" + BL.fixed).css({
                                    bottom: 0 + "px",
                                    transition: "all 0.3s"
                                });
                            };
                            $(aY).removeClass("pwdkeyboard").addClass("pwdkeyboardout");
                            document.querySelector(aY).addEventListener("webkitAnimationEnd", R.cf, false);
                            document.querySelector(aY).addEventListener("animationEnd", R.cf, false);
                            event = 1;
                            setTimeout(function() {
                                PH.s_n();
                                PH.add++;
                                PH.cs = 1;
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                            }, 80)
                        } else if(aK == "kb_n_S") {
                            event = 6;
                        } else if(aK == "kb_n_C") {
                            event = 5;
                        } else {}
                        e.preventDefault();
                    });
                }
                if(nV.test(aK)) {
                    function del4() {
                        if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                            setTimeout(function() {
                                var url = BL.SV + "/quanDEL.svg";
                                var img = new Image;
                                img.src = url;
                                img.onload = function() {
                                    $("#kb_s_D").css({
                                        backgroundImage: 'url(' + BL.SV + '/quanDEL.svg' + ')',
                                        backgroundSize: "100% 100%"
                                    });
                                    $("#kb_s_D>div").css({
                                        backgroundImage: 'url(' + BL.SV + '/DEL.svg' + ')',
                                        backgroundSize: "100% 100%"
                                    });
                                }
                            }, 80);
                        }
                        clearInterval(dv);
                        for(var bG = 0; bG < av.length; bG++) {
                            clearInterval(av[bG]);
                        }
                        av = [];
                        //var input = document.getElementById(passGuardThis.settings.id);// modified by landry 修改软键盘删除键this指向错误
                        var input = document.getElementById(R.settings.aQ.settings.id);
                        var dO = "";
                        for(var i = 0; i < R.settings.aQ.bu.length; i++) {
                            dO += "*";
                        };
                        if(R.settings.aQ.settings.displayMode == 1) {
                            input.value = R.settings.aQ.bg;
                        } else {
                            input.value = dO;
                        };
                        BL.double = 0;
                    };
                    if((aK != "kb_s_D") && (aK != "kb_s_CLOSE") && (aK != "kb_s_SURE")) {
                        $("#" + aK).on("touchstart", function(e) {
                            if(BL.double == 0) {
                                var key = this.innerText;
                                clearTimeout(BL.FD);
                                BL.once = 0;
                                BL.Mov = 0;
                                BL.key2 = $(this).attr("data-name");
                                BL.double = 1;
                                $(".fd").remove();
                                if(/_SPACE/i.test(aK)) {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        var url = BL.SV + "/space_2.svg";
                                        var img = new Image;
                                        img.src = url;
                                        img.onload = function() {
                                            $(".anxia").css({
                                                background: "url(" + BL.SV + "/anjian.svg) ",
                                                backgroundSize: "100% 100%"
                                            });
                                            $(".anxia5").css({
                                                background: "url(" + BL.SV + "/quanDEL_2.svg) ",
                                                backgroundSize: "100% 100%"
                                            });
                                            $("#symble_keyboard div").removeClass("anxia");
                                            $("#symble_keyboard div").removeClass("anxia5");
                                            $("#kb_s_SPACE").css({
                                                backgroundImage: 'url(' + BL.SV + '/space_2.svg' + ')',
                                                backgroundSize: "100% 100%"
                                            });
                                        }
                                    }
                                } else if(aK == "kb_s_C1") {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        call(BL.SV + "/123_2.svg");
                                    }
                                } else if(aK == "kb_s_C" || aK == "kb_s_P") {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        call(BL.SV + "/quanDEL_2.svg");
                                    }
                                } else if(aK == "kb_s_N" || aK == "kb_s_N1") {
                                    event = 7;
                                    if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                        call(BL.SV + "/quanDEL_2.svg");
                                    }
                                } else {
                                    if(R.settings.pressStatus == 1) {
                                        var cc = $(this);
                                        var Html = $(this).html();
                                        cc.append($("<div class='fd'></div>"));
                                        cc.css({
                                            position: "relative"
                                        });
                                        if(cc.hasClass("rowleft")) {
                                            var img = new Image;
                                            img.src = BL.SV + '/dianji_left.svg';
                                            img.onload = function() {
                                                $(".fd").eq(0).html(Html);
                                                $(".fd").eq(0).css({
                                                    backgroundImage: 'url(' + img.src + ')',
                                                    position: "absolute",
                                                    width: "140%",
                                                    height: aE * 0.4 + "px",
                                                    left: 0,
                                                    bottom: "0px",
                                                    textAlign: "center",
                                                    fontSize: "1.5em",
                                                    zIndex: "999",
                                                    lineHeight: aE * 0.19 + "px",
                                                    backgroundSize: "100% 100%"
                                                });
                                            }
                                        } else if(cc.hasClass("rowright")) {
                                            var img = new Image;
                                            img.src = BL.SV + '/dianji_right.svg';
                                            img.onload = function() {
                                                $(".fd").eq(0).html(Html);
                                                $(".fd").eq(0).css({
                                                    backgroundImage: 'url(' + img.src + ')',
                                                    position: "absolute",
                                                    width: "140%",
                                                    height: aE * 0.4 + "px",
                                                    right: 0,
                                                    bottom: "0px",
                                                    textAlign: "center",
                                                    fontSize: "1.5em",
                                                    zIndex: "999",
                                                    lineHeight: aE * 0.19 + "px",
                                                    backgroundSize: "100% 100%"
                                                });
                                            }
                                        } else {
                                            if($("#" + aK).hasClass("row3pwdd")) {
                                                var img = new Image;
                                                img.src = BL.SV + '/dianji.svg';
                                                img.onload = function() {
                                                    $(".fd").eq(0).html(Html);
                                                    $(".fd").eq(0).css({
                                                        backgroundImage: 'url(' + img.src + ')',
                                                        position: "absolute",
                                                        width: "150%",
                                                        height: aE * 0.4 + "px",
                                                        left: "-25%",
                                                        bottom: "0px",
                                                        textAlign: "center",
                                                        fontSize: "1.5em",
                                                        zIndex: "999",
                                                        lineHeight: aE * 0.19 + "px",
                                                        backgroundSize: "100% 100%"
                                                    });
                                                }
                                            } else {
                                                var img = new Image;
                                                img.src = BL.SV + '/dianji.svg';
                                                img.onload = function() {
                                                    $(".fd").eq(0).html(Html);
                                                    $(".fd").eq(0).css({
                                                        backgroundImage: 'url(' + img.src + ')',
                                                        position: "absolute",
                                                        width: "140%",
                                                        height: aE * 0.4 + "px",
                                                        left: "-20%",
                                                        bottom: "0px",
                                                        textAlign: "center",
                                                        fontSize: "1.5em",
                                                        zIndex: "999",
                                                        lineHeight: aE * 0.19 + "px",
                                                        backgroundSize: "100% 100%"
                                                    });
                                                }
                                            }
                                        }
                                    } else if(R.settings.pressStatus == 2) {
                                        if($("#" + aK).hasClass("row3pwdd")) {
                                            var url = BL.SV + "/quanDEL.svg";
                                            var img = new Image;
                                            img.src = url;
                                            img.onload = function() {
                                                $(".anxia").css({
                                                    background: "url(" + BL.SV + "/anjian.svg) ",
                                                    backgroundSize: "100% 100%"
                                                });
                                                $(".anxia5").css({
                                                    background: "url(" + BL.SV + "/quanDEL_2.svg) ",
                                                    backgroundSize: "100% 100%"
                                                });
                                                $("#symble_keyboard div").removeClass("anxia");
                                                $("#symble_keyboard div").removeClass("anxia5");
                                                $("#" + aK).addClass("anxia5");
                                                $("#" + aK).css({
                                                    backgroundImage: 'url(' + BL.SV + '/quanDEL.svg' + ')',
                                                    backgroundSize: "100% 100%"
                                                });
                                            }
                                        } else {
                                            var url = BL.SV + "/anjian_2.svg";
                                            var img = new Image;
                                            img.src = url;
                                            img.onload = function() {
                                                $(".anxia").css({
                                                    background: "url(" + BL.SV + "/anjian.svg) ",
                                                    backgroundSize: "100% 100%"
                                                });
                                                $(".anxia5").css({
                                                    background: "url(" + BL.SV + "/quanDEL_2.svg) ",
                                                    backgroundSize: "100% 100%"
                                                });
                                                $("#symble_keyboard div").removeClass("anxia");
                                                $("#symble_keyboard div").removeClass("anxia5");
                                                $("#" + aK).addClass("anxia");
                                                $("#" + aK).css({
                                                    backgroundImage: 'url(' + BL.SV + '/anjian_2.svg' + ')',
                                                    backgroundSize: "100% 100%"
                                                });
                                            }
                                        }
                                    }
                                }
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                                e.preventDefault();
                            }
                        });
                        $("#" + aK).on("touchmove", function(e) {
                            var MH = e.originalEvent.changedTouches[0].clientY;
                            if(BL.WH <= MH + 1 && BL.Mov == 0) {
                                if(/_SPACE/i.test(aK)) {
                                    setTimeout(function() {
                                        call(BL.SV + "/space.svg");
                                    }, 100);
                                } else if(aK == "kb_s_C1") {
                                    setTimeout(function() {
                                        call(BL.SV + "/123.svg");
                                    }, 100);
                                } else if(aK == "kb_s_C" || aK == "kb_s_P") {
                                    setTimeout(function() {
                                        call(BL.SV + "/quanDEL.svg");
                                    }, 100);
                                } else if(aK == "kb_s_N" || aK == "kb_s_N1") {
                                    setTimeout(function() {
                                        call(BL.SV + "/quanDEL.svg");
                                    }, 100);
                                } else {
                                    if($("#" + aK).hasClass("row3pwdd")) {
                                        call(BL.SV + "/quanDEL_2.svg");
                                    } else {
                                        call(BL.SV + "/anjian.svg");
                                    }
                                    BL.FD = setTimeout(function() {
                                        $(".fd").remove();
                                    }, 100);
                                };
                                BL.once = 1;
                                BL.Mov = 1;
                                BL.double = 0;
                                e.preventDefault();
                            }
                        });
                        $("#" + aK).on("touchend", function(e) {
                            if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                                BL.double = 0;
                                if(/_SPACE/i.test(aK)) {
                                    setTimeout(function() {
                                        call(BL.SV + "/space.svg");
                                    }, 100);
                                } else if(aK == "kb_s_C1") {
                                    setTimeout(function() {
                                        call(BL.SV + "/123.svg");
                                    }, 100);
                                } else if(aK == "kb_s_C" || aK == "kb_s_P") {
                                    setTimeout(function() {
                                        call(BL.SV + "/quanDEL.svg");
                                    }, 100);
                                } else if(aK == "kb_s_N" || aK == "kb_s_N1") {
                                    setTimeout(function() {
                                        call(BL.SV + "/quanDEL.svg");
                                    }, 100);
                                } else {
                                    setTimeout(function() {
                                        if($("#" + aK).hasClass("row3pwdd")) {
                                            call(BL.SV + "/quanDEL_2.svg");
                                        } else {
                                            call(BL.SV + "/anjian.svg");
                                        }
                                    }, 100);
                                    BL.FD = setTimeout(function() {
                                        $(".fd").remove();
                                    }, 100);
                                };
                                e.preventDefault();
                            }
                        });
                    } else if(aK == "kb_s_D") {
                        var event = 2;
                        var dv;
                        var av = [];
                        $("#" + aK).on("touchstart", function(e) {
                            if(BL.double == 0) {
                                BL.double = 1;
                                BL.once = 0;
                                BL.Mov = 0;
                                BL.key2 = $(this).attr("data-name");
                                var key = this.innerText;
                                if(R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                    $(".anxia").css({
                                        background: "url(" + BL.SV + "/anjian.svg) ",
                                        backgroundSize: "100% 100%"
                                    });
                                    $(".anxia5").css({
                                        background: "url(" + BL.SV + "/quanDEL_2.svg) ",
                                        backgroundSize: "100% 100%"
                                    });
                                    $("#symble_keyboard div").removeClass("anxia");
                                    $("#symble_keyboard div").removeClass("anxia5");
                                    var url = BL.SV + "/quanDEL_2.svg";
                                    var img = new Image;
                                    img.src = url;
                                    img.onload = function() {
                                        $("#kb_s_D").css({
                                            backgroundImage: 'url(' + BL.SV + '/quanDEL_2.svg' + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                        $("#kb_s_D>div").css({
                                            backgroundImage: 'url(' + BL.SV + '/DEL_2.svg' + ')',
                                            backgroundSize: "100% 100%"
                                        });
                                    }
                                }
                                fr = new Date().getTime();
                                dv = window.setInterval(function() {
                                    eO = new Date().getTime();
                                    var x = eO - fr;
                                    if(x > 500) {
                                        //var input = document.getElementById(passGuardThis.settings.id);// modified by landry 修改软键盘删除键this指向错误
                                    	var input = document.getElementById(R.settings.aQ.settings.id);
                                    	var fK = setInterval(function() {
                                            R.settings.aQ.bu = R.settings.aQ.bu.substr(0, R.settings.aQ.bu.length - 1);
                                            R.settings.aQ.bg -= 1;
                                            if(R.settings.aQ.bg < 0) R.settings.aQ.bg = 0;
                                            if(R.settings.aQ.bg == 0) R.settings.aQ.asm = [""];
                                            if(R.settings.aQ.bg == 1) R.settings.aQ.bsm = [""];
                                            if(R.settings.aQ.bg == 2) R.settings.aQ.csm = [""];
                                            if(R.settings.aQ.bg == 3) R.settings.aQ.dsm = [""];
                                            if(R.settings.aQ.bg == 4) R.settings.aQ.esm = [""];
                                            if(R.settings.aQ.bg == 5) R.settings.aQ.fsm = [""];
                                            if(R.settings.aQ.bg == 6) R.settings.aQ.gsm = [""];
                                            if(R.settings.aQ.bg == 7) R.settings.aQ.hsm = [""];
                                            if(R.settings.aQ.bg == 8) R.settings.aQ.ism = [""];
                                            if(R.settings.aQ.bg == 9) R.settings.aQ.jsm = [""];
                                            if(R.settings.aQ.bg == 10) R.settings.aQ.ksm = [""];
                                            if(R.settings.aQ.bg == 11) R.settings.aQ.lsm = [""];
                                            if(R.settings.aQ.bg == 12) R.settings.aQ.msm = [""];
                                            if(R.settings.aQ.bg == 13) R.settings.aQ.nsm = [""];
                                            if(R.settings.aQ.bg == 14) R.settings.aQ.osm = [""];
                                            if(R.settings.aQ.bg == 15) R.settings.aQ.psm = [""];
                                            if(R.settings.aQ.bg == 16) R.settings.aQ.qsm = [""];
                                            if(R.settings.aQ.bg == 17) R.settings.aQ.rsm = [""];
                                            if(R.settings.aQ.bg == 18) R.settings.aQ.ssm = [""];
                                            if(R.settings.aQ.bg == 19) R.settings.aQ.tsm = [""];
                                            if(R.settings.aQ.bg == 20) R.settings.aQ.usm = [""];
                                            if(R.settings.aQ.bg == 21) R.settings.aQ.vsm = [""];
                                            if(R.settings.aQ.bg == 22) R.settings.aQ.wsm = [""];
                                            if(R.settings.aQ.bg == 23) R.settings.aQ.xsm = [""];
                                            if(R.settings.aQ.bg == 24) R.settings.aQ.ysm = [""];
                                            if(R.settings.aQ.bg == 25) R.settings.aQ.zsm = [""];
                                            if(R.settings.aQ.bg == 26) R.settings.aQ.aasm = [""];
                                            if(R.settings.aQ.bg == 27) R.settings.aQ.bbsm = [""];
                                            if(R.settings.aQ.bg == 28) R.settings.aQ.ccsm = [""];
                                            if(R.settings.aQ.bg == 29) R.settings.aQ.ddsm = [""];
                                            input.value = input.value.substr(0, input.value.length - 1);
                                            if(R.settings.aQ.settings.jump == 1) {
                                                R.settings.aQ.settings.del(R.settings.aQ.bg);
                                            }
                                        }, 50);
                                        av.push(fK);
                                        clearInterval(fK)
                                        clearInterval(dv);
                                    }
                                }, 1);
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                                e.preventDefault();
                                return false;
                            }
                        });
                        $("#" + aK).on("touchmove", function(e) {
                            var MH = e.originalEvent.changedTouches[0].clientY;
                            if(BL.WH <= MH + 1) {
                                del4();
                                e.preventDefault();
                                return false;
                            }
                        });
                        $("#" + aK).on("touchend", function(e) {
                            if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                                del4();
                                BL.Mov = 1;
                                e.preventDefault();
                                return false;
                            }
                        });
                    };
                    $("#" + aK).on("touchend", function(e) {
                        if(BL.key2 == $(this).attr("data-name") && BL.Mov == 0) {
                            if(aK == "kb_s_CLOSE" && (R.settings.pressStatus == 1 || R.settings.pressStatus == 2)) {
                                setTimeout(function() {
                                    call(BL.SV + "/123.svg");
                                }, 100);
                            };
                            if(aK == "kb_s_N" || aK == "kb_s_N1") {
                                setTimeout(function() {
                                    $("#char_keyboard").css("display", "none");
                                    $("#number_keyboard").css("display", "block");
                                    $("#symble_keyboard").css("display", "none");
                                    $("#purenumber_keyboard").css("display", "none");
                                    justic3();
                                }, 80);
                            };
                            if(aK == "kb_s_P") {
                                setTimeout(function() {
                                    $("#char_keyboard").css("display", "none");
                                    $("#number_keyboard").css("display", "none");
                                    $("#symble_keyboard").css("display", "none");
                                    $("#purenumber_keyboard").css("display", "block");
                                    justic3();
                                }, 80);
                            };
                            if(aK == "kb_s_C" || aK == "kb_s_C1") {
                                setTimeout(function() {
                                    $("#char_keyboard").css("display", "block");
                                    $("#number_keyboard").css("display", "none");
                                    $("#symble_keyboard").css("display", "none");
                                    $("#purenumber_keyboard").css("display", "none");
                                    justic3();
                                }, 80);
                            };
                            BL.Mov = 1;
                        }
                    });
                    $("#" + aK).on("touchstart", function(e) {
                        BL.WH = $(window).height();
                        var key = this.innerText;
                        if(aK == "kb_s_SPACE") {
                            return;
                        } else if(aK == "kb_s_D") {
                            event = 2;
                        } else if((aK == "kb_s_SURE" || aK == "kb_s_CLOSE") && BL.double == 0) {
                            if((aK == "kb_s_CLOSE") && R.settings.pressStatus == 1 || R.settings.pressStatus == 2) {
                                call(BL.SV + "/123_2.svg");
                            };
                            if(BL.fixed == "") {
                                $("body").css({
                                    position: "relative",
                                    left: 0,
                                    top: 0,
                                    transition: "all 0.3s"
                                });
                            } else {
                                $("body").css({
                                    position: "relative",
                                    left: 0,
                                    top: 0,
                                    transition: "all 0.3s"
                                });
                                $("#" + BL.fixed).css({
                                    bottom: 0 + "px",
                                    transition: "all 0.3s"
                                });
                            };
                            $(aY).removeClass("pwdkeyboard").addClass("pwdkeyboardout");
                            document.querySelector(aY).addEventListener("webkitAnimationEnd", R.cf, false);
                            document.querySelector(aY).addEventListener("animationEnd", R.cf, false);
                            event = 1;
                            setTimeout(function() {
                                PH.s_n();
                                PH.add++;
                                PH.cs = 1;
                                R.settings.aQ.cY(event, key.charCodeAt() ^ R.settings.odd);
                            }, 80)
                        } else if(aK == "kb_s_N") {
                            event = 4;
                        } else if(aK == "kb_s_C") {
                            event = 5;
                        } else {}
                        e.preventDefault();
                    });
                }
            }
        };
        $(aY).contents("div").filter(function() {
            if(pe.test(this.id)) {
                $("#" + this.id).contents("div").filter(function() {
                    if(nQ.test(this.className)) {
                        $(this).contents("div").filter(function() {
                            kZ(this.id);
                        });
                    } else {
                        $(this).contents("div").filter(function() {
                            kZ(this.id);
                        });
                    }
                });
            }
        });
        $("div").on("touchstart", ".fd", function(e) {
            e.stopPropagation();
        });
        $(".de2").focus(function() {
            for(var bG = 0; bG < M.length; bG++) {
                window.clearInterval(M[bG]);
            };
            for(var bG = 0; bG < MM.length; bG++) {
                window.clearInterval(MM[bG]);
            };
            for(var bG = 0; bG < PH.L.length; bG++) {
                window.clearInterval(PH.L[bG]);
            };
            kJ = mI = undefined;
            miss = "";
            BL.sh = 1;
            clearTimeout(M3);
            clearTimeout(BL.ad);
            BL.ad = setTimeout(function() {
                if(BL.fixed == "") {
                    $("body").css({
                        position: "relative",
                        left: 0,
                        top: 0,
                        transition: "all 0.3s"
                    });
                } else {
                    $("body").css({
                        position: "relative",
                        left: 0,
                        top: 0,
                        transition: "all 0.3s"
                    });
                    $("#" + BL.fixed).css({
                        bottom: 0 + "px",
                        transition: "all 0.3s"
                    });
                };
                BL.sh = 0;
            }, 400);
            $("#testkbid").removeClass("pwdkeyboard").addClass("pwdkeyboardout2").hide();
            if(miss == "") {
                miss = setInterval(function() {
                    $("#testkbid").removeClass("pwdkeyboard").addClass("pwdkeyboardout2").hide();
                }, 20);
                PH.L.push(miss);
            };
            if(BL.olo == 1) {
                event = 1;
                R.settings.aQ.cY(event, '0'.charCodeAt() ^ R.settings.odd);
                if(PH.Id != "" && ($("#testkbid").hasClass("pwdkeyboardout") || $("#testkbid").hasClass("pwdkeyboardout2"))) {
                    if(PH.add == 1) {
                        PH.s_n();
                        PH.add++;
                    }
                    PH.cs = 1;
                };
            }
        });
        $(document).on("touchstart", function(e) {
            var hO = $(aY);
            var input = $("#" + R.settings.id);
            if(hO.has(e.target).length === 0) {
                if(typeof e.target.className == "string" && (e.target.className.toString().indexOf("default") > -1 || e.target.className == "fd" || e.target.className.toString().indexOf("de2") > -1)) {
                    $(".fd").remove();
                    return;
                } else {
                    for(var bG = 0; bG < PH.L.length; bG++) {
                        window.clearInterval(PH.L[bG]);
                    };
                    miss = "";
                    Show = 1;
                    clearTimeout(Show2);
                    Show2 = setTimeout(function() {
                        Show = 0;
                    }, 350);
                    clearTimeout(Hid);
                    Hid = setTimeout(function() {
                        for(var bG = 0; bG < M.length; bG++) {
                            window.clearInterval(M[bG]);
                        };
                        for(var bG = 0; bG < MM.length; bG++) {
                            window.clearInterval(MM[bG]);
                        };
                        setTimeout(function() {
                            if(PH.Id != "" && ($("#testkbid").hasClass("pwdkeyboardout") || $("#testkbid").hasClass("pwdkeyboardout2"))) {
                                if(PH.add == 1) {
                                    PH.s_n();
                                    PH.add++;
                                }
                                PH.cs = 1;
                            };
                            BL.kb = setInterval(function() {
                                var kb = document.getElementById("testkbid");
                                if(kb.className == "pwdkeyboard" && $("#testkbid").css("display") == 'none') {
                                    $("#testkbid").css({
                                        display: "block"
                                    });
                                }
                            }, 20);
                            T.push(BL.kb);
                            setTimeout(function() {
                                for(var i = 0; i < T.length; i++) {
                                    window.clearInterval(T[i]);
                                };
                            }, 2000);
                        }, 300);
                        var vN;
                        clearTimeout(vN);
                        var b = hO.is(':visible');
                        if(BL.lol == 0 && Tim == 0 && b && e.target.className.toString().indexOf("de2") == -1) {
                            if(BL.fixed == "") {
                                $("body").css({
                                    position: "relative",
                                    left: 0,
                                    top: 0 + "px",
                                    transition: "all 0.3s"
                                });
                            } else {
                                $("body").css({
                                    position: "relative",
                                    left: 0,
                                    top: 0 + "px",
                                    transition: "all 0.3s"
                                });
                                $("#" + BL.fixed).css({
                                    bottom: 0 + "px",
                                    transition: "all 0.3s"
                                });
                            };
                            hO.removeClass("pwdkeyboard").addClass("pwdkeyboardout");
                            document.querySelector(aY).addEventListener("webkitAnimationEnd", R.cf, false);
                            document.querySelector(aY).addEventListener("animationEnd", R.cf, false);
                            event = 1;
                            R.settings.aQ.cY(event, '0'.charCodeAt() ^ R.settings.odd);
                            var fq;
                            PH.pm = PH.arrId.indexOf(PH.Id);
                            $("#" + PH.Id).attr('placeholder', PH.arrPlace[PH.pm]);
                            Tim = 1;
                            vN = setTimeout(function() {
                                Tim = 0
                            }, 300);
                        };
                    }, 300)
                }
            };
        });
        window.addEventListener("orientationchange", function() {
            $(".fd").hide();
            var Ah = lA ? 300 : 0;
            if(/ OPR/i.test(u) || / OPiOS/i.test(u) || /MicroMessenger/i.test(u)) {
                R.gg = document.activeElement;
                if(R.gg != undefined) {
                    R.gg.blur();
                    setTimeout(function() {
                        if(R.gg != undefined) {
                            R.gg = undefined;
                        }
                    }, 300);
                }
            };
            var ct;
            if($(aY).is(':visible')) {
                if($("#char_keyboard").is(":hidden")) {
                    ct = 1;
                } else {
                    ct = 0;
                }
            } else {
                ct = 2;
            };
            $(aY).addClass("pwdkeyboardout2").hide();
            if(BL.fixed == "") {
                $("body").css({
                    position: "relative",
                    left: 0,
                    top: 0 + "px",
                    transition: "all 0.3s"
                });
            } else {
                $("body").css({
                    position: "relative",
                    left: 0,
                    top: 0 + "px",
                    transition: "all 0.3s"
                });
                $("#" + BL.fixed).css({
                    bottom: 0 + "px",
                    transition: "all 0.3s"
                });
            };
            $(window).one("resize", function() {
                setTimeout(function() {
                    kM();
                    eW = $(window).height();
                    if($("body").height() < $(window).height()) {
                        $("body").css({
                            height: "100%"
                        });
                        $("html").css({
                            height: "100%"
                        });
                    } else {
                        $("body").css({
                            height: "auto"
                        });
                        $("html").css({
                            height: "auto"
                        });
                    }
                }, 200);
                if(ct != 2) {
                    setTimeout(function() {
                        if(ct == 1) {
                            BL.KK = aE;
                        } else {
                            BL.KK = aE;
                        };
                        R.show(ct);
                    }, 300);
                }
            });
        });
        this.aV = true;
    }
};
keyBoard.prototype.gC = function(data, hu) {    ;
    var lm = data.length;    ;
    var try1 = new Array();
    for(var i = 0; i < lm; i++) {
        try1[i] = i;
    }    ;
    var try2 = new Array();
    for(var i = 0; i < lm; i++) {
        try2[i] = try1.splice(Math.floor(Math.random() * try1.length), 1);
    }    ;
    var try3 = new Array();
    for(var i = 0; i < lm; i++) {
        if(hu) {
            try3[i] = data[try2[i]].toUpperCase();
        } else {
            try3[i] = data[try2[i]].toLowerCase();
        }
    }
    return try3;
};
keyBoard.prototype.yp = function() {
    var h1 = (document.documentElement.clientHeight);
    var h2 = (window.innerHeight);
    alert(document.documentElement.clientHeight);
    alert(window.innerHeight);
    alert(h1);
    alert(h2);
    if(document.documentElement.clientHeight > window.innerHeight) return document.documentElement.clientHeight;
    else return window.innerHeight;
};
var kJ = null;
var mI = null;
keyBoard.prototype.show = function(ct) {
    var R = this;
    var aY = "#" + this.settings.id;
    if((R.settings.chaosMode == 1 || R.settings.chaosMode == 2) && ct == 0) {
        var aN = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
        aN = R.gC(aN);
        var au = $("#char_keyboard")[0].querySelectorAll(".row1pwd,.row2pwd,.row3pwdb");
        if(R.caps || R.shift) {
            for(var j = 0; j < au.length; j++) {
                au[j].innerHTML = aN[j].toUpperCase();
            }
        } else {
            for(var j = 0; j < au.length; j++) {
                au[j].innerHTML = aN[j];
            }
        }
    }
    if((R.settings.chaosMode == 1 || R.settings.chaosMode == 2) && ct == 1) {
        var aN = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
        aN = R.gC(aN);
        var au = $("#purenumber_keyboard")[0].querySelectorAll(".pwd");
        var bY = [];
        for(var j = 0, i = 0; j < au.length; j++) {
            if(au[j].id == "kb_p_CLOSE" || au[j].id == "kb_p_D") continue;
            i++;
            bY.push(au[j]);
        }
        for(var j = 0; j < bY.length; j++) {
            bY[j].innerHTML = aN[j];
        }
    };
    $(aY).css("height", "213px");
    var ii = parseInt($(aY).css("height"));
    $("#purenumber_keyboard").css("height", aE);
    $("#char_keyboard").css("height", aE);
    $("#number_keyboard").css("height", aE);
    $("#symble_keyboard").css("height", aE);
    $(aY).css("height", aE);
    if(BL.lol == 0) {
        if(BL.sh == 0) {
            kJ = setInterval(function() {
                if($(window).height() >= eW * 0.8) {
                    BL.T = 0;
                    setTimeout(function() {
                        var H3 = $(window).height() - $("#" + PH.Id).outerHeight() - $("#" + PH.Id).offset().top + $(document).scrollTop();
                        var H = BL.KK;
                        if(H3 < H) {
                            rD = H3 - H - 2;
                            if(BL.fixed == "") {
                                $("body").css({
                                    position: "relative",
                                    left: 0,
                                    top: (H3 - H - 2) + "px",
                                    transition: "all 0.3s"
                                });
                            } else {
                                $("body").css({
                                    position: "relative",
                                    left: 0,
                                    top: 0 + "px",//-H + "px", // landry修改 弹框密码框总是上移
                                    transition: "all 0.3s"
                                });
                                $("#" + BL.fixed).css({
                                    bottom: H + "px",
                                    transition: "all 0.3s"
                                });
                            }
                        };
                        $(aY).show();
                        if($(aY).hasClass("pwdkeyboardout") || $(aY).hasClass("pwdkeyboardout2")) {
                            $(aY).removeClass("pwdkeyboardout").removeClass("pwdkeyboardout2").addClass("pwdkeyboard");
                        };
                        R.ht();
                    }, 100);
                    BL.lol = 1;
                    setTimeout(function() {
                        BL.lol = 0;
                    }, 300);
                    clearInterval(kJ);
                    for (var int = 0; int < M.length; int++) {
						window.clearInterval(M[int])
					};
                    kJ == undefined;
                }
            }, 20);
            M.push(kJ);
        } else {
            clearTimeout(M3);
            M3 = setTimeout(function() {
                mI = setInterval(function() {
                    if($(window).height() >= eW * 0.8) {
                        BL.T = 0;
                        setTimeout(function() {
                            var H3 = $(window).height() - $("#" + PH.Id).outerHeight() - $("#" + PH.Id).offset().top + $(document).scrollTop();
                            var H = BL.KK;
                            if(H3 < H) {
                                rD = H3 - H - 2;
                                if(BL.fixed == "") {
                                    $("body").css({
                                        position: "relative",
                                        left: 0,
                                        top: (H3 - H - 2) + "px",
                                        transition: "all 0.3s"
                                    });
                                } else {
                                    $("body").css({
                                        position: "relative",
                                        left: 0,
                                        top: -H + "px",
                                        transition: "all 0.3s"
                                    });
                                    $("#" + BL.fixed).css({
                                        bottom: H + "px",
                                        transition: "all 0.3s"
                                    });
                                };
                            };
                            $(aY).show();
                            if($(aY).hasClass("pwdkeyboardout") || $(aY).hasClass("pwdkeyboardout2")) {
                                $(aY).removeClass("pwdkeyboardout").removeClass("pwdkeyboardout2").addClass("pwdkeyboard");
                            };
                            R.ht();
                        }, 100);
                        BL.lol = 1;
                        setTimeout(function() {
                            BL.lol = 0;
                        }, 300);
                        clearInterval(mI);
                        mI == undefined;
                    }
                }, 20);
                MM.push(mI);
            }, 300);
        }
    }
};
keyBoard.prototype.hide = function(e) {
    var R = this;
    var aY = "#" + this.settings.id;
    for(var bG = 0; bG < M.length; bG++) {
        window.clearInterval(M[bG]);
    };
    for(var bG = 0; bG < MM.length; bG++) {
        window.clearInterval(MM[bG]);
    };
    for(var bG = 0; bG < PH.L.length; bG++) {
        window.clearInterval(PH.L[bG]);
    };
    $(aY).removeClass("pwdkeyboard").addClass("pwdkeyboardout2").hide();
    if(BL.olo == 1) {
        event = 1;
        R.settings.aQ.cY(event, '0'.charCodeAt() ^ R.settings.odd);
        if(PH.Id != "" && ($("#testkbid").hasClass("pwdkeyboardout") || $("#testkbid").hasClass("pwdkeyboardout2"))) {
            if(PH.add == 1) {
                PH.s_n();
                PH.add++;
            }
            PH.cs = 1;
        };
    }
    return false;
};

function get_time(){
    return new Date().getTime().toString();
}
let passGuardThis;//modified by landry 20180912 解决两个密码控件页面跳转this指向的问题
function passGuard(options) {
    this.settings = {
        "id": "",
        "maxLength": 12,
        "regExp1": "[0-9]",
        "displayMode": 0,
        "rsaPublicKey": "",
        "mappurl": "",
        "mC": "",
        "enterEvent": "",
        "keyBoard": {},
        "mappingArray": []
    };
    this.bg = 0;
    this.bu = "";
    if(options != undefined) {
        this.settings = options;
    }
};
passGuard.ep = [];
passGuard.gA = [];
passGuard.ib = undefined;
passGuard.prototype.generate = function(id, keyBoard, pgd, ct, passGuardObj) {
    passGuard.ib = undefined;//modified by landry 20180912 解决两个密码控件页面跳转this指向的问题
    //passGuardThis = this;//modified by landry 20180912 解决两个密码控件页面跳转this指向的问题
    //passGuardThis.bu.length = 0;
    PH.arrPlace.push($("#" + id).attr("placeholder"));
    this.settings.keyBoard = keyBoard;
    this.settings.id = id;
    PH.Id = id;//modified by wsj 20180926 解决两个密码控件id未覆盖问题
    PH.arrId.push(id);
    PH.arrPGD.push(pgd);
    var bA = document.getElementById(id);
    passGuard.gA.push(bA);
    var aQ = this;
    var oJ = null;
    var hu = false;
    var flag2 = false;
    PH.s_n = function() {
        var fq;
        PH.pm = PH.arrId.indexOf(PH.Id);
        fq = eval("(" + PH.arrPGD[PH.pm] + ")");
        if(fq.bg == 0) {
            $("#" + PH.Id).attr('placeholder', PH.arrPlace[PH.pm]);
        }
        if(PH.cs == 0) {
            if(fq.settings.blur != undefined) {
                fq.settings.blur()
            };
            if(fq.settings.callBack == "") {} else {
                var pz = new RegExp(fq.settings.regExp2);
                if(fq.asm == undefined) {
                    fq.settings.errorCallBack();
                } else if(pz.test((fq.asm + fq.bsm + fq.csm + fq.dsm + fq.esm + fq.fsm + fq.gsm + fq.hsm + fq.ism + fq.jsm + fq.ksm + fq.lsm + fq.msm + fq.nsm + fq.osm + fq.psm + fq.qsm + fq.rsm + fq.ssm + fq.tsm + fq.usm + fq.vsm + fq.wsm + fq.xsm + fq.ysm + fq.zsm + fq.aasm + fq.bbsm + fq.ccsm + fq.ddsm).substring(0, fq.bg))) {
                    fq.settings.callBack()
                } else {
                    fq.settings.errorCallBack()
                }
            }
        }
    };
    bA.onclick = function(e) {
        var fq;
        PH.pm = PH.arrId.indexOf(id);
        fq = eval("(" + PH.arrPGD[PH.pm] + ")");
        if(fq.settings.focus != undefined) {
            fq.settings.focus()
        };
        this.blur();
        if(Show == 0 || id != PH.Id) {
            for(var bG = 0; bG < PH.L.length; bG++) {
                window.clearInterval(PH.L[bG]);
            };
            for(var i = 0; i < T.length; i++) {
                window.clearInterval(T[i]);
            };
            $(this).attr('placeholder', '');
            PH.add = 1;
            BL.olo = 1;
            press2 = 1;
            BL.T = 1;
            clearTimeout(Hid);
            if(ct == 1) {
                $("#char_keyboard").hide();
                $("#number_keyboard").hide();
                $("#symble_keyboard").hide();
                $("#purenumber_keyboard").show();
                BL.KK = aE;
            } else {
                $("#char_keyboard").show();
                $("#number_keyboard").hide();
                $("#symble_keyboard").hide();
                $("#purenumber_keyboard").hide();
                BL.KK = aE;
            };
            var input = this;
            this.bu = "";
            for(var bG = 0; bG < passGuard.ep.length; bG++) {
                window.clearInterval(passGuard.ep[bG]);
            }
            for(var fT = 0; fT < passGuard.gA.length; fT++) {
                passGuard.gA[fT].value = passGuard.gA[fT].value.replace("|", "");
            }
            passGuard.ep = [];
            if(passGuard.ib == undefined) {
                if(aQ.settings.mappurl == undefined || aQ.settings.mappurl == "") {
                    // Toast.info("无效的映射申请地址。", 2);
                    return;
                }

                $('#app').append('<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>');
                // Toast.loading("加载中", 0)

                var opts = {
                    url: aQ.settings.mappurl + "?" + get_time(),
                    type: "POST",
                    headers:{'X-SSO-Auth': '', 'X-KJT-Agent': ';;;;;;;mark'},
                    data: {
                        sessionId: sessionStorage.sessionId
                    },
                    success: function(bu) {
                        if(!(bu && bu.bizContent)){console.log("随机因子接口返回有误"); return;}
                        sessionStorage.mapArr = bu.bizContent.mapArr;

                        sessionStorage.mapArr = bu.bizContent.mapArr;
                        sessionStorage.randKey = bu.bizContent.randKey;
                        sessionStorage.rsaPublicKey = bu.bizContent.publicKey;
                        sessionStorage.sm2KeyHex = bu.bizContent.publicKey;
                        aQ.tI(bu.bizContent.mapArr);
                        passGuardObj.setRandKey(bu.bizContent.randKey);
                        $('.lds-spinner').remove();
                        // Toast.hide();

                        //modified by wsj 20180926 解决接口未请求完成，键盘弹出
                        oJ = setInterval(function() {
                            if(!document.getElementById(id)) {
                                return;
                            }
                            document.getElementById(id).setAttribute("placeholder", "");
                            if(hu) {
                                input.value = input.value.replace("|", "");
                                hu = false;
                            } else {
                                input.value = input.value + "|";
                                hu = true;
                            }
                        }, 500);
                        passGuard.ep.push(oJ);
                        setTimeout(function() {
                            keyBoard.cY = aQ.cY;
                            if(aQ.settings.fixed == undefined) {
                                BL.fixed = "";
                            } else {
                                BL.fixed = aQ.settings.fixed;
                            }
                            keyBoard.settings.aQ = aQ;
                            keyBoard.settings.odd = 51;
                            keyBoard.show(ct);
                            PH.Id = id;
                            if(PH.Id != "" && PH.Id != id) {
                                PH.s_n();
                                PH.cs = 0;
                            }
                            PH.cs = 0;

                            e.preventDefault();
                            return false;
                        }, 20)
                    },
                    error: function (data) {
                        // Toast.info("密码控件请求错误", 2);
                    }

                };
                $.ajax(opts);
            }else{
                //modified by wsj 20180926 解决接口未请求完成，键盘弹出
                oJ = setInterval(function() {
                    if(!document.getElementById(id)) {
                        return;
                    }
                    document.getElementById(id).setAttribute("placeholder", "");
                    if(hu) {
                        input.value = input.value.replace("|", "");
                        hu = false;
                    } else {
                        input.value = input.value + "|";
                        hu = true;
                    }
                }, 500);
                passGuard.ep.push(oJ);
                setTimeout(function() {
                    keyBoard.cY = aQ.cY;
                    if(aQ.settings.fixed == undefined) {
                        BL.fixed = "";
                    } else {
                        BL.fixed = aQ.settings.fixed;
                    }
                    keyBoard.settings.aQ = aQ;
                    keyBoard.settings.odd = 51;
                    keyBoard.show(ct);
                    PH.Id = id;
                    if(PH.Id != "" && PH.Id != id) {
                        PH.s_n();
                        PH.cs = 0;
                    }
                    PH.cs = 0;

                    e.preventDefault();
                    return false;
                }, 20)
            }
            // aQ.tI(sessionStorage.mapArr);

        }
    }
};
var SH;
passGuard.prototype.cY = function(event, key) {
    var pB = key ^ 51;
    //var aQ = passGuardThis; //modified by landry 20180912 解决两个密码控件页面跳转this指向的问题
    var aQ = this; //modified by landry 20180912 解决两个密码控件页面跳转this指向的问题
    var input = document.getElementById(aQ.settings.id);
    if(!input) return false;
    input.value = input.value.replace("|", "");
    if(event == 1) {
        BL.T = 0;
        for(var bG = 0; bG < passGuard.ep.length; bG++) {
            window.clearInterval(passGuard.ep[bG]);
        }
    } else if(event == 2) {
        input.value = input.value.substr(0, input.value.length - 1);
        aQ.bu = aQ.bu.substr(0, aQ.bu.length - 1);
        aQ.bg -= 1;
        if(aQ.bg < 0) aQ.bg = 0;
        if(aQ.bg == 0) aQ.asm = [""];
        if(aQ.bg == 1) aQ.bsm = [""];
        if(aQ.bg == 2) aQ.csm = [""];
        if(aQ.bg == 3) aQ.dsm = [""];
        if(aQ.bg == 4) aQ.esm = [""];
        if(aQ.bg == 5) aQ.fsm = [""];
        if(aQ.bg == 6) aQ.gsm = [""];
        if(aQ.bg == 7) aQ.hsm = [""];
        if(aQ.bg == 8) aQ.ism = [""];
        if(aQ.bg == 9) aQ.jsm = [""];
        if(aQ.bg == 10) aQ.ksm = [""];
        if(aQ.bg == 11) aQ.lsm = [""];
        if(aQ.bg == 12) aQ.msm = [""];
        if(aQ.bg == 13) aQ.nsm = [""];
        if(aQ.bg == 14) aQ.osm = [""];
        if(aQ.bg == 15) aQ.psm = [""];
        if(aQ.bg == 16) aQ.qsm = [""];
        if(aQ.bg == 17) aQ.rsm = [""];
        if(aQ.bg == 18) aQ.ssm = [""];
        if(aQ.bg == 19) aQ.tsm = [""];
        if(aQ.bg == 20) aQ.usm = [""];
        if(aQ.bg == 21) aQ.vsm = [""];
        if(aQ.bg == 22) aQ.wsm = [""];
        if(aQ.bg == 23) aQ.xsm = [""];
        if(aQ.bg == 24) aQ.ysm = [""];
        if(aQ.bg == 25) aQ.zsm = [""];
        if(aQ.bg == 26) aQ.aasm = [""];
        if(aQ.bg == 27) aQ.bbsm = [""];
        if(aQ.bg == 28) aQ.ccsm = [""];
        if(aQ.bg == 29) aQ.ddsm = [""];
        if(aQ.settings.jump == 1) {
            aQ.settings.del(aQ.bg);
        }
    } else if(event == 0) {
        var ld = new RegExp(aQ.settings.regExp1);
        var iC = String.fromCharCode(pB);
        var wD = aQ.settings.maxLength;
        var aG = input.value.length;
        if(aG < wD) {
            aQ.bg += 1;
            if(aQ.bg == 1) aQ.asm = [iC];
            if(aQ.bg == 2) aQ.bsm = [iC];
            if(aQ.bg == 3) aQ.csm = [iC];
            if(aQ.bg == 4) aQ.dsm = [iC];
            if(aQ.bg == 5) aQ.esm = [iC];
            if(aQ.bg == 6) aQ.fsm = [iC];
            if(aQ.bg == 7) aQ.gsm = [iC];
            if(aQ.bg == 8) aQ.hsm = [iC];
            if(aQ.bg == 9) aQ.ism = [iC];
            if(aQ.bg == 10) aQ.jsm = [iC];
            if(aQ.bg == 11) aQ.ksm = [iC];
            if(aQ.bg == 12) aQ.lsm = [iC];
            if(aQ.bg == 13) aQ.msm = [iC];
            if(aQ.bg == 14) aQ.nsm = [iC];
            if(aQ.bg == 15) aQ.osm = [iC];
            if(aQ.bg == 16) aQ.psm = [iC];
            if(aQ.bg == 17) aQ.qsm = [iC];
            if(aQ.bg == 18) aQ.rsm = [iC];
            if(aQ.bg == 19) aQ.ssm = [iC];
            if(aQ.bg == 20) aQ.tsm = [iC];
            if(aQ.bg == 21) aQ.usm = [iC];
            if(aQ.bg == 22) aQ.vsm = [iC];
            if(aQ.bg == 23) aQ.wsm = [iC];
            if(aQ.bg == 24) aQ.xsm = [iC];
            if(aQ.bg == 25) aQ.ysm = [iC];
            if(aQ.bg == 26) aQ.zsm = [iC];
            if(aQ.bg == 27) aQ.aasm = [iC];
            if(aQ.bg == 28) aQ.bbsm = [iC];
            if(aQ.bg == 29) aQ.ccsm = [iC];
            if(aQ.bg == 30) aQ.ddsm = [iC];
            aQ.bu += aQ.wQ(iC);
            input.value += "*";
            if(aQ.settings.jump == 1) {
                aQ.settings.add(aQ.bg);
            }
        }
    }
};
passGuard.prototype.tI = function(mO) {
    if(mO) {
        passGuard.ib = mO;
    }
};
passGuard.prototype.clearpwd = function() {
    var input = document.getElementById(this.settings.id);
    input.value = "";
    this.bg = 0;
    passGuard.ib = undefined;
    this.asm = "";
    this.bsm = "";
    this.csm = "";
    this.dsm = "";
    this.esm = "";
    this.fsm = "";
    this.gsm = "";
    this.hsm = "";
    this.ism = "";
    this.jsm = "";
    this.ksm = "";
    this.lsm = "";
    this.msm = "";
    this.nsm = "";
    this.osm = "";
    this.psm = "";
    this.qsm = "";
    this.rsm = "";
    this.ssm = "";
    this.tsm = "";
    this.usm = "";
    this.vsm = "";
    this.wsm = "";
    this.xsm = "";
    this.ysm = "";
    this.zsm = "";
    this.aasm = "";
    this.bbsm = "";
    this.ccsm = "";
    this.ddsm = "";
    this.bu = "";
};
passGuard.prototype.getValid = function() {
    var aQ = this;
    if(aQ.bg < 0) aQ.bg = 0;
    if(aQ.bg == 0) aQ.asm = [""];
    if(aQ.bg == 1) aQ.bsm = [""];
    if(aQ.bg <= 2) aQ.csm = [""];
    if(aQ.bg <= 3) aQ.dsm = [""];
    if(aQ.bg <= 4) aQ.esm = [""];
    if(aQ.bg <= 5) aQ.fsm = [""];
    if(aQ.bg <= 6) aQ.gsm = [""];
    if(aQ.bg <= 7) aQ.hsm = [""];
    if(aQ.bg <= 8) aQ.ism = [""];
    if(aQ.bg <= 9) aQ.jsm = [""];
    if(aQ.bg <= 10) aQ.ksm = [""];
    if(aQ.bg <= 11) aQ.lsm = [""];
    if(aQ.bg <= 12) aQ.msm = [""];
    if(aQ.bg <= 13) aQ.nsm = [""];
    if(aQ.bg <= 14) aQ.osm = [""];
    if(aQ.bg <= 15) aQ.psm = [""];
    if(aQ.bg <= 16) aQ.qsm = [""];
    if(aQ.bg <= 17) aQ.rsm = [""];
    if(aQ.bg <= 18) aQ.ssm = [""];
    if(aQ.bg <= 19) aQ.tsm = [""];
    if(aQ.bg <= 20) aQ.usm = [""];
    if(aQ.bg <= 21) aQ.vsm = [""];
    if(aQ.bg <= 22) aQ.wsm = [""];
    if(aQ.bg <= 23) aQ.xsm = [""];
    if(aQ.bg <= 24) aQ.ysm = [""];
    if(aQ.bg <= 25) aQ.zsm = [""];
    if(aQ.bg <= 26) aQ.aasm = [""];
    if(aQ.bg <= 27) aQ.bbsm = [""];
    if(aQ.bg <= 28) aQ.ccsm = [""];
    if(aQ.bg <= 29) aQ.ddsm = [""];
    var ld = new RegExp(aQ.settings.regExp2);
    var pare = ld.test(aQ.asm + aQ.bsm + aQ.csm + aQ.dsm + aQ.esm + aQ.fsm + aQ.gsm + aQ.hsm + aQ.ism + aQ.jsm + aQ.ksm + aQ.lsm + aQ.msm + aQ.nsm + aQ.osm + aQ.psm + aQ.qsm + aQ.rsm + aQ.ssm + aQ.tsm + aQ.usm + aQ.vsm + aQ.wsm + aQ.xsm + aQ.ysm + aQ.zsm + aQ.aasm + aQ.bbsm + aQ.ccsm + aQ.ddsm) ? 0 : 1;
    return pare;
};
passGuard.prototype.getOutput = function() {
    var kW = new db();
    kW.uO(sessionStorage.rsaPublicKey);
    var sn = kW.bZ(this.bu);
    var xS = this.qh($.trim(this.settings.mC), $.trim(sn));
    passGuard.ib = undefined;
    this.bg = 0;
    this.asm = [""];
    this.bsm = [""];
    this.csm = [""];
    this.dsm = [""];
    this.esm = [""];
    this.fsm = [""];
    this.gsm = [""];
    this.hsm = [""];
    this.ism = [""];
    this.jsm = [""];
    this.ksm = [""];
    this.lsm = [""];
    this.msm = [""];
    this.nsm = [""];
    this.osm = [""];
    this.psm = [""];
    this.qsm = [""];
    this.rsm = [""];
    this.ssm = [""];
    this.tsm = [""];
    this.usm = [""];
    this.vsm = [""];
    this.dsm = [""];
    this.xsm = [""];
    this.ysm = [""];
    this.zsm = [""];
    this.aasm = [""];
    this.bbsm = [""];
    this.ccsm = [""];
    this.ddsm = [""];
    this.bu = "";
    return xS;
};
passGuard.prototype.getOutputSM = function() {
    if(this.asm == undefined) this.asm = "";
    if(this.bsm == undefined) this.bsm = "";
    if(this.csm == undefined) this.csm = "";
    if(this.dsm == undefined) this.dsm = "";
    if(this.esm == undefined) this.esm = "";
    if(this.fsm == undefined) this.fsm = "";
    if(this.gsm == undefined) this.gsm = "";
    if(this.hsm == undefined) this.hsm = "";
    if(this.ism == undefined) this.ism = "";
    if(this.jsm == undefined) this.jsm = "";
    if(this.ksm == undefined) this.ksm = "";
    if(this.lsm == undefined) this.lsm = "";
    if(this.msm == undefined) this.msm = "";
    if(this.nsm == undefined) this.nsm = "";
    if(this.osm == undefined) this.osm = "";
    if(this.psm == undefined) this.psm = "";
    if(this.qsm == undefined) this.qsm = "";
    if(this.rsm == undefined) this.rsm = "";
    if(this.ssm == undefined) this.ssm = "";
    if(this.tsm == undefined) this.tsm = "";
    if(this.usm == undefined) this.usm = "";
    if(this.vsm == undefined) this.vsm = "";
    if(this.wsm == undefined) this.wsm = "";
    if(this.xsm == undefined) this.xsm = "";
    if(this.ysm == undefined) this.ysm = "";
    if(this.zsm == undefined) this.zsm = "";
    if(this.aasm == undefined) this.aasm = "";
    if(this.bbsm == undefined) this.bbsm = "";
    if(this.ccsm == undefined) this.ccsm = "";
    if(this.ddsm == undefined) this.ddsm = "";
    var vlen = this.bg;
    var retdata;
    /*if(vlen < 10) {
        var strr = "0" + vlen + this.asm + this.bsm + this.csm + this.dsm + this.esm + this.fsm + this.gsm + this.hsm + this.ism + this.jsm + this.ksm + this.lsm + this.msm + this.nsm + this.osm + this.psm + this.qsm + this.rsm + this.ssm + this.tsm + this.usm + this.vsm + this.wsm + this.xsm + this.ysm + this.zsm + this.aasm + this.bbsm + this.ccsm + this.ddsm;
        var strrl = strr.length;
        if(strr.length < 32) {
            for(var fT = 0; fT < 32 - strrl; fT++) {
                strr += "0";
            }
        }
        retdata = SM2_Encrypt(strr, this.settings.sm2KeyHex, '1', false);
    } else if(vlen >= 10) {
        var strr = vlen + this.asm + this.bsm + this.csm + this.dsm + this.esm + this.fsm + this.gsm + this.hsm + this.ism + this.jsm + this.ksm + this.lsm + this.msm + this.nsm + this.osm + this.psm + this.qsm + this.rsm + this.ssm + this.tsm + this.usm + this.vsm + this.wsm + this.xsm + this.ysm + this.zsm + this.aasm + this.bbsm + this.ccsm + this.ddsm;
        var strrl = strr.length;
        if(strr.length < 32) {
            for(var fT = 0; fT < 32 - strrl; fT++) {
                strr += "0";
            }
        }
        retdata = SM2_Encrypt(strr, this.settings.sm2KeyHex, '1', false);
    }*/
    retdata = SM2_Encrypt(this.bu,sessionStorage.sm2KeyHex, '1', false);
    retdata = retdata.toUpperCase();
	var sm4obj = sm4();
	var rks = sessionStorage.randKey;
	var kks = [];
	for(var i = 0;i<32;i++){
		kks.push(rks.charCodeAt(i));
	}
    var key = [],input = [];
	for(var i = 0,j = 16;i<16,j<32;i++,j++){
		key.push(kks[i]);
		input.push(kks[j]);
	}
    sm4obj.init(key);
    var cipher = sm4obj.cipher(input, true);
    var plian = sm4obj.cipher(cipher, false);
    var encryptdata = sm4obj.encryptstring(retdata, true);
    var base64str = sm4obj.base64encode(encryptdata);
    passGuard.ib = undefined;
    this.bg = 0;
    this.asm = [""];
    this.bsm = [""];
    this.csm = [""];
    this.dsm = [""];
    this.esm = [""];
    this.fsm = [""];
    this.gsm = [""];
    this.hsm = [""];
    this.ism = [""];
    this.jsm = [""];
    this.ksm = [""];
    this.lsm = [""];
    this.msm = [""];
    this.nsm = [""];
    this.osm = [""];
    this.psm = [""];
    this.qsm = [""];
    this.rsm = [""];
    this.ssm = [""];
    this.tsm = [""];
    this.usm = [""];
    this.vsm = [""];
    this.dsm = [""];
    this.xsm = [""];
    this.ysm = [""];
    this.zsm = [""];
    this.aasm = [""];
    this.bbsm = [""];
    this.ccsm = [""];
    this.ddsm = [""];
    this.bu = "";
    return base64str;
};
passGuard.prototype.getLength = function() {
    return this.bu.length;
};
passGuard.prototype.getHash = function() {
    return aX.AF(this.bu).toString();
};
passGuard.prototype.wQ = function(key) {
    var keyCode = key.charCodeAt(0);
    var bS = aB(passGuard.ib);
    var wl = eval(bS);
    return String.fromCharCode(wl[keyCode - 33]);
};
passGuard.prototype.setRandKey = function(lP) {
    try {
        if(lP) this.settings.mC = lP;
    } catch(e) {
        alert(e);
    }
};
passGuard.prototype.qh = function(ns, rd) {
    var yg = [0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x1A, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x3A, 0x3B, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x1A, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x3A, 0x3B];
    var gf = "";
    var mj = "";
    for(var i = 0; i < ns.length; i++) {
        mj = String.fromCharCode(ns[i].charCodeAt(0) ^ yg[i]);
        gf += mj;
    }
    gf = gf.substr(0, gf.length - 16);
    var wv = aX.bJ.hh.parse(gf);
    var wy = aX.iG.bZ(rd, wv, {
        mode: aX.mode.ed,
        padding: aX.bH.kB
    });
    return wy.toString();
};
(function(window, document, Math) {
    var nF = window.zs || window.yX || window.mozRequestAnimationFrame || window.yD || window.zz || function(callback) {
        window.setTimeout(callback, 1000 / 60);
    };
    var ap = (function() {
        var ar = {};
        var kz = document.createElement('div').style;
        var ko = (function() {
            var jv = ['t', 'webkitT', 'MozT', 'msT', 'OT'],
                transform, i = 0,
                l = jv.length;
            for(; i < l; i++) {
                transform = jv[i] + 'ransform';
                if(transform in kz) return jv[i].substr(0, jv[i].length - 1);
            }
            return false;
        })();

        function eC(style) {
            if(ko === false) return false;
            if(ko === '') return style;
            return ko + style.charAt(0).toUpperCase() + style.substr(1);
        };
        ar.getTime = Date.now || function getTime() {
            return new Date().getTime();
        };
        ar.extend = function(target, bA) {
            for(var i in bA) {
                target[i] = bA[i];
            }
        };
        ar.cZ = function(by, type, bX, kD) {
            by.addEventListener(type, bX, !!kD);
        };
        ar.bo = function(by, type, bX, kD) {
            by.removeEventListener(type, bX, !!kD);
        };
        ar.cI = function(kP) {
            return window.xD ? 'MSPointer' + kP.charAt(9).toUpperCase() + kP.substr(10) : kP;
        };
        ar.hK = function(current, start, time, lb, iE, fa) {
            var distance = current - start,
                eg = Math.abs(distance) / time,
                destination, duration;
            fa = fa === undefined ? 0.0006 : fa;
            destination = current + (eg * eg) / (2 * fa) * (distance < 0 ? -1 : 1);
            duration = eg / fa;
            if(destination < lb) {
                destination = iE ? lb - (iE / 2.5 * (eg / 8)) : lb;
                distance = Math.abs(destination - current);
                duration = distance / eg;
            } else if(destination > 0) {
                destination = iE ? iE / 2.5 * (eg / 8) : 0;
                distance = Math.abs(current) + destination;
                duration = distance / eg;
            }
            return {
                destination: Math.round(destination),
                duration: duration
            };
        };
        var lD = eC('transform');
        ar.extend(ar, {
            wa: lD !== false,
            vH: eC('perspective') in kz,
            xd: 'ontouchstart' in window,
            vM: window.zg || window.xD,
            vY: eC('transition') in kz
        });
        ar.gs = /Android /.test(window.navigator.appVersion) && !(/Chrome\/\d/.test(window.navigator.appVersion));
        ar.extend(ar.style = {}, {
            transform: lD,
            gv: eC('gv'),
            fo: eC('fo'),
            qV: eC('qV'),
            sv: eC('sv')
        });
        ar.hasClass = function(e, c) {
            var eT = new RegExp("(^|\\s)" + c + "(\\s|$)");
            return eT.test(e.className);
        };
        ar.addClass = function(e, c) {
            if(ar.hasClass(e, c)) {
                return;
            }
            var mW = e.className.split(' ');
            mW.push(c);
            e.className = mW.join(' ');
        };
        ar.removeClass = function(e, c) {
            if(!ar.hasClass(e, c)) {
                return;
            }
            var eT = new RegExp("(^|\\s)" + c + "(\\s|$)", 'g');
            e.className = e.className.replace(eT, ' ');
        };
        ar.offset = function(by) {
            var left = -by.offsetLeft,
                top = -by.offsetTop;
            while(by = by.offsetParent) {
                left -= by.offsetLeft;
                top -= by.offsetTop;
            }
            return {
                left: left,
                top: top
            };
        };
        ar.gp = function(by, oI) {
            for(var i in oI) {
                if(oI[i].test(by[i])) {
                    return true;
                }
            }
            return false;
        };
        ar.extend(ar.aP = {}, {
            touchstart: 1,
            touchmove: 1,
            touchend: 1,
            mousedown: 2,
            mousemove: 2,
            mouseup: 2,
            gQ: 3,
            fu: 3,
            hb: 3,
            nn: 3,
            mV: 3,
            mZ: 3
        });
        ar.extend(ar.jF = {}, {
            pi: {
                style: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
                bX: function(k) {
                    return k * (2 - k);
                }
            },
            ln: {
                style: 'cubic-bezier(0.1, 0.57, 0.1, 1)',
                bX: function(k) {
                    return Math.sqrt(1 - (--k * k));
                }
            },
            back: {
                style: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)',
                bX: function(k) {
                    var b = 4;
                    return(k = k - 1) * k * ((b + 1) * k + b) + 1;
                }
            },
            bounce: {
                style: '',
                bX: function(k) {
                    if((k /= 1) < (1 / 2.75)) {
                        return 7.5625 * k * k;
                    } else if(k < (2 / 2.75)) {
                        return 7.5625 * (k -= (1.5 / 2.75)) * k + 0.75;
                    } else if(k < (2.5 / 2.75)) {
                        return 7.5625 * (k -= (2.25 / 2.75)) * k + 0.9375;
                    } else {
                        return 7.5625 * (k -= (2.625 / 2.75)) * k + 0.984375;
                    }
                }
            },
            As: {
                style: '',
                bX: function(k) {
                    var f = 0.22,
                        e = 0.4;
                    if(k === 0) {
                        return 0;
                    }
                    if(k == 1) {
                        return 1;
                    }
                    return(e * Math.pow(2, -10 * k) * Math.sin((k - f / 4) * (2 * Math.PI) / f) + 1);
                }
            }
        });
        ar.eJ = function(e, vs) {
            var cv = document.createEvent('Event');
            cv.initEvent(vs, true, true);
            cv.pageX = e.pageX;
            cv.pageY = e.pageY;
            e.target.dispatchEvent(cv);
        };
        ar.click = function(e) {
            var target = e.target,
                cv;
            if(!(/(SELECT|INPUT|TEXTAREA)/i).test(target.tagName)) {
                cv = document.createEvent('MouseEvents');
                cv.initMouseEvent('click', true, true, e.view, 1, target.screenX, target.screenY, target.clientX, target.clientY, e.ctrlKey, e.altKey, e.shiftKey, e.metaKey, 0, null);
                cv.ob = true;
                target.dispatchEvent(cv);
            }
        };
        return ar;
    })();

    function gX(by, options) {
        this.bI = typeof by == 'string' ? document.querySelector(by) : by;
        this.af = this.bI.children[0];
        this.hq = this.af.style;
        this.options = {
            ku: true,
            fz: 20,
            gu: 0.334,
            cT: 0,
            cj: 0,
            scrollY: true,
            ic: 5,
            hK: true,
            bounce: true,
            kE: 600,
            fI: '',
            preventDefault: true,
            gp: {
                tagName: /^(INPUT|TEXTAREA|BUTTON|SELECT)$/
            },
            wM: true,
            eo: true,
            gS: true
        };
        for(var i in options) {
            this.options[i] = options[i];
        }
        this.ig = this.options.wM && ap.vH ? ' ig(0)' : '';
        this.options.eo = ap.vY && this.options.eo;
        this.options.gS = ap.wa && this.options.gS;
        this.options.df = this.options.df === true ? 'vertical' : this.options.df;
        this.options.preventDefault = !this.options.df && this.options.preventDefault;
        this.options.scrollY = this.options.df == 'vertical' ? false : this.options.scrollY;
        this.options.scrollX = this.options.df == 'horizontal' ? false : this.options.scrollX;
        this.options.nJ = this.options.nJ && !this.options.df;
        this.options.ic = this.options.df ? 0 : this.options.ic;
        this.options.fI = typeof this.options.fI == 'string' ? ap.jF[this.options.fI] || ap.jF.ln : this.options.fI;
        this.options.lz = this.options.lz === undefined ? 60 : this.options.lz;
        if(this.options.eJ === true) {
            this.options.eJ = 'eJ';
        }
        if(this.options.nv == 'scale') {
            this.options.eo = false;
        }
        this.options.kA = this.options.kA ? -1 : 1;
        this.x = 0;
        this.y = 0;
        this.eu = 0;
        this.ft = 0;
        this.dH = {};
        this.qL();
        this.refresh();
        this.scrollTo(this.options.cT, this.options.cj);
        this.enable();
    };
    gX.prototype = {
        version: '5.1.3',
        qL: function() {
            this.mX();
            if(this.options.scrollbars || this.options.bU) {
                this.qa();
            }
            if(this.options.mouseWheel) {
                this.qw();
            }
            if(this.options.snap) {
                this.qe();
            }
            if(this.options.ab) {
                this.pK();
            }
        },
        destroy: function() {
            this.mX(true);
            this.aS('destroy');
        },
        yw: function(e) {
            if(e.target != this.af || !this.dZ) {
                return;
            }
            this.kH();
            if(!this.im(this.options.kE)) {
                this.dZ = false;
                this.aS('scrollEnd');
            }
        },
        kk: function(e) {
            if(ap.aP[e.type] != 1) {
                if(e.button !== 0) {
                    return;
                }
            }
            if(!this.enabled || (this.de && ap.aP[e.type] !== this.de)) {
                return;
            }
            if(this.options.preventDefault && !ap.gs && !ap.gp(e.target, this.options.gp)) {
                e.preventDefault();
            }
            var an = e.fH ? e.fH[0] : e,
                pos;
            this.de = ap.aP[e.type];
            this.fM = false;
            this.oU = 0;
            this.oy = 0;
            this.eu = 0;
            this.ft = 0;
            this.hc = 0;
            this.kH();
            this.startTime = ap.getTime();
            if(this.options.eo && this.dZ) {
                this.dZ = false;
                pos = this.om();
                this.hk(Math.round(pos.x), Math.round(pos.y));
                this.aS('scrollEnd');
            } else if(!this.options.eo && this.gN) {
                this.gN = false;
                this.aS('scrollEnd');
            }
            this.cT = this.x;
            this.cj = this.y;
            this.vO = this.x;
            this.wp = this.y;
            this.nh = an.pageX;
            this.np = an.pageY;
            this.aS('beforeScrollStart');
        },
        lg: function(e) {
            if(!this.enabled || ap.aP[e.type] !== this.de) {
                return;
            }
            if(this.options.preventDefault) {
                e.preventDefault();
            }
            var an = e.fH ? e.fH[0] : e,
                deltaX = an.pageX - this.nh,
                deltaY = an.pageY - this.np,
                iV = ap.getTime(),
                bx, bM, iz, iy;
            this.nh = an.pageX;
            this.np = an.pageY;
            this.oU += deltaX;
            this.oy += deltaY;
            iz = Math.abs(this.oU);
            iy = Math.abs(this.oy);
            if(iV - this.oC > 300 && (iz < 10 && iy < 10)) {
                return;
            }
            if(!this.hc && !this.options.nJ) {
                if(iz > iy + this.options.ic) {
                    this.hc = 'h';
                } else if(iy >= iz + this.options.ic) {
                    this.hc = 'v';
                } else {
                    this.hc = 'n';
                }
            }
            if(this.hc == 'h') {
                if(this.options.df == 'vertical') {
                    e.preventDefault();
                } else if(this.options.df == 'horizontal') {
                    this.de = false;
                    return;
                }
                deltaY = 0;
            } else if(this.hc == 'v') {
                if(this.options.df == 'horizontal') {
                    e.preventDefault();
                } else if(this.options.df == 'vertical') {
                    this.de = false;
                    return;
                }
                deltaX = 0;
            }
            deltaX = this.dF ? deltaX : 0;
            deltaY = this.di ? deltaY : 0;
            bx = this.x + deltaX;
            bM = this.y + deltaY;
            if(bx > 0 || bx < this.be) {
                bx = this.options.bounce ? this.x + deltaX / 3 : bx > 0 ? 0 : this.be;
            }
            if(bM > 0 || bM < this.ak) {
                bM = this.options.bounce ? this.y + deltaY / 3 : bM > 0 ? 0 : this.ak;
            }
            this.eu = deltaX > 0 ? -1 : deltaX < 0 ? 1 : 0;
            this.ft = deltaY > 0 ? -1 : deltaY < 0 ? 1 : 0;
            if(!this.fM) {
                this.aS('scrollStart');
            }
            this.fM = true;
            this.hk(bx, bM);
            if(iV - this.startTime > 300) {
                this.startTime = iV;
                this.cT = this.x;
                this.cj = this.y;
            }
        },
        ky: function(e) {
            if(!this.enabled || ap.aP[e.type] !== this.de) {
                return;
            }
            if(this.options.preventDefault && !ap.gp(e.target, this.options.gp)) {
                e.preventDefault();
            }
            var an = e.changedTouches ? e.changedTouches[0] : e,
                lc, jN, duration = ap.getTime() - this.startTime,
                bx = Math.round(this.x),
                bM = Math.round(this.y),
                vr = Math.abs(bx - this.cT),
                wd = Math.abs(bM - this.cj),
                time = 0,
                easing = '';
            this.dZ = 0;
            this.de = 0;
            this.oC = ap.getTime();
            if(this.im(this.options.kE)) {
                return;
            }
            this.scrollTo(bx, bM);
            if(!this.fM) {
                if(this.options.eJ) {
                    ap.eJ(e, this.options.eJ);
                }
                if(this.options.click) {
                    ap.click(e);
                }
                this.aS('scrollCancel');
                return;
            }
            if(this.dH.nY && duration < 200 && vr < 100 && wd < 100) {
                this.aS('nY');
                return;
            }
            if(this.options.hK && duration < 300) {
                lc = this.dF ? ap.hK(this.x, this.cT, duration, this.be, this.options.bounce ? this.dB : 0, this.options.fa) : {
                    destination: bx,
                    duration: 0
                };
                jN = this.di ? ap.hK(this.y, this.cj, duration, this.ak, this.options.bounce ? this.dp : 0, this.options.fa) : {
                    destination: bM,
                    duration: 0
                };
                bx = lc.destination;
                bM = jN.destination;
                time = Math.max(lc.duration, jN.duration);
                this.dZ = 1;
            }
            if(this.options.snap) {
                var snap = this.mq(bx, bM);
                this.bF = snap;
                time = this.options.ia || Math.max(Math.max(Math.min(Math.abs(bx - snap.x), 1000), Math.min(Math.abs(bM - snap.y), 1000)), 300);
                bx = snap.x;
                bM = snap.y;
                this.eu = 0;
                this.ft = 0;
                easing = this.options.fI;
            }
            if(bx != this.x || bM != this.y) {
                if(bx > 0 || bx < this.be || bM > 0 || bM < this.ak) {
                    easing = ap.jF.pi;
                }
                this.scrollTo(bx, bM, time, easing);
                return;
            }
            this.aS('scrollEnd');
        },
        pP: function() {
            var bQ = this;
            clearTimeout(this.rw);
            this.rw = setTimeout(function() {
                bQ.refresh();
            }, this.options.lz);
        },
        im: function(time) {
            var x = this.x,
                y = this.y;
            time = time || 0;
            if(!this.dF || this.x > 0) {
                x = 0;
            } else if(this.x < this.be) {
                x = this.be;
            }
            if(!this.di || this.y > 0) {
                y = 0;
            } else if(this.y < this.ak) {
                y = this.ak;
            }
            if(x == this.x && y == this.y) {
                return false;
            }
            this.scrollTo(x, y, time, this.options.fI);
            return true;
        },
        disable: function() {
            this.enabled = false;
        },
        enable: function() {
            this.enabled = true;
        },
        refresh: function() {
            var zG = this.bI.offsetHeight;
            this.dB = this.bI.clientWidth;
            this.dp = this.bI.clientHeight;
            this.hm = this.af.offsetWidth;
            this.gj = this.af.offsetHeight;
            this.be = this.dB - this.hm;
            this.ak = this.dp - this.gj;
            this.dF = this.options.scrollX && this.be < 0;
            this.di = this.options.scrollY && this.ak < 0;
            if(!this.dF) {
                this.be = 0;
                this.hm = this.dB;
            }
            if(!this.di) {
                this.ak = 0;
                this.gj = this.dp;
            }
            this.oC = 0;
            this.eu = 0;
            this.ft = 0;
            this.og = ap.offset(this.bI);
            this.aS('refresh');
            this.im();
        },
        on: function(type, bX) {
            if(!this.dH[type]) {
                this.dH[type] = [];
            }
            this.dH[type].push(bX);
        },
        off: function(type, bX) {
            if(!this.dH[type]) {
                return;
            }
            var index = this.dH[type].indexOf(bX);
            if(index > -1) {
                this.dH[type].splice(index, 1);
            }
        },
        aS: function(type) {
            if(!this.dH[type]) {
                return;
            }
            var i = 0,
                l = this.dH[type].length;
            if(!l) {
                return;
            }
            for(; i < l; i++) {
                this.dH[type][i].apply(this, [].slice.call(arguments, 1));
            }
        },
        scrollBy: function(x, y, time, easing) {
            x = this.x + x;
            y = this.y + y;
            time = time || 0;
            this.scrollTo(x, y, time, easing);
        },
        scrollTo: function(x, y, time, easing) {
            easing = easing || ap.jF.ln;
            this.dZ = this.options.eo && time > 0;
            if(!time || (this.options.eo && easing.style)) {
                this.xP(easing.style);
                this.kH(time);
                this.hk(x, y);
            } else {
                this.oc(x, y, time, easing.bX);
            }
        },
        zx: function(by, time, offsetX, offsetY, easing) {
            by = by.nodeType ? by : this.af.querySelector(by);
            if(!by) {
                return;
            }
            var pos = ap.offset(by);
            pos.left -= this.og.left;
            pos.top -= this.og.top;
            if(offsetX === true) {
                offsetX = Math.round(by.offsetWidth / 2 - this.bI.offsetWidth / 2);
            }
            if(offsetY === true) {
                offsetY = Math.round(by.offsetHeight / 2 - this.bI.offsetHeight / 2);
            }
            pos.left -= offsetX || 0;
            pos.top -= offsetY || 0;
            pos.left = pos.left > 0 ? 0 : pos.left < this.be ? this.be : pos.left;
            pos.top = pos.top > 0 ? 0 : pos.top < this.ak ? this.ak : pos.top;
            time = time === undefined || time === null || time === 'auto' ? Math.max(Math.abs(this.x - pos.left), Math.abs(this.y - pos.top)) : time;
            this.scrollTo(pos.left, pos.top, time, easing);
        },
        kH: function(time) {
            time = time || 0;
            this.hq[ap.style.fo] = time + 'ms';
            if(!time && ap.gs) {
                this.hq[ap.style.fo] = '0.001s';
            }
            if(this.bU) {
                for(var i = this.bU.length; i--;) {
                    this.bU[i].io(time);
                }
            }
        },
        xP: function(easing) {
            this.hq[ap.style.gv] = easing;
            if(this.bU) {
                for(var i = this.bU.length; i--;) {
                    this.bU[i].gv(easing);
                }
            }
        },
        hk: function(x, y) {
            if(this.options.gS) {
                this.hq[ap.style.transform] = 'translate(' + x + 'px,' + y + 'px)' + this.ig;
            } else {
                x = Math.round(x);
                y = Math.round(y);
                this.hq.left = x + 'px';
                this.hq.top = y + 'px';
            }
            this.x = x;
            this.y = y;
            if(this.bU) {
                for(var i = this.bU.length; i--;) {
                    this.bU[i].nz();
                }
            }
        },
        mX: function(remove) {
            var aP = remove ? ap.bo : ap.cZ,
                target = this.options.zw ? this.bI : window;
            aP(window, 'orientationchange', this);
            aP(window, 'resize', this);
            if(this.options.click) {
                aP(this.bI, 'click', this, true);
            }
            if(!this.options.ps) {
                aP(this.bI, 'mousedown', this);
                aP(target, 'mousemove', this);
                aP(target, 'mousecancel', this);
                aP(target, 'mouseup', this);
            }
            if(ap.vM && !this.options.oD) {
                aP(this.bI, ap.cI('gQ'), this);
                aP(target, ap.cI('fu'), this);
                aP(target, ap.cI('pointercancel'), this);
                aP(target, ap.cI('hb'), this);
            }
            if(ap.xd && !this.options.oj) {
                aP(this.bI, 'touchstart', this);
                aP(target, 'touchmove', this);
                aP(target, 'touchcancel', this);
                aP(target, 'touchend', this);
            }
            aP(this.af, 'transitionend', this);
            aP(this.af, 'webkitTransitionEnd', this);
            aP(this.af, 'oTransitionEnd', this);
            aP(this.af, 'MSTransitionEnd', this);
        },
        om: function() {
            var matrix = window.getComputedStyle(this.af, null),
                x, y;
            if(this.options.gS) {
                matrix = matrix[ap.style.transform].split(')')[0].split(', ');
                x = +(matrix[12] || matrix[4]);
                y = +(matrix[13] || matrix[5]);
            } else {
                x = +matrix.left.replace(/[^-\d.]/g, '');
                y = +matrix.top.replace(/[^-\d.]/g, '');
            }
            return {
                x: x,
                y: y
            };
        },
        qa: function() {
            var interactive = this.options.zk,
                gP = typeof this.options.scrollbars != 'string',
                bU = [],
                bp;
            var bQ = this;
            this.bU = [];
            if(this.options.scrollbars) {
                if(this.options.scrollY) {
                    bp = {
                        by: oe('v', interactive, this.options.scrollbars),
                        interactive: interactive,
                        hd: true,
                        gP: gP,
                        resize: this.options.ku,
                        dg: this.options.nv,
                        fade: this.options.oH,
                        fE: false
                    };
                    this.bI.appendChild(bp.by);
                    bU.push(bp);
                }
                if(this.options.scrollX) {
                    bp = {
                        by: oe('h', interactive, this.options.scrollbars),
                        interactive: interactive,
                        hd: true,
                        gP: gP,
                        resize: this.options.ku,
                        dg: this.options.nv,
                        fade: this.options.oH,
                        fS: false
                    };
                    this.bI.appendChild(bp.by);
                    bU.push(bp);
                }
            }
            if(this.options.bU) {
                bU = bU.concat(this.options.bU);
            }
            for(var i = bU.length; i--;) {
                this.bU.push(new oY(this, bU[i]));
            }

            function dU(bX) {
                for(var i = bQ.bU.length; i--;) {
                    bX.call(bQ.bU[i]);
                }
            };
            if(this.options.oH) {
                this.on('scrollEnd', function() {
                    dU(function() {
                        this.fade();
                    });
                });
                this.on('scrollCancel', function() {
                    dU(function() {
                        this.fade();
                    });
                });
                this.on('scrollStart', function() {
                    dU(function() {
                        this.fade(1);
                    });
                });
                this.on('beforeScrollStart', function() {
                    dU(function() {
                        this.fade(1, true);
                    });
                });
            }
            this.on('refresh', function() {
                dU(function() {
                    this.refresh();
                });
            });
            this.on('destroy', function() {
                dU(function() {
                    this.destroy();
                });
                delete this.bU;
            });
        },
        qw: function() {
            ap.cZ(this.bI, 'wheel', this);
            ap.cZ(this.bI, 'mousewheel', this);
            ap.cZ(this.bI, 'DOMMouseScroll', this);
            this.on('destroy', function() {
                ap.bo(this.bI, 'wheel', this);
                ap.bo(this.bI, 'mousewheel', this);
                ap.bo(this.bI, 'DOMMouseScroll', this);
            });
        },
        yP: function(e) {
            if(!this.enabled) {
                return;
            }
            e.preventDefault();
            e.stopPropagation();
            var dw, dJ, bx, bM, bQ = this;
            if(this.jA === undefined) {
                bQ.aS('scrollStart');
            }
            clearTimeout(this.jA);
            this.jA = setTimeout(function() {
                bQ.aS('scrollEnd');
                bQ.jA = undefined;
            }, 400);
            if('deltaX' in e) {
                if(e.deltaMode === 1) {
                    dw = -e.deltaX * this.options.fz;
                    dJ = -e.deltaY * this.options.fz;
                } else {
                    dw = -e.deltaX;
                    dJ = -e.deltaY;
                }
            } else if('dw' in e) {
                dw = e.dw / 120 * this.options.fz;
                dJ = e.dJ / 120 * this.options.fz;
            } else if('wheelDelta' in e) {
                dw = dJ = e.wheelDelta / 120 * this.options.fz;
            } else if('detail' in e) {
                dw = dJ = -e.detail / 3 * this.options.fz;
            } else {
                return;
            }
            dw *= this.options.kA;
            dJ *= this.options.kA;
            if(!this.di) {
                dw = dJ;
                dJ = 0;
            }
            if(this.options.snap) {
                bx = this.bF.pageX;
                bM = this.bF.pageY;
                if(dw > 0) {
                    bx--;
                } else if(dw < 0) {
                    bx++;
                }
                if(dJ > 0) {
                    bM--;
                } else if(dJ < 0) {
                    bM++;
                }
                this.hi(bx, bM);
                return;
            }
            bx = this.x + Math.round(this.dF ? dw : 0);
            bM = this.y + Math.round(this.di ? dJ : 0);
            if(bx > 0) {
                bx = 0;
            } else if(bx < this.be) {
                bx = this.be;
            }
            if(bM > 0) {
                bM = 0;
            } else if(bM < this.ak) {
                bM = this.ak;
            }
            this.scrollTo(bx, bM, 0);
        },
        qe: function() {
            this.bF = {};
            if(typeof this.options.snap == 'string') {
                this.options.snap = this.af.querySelectorAll(this.options.snap);
            }
            this.on('refresh', function() {
                var i = 0,
                    l, m = 0,
                    n, cx, cy, x = 0,
                    y, ir = this.options.Aw || this.dB,
                    iP = this.options.zy || this.dp,
                    by;
                this.pages = [];
                if(!this.dB || !this.dp || !this.hm || !this.gj) {
                    return;
                }
                if(this.options.snap === true) {
                    cx = Math.round(ir / 2);
                    cy = Math.round(iP / 2);
                    while(x > -this.hm) {
                        this.pages[i] = [];
                        l = 0;
                        y = 0;
                        while(y > -this.gj) {
                            this.pages[i][l] = {
                                x: Math.max(x, this.be),
                                y: Math.max(y, this.ak),
                                width: ir,
                                height: iP,
                                cx: x - cx,
                                cy: y - cy
                            };
                            y -= iP;
                            l++;
                        }
                        x -= ir;
                        i++;
                    }
                } else {
                    by = this.options.snap;
                    l = by.length;
                    n = -1;
                    for(; i < l; i++) {
                        if(i === 0 || by[i].offsetLeft <= by[i - 1].offsetLeft) {
                            m = 0;
                            n++;
                        }
                        if(!this.pages[m]) {
                            this.pages[m] = [];
                        }
                        x = Math.max(-by[i].offsetLeft, this.be);
                        y = Math.max(-by[i].offsetTop, this.ak);
                        cx = x - Math.round(by[i].offsetWidth / 2);
                        cy = y - Math.round(by[i].offsetHeight / 2);
                        this.pages[m][n] = {
                            x: x,
                            y: y,
                            width: by[i].offsetWidth,
                            height: by[i].offsetHeight,
                            cx: cx,
                            cy: cy
                        };
                        if(x > this.be) {
                            m++;
                        }
                    }
                }
                this.hi(this.bF.pageX || 0, this.bF.pageY || 0, 0);
                if(this.options.gu % 1 === 0) {
                    this.mx = this.options.gu;
                    this.lW = this.options.gu;
                } else {
                    this.mx = Math.round(this.pages[this.bF.pageX][this.bF.pageY].width * this.options.gu);
                    this.lW = Math.round(this.pages[this.bF.pageX][this.bF.pageY].height * this.options.gu);
                }
            });
            this.on('nY', function() {
                var time = this.options.ia || Math.max(Math.max(Math.min(Math.abs(this.x - this.cT), 1000), Math.min(Math.abs(this.y - this.cj), 1000)), 300);
                this.hi(this.bF.pageX + this.eu, this.bF.pageY + this.ft, time);
            });
        },
        mq: function(x, y) {
            if(!this.pages.length) {
                return {
                    x: 0,
                    y: 0,
                    pageX: 0,
                    pageY: 0
                };
            }
            var i = 0,
                l = this.pages.length,
                m = 0;
            if(Math.abs(x - this.vO) < this.mx && Math.abs(y - this.wp) < this.lW) {
                return this.bF;
            }
            if(x > 0) {
                x = 0;
            } else if(x < this.be) {
                x = this.be;
            }
            if(y > 0) {
                y = 0;
            } else if(y < this.ak) {
                y = this.ak;
            }
            for(; i < l; i++) {
                if(x >= this.pages[i][0].cx) {
                    x = this.pages[i][0].x;
                    break;
                }
            }
            l = this.pages[i].length;
            for(; m < l; m++) {
                if(y >= this.pages[0][m].cy) {
                    y = this.pages[0][m].y;
                    break;
                }
            }
            if(i == this.bF.pageX) {
                i += this.eu;
                if(i < 0) {
                    i = 0;
                } else if(i >= this.pages.length) {
                    i = this.pages.length - 1;
                }
                x = this.pages[i][0].x;
            }
            if(m == this.bF.pageY) {
                m += this.ft;
                if(m < 0) {
                    m = 0;
                } else if(m >= this.pages[0].length) {
                    m = this.pages[0].length - 1;
                }
                y = this.pages[0][m].y;
            }
            return {
                x: x,
                y: y,
                pageX: i,
                pageY: m
            };
        },
        hi: function(x, y, time, easing) {
            easing = easing || this.options.fI;
            if(x >= this.pages.length) {
                x = this.pages.length - 1;
            } else if(x < 0) {
                x = 0;
            }
            if(y >= this.pages[x].length) {
                y = this.pages[x].length - 1;
            } else if(y < 0) {
                y = 0;
            }
            var jR = this.pages[x][y].x,
                kN = this.pages[x][y].y;
            time = time === undefined ? this.options.ia || Math.max(Math.max(Math.min(Math.abs(jR - this.x), 1000), Math.min(Math.abs(kN - this.y), 1000)), 300) : time;
            this.bF = {
                x: jR,
                y: kN,
                pageX: x,
                pageY: y
            };
            this.scrollTo(jR, kN, time, easing);
        },
        next: function(time, easing) {
            var x = this.bF.pageX,
                y = this.bF.pageY;
            x++;
            if(x >= this.pages.length && this.di) {
                x = 0;
                y++;
            }
            this.hi(x, y, time, easing);
        },
        prev: function(time, easing) {
            var x = this.bF.pageX,
                y = this.bF.pageY;
            x--;
            if(x < 0 && this.di) {
                x = 0;
                y--;
            }
            this.hi(x, y, time, easing);
        },
        pK: function(e) {
            var keys = {
                pageUp: 33,
                pageDown: 34,
                end: 35,
                home: 36,
                left: 37,
                up: 38,
                right: 39,
                down: 40
            };
            var i;
            if(typeof this.options.ab == 'object') {
                for(i in this.options.ab) {
                    if(typeof this.options.ab[i] == 'string') {
                        this.options.ab[i] = this.options.ab[i].toUpperCase().charCodeAt(0);
                    }
                }
            } else {
                this.options.ab = {};
            }
            for(i in keys) {
                this.options.ab[i] = this.options.ab[i] || keys[i];
            }
            ap.cZ(window, 'keydown', this);
            this.on('destroy', function() {
                ap.bo(window, 'keydown', this);
            });
        },
        hJ: function(e) {
            if(!this.enabled) {
                return;
            }
            var snap = this.options.snap,
                bx = snap ? this.bF.pageX : this.x,
                bM = snap ? this.bF.pageY : this.y,
                now = ap.getTime(),
                qJ = this.yo || 0,
                yT = 0.250,
                pos;
            if(this.options.eo && this.dZ) {
                pos = this.om();
                this.hk(Math.round(pos.x), Math.round(pos.y));
                this.dZ = false;
            }
            this.dP = now - qJ < 200 ? Math.min(this.dP + yT, 50) : 0;
            switch(e.keyCode) {
                case this.options.ab.pageUp:
                    if(this.dF && !this.di) {
                        bx += snap ? 1 : this.dB;
                    } else {
                        bM += snap ? 1 : this.dp;
                    }
                    break;
                case this.options.ab.pageDown:
                    if(this.dF && !this.di) {
                        bx -= snap ? 1 : this.dB;
                    } else {
                        bM -= snap ? 1 : this.dp;
                    }
                    break;
                case this.options.ab.end:
                    bx = snap ? this.pages.length - 1 : this.be;
                    bM = snap ? this.pages[0].length - 1 : this.ak;
                    break;
                case this.options.ab.home:
                    bx = 0;
                    bM = 0;
                    break;
                case this.options.ab.left:
                    bx += snap ? -1 : 5 + this.dP >> 0;
                    break;
                case this.options.ab.up:
                    bM += snap ? 1 : 5 + this.dP >> 0;
                    break;
                case this.options.ab.right:
                    bx -= snap ? -1 : 5 + this.dP >> 0;
                    break;
                case this.options.ab.down:
                    bM -= snap ? 1 : 5 + this.dP >> 0;
                    break;
                default:
                    return;
            }
            if(snap) {
                this.hi(bx, bM);
                return;
            }
            if(bx > 0) {
                bx = 0;
                this.dP = 0;
            } else if(bx < this.be) {
                bx = this.be;
                this.dP = 0;
            }
            if(bM > 0) {
                bM = 0;
                this.dP = 0;
            } else if(bM < this.ak) {
                bM = this.ak;
                this.dP = 0;
            }
            this.scrollTo(bx, bM, 0);
            this.yo = now;
        },
        oc: function(pn, po, duration, wR) {
            var bQ = this,
                cT = this.x,
                cj = this.y,
                startTime = ap.getTime(),
                vK = startTime + duration;

            function step() {
                var now = ap.getTime(),
                    bx, bM, easing;
                if(now >= vK) {
                    bQ.gN = false;
                    bQ.hk(pn, po);
                    if(!bQ.im(bQ.options.kE)) {
                        bQ.aS('scrollEnd');
                    }
                    return;
                }
                now = (now - startTime) / duration;
                easing = wR(now);
                bx = (pn - cT) * easing + cT;
                bM = (po - cj) * easing + cj;
                bQ.hk(bx, bM);
                if(bQ.gN) {
                    nF(step);
                }
            };
            this.gN = true;
            step();
        },
        vk: function(e) {
            switch(e.type) {
                case 'touchstart':
                case 'gQ':
                case 'nn':
                case 'mousedown':
                    this.kk(e);
                    break;
                case 'touchmove':
                case 'fu':
                case 'mV':
                case 'mousemove':
                    this.lg(e);
                    break;
                case 'touchend':
                case 'hb':
                case 'mZ':
                case 'mouseup':
                case 'touchcancel':
                case 'pointercancel':
                case 'MSPointerCancel':
                case 'mousecancel':
                    this.ky(e);
                    break;
                case 'orientationchange':
                case 'resize':
                    this.pP();
                    break;
                case 'transitionend':
                case 'webkitTransitionEnd':
                case 'oTransitionEnd':
                case 'MSTransitionEnd':
                    this.yw(e);
                    break;
                case 'wheel':
                case 'DOMMouseScroll':
                case 'mousewheel':
                    this.yP(e);
                    break;
                case 'keydown':
                    this.hJ(e);
                    break;
                case 'click':
                    if(!e.ob) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                    break;
            }
        }
    };

    function oe(direction, interactive, type) {
        var scrollbar = document.createElement('div'),
            bp = document.createElement('div');
        if(type === true) {
            scrollbar.style.cssText = 'position:absolute;z-index:9999';
            bp.style.cssText = '-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;background:rgba(0,0,0,0.5);border:1px solid rgba(255,255,255,0.9);border-radius:3px';
        }
        bp.className = 'iScrollIndicator';
        if(direction == 'h') {
            if(type === true) {
                scrollbar.style.cssText += ';height:7px;left:2px;right:2px;bottom:0';
                bp.style.height = '100%';
            }
            scrollbar.className = 'iScrollHorizontalScrollbar';
        } else {
            if(type === true) {
                scrollbar.style.cssText += ';width:7px;bottom:2px;top:2px;right:1px';
                bp.style.width = '100%';
            }
            scrollbar.className = 'iScrollVerticalScrollbar';
        }
        scrollbar.style.cssText += ';overflow:hidden';
        if(!interactive) {
            scrollbar.style.pointerEvents = 'none';
        }
        scrollbar.appendChild(bp);
        return scrollbar;
    };

    function oY(af, options) {
        this.bI = typeof options.by == 'string' ? document.querySelector(options.by) : options.by;
        this.gW = this.bI.style;
        this.bp = this.bI.children[0];
        this.aw = this.bp.style;
        this.af = af;
        this.options = {
            fE: true,
            fS: true,
            interactive: false,
            resize: true,
            hd: false,
            dg: false,
            fade: false,
            qS: 0,
            qQ: 0
        };
        for(var i in options) {
            this.options[i] = options[i];
        }
        this.iu = 1;
        this.iT = 1;
        this.fJ = 0;
        this.fe = 0;
        if(this.options.interactive) {
            if(!this.options.oj) {
                ap.cZ(this.bp, 'touchstart', this);
                ap.cZ(window, 'touchend', this);
            }
            if(!this.options.oD) {
                ap.cZ(this.bp, ap.cI('gQ'), this);
                ap.cZ(window, ap.cI('hb'), this);
            }
            if(!this.options.ps) {
                ap.cZ(this.bp, 'mousedown', this);
                ap.cZ(window, 'mouseup', this);
            }
        }
        if(this.options.fade) {
            this.gW[ap.style.transform] = this.af.ig;
            this.gW[ap.style.fo] = ap.gs ? '0.001s' : '0ms';
            this.gW.opacity = '0';
        }
    };
    oY.prototype = {
        vk: function(e) {
            switch(e.type) {
                case 'touchstart':
                case 'gQ':
                case 'nn':
                case 'mousedown':
                    this.kk(e);
                    break;
                case 'touchmove':
                case 'fu':
                case 'mV':
                case 'mousemove':
                    this.lg(e);
                    break;
                case 'touchend':
                case 'hb':
                case 'mZ':
                case 'mouseup':
                case 'touchcancel':
                case 'pointercancel':
                case 'MSPointerCancel':
                case 'mousecancel':
                    this.ky(e);
                    break;
            }
        },
        destroy: function() {
            if(this.options.interactive) {
                ap.bo(this.bp, 'touchstart', this);
                ap.bo(this.bp, ap.cI('gQ'), this);
                ap.bo(this.bp, 'mousedown', this);
                ap.bo(window, 'touchmove', this);
                ap.bo(window, ap.cI('fu'), this);
                ap.bo(window, 'mousemove', this);
                ap.bo(window, 'touchend', this);
                ap.bo(window, ap.cI('hb'), this);
                ap.bo(window, 'mouseup', this);
            }
            if(this.options.hd) {
                this.bI.parentNode.removeChild(this.bI);
            }
        },
        kk: function(e) {
            var an = e.fH ? e.fH[0] : e;
            e.preventDefault();
            e.stopPropagation();
            this.io();
            this.de = true;
            this.fM = false;
            this.jX = an.pageX;
            this.kv = an.pageY;
            this.startTime = ap.getTime();
            if(!this.options.oj) {
                ap.cZ(window, 'touchmove', this);
            }
            if(!this.options.oD) {
                ap.cZ(window, ap.cI('fu'), this);
            }
            if(!this.options.ps) {
                ap.cZ(window, 'mousemove', this);
            }
            this.af.aS('beforeScrollStart');
        },
        lg: function(e) {
            var an = e.fH ? e.fH[0] : e,
                deltaX, deltaY, bx, bM, iV = ap.getTime();
            if(!this.fM) {
                this.af.aS('scrollStart');
            }
            this.fM = true;
            deltaX = an.pageX - this.jX;
            this.jX = an.pageX;
            deltaY = an.pageY - this.kv;
            this.kv = an.pageY;
            bx = this.x + deltaX;
            bM = this.y + deltaY;
            this.rk(bx, bM);
            e.preventDefault();
            e.stopPropagation();
        },
        ky: function(e) {
            if(!this.de) {
                return;
            }
            this.de = false;
            e.preventDefault();
            e.stopPropagation();
            ap.bo(window, 'touchmove', this);
            ap.bo(window, ap.cI('fu'), this);
            ap.bo(window, 'mousemove', this);
            if(this.af.options.snap) {
                var snap = this.af.mq(this.af.x, this.af.y);
                var time = this.options.ia || Math.max(Math.max(Math.min(Math.abs(this.af.x - snap.x), 1000), Math.min(Math.abs(this.af.y - snap.y), 1000)), 300);
                if(this.af.x != snap.x || this.af.y != snap.y) {
                    this.af.eu = 0;
                    this.af.ft = 0;
                    this.af.bF = snap;
                    this.af.scrollTo(snap.x, snap.y, time, this.af.options.fI);
                }
            }
            if(this.fM) {
                this.af.aS('scrollEnd');
            }
        },
        io: function(time) {
            time = time || 0;
            this.aw[ap.style.fo] = time + 'ms';
            if(!time && ap.gs) {
                this.aw[ap.style.fo] = '0.001s';
            }
        },
        gv: function(easing) {
            this.aw[ap.style.gv] = easing;
        },
        refresh: function() {
            this.io();
            if(this.options.fE && !this.options.fS) {
                this.aw.display = this.af.dF ? 'block' : 'none';
            } else if(this.options.fS && !this.options.fE) {
                this.aw.display = this.af.di ? 'block' : 'none';
            } else {
                this.aw.display = this.af.dF || this.af.di ? 'block' : 'none';
            }
            if(this.af.dF && this.af.di) {
                ap.addClass(this.bI, 'iScrollBothScrollbars');
                ap.removeClass(this.bI, 'iScrollLoneScrollbar');
                if(this.options.hd && this.options.gP) {
                    if(this.options.fE) {
                        this.bI.style.right = '8px';
                    } else {
                        this.bI.style.bottom = '8px';
                    }
                }
            } else {
                ap.removeClass(this.bI, 'iScrollBothScrollbars');
                ap.addClass(this.bI, 'iScrollLoneScrollbar');
                if(this.options.hd && this.options.gP) {
                    if(this.options.fE) {
                        this.bI.style.right = '2px';
                    } else {
                        this.bI.style.bottom = '2px';
                    }
                }
            }
            var r = this.bI.offsetHeight;
            if(this.options.fE) {
                this.dB = this.bI.clientWidth;
                if(this.options.resize) {
                    this.cl = Math.max(Math.round(this.dB * this.dB / (this.af.hm || this.dB || 1)), 8);
                    this.aw.width = this.cl + 'px';
                } else {
                    this.cl = this.bp.clientWidth;
                }
                this.fJ = this.dB - this.cl;
                if(this.options.dg == 'clip') {
                    this.kK = -this.cl + 8;
                    this.kh = this.dB - 8;
                } else {
                    this.kK = 0;
                    this.kh = this.fJ;
                }
                this.iu = this.options.qS || (this.af.be && (this.fJ / this.af.be));
            }
            if(this.options.fS) {
                this.dp = this.bI.clientHeight;
                if(this.options.resize) {
                    this.cp = Math.max(Math.round(this.dp * this.dp / (this.af.gj || this.dp || 1)), 8);
                    this.aw.height = this.cp + 'px';
                } else {
                    this.cp = this.bp.clientHeight;
                }
                this.fe = this.dp - this.cp;
                if(this.options.dg == 'clip') {
                    this.kO = -this.cp + 8;
                    this.jI = this.dp - 8;
                } else {
                    this.kO = 0;
                    this.jI = this.fe;
                }
                this.fe = this.dp - this.cp;
                this.iT = this.options.qQ || (this.af.ak && (this.fe / this.af.ak));
            }
            this.nz();
        },
        nz: function() {
            var x = this.options.fE && Math.round(this.iu * this.af.x) || 0,
                y = this.options.fS && Math.round(this.iT * this.af.y) || 0;
            if(!this.options.Ax) {
                if(x < this.kK) {
                    if(this.options.dg == 'scale') {
                        this.width = Math.max(this.cl + x, 8);
                        this.aw.width = this.width + 'px';
                    }
                    x = this.kK;
                } else if(x > this.kh) {
                    if(this.options.dg == 'scale') {
                        this.width = Math.max(this.cl - (x - this.fJ), 8);
                        this.aw.width = this.width + 'px';
                        x = this.fJ + this.cl - this.width;
                    } else {
                        x = this.kh;
                    }
                } else if(this.options.dg == 'scale' && this.width != this.cl) {
                    this.width = this.cl;
                    this.aw.width = this.width + 'px';
                }
                if(y < this.kO) {
                    if(this.options.dg == 'scale') {
                        this.height = Math.max(this.cp + y * 3, 8);
                        this.aw.height = this.height + 'px';
                    }
                    y = this.kO;
                } else if(y > this.jI) {
                    if(this.options.dg == 'scale') {
                        this.height = Math.max(this.cp - (y - this.fe) * 3, 8);
                        this.aw.height = this.height + 'px';
                        y = this.fe + this.cp - this.height;
                    } else {
                        y = this.jI;
                    }
                } else if(this.options.dg == 'scale' && this.height != this.cp) {
                    this.height = this.cp;
                    this.aw.height = this.height + 'px';
                }
            }
            this.x = x;
            this.y = y;
            if(this.af.options.gS) {
                this.aw[ap.style.transform] = 'translate(' + x + 'px,' + y + 'px)' + this.af.ig;
            } else {
                this.aw.left = x + 'px';
                this.aw.top = y + 'px';
            }
        },
        rk: function(x, y) {
            if(x < 0) {
                x = 0;
            } else if(x > this.fJ) {
                x = this.fJ;
            }
            if(y < 0) {
                y = 0;
            } else if(y > this.fe) {
                y = this.fe;
            }
            x = this.options.fE ? Math.round(x / this.iu) : this.af.x;
            y = this.options.fS ? Math.round(y / this.iT) : this.af.y;
            this.af.scrollTo(x, y);
        },
        fade: function(val, vN) {
            if(vN && !this.visible) {
                return;
            }
            clearTimeout(this.nA);
            this.nA = null;
            var time = val ? 250 : 500,
                delay = val ? 0 : 300;
            val = val ? '1' : '0';
            this.gW[ap.style.fo] = time + 'ms';
            this.nA = setTimeout((function(val) {
                this.gW.opacity = val;
                this.visible = +val;
            }).bind(this, val), delay);
        }
    };
    gX.ap = ap;
    if(typeof la != 'undefined' && la.fg) {
        la.fg = gX;
    } else {
        window.gX = gX;
    }
})(window, document, Math);;
(function(root, lo) {
    if(typeof fg === "object") {
        la.fg = fg = lo();
    } else if(typeof define === "function" && define.xg) {
        define([], lo);
    } else {
        root.aX = lo();
    }
}(window, function() {
    var aX = aX || (function(Math, undefined) {
        var C = {};
        var bC = C.bf = {};
        var Base = bC.Base = (function() {
            function F() {};
            return {
                extend: function(fj) {
                    F.prototype = this;
                    var fl = new F();
                    if(fj) {
                        fl.jT(fj);
                    }
                    if(!fl.hasOwnProperty('aV')) {
                        fl.aV = function() {
                            fl.ro.aV.apply(this, arguments);
                        };
                    }
                    fl.aV.prototype = fl;
                    fl.ro = this;
                    return fl;
                },
                create: function() {
                    var kf = this.extend();
                    kf.aV.apply(kf, arguments);
                    return kf;
                },
                aV: function() {},
                jT: function(gY) {
                    for(var propertyName in gY) {
                        if(gY.hasOwnProperty(propertyName)) {
                            this[propertyName] = gY[propertyName];
                        }
                    }
                    if(gY.hasOwnProperty('toString')) {
                        this.toString = gY.toString;
                    }
                },
                clone: function() {
                    return this.aV.prototype.extend(this);
                }
            };
        }());
        var bd = bC.bd = Base.extend({
            aV: function(bq, aI) {
                bq = this.bq = bq || [];
                if(aI != undefined) {
                    this.aI = aI;
                } else {
                    this.aI = bq.length * 4;
                }
            },
            toString: function(vX) {
                return(vX || cJ).stringify(this);
            },
            concat: function(aH) {
                var mg = this.bq;
                var ng = aH.bq;
                var jo = this.aI;
                var jJ = aH.aI;
                this.ag();
                if(jo % 4) {
                    for(var i = 0; i < jJ; i++) {
                        var so = (ng[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
                        mg[(jo + i) >>> 2] |= so << (24 - ((jo + i) % 4) * 8);
                    }
                } else {
                    for(var i = 0; i < jJ; i += 4) {
                        mg[(jo + i) >>> 2] = ng[i >>> 2];
                    }
                }
                this.aI += jJ;
                return this;
            },
            ag: function() {
                var bq = this.bq;
                var aI = this.aI;
                bq[aI >>> 2] &= 0xffffffff << (32 - (aI % 4) * 8);
                bq.length = Math.ceil(aI / 4);
            },
            clone: function() {
                var clone = Base.clone.call(this);
                clone.bq = this.bq.slice(0);
                return clone;
            },
            random: function(eE) {
                var bq = [];
                var r = (function(eH) {
                    var eH = eH;
                    var iH = 0x3ade68b1;
                    var mask = 0xffffffff;
                    return function() {
                        iH = (0x9069 * (iH & 0xFFFF) + (iH >> 0x10)) & mask;
                        eH = (0x4650 * (eH & 0xFFFF) + (eH >> 0x10)) & mask;
                        var result = ((iH << 0x10) + eH) & mask;
                        result /= 0x100000000;
                        result += 0.5;
                        return result * (Math.random() > .5 ? 1 : -1);
                    }
                });
                for(var i = 0, mt; i < eE; i += 4) {
                    var mz = r((mt || Math.random()) * 0x100000000);
                    mt = mz() * 0x3ade67b7;
                    bq.push((mz() * 0x100000000) | 0);
                }
                return new bd.aV(bq, eE);
            }
        });
        var ck = C.bJ = {};
        var cJ = ck.cJ = {
            stringify: function(aH) {
                var bq = aH.bq;
                var aI = aH.aI;
                var jm = [];
                for(var i = 0; i < aI; i++) {
                    var iY = (bq[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
                    jm.push((iY >>> 4).toString(16));
                    jm.push((iY & 0x0f).toString(16));
                }
                return jm.join('');
            },
            parse: function(oX) {
                var nU = oX.length;
                var bq = [];
                for(var i = 0; i < nU; i += 2) {
                    bq[i >>> 3] |= parseInt(oX.substr(i, 2), 16) << (24 - (i % 8) * 4);
                }
                return new bd.aV(bq, nU / 2);
            }
        };
        var lq = ck.lq = {
            stringify: function(aH) {
                var bq = aH.bq;
                var aI = aH.aI;
                var mw = [];
                for(var i = 0; i < aI; i++) {
                    var iY = (bq[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
                    mw.push(String.fromCharCode(iY));
                }
                return mw.join('');
            },
            parse: function(mK) {
                var mn = mK.length;
                var bq = [];
                for(var i = 0; i < mn; i++) {
                    bq[i >>> 2] |= (mK.charCodeAt(i) & 0xff) << (24 - (i % 4) * 8);
                }
                return new bd.aV(bq, mn);
            }
        };
        var hh = ck.hh = {
            stringify: function(aH) {
                try {
                    return decodeURIComponent(escape(lq.stringify(aH)));
                } catch(e) {
                    throw new Error('Malformed UTF-8 data');
                }
            },
            parse: function(ox) {
                return lq.parse(unescape(encodeURIComponent(ox)));
            }
        };
        var fk = bC.fk = Base.extend({
            reset: function() {
                this.hs = new bd.aV();
                this.qY = 0;
            },
            ji: function(data) {
                if(typeof data == 'string') {
                    data = hh.parse(data);
                }
                this.hs.concat(data);
                this.qY += data.aI;
            },
            gq: function(wx) {
                var data = this.hs;
                var jk = data.bq;
                var gF = data.aI;
                var bB = this.bB;
                var cq = bB * 4;
                var gD = gF / cq;
                if(wx) {
                    gD = Math.ceil(gD);
                } else {
                    gD = Math.max((gD | 0) - this.lN, 0);
                }
                var iO = gD * bB;
                var nu = Math.min(iO * 4, gF);
                if(iO) {
                    for(var offset = 0; offset < iO; offset += bB) {
                        this.kq(jk, offset);
                    }
                    var rj = jk.splice(0, iO);
                    data.aI -= nu;
                }
                return new bd.aV(rj, nu);
            },
            clone: function() {
                var clone = Base.clone.call(this);
                clone.hs = this.hs.clone();
                return clone;
            },
            lN: 0
        });
        var wh = bC.wh = fk.extend({
            aL: Base.extend(),
            aV: function(aL) {
                this.aL = this.aL.extend(aL);
                this.reset();
            },
            reset: function() {
                fk.reset.call(this);
                this.jf();
            },
            update: function(jw) {
                this.ji(jw);
                this.gq();
                return this;
            },
            fU: function(jw) {
                if(jw) {
                    this.ji(jw);
                }
                var hash = this.kG();
                return hash;
            },
            bB: 512 / 32,
            jx: function(jM) {
                return function(message, aL) {
                    return new jM.aV(aL).fU(message);
                };
            },
            zS: function(jM) {
                return function(message, key) {
                    return new cw.AP.aV(jM, key).fU(message);
                };
            }
        });
        var cw = C.jg = {};
        return C;
    }(Math));
    (function() {
        var C = aX;
        var bC = C.bf;
        var bd = bC.bd;
        var ck = C.bJ;
        var aM = ck.aM = {
            stringify: function(aH) {
                var bq = aH.bq;
                var aI = aH.aI;
                var map = this.mo;
                aH.ag();
                var hQ = [];
                for(var i = 0; i < aI; i += 3) {
                    var wT = (bq[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
                    var vx = (bq[(i + 1) >>> 2] >>> (24 - ((i + 1) % 4) * 8)) & 0xff;
                    var wB = (bq[(i + 2) >>> 2] >>> (24 - ((i + 2) % 4) * 8)) & 0xff;
                    var ue = (wT << 16) | (vx << 8) | wB;
                    for(var j = 0;
                        (j < 4) && (i + j * 0.75 < aI); j++) {
                        hQ.push(map.charAt((ue >>> (6 * (3 - j))) & 0x3f));
                    }
                }
                var ge = map.charAt(64);
                if(ge) {
                    while(hQ.length % 4) {
                        hQ.push(ge);
                    }
                }
                return hQ.join('');
            },
            parse: function(jD) {
                var mc = jD.length;
                var map = this.mo;
                var ge = map.charAt(64);
                if(ge) {
                    var lU = jD.indexOf(ge);
                    if(lU != -1) {
                        mc = lU;
                    }
                }
                var bq = [];
                var eE = 0;
                for(var i = 0; i < mc; i++) {
                    if(i % 4) {
                        var wi = map.indexOf(jD.charAt(i - 1)) << ((i % 4) * 2);
                        var wu = map.indexOf(jD.charAt(i)) >>> (6 - (i % 4) * 2);
                        bq[eE >>> 2] |= (wi | wu) << (24 - (eE % 4) * 8);
                        eE++;
                    }
                }
                return bd.create(bq, eE);
            },
            mo: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
        };
    }());
    (function() {
        if(typeof xF != 'function') {
            return;
        }
        var C = aX;
        var bC = C.bf;
        var bd = bC.bd;
        var lH = bd.aV;
        var qo = bd.aV = function(ao) {
            if(ao instanceof xF) {
                ao = new ny(ao);
            }
            if(ao instanceof AH || (typeof rX !== "undefined" && ao instanceof rX) || ao instanceof zp || ao instanceof AK || ao instanceof zn || ao instanceof sI || ao instanceof za || ao instanceof AG) {
                ao = new ny(ao.AQ, ao.Az, ao.vu);
            }
            if(ao instanceof ny) {
                var pk = ao.vu;
                var bq = [];
                for(var i = 0; i < pk; i++) {
                    bq[i >>> 2] |= ao[i] << (24 - (i % 4) * 8);
                }
                lH.call(this, bq, pk);
            } else {
                lH.apply(this, arguments);
            }
        };
        qo.prototype = bd;
    }());
    aX.bf.eU || (function(undefined) {
        var C = aX;
        var bC = C.bf;
        var Base = bC.Base;
        var bd = bC.bd;
        var fk = bC.fk;
        var ck = C.bJ;
        var hh = ck.hh;
        var aM = ck.aM;
        var cw = C.jg;
        var nZ = cw.nZ;
        var eU = bC.eU = fk.extend({
            aL: Base.extend(),
            kR: function(key, aL) {
                return this.create(this.jU, key, aL);
            },
            kg: function(key, aL) {
                return this.create(this.qp, key, aL);
            },
            aV: function(qN, key, aL) {
                this.aL = this.aL.extend(aL);
                this.nt = qN;
                this.hJ = key;
                this.reset();
            },
            reset: function() {
                fk.reset.call(this);
                this.jf();
            },
            zW: function(jy) {
                this.ji(jy);
                return this.gq();
            },
            fU: function(jy) {
                if(jy) {
                    this.ji(jy);
                }
                var xb = this.kG();
                return xb;
            },
            ah: 128 / 32,
            fN: 128 / 32,
            jU: 1,
            qp: 2,
            jx: (function() {
                function ou(key) {
                    if(typeof key == 'string') {
                        return ni;
                    } else {
                        return hw;
                    }
                };
                return function(aT) {
                    return {
                        bZ: function(message, key, aL) {
                            return ou(key).bZ(aT, message, key, aL);
                        },
                        eV: function(bD, key, aL) {
                            return ou(key).eV(aT, bD, key, aL);
                        }
                    };
                };
            }())
        });
        var dC = bC.dC = eU.extend({
            kG: function() {
                var gH = this.gq(!!'flush');
                return gH;
            },
            bB: 1
        });
        var wJ = C.mode = {};
        var eL = bC.eL = Base.extend({
            kR: function(aT, bP) {
                return this.cB.create(aT, bP);
            },
            kg: function(aT, bP) {
                return this.eZ.create(aT, bP);
            },
            aV: function(aT, bP) {
                this.cd = aT;
                this.cQ = bP;
            }
        });
        var fi = wJ.fi = (function() {
            var fi = eL.extend();
            fi.cB = fi.extend({
                co: function(bq, offset) {
                    var aT = this.cd;
                    var bB = aT.bB;
                    ph.call(this, bq, offset, bB);
                    aT.fL(bq, offset);
                    this.fO = bq.slice(offset, offset + bB);
                }
            });
            fi.eZ = fi.extend({
                co: function(bq, offset) {
                    var aT = this.cd;
                    var bB = aT.bB;
                    var iM = bq.slice(offset, offset + bB);
                    aT.kQ(bq, offset);
                    ph.call(this, bq, offset, bB);
                    this.fO = iM;
                }
            });

            function ph(bq, offset, bB) {
                var bP = this.cQ;
                if(bP) {
                    var block = bP;
                    this.cQ = undefined;
                } else {
                    var block = this.fO;
                }
                for(var i = 0; i < bB; i++) {
                    bq[offset + i] ^= block[i];
                }
            };
            return fi;
        }());
        var vt = C.bH = {};
        var kB = vt.kB = {
            bH: function(data, bB) {
                var cq = bB * 4;
                var aZ = cq - data.aI % cq;
                var xu = (aZ << 24) | (aZ << 16) | (aZ << 8) | aZ;
                var mB = [];
                for(var i = 0; i < aZ; i += 4) {
                    mB.push(xu);
                }
                var padding = bd.create(mB, aZ);
                data.concat(padding);
            },
            es: function(data) {
                var aZ = data.bq[(data.aI - 1) >>> 2] & 0xff;
                data.aI -= aZ;
            }
        };
        var gi = bC.gi = eU.extend({
            aL: eU.aL.extend({
                mode: fi,
                padding: kB
            }),
            reset: function() {
                eU.reset.call(this);
                var aL = this.aL;
                var bP = aL.bP;
                var mode = aL.mode;
                if(this.nt == this.jU) {
                    var nd = mode.kR;
                } else {
                    var nd = mode.kg;
                    this.lN = 1;
                }
                this.pu = nd.call(mode, this, bP && bP.bq);
            },
            kq: function(bq, offset) {
                this.pu.co(bq, offset);
            },
            kG: function() {
                var padding = this.aL.padding;
                if(this.nt == this.jU) {
                    padding.bH(this.hs, this.bB);
                    var gH = this.gq(!!'flush');
                } else {
                    var gH = this.gq(!!'flush');
                    padding.es(gH);
                }
                return gH;
            },
            bB: 128 / 32
        });
        var fw = bC.fw = Base.extend({
            aV: function(eI) {
                this.jT(eI);
            },
            toString: function(kU) {
                return(kU || this.kU).stringify(this);
            }
        });
        var lf = C.format = {};
        var yV = lf.vj = {
            stringify: function(eI) {
                var bD = eI.bD;
                var dG = eI.dG;
                if(dG) {
                    var aH = bd.create([0x53616c74, 0x65645f5f]).concat(dG).concat(bD);
                } else {
                    var aH = bD;
                }
                return aH.toString(aM);
            },
            parse: function(xW) {
                var bD = aM.parse(xW);
                var iR = bD.bq;
                if(iR[0] == 0x53616c74 && iR[1] == 0x65645f5f) {
                    var dG = bd.create(iR.slice(2, 4));
                    iR.splice(0, 4);
                    bD.aI -= 16;
                }
                return fw.create({
                    bD: bD,
                    dG: dG
                });
            }
        };
        var hw = bC.hw = Base.extend({
            aL: Base.extend({
                format: yV
            }),
            bZ: function(aT, message, key, aL) {
                aL = this.aL.extend(aL);
                var nX = aT.kR(key, aL);
                var bD = nX.fU(message);
                var kd = nX.aL;
                return fw.create({
                    bD: bD,
                    key: key,
                    bP: kd.bP,
                    yC: aT,
                    mode: kd.mode,
                    padding: kd.padding,
                    bB: aT.bB,
                    kU: aL.format
                });
            },
            eV: function(aT, bD, key, aL) {
                aL = this.aL.extend(aL);
                bD = this.lX(bD, aL.format);
                var plaintext = aT.kg(key, aL).fU(bD.bD);
                return plaintext;
            },
            lX: function(bD, format) {
                if(typeof bD == 'string') {
                    return format.parse(bD, this);
                } else {
                    return bD;
                }
            }
        });
        var wV = C.ly = {};
        var yd = wV.vj = {
            oB: function(password, ah, fN, dG) {
                if(!dG) {
                    dG = bd.random(64 / 8);
                }
                var key = nZ.create({
                    ah: ah + fN
                }).Aj(password, dG);
                var bP = bd.create(key.bq.slice(ah), fN * 4);
                key.aI = ah * 4;
                return fw.create({
                    key: key,
                    bP: bP,
                    dG: dG
                });
            }
        };
        var ni = bC.ni = hw.extend({
            aL: hw.aL.extend({
                ly: yd
            }),
            bZ: function(aT, message, password, aL) {
                aL = this.aL.extend(aL);
                var er = aL.ly.oB(password, aT.ah, aT.fN);
                aL.bP = er.bP;
                var bD = hw.bZ.call(this, aT, message, er.key, aL);
                bD.jT(er);
                return bD;
            },
            eV: function(aT, bD, password, aL) {
                aL = this.aL.extend(aL);
                bD = this.lX(bD, aL.format);
                var er = aL.ly.oB(password, aT.ah, aT.fN, bD.dG);
                aL.bP = er.bP;
                var plaintext = hw.eV.call(this, aT, bD, er.key, aL);
                return plaintext;
            }
        });
    }());
    aX.mode.gn = (function() {
        var gn = aX.bf.eL.extend();
        gn.cB = gn.extend({
            co: function(bq, offset) {
                var aT = this.cd;
                var bB = aT.bB;
                nI.call(this, bq, offset, bB, aT);
                this.fO = bq.slice(offset, offset + bB);
            }
        });
        gn.eZ = gn.extend({
            co: function(bq, offset) {
                var aT = this.cd;
                var bB = aT.bB;
                var iM = bq.slice(offset, offset + bB);
                nI.call(this, bq, offset, bB, aT);
                this.fO = iM;
            }
        });

        function nI(bq, offset, bB, aT) {
            var bP = this.cQ;
            if(bP) {
                var cP = bP.slice(0);
                this.cQ = undefined;
            } else {
                var cP = this.fO;
            }
            aT.fL(cP, 0);
            for(var i = 0; i < bB; i++) {
                bq[offset + i] ^= cP[i];
            }
        };
        return gn;
    }());
    aX.mode.ed = (function() {
        var ed = aX.bf.eL.extend();
        ed.cB = ed.extend({
            co: function(bq, offset) {
                this.cd.fL(bq, offset);
            }
        });
        ed.eZ = ed.extend({
            co: function(bq, offset) {
                this.cd.kQ(bq, offset);
            }
        });
        return ed;
    }());
    aX.bH.yr = {
        bH: function(data, bB) {
            var gF = data.aI;
            var cq = bB * 4;
            var aZ = cq - gF % cq;
            var jK = gF + aZ - 1;
            data.ag();
            data.bq[jK >>> 2] |= aZ << (24 - (jK % 4) * 8);
            data.aI += aZ;
        },
        es: function(data) {
            var aZ = data.bq[(data.aI - 1) >>> 2] & 0xff;
            data.aI -= aZ;
        }
    };
    aX.bH.Ak = {
        bH: function(data, bB) {
            var cq = bB * 4;
            var aZ = cq - data.aI % cq;
            data.concat(aX.bf.bd.random(aZ - 1)).concat(aX.bf.bd.create([aZ << 24], 1));
        },
        es: function(data) {
            var aZ = data.bq[(data.aI - 1) >>> 2] & 0xff;
            data.aI -= aZ;
        }
    };
    aX.bH.Ai = {
        bH: function(data, bB) {
            data.concat(aX.bf.bd.create([0x80000000], 1));
            aX.bH.oQ.bH(data, bB);
        },
        es: function(data) {
            aX.bH.oQ.es(data);
            data.aI--;
        }
    };
    aX.mode.gK = (function() {
        var gK = aX.bf.eL.extend();
        var cB = gK.cB = gK.extend({
            co: function(bq, offset) {
                var aT = this.cd;
                var bB = aT.bB;
                var bP = this.cQ;
                var cP = this.qF;
                if(bP) {
                    cP = this.qF = bP.slice(0);
                    this.cQ = undefined;
                }
                aT.fL(cP, 0);
                for(var i = 0; i < bB; i++) {
                    bq[offset + i] ^= cP[i];
                }
            }
        });
        gK.eZ = cB;
        return gK;
    }());
    aX.bH.yh = {
        bH: function() {},
        es: function() {}
    };
    (function(undefined) {
        var C = aX;
        var bC = C.bf;
        var fw = bC.fw;
        var ck = C.bJ;
        var cJ = ck.cJ;
        var lf = C.format;
        var xm = lf.cJ = {
            stringify: function(eI) {
                return eI.bD.toString(cJ);
            },
            parse: function(input) {
                var bD = cJ.parse(input);
                return fw.create({
                    bD: bD
                });
            }
        };
    }());
    (function() {
        var C = aX;
        var bC = C.bf;
        var gi = bC.gi;
        var cw = C.jg;
        var bj = [];
        var oP = [];
        var eF = [];
        var eG = [];
        var fD = [];
        var ek = [];
        var kr = [];
        var lj = [];
        var ka = [];
        var lk = [];
        (function() {
            var d = [];
            for(var i = 0; i < 256; i++) {
                if(i < 128) {
                    d[i] = i << 1;
                } else {
                    d[i] = (i << 1) ^ 0x11b;
                }
            }
            var x = 0;
            var fm = 0;
            for(var i = 0; i < 256; i++) {
                var cr = fm ^ (fm << 1) ^ (fm << 2) ^ (fm << 3) ^ (fm << 4);
                cr = (cr >>> 8) ^ (cr & 0xff) ^ 0x63;
                bj[x] = cr;
                oP[cr] = x;
                var x2 = d[x];
                var pa = d[x2];
                var nm = d[pa];
                var t = (d[cr] * 0x101) ^ (cr * 0x1010100);
                eF[x] = (t << 24) | (t >>> 8);
                eG[x] = (t << 16) | (t >>> 16);
                fD[x] = (t << 8) | (t >>> 24);
                ek[x] = t;
                var t = (nm * 0x1010101) ^ (pa * 0x10001) ^ (x2 * 0x101) ^ (x * 0x1010100);
                kr[cr] = (t << 24) | (t >>> 8);
                lj[cr] = (t << 16) | (t >>> 16);
                ka[cr] = (t << 8) | (t >>> 24);
                lk[cr] = t;
                if(!x) {
                    x = fm = 1;
                } else {
                    x = x2 ^ d[d[d[nm ^ x2]]];
                    fm ^= d[d[fm]];
                }
            }
        }());
        var pS = [0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36];
        var iG = cw.iG = gi.extend({
            jf: function() {
                var key = this.hJ;
                var keyWords = key.bq;
                var ah = key.aI / 4;
                var jO = this.qv = ah + 6;
                var kF = (jO + 1) * 4;
                var bV = this.qg = [];
                for(var aq = 0; aq < kF; aq++) {
                    if(aq < ah) {
                        bV[aq] = keyWords[aq];
                    } else {
                        var t = bV[aq - 1];
                        if(!(aq % ah)) {
                            t = (t << 8) | (t >>> 24);
                            t = (bj[t >>> 24] << 24) | (bj[(t >>> 16) & 0xff] << 16) | (bj[(t >>> 8) & 0xff] << 8) | bj[t & 0xff];
                            t ^= pS[(aq / ah) | 0] << 24;
                        } else if(ah > 6 && aq % ah == 4) {
                            t = (bj[t >>> 24] << 24) | (bj[(t >>> 16) & 0xff] << 16) | (bj[(t >>> 8) & 0xff] << 8) | bj[t & 0xff];
                        }
                        bV[aq] = bV[aq - ah] ^ t;
                    }
                }
                var oA = this.qM = [];
                for(var fp = 0; fp < kF; fp++) {
                    var aq = kF - fp;
                    if(fp % 4) {
                        var t = bV[aq];
                    } else {
                        var t = bV[aq - 4];
                    }
                    if(fp < 4 || aq <= 4) {
                        oA[fp] = t;
                    } else {
                        oA[fp] = kr[bj[t >>> 24]] ^ lj[bj[(t >>> 16) & 0xff]] ^ ka[bj[(t >>> 8) & 0xff]] ^ lk[bj[t & 0xff]];
                    }
                }
            },
            fL: function(M, offset) {
                this.lG(M, offset, this.qg, eF, eG, fD, ek, bj);
            },
            kQ: function(M, offset) {
                var t = M[offset + 1];
                M[offset + 1] = M[offset + 3];
                M[offset + 3] = t;
                this.lG(M, offset, this.qM, kr, lj, ka, lk, oP);
                var t = M[offset + 1];
                M[offset + 1] = M[offset + 3];
                M[offset + 3] = t;
            },
            lG: function(M, offset, bV, eF, eG, fD, ek, bj) {
                var jO = this.qv;
                var du = M[offset] ^ bV[0];
                var cC = M[offset + 1] ^ bV[1];
                var ci = M[offset + 2] ^ bV[2];
                var cG = M[offset + 3] ^ bV[3];
                var aq = 4;
                for(var round = 1; round < jO; round++) {
                    var hW = eF[du >>> 24] ^ eG[(cC >>> 16) & 0xff] ^ fD[(ci >>> 8) & 0xff] ^ ek[cG & 0xff] ^ bV[aq++];
                    var jr = eF[cC >>> 24] ^ eG[(ci >>> 16) & 0xff] ^ fD[(cG >>> 8) & 0xff] ^ ek[du & 0xff] ^ bV[aq++];
                    var iA = eF[ci >>> 24] ^ eG[(cG >>> 16) & 0xff] ^ fD[(du >>> 8) & 0xff] ^ ek[cC & 0xff] ^ bV[aq++];
                    var jz = eF[cG >>> 24] ^ eG[(du >>> 16) & 0xff] ^ fD[(cC >>> 8) & 0xff] ^ ek[ci & 0xff] ^ bV[aq++];
                    du = hW;
                    cC = jr;
                    ci = iA;
                    cG = jz;
                }
                var hW = ((bj[du >>> 24] << 24) | (bj[(cC >>> 16) & 0xff] << 16) | (bj[(ci >>> 8) & 0xff] << 8) | bj[cG & 0xff]) ^ bV[aq++];
                var jr = ((bj[cC >>> 24] << 24) | (bj[(ci >>> 16) & 0xff] << 16) | (bj[(cG >>> 8) & 0xff] << 8) | bj[du & 0xff]) ^ bV[aq++];
                var iA = ((bj[ci >>> 24] << 24) | (bj[(cG >>> 16) & 0xff] << 16) | (bj[(du >>> 8) & 0xff] << 8) | bj[cC & 0xff]) ^ bV[aq++];
                var jz = ((bj[cG >>> 24] << 24) | (bj[(du >>> 16) & 0xff] << 16) | (bj[(cC >>> 8) & 0xff] << 8) | bj[ci & 0xff]) ^ bV[aq++];
                M[offset] = hW;
                M[offset + 1] = jr;
                M[offset + 2] = iA;
                M[offset + 3] = jz;
            },
            ah: 256 / 32
        });
        C.iG = gi.jx(iG);
    }());
    (function(Math) {
        var C = aX;
        var bC = C.bf;
        var bd = bC.bd;
        var wh = bC.wh;
        var cw = C.jg;
        var T = [];
        (function() {
            for(var i = 0; i < 64; i++) {
                T[i] = (Math.abs(Math.sin(i + 1)) * 0x100000000) | 0;
            }
        }());
        var AF = cw.AF = wh.extend({
            jf: function() {
                this._hash = new bd.aV([0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476]);
            },
            kq: function(M, offset) {
                for(var i = 0; i < 16; i++) {
                    var offset_i = offset + i;
                    var M_offset_i = M[offset_i];
                    M[offset_i] = ((((M_offset_i << 8) | (M_offset_i >>> 24)) & 0x00ff00ff) | (((M_offset_i << 24) | (M_offset_i >>> 8)) & 0xff00ff00));
                }
                var H = this._hash.bq;
                var M_offset_0 = M[offset + 0];
                var M_offset_1 = M[offset + 1];
                var M_offset_2 = M[offset + 2];
                var M_offset_3 = M[offset + 3];
                var M_offset_4 = M[offset + 4];
                var M_offset_5 = M[offset + 5];
                var M_offset_6 = M[offset + 6];
                var M_offset_7 = M[offset + 7];
                var M_offset_8 = M[offset + 8];
                var M_offset_9 = M[offset + 9];
                var M_offset_10 = M[offset + 10];
                var M_offset_11 = M[offset + 11];
                var M_offset_12 = M[offset + 12];
                var M_offset_13 = M[offset + 13];
                var M_offset_14 = M[offset + 14];
                var M_offset_15 = M[offset + 15];
                var a = H[0];
                var b = H[1];
                var c = H[2];
                var d = H[3];
                a = FF(a, b, c, d, M_offset_0, 7, T[0]);
                d = FF(d, a, b, c, M_offset_1, 12, T[1]);
                c = FF(c, d, a, b, M_offset_2, 17, T[2]);
                b = FF(b, c, d, a, M_offset_3, 22, T[3]);
                a = FF(a, b, c, d, M_offset_4, 7, T[4]);
                d = FF(d, a, b, c, M_offset_5, 12, T[5]);
                c = FF(c, d, a, b, M_offset_6, 17, T[6]);
                b = FF(b, c, d, a, M_offset_7, 22, T[7]);
                a = FF(a, b, c, d, M_offset_8, 7, T[8]);
                d = FF(d, a, b, c, M_offset_9, 12, T[9]);
                c = FF(c, d, a, b, M_offset_10, 17, T[10]);
                b = FF(b, c, d, a, M_offset_11, 22, T[11]);
                a = FF(a, b, c, d, M_offset_12, 7, T[12]);
                d = FF(d, a, b, c, M_offset_13, 12, T[13]);
                c = FF(c, d, a, b, M_offset_14, 17, T[14]);
                b = FF(b, c, d, a, M_offset_15, 22, T[15]);
                a = GG(a, b, c, d, M_offset_1, 5, T[16]);
                d = GG(d, a, b, c, M_offset_6, 9, T[17]);
                c = GG(c, d, a, b, M_offset_11, 14, T[18]);
                b = GG(b, c, d, a, M_offset_0, 20, T[19]);
                a = GG(a, b, c, d, M_offset_5, 5, T[20]);
                d = GG(d, a, b, c, M_offset_10, 9, T[21]);
                c = GG(c, d, a, b, M_offset_15, 14, T[22]);
                b = GG(b, c, d, a, M_offset_4, 20, T[23]);
                a = GG(a, b, c, d, M_offset_9, 5, T[24]);
                d = GG(d, a, b, c, M_offset_14, 9, T[25]);
                c = GG(c, d, a, b, M_offset_3, 14, T[26]);
                b = GG(b, c, d, a, M_offset_8, 20, T[27]);
                a = GG(a, b, c, d, M_offset_13, 5, T[28]);
                d = GG(d, a, b, c, M_offset_2, 9, T[29]);
                c = GG(c, d, a, b, M_offset_7, 14, T[30]);
                b = GG(b, c, d, a, M_offset_12, 20, T[31]);
                a = HH(a, b, c, d, M_offset_5, 4, T[32]);
                d = HH(d, a, b, c, M_offset_8, 11, T[33]);
                c = HH(c, d, a, b, M_offset_11, 16, T[34]);
                b = HH(b, c, d, a, M_offset_14, 23, T[35]);
                a = HH(a, b, c, d, M_offset_1, 4, T[36]);
                d = HH(d, a, b, c, M_offset_4, 11, T[37]);
                c = HH(c, d, a, b, M_offset_7, 16, T[38]);
                b = HH(b, c, d, a, M_offset_10, 23, T[39]);
                a = HH(a, b, c, d, M_offset_13, 4, T[40]);
                d = HH(d, a, b, c, M_offset_0, 11, T[41]);
                c = HH(c, d, a, b, M_offset_3, 16, T[42]);
                b = HH(b, c, d, a, M_offset_6, 23, T[43]);
                a = HH(a, b, c, d, M_offset_9, 4, T[44]);
                d = HH(d, a, b, c, M_offset_12, 11, T[45]);
                c = HH(c, d, a, b, M_offset_15, 16, T[46]);
                b = HH(b, c, d, a, M_offset_2, 23, T[47]);
                a = II(a, b, c, d, M_offset_0, 6, T[48]);
                d = II(d, a, b, c, M_offset_7, 10, T[49]);
                c = II(c, d, a, b, M_offset_14, 15, T[50]);
                b = II(b, c, d, a, M_offset_5, 21, T[51]);
                a = II(a, b, c, d, M_offset_12, 6, T[52]);
                d = II(d, a, b, c, M_offset_3, 10, T[53]);
                c = II(c, d, a, b, M_offset_10, 15, T[54]);
                b = II(b, c, d, a, M_offset_1, 21, T[55]);
                a = II(a, b, c, d, M_offset_8, 6, T[56]);
                d = II(d, a, b, c, M_offset_15, 10, T[57]);
                c = II(c, d, a, b, M_offset_6, 15, T[58]);
                b = II(b, c, d, a, M_offset_13, 21, T[59]);
                a = II(a, b, c, d, M_offset_4, 6, T[60]);
                d = II(d, a, b, c, M_offset_11, 10, T[61]);
                c = II(c, d, a, b, M_offset_2, 15, T[62]);
                b = II(b, c, d, a, M_offset_9, 21, T[63]);
                H[0] = (H[0] + a) | 0;
                H[1] = (H[1] + b) | 0;
                H[2] = (H[2] + c) | 0;
                H[3] = (H[3] + d) | 0;
            },
            kG: function() {
                var data = this.hs;
                var jk = data.bq;
                var nBitsTotal = this.qY * 8;
                var nBitsLeft = data.aI * 8;
                jk[nBitsLeft >>> 5] |= 0x80 << (24 - nBitsLeft % 32);
                var nBitsTotalH = Math.floor(nBitsTotal / 0x100000000);
                var nBitsTotalL = nBitsTotal;
                jk[(((nBitsLeft + 64) >>> 9) << 4) + 15] = ((((nBitsTotalH << 8) | (nBitsTotalH >>> 24)) & 0x00ff00ff) | (((nBitsTotalH << 24) | (nBitsTotalH >>> 8)) & 0xff00ff00));
                jk[(((nBitsLeft + 64) >>> 9) << 4) + 14] = ((((nBitsTotalL << 8) | (nBitsTotalL >>> 24)) & 0x00ff00ff) | (((nBitsTotalL << 24) | (nBitsTotalL >>> 8)) & 0xff00ff00));
                data.aI = (jk.length + 1) * 4;
                this.gq();
                var hash = this._hash;
                var H = hash.bq;
                for(var i = 0; i < 4; i++) {
                    var H_i = H[i];
                    H[i] = (((H_i << 8) | (H_i >>> 24)) & 0x00ff00ff) | (((H_i << 24) | (H_i >>> 8)) & 0xff00ff00);
                }
                return hash;
            },
            clone: function() {
                var clone = wh.clone.call(this);
                clone._hash = this._hash.clone();
                return clone;
            }
        });

        function FF(a, b, c, d, x, s, t) {
            var n = a + ((b & c) | (~b & d)) + x + t;
            return((n << s) | (n >>> (32 - s))) + b;
        };

        function GG(a, b, c, d, x, s, t) {
            var n = a + ((b & d) | (c & ~d)) + x + t;
            return((n << s) | (n >>> (32 - s))) + b;
        };

        function HH(a, b, c, d, x, s, t) {
            var n = a + (b ^ c ^ d) + x + t;
            return((n << s) | (n >>> (32 - s))) + b;
        };

        function II(a, b, c, d, x, s, t) {
            var n = a + (c ^ (b | ~d)) + x + t;
            return((n << s) | (n >>> (32 - s))) + b;
        };
        C.AF = wh.jx(AF);
        C.HmacMD5 = wh.zS(AF);
    }(Math));
    aX.mode.hn = (function() {
        var hn = aX.bf.eL.extend();

        function oT(word) {
            if(((word >> 24) & 0xff) === 0xff) {
                var il = (word >> 16) & 0xff;
                var iL = (word >> 8) & 0xff;
                var jq = word & 0xff;
                if(il === 0xff) {
                    il = 0;
                    if(iL === 0xff) {
                        iL = 0;
                        if(jq === 0xff) {
                            jq = 0;
                        } else {
                            ++jq;
                        }
                    } else {
                        ++iL;
                    }
                } else {
                    ++il;
                }
                word = 0;
                word += (il << 16);
                word += (iL << 8);
                word += jq;
            } else {
                word += (0x01 << 24);
            }
            return word;
        };

        function pp(counter) {
            if((counter[0] = oT(counter[0])) === 0) {
                counter[1] = oT(counter[1]);
            }
            return counter;
        };
        var cB = hn.cB = hn.extend({
            co: function(bq, offset) {
                var aT = this.cd;
                var bB = aT.bB;
                var bP = this.cQ;
                var counter = this.jY;
                if(bP) {
                    counter = this.jY = bP.slice(0);
                    this.cQ = undefined;
                }
                pp(counter);
                var cP = counter.slice(0);
                aT.fL(cP, 0);
                for(var i = 0; i < bB; i++) {
                    bq[offset + i] ^= cP[i];
                }
            }
        });
        hn.eZ = cB;
        return hn;
    }());
    (function() {
        var C = aX;
        var bC = C.bf;
        var dC = bC.dC;
        var cw = C.jg;
        var S = [];
        var aJ = [];
        var G = [];
        var jG = cw.jG = dC.extend({
            jf: function() {
                var K = this.hJ.bq;
                var bP = this.aL.bP;
                for(var i = 0; i < 4; i++) {
                    K[i] = (((K[i] << 8) | (K[i] >>> 24)) & 0x00ff00ff) | (((K[i] << 24) | (K[i] >>> 8)) & 0xff00ff00);
                }
                var X = this.hC = [K[0], (K[3] << 16) | (K[2] >>> 16), K[1], (K[0] << 16) | (K[3] >>> 16), K[2], (K[1] << 16) | (K[0] >>> 16), K[3], (K[2] << 16) | (K[1] >>> 16)];
                var C = this.kt = [(K[2] << 16) | (K[2] >>> 16), (K[0] & 0xffff0000) | (K[1] & 0x0000ffff), (K[3] << 16) | (K[3] >>> 16), (K[1] & 0xffff0000) | (K[2] & 0x0000ffff), (K[0] << 16) | (K[0] >>> 16), (K[2] & 0xffff0000) | (K[3] & 0x0000ffff), (K[1] << 16) | (K[1] >>> 16), (K[3] & 0xffff0000) | (K[0] & 0x0000ffff)];
                this.fP = 0;
                for(var i = 0; i < 4; i++) {
                    eQ.call(this);
                }
                for(var i = 0; i < 8; i++) {
                    C[i] ^= X[(i + 4) & 7];
                }
                if(bP) {
                    var hD = bP.bq;
                    var cz = hD[0];
                    var dz = hD[1];
                    var dc = (((cz << 8) | (cz >>> 24)) & 0x00ff00ff) | (((cz << 24) | (cz >>> 8)) & 0xff00ff00);
                    var cN = (((dz << 8) | (dz >>> 24)) & 0x00ff00ff) | (((dz << 24) | (dz >>> 8)) & 0xff00ff00);
                    var fQ = (dc >>> 16) | (cN & 0xffff0000);
                    var hH = (cN << 16) | (dc & 0x0000ffff);
                    C[0] ^= dc;
                    C[1] ^= fQ;
                    C[2] ^= cN;
                    C[3] ^= hH;
                    C[4] ^= dc;
                    C[5] ^= fQ;
                    C[6] ^= cN;
                    C[7] ^= hH;
                    for(var i = 0; i < 4; i++) {
                        eQ.call(this);
                    }
                }
            },
            kq: function(M, offset) {
                var X = this.hC;
                eQ.call(this);
                S[0] = X[0] ^ (X[5] >>> 16) ^ (X[3] << 16);
                S[1] = X[2] ^ (X[7] >>> 16) ^ (X[5] << 16);
                S[2] = X[4] ^ (X[1] >>> 16) ^ (X[7] << 16);
                S[3] = X[6] ^ (X[3] >>> 16) ^ (X[1] << 16);
                for(var i = 0; i < 4; i++) {
                    S[i] = (((S[i] << 8) | (S[i] >>> 24)) & 0x00ff00ff) | (((S[i] << 24) | (S[i] >>> 8)) & 0xff00ff00);
                    M[offset + i] ^= S[i];
                }
            },
            bB: 128 / 32,
            fN: 64 / 32
        });

        function eQ() {
            var X = this.hC;
            var C = this.kt;
            for(var i = 0; i < 8; i++) {
                aJ[i] = C[i];
            }
            C[0] = (C[0] + 0x4d34d34d + this.fP) | 0;
            C[1] = (C[1] + 0xd34d34d3 + ((C[0] >>> 0) < (aJ[0] >>> 0) ? 1 : 0)) | 0;
            C[2] = (C[2] + 0x34d34d34 + ((C[1] >>> 0) < (aJ[1] >>> 0) ? 1 : 0)) | 0;
            C[3] = (C[3] + 0x4d34d34d + ((C[2] >>> 0) < (aJ[2] >>> 0) ? 1 : 0)) | 0;
            C[4] = (C[4] + 0xd34d34d3 + ((C[3] >>> 0) < (aJ[3] >>> 0) ? 1 : 0)) | 0;
            C[5] = (C[5] + 0x34d34d34 + ((C[4] >>> 0) < (aJ[4] >>> 0) ? 1 : 0)) | 0;
            C[6] = (C[6] + 0x4d34d34d + ((C[5] >>> 0) < (aJ[5] >>> 0) ? 1 : 0)) | 0;
            C[7] = (C[7] + 0xd34d34d3 + ((C[6] >>> 0) < (aJ[6] >>> 0) ? 1 : 0)) | 0;
            this.fP = (C[7] >>> 0) < (aJ[7] >>> 0) ? 1 : 0;
            for(var i = 0; i < 8; i++) {
                var da = X[i] + C[i];
                var eS = da & 0xffff;
                var dX = da >>> 16;
                var jE = ((((eS * eS) >>> 17) + eS * dX) >>> 15) + dX * dX;
                var jc = (((da & 0xffff0000) * da) | 0) + (((da & 0x0000ffff) * da) | 0);
                G[i] = jE ^ jc;
            }
            X[0] = (G[0] + ((G[7] << 16) | (G[7] >>> 16)) + ((G[6] << 16) | (G[6] >>> 16))) | 0;
            X[1] = (G[1] + ((G[0] << 8) | (G[0] >>> 24)) + G[7]) | 0;
            X[2] = (G[2] + ((G[1] << 16) | (G[1] >>> 16)) + ((G[0] << 16) | (G[0] >>> 16))) | 0;
            X[3] = (G[3] + ((G[2] << 8) | (G[2] >>> 24)) + G[1]) | 0;
            X[4] = (G[4] + ((G[3] << 16) | (G[3] >>> 16)) + ((G[2] << 16) | (G[2] >>> 16))) | 0;
            X[5] = (G[5] + ((G[4] << 8) | (G[4] >>> 24)) + G[3]) | 0;
            X[6] = (G[6] + ((G[5] << 16) | (G[5] >>> 16)) + ((G[4] << 16) | (G[4] >>> 16))) | 0;
            X[7] = (G[7] + ((G[6] << 8) | (G[6] >>> 24)) + G[5]) | 0;
        };
        C.jG = dC.jx(jG);
    }());
    aX.mode.gM = (function() {
        var gM = aX.bf.eL.extend();
        var cB = gM.cB = gM.extend({
            co: function(bq, offset) {
                var aT = this.cd;
                var bB = aT.bB;
                var bP = this.cQ;
                var counter = this.jY;
                if(bP) {
                    counter = this.jY = bP.slice(0);
                    this.cQ = undefined;
                }
                var cP = counter.slice(0);
                aT.fL(cP, 0);
                counter[bB - 1] = (counter[bB - 1] + 1) | 0;
                for(var i = 0; i < bB; i++) {
                    bq[offset + i] ^= cP[i];
                }
            }
        });
        gM.eZ = cB;
        return gM;
    }());
    (function() {
        var C = aX;
        var bC = C.bf;
        var dC = bC.dC;
        var cw = C.jg;
        var S = [];
        var aJ = [];
        var G = [];
        var kS = cw.kS = dC.extend({
            jf: function() {
                var K = this.hJ.bq;
                var bP = this.aL.bP;
                var X = this.hC = [K[0], (K[3] << 16) | (K[2] >>> 16), K[1], (K[0] << 16) | (K[3] >>> 16), K[2], (K[1] << 16) | (K[0] >>> 16), K[3], (K[2] << 16) | (K[1] >>> 16)];
                var C = this.kt = [(K[2] << 16) | (K[2] >>> 16), (K[0] & 0xffff0000) | (K[1] & 0x0000ffff), (K[3] << 16) | (K[3] >>> 16), (K[1] & 0xffff0000) | (K[2] & 0x0000ffff), (K[0] << 16) | (K[0] >>> 16), (K[2] & 0xffff0000) | (K[3] & 0x0000ffff), (K[1] << 16) | (K[1] >>> 16), (K[3] & 0xffff0000) | (K[0] & 0x0000ffff)];
                this.fP = 0;
                for(var i = 0; i < 4; i++) {
                    eQ.call(this);
                }
                for(var i = 0; i < 8; i++) {
                    C[i] ^= X[(i + 4) & 7];
                }
                if(bP) {
                    var hD = bP.bq;
                    var cz = hD[0];
                    var dz = hD[1];
                    var dc = (((cz << 8) | (cz >>> 24)) & 0x00ff00ff) | (((cz << 24) | (cz >>> 8)) & 0xff00ff00);
                    var cN = (((dz << 8) | (dz >>> 24)) & 0x00ff00ff) | (((dz << 24) | (dz >>> 8)) & 0xff00ff00);
                    var fQ = (dc >>> 16) | (cN & 0xffff0000);
                    var hH = (cN << 16) | (dc & 0x0000ffff);
                    C[0] ^= dc;
                    C[1] ^= fQ;
                    C[2] ^= cN;
                    C[3] ^= hH;
                    C[4] ^= dc;
                    C[5] ^= fQ;
                    C[6] ^= cN;
                    C[7] ^= hH;
                    for(var i = 0; i < 4; i++) {
                        eQ.call(this);
                    }
                }
            },
            kq: function(M, offset) {
                var X = this.hC;
                eQ.call(this);
                S[0] = X[0] ^ (X[5] >>> 16) ^ (X[3] << 16);
                S[1] = X[2] ^ (X[7] >>> 16) ^ (X[5] << 16);
                S[2] = X[4] ^ (X[1] >>> 16) ^ (X[7] << 16);
                S[3] = X[6] ^ (X[3] >>> 16) ^ (X[1] << 16);
                for(var i = 0; i < 4; i++) {
                    S[i] = (((S[i] << 8) | (S[i] >>> 24)) & 0x00ff00ff) | (((S[i] << 24) | (S[i] >>> 8)) & 0xff00ff00);
                    M[offset + i] ^= S[i];
                }
            },
            bB: 128 / 32,
            fN: 64 / 32
        });

        function eQ() {
            var X = this.hC;
            var C = this.kt;
            for(var i = 0; i < 8; i++) {
                aJ[i] = C[i];
            }
            C[0] = (C[0] + 0x4d34d34d + this.fP) | 0;
            C[1] = (C[1] + 0xd34d34d3 + ((C[0] >>> 0) < (aJ[0] >>> 0) ? 1 : 0)) | 0;
            C[2] = (C[2] + 0x34d34d34 + ((C[1] >>> 0) < (aJ[1] >>> 0) ? 1 : 0)) | 0;
            C[3] = (C[3] + 0x4d34d34d + ((C[2] >>> 0) < (aJ[2] >>> 0) ? 1 : 0)) | 0;
            C[4] = (C[4] + 0xd34d34d3 + ((C[3] >>> 0) < (aJ[3] >>> 0) ? 1 : 0)) | 0;
            C[5] = (C[5] + 0x34d34d34 + ((C[4] >>> 0) < (aJ[4] >>> 0) ? 1 : 0)) | 0;
            C[6] = (C[6] + 0x4d34d34d + ((C[5] >>> 0) < (aJ[5] >>> 0) ? 1 : 0)) | 0;
            C[7] = (C[7] + 0xd34d34d3 + ((C[6] >>> 0) < (aJ[6] >>> 0) ? 1 : 0)) | 0;
            this.fP = (C[7] >>> 0) < (aJ[7] >>> 0) ? 1 : 0;
            for(var i = 0; i < 8; i++) {
                var da = X[i] + C[i];
                var eS = da & 0xffff;
                var dX = da >>> 16;
                var jE = ((((eS * eS) >>> 17) + eS * dX) >>> 15) + dX * dX;
                var jc = (((da & 0xffff0000) * da) | 0) + (((da & 0x0000ffff) * da) | 0);
                G[i] = jE ^ jc;
            }
            X[0] = (G[0] + ((G[7] << 16) | (G[7] >>> 16)) + ((G[6] << 16) | (G[6] >>> 16))) | 0;
            X[1] = (G[1] + ((G[0] << 8) | (G[0] >>> 24)) + G[7]) | 0;
            X[2] = (G[2] + ((G[1] << 16) | (G[1] >>> 16)) + ((G[0] << 16) | (G[0] >>> 16))) | 0;
            X[3] = (G[3] + ((G[2] << 8) | (G[2] >>> 24)) + G[1]) | 0;
            X[4] = (G[4] + ((G[3] << 16) | (G[3] >>> 16)) + ((G[2] << 16) | (G[2] >>> 16))) | 0;
            X[5] = (G[5] + ((G[4] << 8) | (G[4] >>> 24)) + G[3]) | 0;
            X[6] = (G[6] + ((G[5] << 16) | (G[5] >>> 16)) + ((G[4] << 16) | (G[4] >>> 16))) | 0;
            X[7] = (G[7] + ((G[6] << 8) | (G[6] >>> 24)) + G[5]) | 0;
        };
        C.kS = dC.jx(kS);
    }());
    aX.bH.oQ = {
        bH: function(data, bB) {
            var cq = bB * 4;
            data.ag();
            data.aI += cq - ((data.aI % cq) || cq);
        },
        es: function(data) {
            var jk = data.bq;
            var i = data.aI - 1;
            while(!((jk[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff)) {
                i--;
            }
            data.aI = i + 1;
        }
    };
    return aX;
}));
var lT = {};
(function(fg) {
    var fC;
    var wr = 0xdeadbeefcafe;
    var mT = ((wr & 0xffffff) == 0xefcafe);

    function O(a, b, c) {
        if(a != null)
            if("number" == typeof a) this.ki(a, b, c);
            else if(b == null && "string" != typeof a) this.gw(a, 256);
            else this.gw(a, b);
    };

    function aO() {
        return new O(null);
    };

    function pD(i, x, w, j, c, n) {
        while(--n >= 0) {
            var v = x * this[i++] + w[j] + c;
            c = Math.floor(v / 0x4000000);
            w[j++] = v & 0x3ffffff;
        }
        return c;
    };

    function sc(i, x, w, j, c, n) {
        var gE = x & 0x7fff,
            fR = x >> 15;
        while(--n >= 0) {
            var l = this[i] & 0x7fff;
            var h = this[i++] >> 15;
            var m = fR * l + h * gE;
            l = gE * l + ((m & 0x7fff) << 15) + w[j] + (c & 0x3fffffff);
            c = (l >>> 30) + (m >>> 15) + fR * h + (c >>> 30);
            w[j++] = l & 0x3fffffff;
        }
        return c;
    };

    function rN(i, x, w, j, c, n) {
        var gE = x & 0x3fff,
            fR = x >> 14;
        while(--n >= 0) {
            var l = this[i] & 0x3fff;
            var h = this[i++] >> 14;
            var m = fR * l + h * gE;
            l = gE * l + ((m & 0x3fff) << 14) + w[j] + c;
            c = (l >> 28) + (m >> 14) + fR * h;
            w[j++] = l & 0xfffffff;
        }
        return c;
    };
    if(mT && (navigator.appName == "Microsoft Internet Explorer")) {
        O.prototype.dN = sc;
        fC = 30;
    } else if(mT && (navigator.appName != "Netscape")) {
        O.prototype.dN = pD;
        fC = 26;
    } else {
        O.prototype.dN = rN;
        fC = 28;
    }
    O.prototype.bb = fC;
    O.prototype.aW = ((1 << fC) - 1);
    O.prototype.dq = (1 << fC);
    var lu = 52;
    O.prototype.wY = Math.pow(2, lu);
    O.prototype.oK = lu - fC;
    O.prototype.oM = 2 * fC - lu;
    var vW = "0123456789abcdefghijklmnopqrstuvwxyz";
    var ix = new Array();
    var eA, cu;
    eA = "0".charCodeAt(0);
    for(cu = 0; cu <= 9; ++cu) ix[eA++] = cu;
    eA = "a".charCodeAt(0);
    for(cu = 10; cu < 36; ++cu) ix[eA++] = cu;
    eA = "A".charCodeAt(0);
    for(cu = 10; cu < 36; ++cu) ix[eA++] = cu;

    function dE(n) {
        return vW.charAt(n);
    };

    function mE(s, i) {
        var c = ix[s.charCodeAt(i)];
        return(c == null) ? -1 : c;
    };

    function sN(r) {
        for(var i = this.t - 1; i >= 0; --i) r[i] = this[i];
        r.t = this.t;
        r.s = this.s;
    };

    function qf(x) {
        this.t = 1;
        this.s = (x < 0) ? -1 : 0;
        if(x > 0) this[0] = x;
        else if(x < -1) this[0] = x + this.dq;
        else this.t = 0;
    };

    function dV(i) {
        var r = aO();
        r.gr(i);
        return r;
    };

    function pR(s, b) {
        var k;
        if(b == 16) k = 4;
        else if(b == 8) k = 3;
        else if(b == 256) k = 8;
        else if(b == 2) k = 1;
        else if(b == 32) k = 5;
        else if(b == 4) k = 2;
        else {
            this.wq(s, b);
            return;
        }
        this.t = 0;
        this.s = 0;
        var i = s.length,
            eP = false,
            sh = 0;
        while(--i >= 0) {
            var x = (k == 8) ? s[i] & 0xff : mE(s, i);
            if(x < 0) {
                if(s.charAt(i) == "-") eP = true;
                continue;
            }
            eP = false;
            if(sh == 0) this[this.t++] = x;
            else if(sh + k > this.bb) {
                this[this.t - 1] |= (x & ((1 << (this.bb - sh)) - 1)) << sh;
                this[this.t++] = (x >> (this.bb - sh));
            } else this[this.t - 1] |= x << sh;
            sh += k;
            if(sh >= this.bb) sh -= this.bb;
        }
        if(k == 8 && (s[0] & 0x80) != 0) {
            this.s = -1;
            if(sh > 0) this[this.t - 1] |= ((1 << (this.bb - sh)) - 1) << sh;
        }
        this.ag();
        if(eP) O.ZERO.aj(this, this);
    };

    function sr() {
        var c = this.s & this.aW;
        while(this.t > 0 && this[this.t - 1] == c) --this.t;
    };

    function sg(b) {
        if(this.s < 0) return "-" + this.fF().toString(b);
        var k;
        if(b == 16) k = 4;
        else if(b == 8) k = 3;
        else if(b == 2) k = 1;
        else if(b == 32) k = 5;
        else if(b == 4) k = 2;
        else return this.tj(b);
        var jt = (1 << k) - 1,
            d, m = false,
            r = "",
            i = this.t;
        var p = this.bb - (i * this.bb) % k;
        if(i-- > 0) {
            if(p < this.bb && (d = this[i] >> p) > 0) {
                m = true;
                r = dE(d);
            }
            while(i >= 0) {
                if(p < k) {
                    d = (this[i] & ((1 << p) - 1)) << (k - p);
                    d |= this[--i] >> (p += this.bb - k);
                } else {
                    d = (this[i] >> (p -= k)) & jt;
                    if(p <= 0) {
                        p += this.bb;
                        --i;
                    }
                }
                if(d > 0) m = true;
                if(m) r += dE(d);
            }
        }
        return m ? r : "0";
    };

    function pF() {
        var r = aO();
        O.ZERO.aj(this, r);
        return r;
    };

    function rG() {
        return(this.s < 0) ? this.fF() : this;
    };

    function uq(a) {
        var r = this.s - a.s;
        if(r != 0) return r;
        var i = this.t;
        r = i - a.t;
        if(r != 0) return(this.s < 0) ? -r : r;
        while(--i >= 0)
            if((r = this[i] - a[i]) != 0) return r;
        return 0;
    };

    function ij(x) {
        var r = 1,
            t;
        if((t = x >>> 16) != 0) {
            x = t;
            r += 16;
        }
        if((t = x >> 8) != 0) {
            x = t;
            r += 8;
        }
        if((t = x >> 4) != 0) {
            x = t;
            r += 4;
        }
        if((t = x >> 2) != 0) {
            x = t;
            r += 2;
        }
        if((t = x >> 1) != 0) {
            x = t;
            r += 1;
        }
        return r;
    };

    function rK() {
        if(this.t <= 0) return 0;
        return this.bb * (this.t - 1) + ij(this[this.t - 1] ^ (this.s & this.aW));
    };

    function qD(n, r) {
        var i;
        for(i = this.t - 1; i >= 0; --i) r[i + n] = this[i];
        for(i = n - 1; i >= 0; --i) r[i] = 0;
        r.t = this.t + n;
        r.s = this.s;
    };

    function rg(n, r) {
        for(var i = n; i < this.t; ++i) r[i - n] = this[i];
        r.t = Math.max(this.t - n, 0);
        r.s = this.s;
    };

    function qZ(n, r) {
        var cO = n % this.bb;
        var gc = this.bb - cO;
        var iJ = (1 << gc) - 1;
        var cX = Math.floor(n / this.bb),
            c = (this.s << cO) & this.aW,
            i;
        for(i = this.t - 1; i >= 0; --i) {
            r[i + cX + 1] = (this[i] >> gc) | c;
            c = (this[i] & iJ) << cO;
        }
        for(i = cX - 1; i >= 0; --i) r[i] = 0;
        r[cX] = c;
        r.t = this.t + cX + 1;
        r.s = this.s;
        r.ag();
    };

    function sm(n, r) {
        r.s = this.s;
        var cX = Math.floor(n / this.bb);
        if(cX >= this.t) {
            r.t = 0;
            return;
        }
        var cO = n % this.bb;
        var gc = this.bb - cO;
        var iJ = (1 << cO) - 1;
        r[0] = this[cX] >> cO;
        for(var i = cX + 1; i < this.t; ++i) {
            r[i - cX - 1] |= (this[i] & iJ) << gc;
            r[i - cX] = this[i] >> cO;
        }
        if(cO > 0) r[this.t - cX - 1] |= (this.s & iJ) << gc;
        r.t = this.t - cX;
        r.ag();
    };

    function sk(a, r) {
        var i = 0,
            c = 0,
            m = Math.min(a.t, this.t);
        while(i < m) {
            c += this[i] - a[i];
            r[i++] = c & this.aW;
            c >>= this.bb;
        }
        if(a.t < this.t) {
            c -= a.s;
            while(i < this.t) {
                c += this[i];
                r[i++] = c & this.aW;
                c >>= this.bb;
            }
            c += this.s;
        } else {
            c += this.s;
            while(i < a.t) {
                c -= a[i];
                r[i++] = c & this.aW;
                c >>= this.bb;
            }
            c -= a.s;
        }
        r.s = (c < 0) ? -1 : 0;
        if(c < -1) r[i++] = this.dq + c;
        else if(c > 0) r[i++] = c;
        r.t = i;
        r.ag();
    };

    function sT(a, r) {
        var x = this.abs(),
            y = a.abs();
        var i = x.t;
        r.t = i + y.t;
        while(--i >= 0) r[i] = 0;
        for(i = 0; i < y.t; ++i) r[i + x.t] = x.dN(0, y[i], r, i, 0, x.t);
        r.s = 0;
        r.ag();
        if(this.s != a.s) O.ZERO.aj(r, r);
    };

    function st(r) {
        var x = this.abs();
        var i = r.t = 2 * x.t;
        while(--i >= 0) r[i] = 0;
        for(i = 0; i < x.t - 1; ++i) {
            var c = x.dN(i, x[i], r, 2 * i, 0, 1);
            if((r[i + x.t] += x.dN(i + 1, 2 * x[i], r, 2 * i + 1, c, x.t - i - 1)) >= x.dq) {
                r[i + x.t] -= x.dq;
                r[i + x.t + 1] = 1;
            }
        }
        if(r.t > 0) r[r.t - 1] += x.dN(i, x[i], r, 2 * i, 0, 1);
        r.s = 0;
        r.ag();
    };

    function qk(m, q, r) {
        var pm = m.abs();
        if(pm.t <= 0) return;
        var pt = this.abs();
        if(pt.t < pm.t) {
            if(q != null) q.gr(0);
            if(r != null) this.eB(r);
            return;
        }
        if(r == null) r = aO();
        var y = aO(),
            pq = this.s,
            ms = m.s;
        var hj = this.bb - ij(pm[pm.t - 1]);
        if(hj > 0) {
            pm.fB(hj, y);
            pt.fB(hj, r);
        } else {
            pm.eB(y);
            pt.eB(r);
        }
        var cE = y.t;
        var ik = y[cE - 1];
        if(ik == 0) return;
        var nN = ik * (1 << this.oK) + ((cE > 1) ? y[cE - 2] >> this.oM : 0);
        var xG = this.wY / nN,
            yn = (1 << this.oK) / nN,
            e = 1 << this.oM;
        var i = r.t,
            j = i - cE,
            t = (q == null) ? aO() : q;
        y.gG(j, t);
        if(r.az(t) >= 0) {
            r[r.t++] = 1;
            r.aj(t, r);
        }
        O.ONE.gG(cE, t);
        t.aj(y, y);
        while(y.t < cE) y[y.t++] = 0;
        while(--j >= 0) {
            var jL = (r[--i] == ik) ? this.aW : Math.floor(r[i] * xG + (r[i - 1] + e) * yn);
            if((r[i] += y.dN(0, jL, r, j, 0, cE)) < jL) {
                y.gG(j, t);
                r.aj(t, r);
                while(r[i] < --jL) r.aj(t, r);
            }
        }
        if(q != null) {
            r.gk(cE, q);
            if(pq != ms) O.ZERO.aj(q, q);
        }
        r.t = cE;
        r.ag();
        if(hj > 0) r.aD(hj, r);
        if(pq < 0) O.ZERO.aj(r, r);
    };

    function sD(a) {
        var r = aO();
        this.abs().ey(a, null, r);
        if(this.s < 0 && r.az(O.ZERO) > 0) a.aj(r, r);
        return r;
    };

    function ew(m) {
        this.m = m;
    };

    function rS(x) {
        if(x.s < 0 || x.az(this.m) >= 0) return x.dn(this.m);
        else return x;
    };

    function sS(x) {
        return x;
    };

    function sj(x) {
        x.ey(this.m, null, x);
    };

    function rZ(x, y, r) {
        x.fW(y, r);
        this.reduce(r);
    };

    function rF(x, r) {
        x.gL(r);
        this.reduce(r);
    };
    ew.prototype.convert = rS;
    ew.prototype.revert = sS;
    ew.prototype.reduce = sj;
    ew.prototype.eb = rZ;
    ew.prototype.cL = rF;

    function rm() {
        if(this.t < 1) return 0;
        var x = this[0];
        if((x & 1) == 0) return 0;
        var y = x & 3;
        y = (y * (2 - (x & 0xf) * y)) & 0xf;
        y = (y * (2 - (x & 0xff) * y)) & 0xff;
        y = (y * (2 - (((x & 0xffff) * y) & 0xffff))) & 0xffff;
        y = (y * (2 - x * y % this.dq)) % this.dq;
        return(y > 0) ? this.dq - y : -y;
    };

    function dY(m) {
        this.m = m;
        this.mQ = m.wc();
        this.mF = this.mQ & 0x7fff;
        this.wf = this.mQ >> 15;
        this.sL = (1 << (m.bb - 15)) - 1;
        this.yF = 2 * m.t;
    };

    function pf(x) {
        var r = aO();
        x.abs().gG(this.m.t, r);
        r.ey(this.m, null, r);
        if(x.s < 0 && r.az(O.ZERO) > 0) this.m.aj(r, r);
        return r;
    };

    function qz(x) {
        var r = aO();
        x.eB(r);
        this.reduce(r);
        return r;
    };

    function pN(x) {
        while(x.t <= this.yF) x[x.t++] = 0;
        for(var i = 0; i < this.m.t; ++i) {
            var j = x[i] & 0x7fff;
            var tc = (j * this.mF + (((j * this.wf + (x[i] >> 15) * this.mF) & this.sL) << 15)) & x.aW;
            j = i + this.m.t;
            x[j] += this.m.dN(0, tc, x, i, 0, this.m.t);
            while(x[j] >= x.dq) {
                x[j] -= x.dq;
                x[++j]++;
            }
        }
        x.ag();
        x.gk(this.m.t, x);
        if(x.az(this.m) >= 0) x.aj(this.m, x);
    };

    function qy(x, r) {
        x.gL(r);
        this.reduce(r);
    };

    function ov(x, y, r) {
        x.fW(y, r);
        this.reduce(r);
    };
    dY.prototype.convert = pf;
    dY.prototype.revert = qz;
    dY.prototype.reduce = pN;
    dY.prototype.eb = ov;
    dY.prototype.cL = qy;

    function qB() {
        return((this.t > 0) ? (this[0] & 1) : this.s) == 0;
    };

    function qK(e, z) {
        if(e > 0xffffffff || e < 1) return O.ONE;
        var r = aO(),
            r2 = aO(),
            g = z.convert(this),
            i = ij(e) - 1;
        g.eB(r);
        while(--i >= 0) {
            z.cL(r, r2);
            if((e & (1 << i)) > 0) z.eb(r2, g, r);
            else {
                var t = r;
                r = r2;
                r2 = t;
            }
        }
        return z.revert(r);
    };

    function rO(e, m) {
        var z;
        if(e < 256 || m.cU()) z = new ew(m);
        else z = new dY(m);
        return this.exp(e, z);
    };
    O.prototype.eB = sN;
    O.prototype.gr = qf;
    O.prototype.gw = pR;
    O.prototype.ag = sr;
    O.prototype.gG = qD;
    O.prototype.gk = rg;
    O.prototype.fB = qZ;
    O.prototype.aD = sm;
    O.prototype.aj = sk;
    O.prototype.fW = sT;
    O.prototype.gL = st;
    O.prototype.ey = qk;
    O.prototype.wc = rm;
    O.prototype.cU = qB;
    O.prototype.exp = qK;
    O.prototype.toString = sg;
    O.prototype.fF = pF;
    O.prototype.abs = rG;
    O.prototype.az = uq;
    O.prototype.ho = rK;
    O.prototype.dn = sD;
    O.prototype.mk = rO;
    O.ZERO = dV(0);
    O.ONE = dV(1);

    function uy() {
        var r = aO();
        this.eB(r);
        return r;
    };

    function sO() {
        if(this.s < 0) {
            if(this.t == 1) return this[0] - this.dq;
            else if(this.t == 0) return -1;
        } else if(this.t == 1) return this[0];
        else if(this.t == 0) return 0;
        return((this[1] & ((1 << (32 - this.bb)) - 1)) << this.bb) | this[0];
    };

    function ss() {
        return(this.t == 0) ? this.s : (this[0] << 24) >> 24;
    };

    function sY() {
        return(this.t == 0) ? this.s : (this[0] << 16) >> 16;
    };

    function sb(r) {
        return Math.floor(Math.LN2 * this.bb / Math.log(r));
    };

    function si() {
        if(this.s < 0) return -1;
        else if(this.t <= 0 || (this.t == 1 && this[0] <= 0)) return 0;
        else return 1;
    };

    function rI(b) {
        if(b == null) b = 10;
        if(this.dh() == 0 || b < 2 || b > 36) return "0";
        var cs = this.lL(b);
        var a = Math.pow(b, cs);
        var d = dV(a),
            y = aO(),
            z = aO(),
            r = "";
        this.ey(d, y, z);
        while(y.dh() > 0) {
            r = (a + z.hy()).toString(b).substr(1) + r;
            y.ey(d, y, z);
        }
        return z.hy().toString(b) + r;
    };

    function qP(s, b) {
        this.gr(0);
        if(b == null) b = 10;
        var cs = this.lL(b);
        var d = Math.pow(b, cs),
            eP = false,
            j = 0,
            w = 0;
        for(var i = 0; i < s.length; ++i) {
            var x = mE(s, i);
            if(x < 0) {
                if(s.charAt(i) == "-" && this.dh() == 0) eP = true;
                continue;
            }
            w = b * w + x;
            if(++j >= cs) {
                this.oa(d);
                this.eD(w, 0);
                j = 0;
                w = 0;
            }
        }
        if(j > 0) {
            this.oa(Math.pow(b, j));
            this.eD(w, 0);
        }
        if(eP) O.ZERO.aj(this, this);
    };

    function qW(a, b, c) {
        if("number" == typeof b) {
            if(a < 2) this.gr(1);
            else {
                this.ki(a, c);
                if(!this.lZ(a - 1)) this.ff(O.ONE.shiftLeft(a - 1), jC, this);
                if(this.cU()) this.eD(1, 0);
                while(!this.gJ(b)) {
                    this.eD(2, 0);
                    if(this.ho() > a) this.aj(O.ONE.shiftLeft(a - 1), this);
                }
            }
        } else {
            var x = new Array(),
                t = a & 7;
            x.length = (a >> 3) + 1;
            b.lp(x);
            if(t > 0) x[0] &= ((1 << t) - 1);
            else x[0] = 0;
            this.gw(x, 256);
        }
    };

    function rJ() {
        var i = this.t,
            r = new Array();
        r[0] = this.s;
        var p = this.bb - (i * this.bb) % 8,
            d, k = 0;
        if(i-- > 0) {
            if(p < this.bb && (d = this[i] >> p) != (this.s & this.aW) >> p) r[k++] = d | (this.s << (this.bb - p));
            while(i >= 0) {
                if(p < 8) {
                    d = (this[i] & ((1 << p) - 1)) << (8 - p);
                    d |= this[--i] >> (p += this.bb - 8);
                } else {
                    d = (this[i] >> (p -= 8)) & 0xff;
                    if(p <= 0) {
                        p += this.bb;
                        --i;
                    }
                }
                if((d & 0x80) != 0) d |= -256;
                if(k == 0 && (this.s & 0x80) != (d & 0x80)) ++k;
                if(k > 0 || d != this.s) r[k++] = d;
            }
        }
        return r;
    };

    function tA(a) {
        return(this.az(a) == 0);
    };

    function te(a) {
        return(this.az(a) < 0) ? this : a;
    };

    function rz(a) {
        return(this.az(a) > 0) ? this : a;
    };

    function pZ(a, ej, r) {
        var i, f, m = Math.min(a.t, this.t);
        for(i = 0; i < m; ++i) r[i] = ej(this[i], a[i]);
        if(a.t < this.t) {
            f = a.s & this.aW;
            for(i = m; i < this.t; ++i) r[i] = ej(this[i], f);
            r.t = this.t;
        } else {
            f = this.s & this.aW;
            for(i = m; i < a.t; ++i) r[i] = ej(f, a[i]);
            r.t = a.t;
        }
        r.s = ej(this.s, a.s);
        r.ag();
    };

    function ri(x, y) {
        return x & y;
    };

    function sM(a) {
        var r = aO();
        this.ff(a, ri, r);
        return r;
    };

    function jC(x, y) {
        return x | y;
    };

    function qI(a) {
        var r = aO();
        this.ff(a, jC, r);
        return r;
    };

    function oz(x, y) {
        return x ^ y;
    };

    function sC(a) {
        var r = aO();
        this.ff(a, oz, r);
        return r;
    };

    function oS(x, y) {
        return x & ~y;
    };

    function sU(a) {
        var r = aO();
        this.ff(a, oS, r);
        return r;
    };

    function qc() {
        var r = aO();
        for(var i = 0; i < this.t; ++i) r[i] = this.aW & ~this[i];
        r.t = this.t;
        r.s = ~this.s;
        return r;
    };

    function rv(n) {
        var r = aO();
        if(n < 0) this.aD(-n, r);
        else this.fB(n, r);
        return r;
    };

    function sQ(n) {
        var r = aO();
        if(n < 0) this.fB(-n, r);
        else this.aD(n, r);
        return r;
    };

    function nB(x) {
        if(x == 0) return -1;
        var r = 0;
        if((x & 0xffff) == 0) {
            x >>= 16;
            r += 16;
        }
        if((x & 0xff) == 0) {
            x >>= 8;
            r += 8;
        }
        if((x & 0xf) == 0) {
            x >>= 4;
            r += 4;
        }
        if((x & 3) == 0) {
            x >>= 2;
            r += 2;
        }
        if((x & 1) == 0) ++r;
        return r;
    };

    function sy() {
        for(var i = 0; i < this.t; ++i)
            if(this[i] != 0) return i * this.bb + nB(this[i]);
        if(this.s < 0) return this.t * this.bb;
        return -1;
    };

    function ta(x) {
        var r = 0;
        while(x != 0) {
            x &= x - 1;
            ++r;
        }
        return r;
    };

    function rM() {
        var r = 0,
            x = this.s & this.aW;
        for(var i = 0; i < this.t; ++i) r += ta(this[i] ^ x);
        return r;
    };

    function sH(n) {
        var j = Math.floor(n / this.bb);
        if(j >= this.t) return(this.s != 0);
        return((this[j] & (1 << (n % this.bb))) != 0);
    };

    function tk(n, ej) {
        var r = O.ONE.shiftLeft(n);
        this.ff(r, ej, r);
        return r;
    };

    function rq(n) {
        return this.jP(n, jC);
    };

    function uT(n) {
        return this.jP(n, oS);
    };

    function tS(n) {
        return this.jP(n, oz);
    };

    function pO(a, r) {
        var i = 0,
            c = 0,
            m = Math.min(a.t, this.t);
        while(i < m) {
            c += this[i] + a[i];
            r[i++] = c & this.aW;
            c >>= this.bb;
        }
        if(a.t < this.t) {
            c += a.s;
            while(i < this.t) {
                c += this[i];
                r[i++] = c & this.aW;
                c >>= this.bb;
            }
            c += this.s;
        } else {
            c += this.s;
            while(i < a.t) {
                c += a[i];
                r[i++] = c & this.aW;
                c >>= this.bb;
            }
            c += a.s;
        }
        r.s = (c < 0) ? -1 : 0;
        if(c > 0) r[i++] = c;
        else if(c < -1) r[i++] = this.dq + c;
        r.t = i;
        r.ag();
    };

    function sX(a) {
        var r = aO();
        this.jh(a, r);
        return r;
    };

    function sl(a) {
        var r = aO();
        this.aj(a, r);
        return r;
    };

    function sV(a) {
        var r = aO();
        this.fW(a, r);
        return r;
    };

    function sR() {
        var r = aO();
        this.gL(r);
        return r;
    };

    function su(a) {
        var r = aO();
        this.ey(a, r, null);
        return r;
    };

    function rH(a) {
        var r = aO();
        this.ey(a, null, r);
        return r;
    };

    function uI(a) {
        var q = aO(),
            r = aO();
        this.ey(a, q, r);
        return new Array(q, r);
    };

    function qt(n) {
        this[this.t] = this.dN(0, n - 1, this, 0, 0, this.t);
        ++this.t;
        this.ag();
    };

    function qH(n, w) {
        if(n == 0) return;
        while(this.t <= w) this[this.t++] = 0;
        this[w] += n;
        while(this[w] >= this.dq) {
            this[w] -= this.dq;
            if(++w >= this.t) this[this.t++] = 0;
            ++this[w];
        }
    };

    function gR() {};

    function oW(x) {
        return x;
    };

    function pL(x, y, r) {
        x.fW(y, r);
    };

    function qr(x, r) {
        x.gL(r);
    };
    gR.prototype.convert = oW;
    gR.prototype.revert = oW;
    gR.prototype.eb = pL;
    gR.prototype.cL = qr;

    function tg(e) {
        return this.exp(e, new gR());
    };

    function rs(a, n, r) {
        var i = Math.min(this.t + a.t, n);
        r.s = 0;
        r.t = i;
        while(i > 0) r[--i] = 0;
        var j;
        for(j = r.t - this.t; i < j; ++i) r[i + this.t] = this.dN(0, a[i], r, i, 0, this.t);
        for(j = Math.min(a.t, n); i < j; ++i) this.dN(0, a[i], r, i, 0, n - i);
        r.ag();
    };

    function rU(a, n, r) {
        --n;
        var i = r.t = this.t + a.t - n;
        r.s = 0;
        while(--i >= 0) r[i] = 0;
        for(i = Math.max(n - this.t, 0); i < a.t; ++i) r[this.t + i - n] = this.dN(n - i, a[i], r, 0, 0, this.t + i - n);
        r.ag();
        r.gk(1, r);
    };

    function dW(m) {
        this.r2 = aO();
        this.nj = aO();
        O.ONE.gG(2 * m.t, this.r2);
        this.xB = this.r2.divide(m);
        this.m = m;
    };

    function rC(x) {
        if(x.s < 0 || x.t > 2 * this.m.t) return x.dn(this.m);
        else if(x.az(this.m) < 0) return x;
        else {
            var r = aO();
            x.eB(r);
            this.reduce(r);
            return r;
        }
    };

    function sf(x) {
        return x;
    };

    function ru(x) {
        x.gk(this.m.t - 1, this.r2);
        if(x.t > this.m.t + 1) {
            x.t = this.m.t + 1;
            x.ag();
        }
        this.xB.yA(this.r2, this.m.t + 1, this.nj);
        this.m.yk(this.nj, this.m.t + 1, this.r2);
        while(x.az(this.r2) < 0) x.eD(1, this.m.t + 1);
        x.aj(this.r2, x);
        while(x.az(this.m) >= 0) x.aj(this.m, x);
    };

    function tf(x, r) {
        x.gL(r);
        this.reduce(r);
    };

    function sB(x, y, r) {
        x.fW(y, r);
        this.reduce(r);
    };
    dW.prototype.convert = rC;
    dW.prototype.revert = sf;
    dW.prototype.reduce = ru;
    dW.prototype.eb = sB;
    dW.prototype.cL = tf;

    function sd(e, m) {
        var i = e.ho(),
            k, r = dV(1),
            z;
        if(i <= 0) return r;
        else if(i < 18) k = 1;
        else if(i < 48) k = 3;
        else if(i < 144) k = 4;
        else if(i < 768) k = 5;
        else k = 6;
        if(i < 8) z = new ew(m);
        else if(m.cU()) z = new dW(m);
        else z = new dY(m);
        var g = new Array(),
            n = 3,
            fY = k - 1,
            jt = (1 << k) - 1;
        g[1] = z.convert(this);
        if(k > 1) {
            var oi = aO();
            z.cL(g[1], oi);
            while(n <= jt) {
                g[n] = aO();
                z.eb(oi, g[n - 2], g[n]);
                n += 2;
            }
        }
        var j = e.t - 1,
            w, nC = true,
            r2 = aO(),
            t;
        i = ij(e[j]) - 1;
        while(j >= 0) {
            if(i >= fY) w = (e[j] >> (i - fY)) & jt;
            else {
                w = (e[j] & ((1 << (i + 1)) - 1)) << (fY - i);
                if(j > 0) w |= e[j - 1] >> (this.bb + i - fY);
            }
            n = k;
            while((w & 1) == 0) {
                w >>= 1;
                --n;
            }
            if((i -= n) < 0) {
                i += this.bb;
                --j;
            }
            if(nC) {
                g[w].eB(r);
                nC = false;
            } else {
                while(n > 1) {
                    z.cL(r, r2);
                    z.cL(r2, r);
                    n -= 2;
                }
                if(n > 0) z.cL(r, r2);
                else {
                    t = r;
                    r = r2;
                    r2 = t;
                }
                z.eb(r2, g[w], r);
            }
            while(j >= 0 && (e[j] & (1 << i)) == 0) {
                z.cL(r, r2);
                t = r;
                r = r2;
                r2 = t;
                if(--i < 0) {
                    i = this.bb - 1;
                    --j;
                }
            }
        }
        return z.revert(r);
    };

    function rV(a) {
        var x = (this.s < 0) ? this.fF() : this.clone();
        var y = (a.s < 0) ? a.fF() : a.clone();
        if(x.az(y) < 0) {
            var t = x;
            x = y;
            y = t;
        }
        var i = x.cg(),
            g = y.cg();
        if(g < 0) return x;
        if(i < g) g = i;
        if(g > 0) {
            x.aD(g, x);
            y.aD(g, y);
        }
        while(x.dh() > 0) {
            if((i = x.cg()) > 0) x.aD(i, x);
            if((i = y.cg()) > 0) y.aD(i, y);
            if(x.az(y) >= 0) {
                x.aj(y, x);
                x.aD(1, x);
            } else {
                y.aj(x, y);
                y.aD(1, y);
            }
        }
        if(g > 0) y.fB(g, y);
        return y;
    };

    function pW(n) {
        if(n <= 0) return 0;
        var d = this.dq % n,
            r = (this.s < 0) ? n - 1 : 0;
        if(this.t > 0)
            if(d == 0) r = this[0] % n;
            else
                for(var i = this.t - 1; i >= 0; --i) r = (d * r + this[i]) % n;
        return r;
    };

    function sG(m) {
        var he = m.cU();
        if((this.cU() && he) || m.dh() == 0) return O.ZERO;
        var u = m.clone(),
            v = this.clone();
        var a = dV(1),
            b = dV(0),
            c = dV(0),
            d = dV(1);
        while(u.dh() != 0) {
            while(u.cU()) {
                u.aD(1, u);
                if(he) {
                    if(!a.cU() || !b.cU()) {
                        a.jh(this, a);
                        b.aj(m, b);
                    }
                    a.aD(1, a);
                } else if(!b.cU()) b.aj(m, b);
                b.aD(1, b);
            }
            while(v.cU()) {
                v.aD(1, v);
                if(he) {
                    if(!c.cU() || !d.cU()) {
                        c.jh(this, c);
                        d.aj(m, d);
                    }
                    c.aD(1, c);
                } else if(!d.cU()) d.aj(m, d);
                d.aD(1, d);
            }
            if(u.az(v) >= 0) {
                u.aj(v, u);
                if(he) a.aj(c, a);
                b.aj(d, b);
            } else {
                v.aj(u, v);
                if(he) c.aj(a, c);
                d.aj(b, d);
            }
        }
        if(v.az(O.ONE) != 0) return O.ZERO;
        if(d.az(m) >= 0) return d.cK(m);
        if(d.dh() < 0) d.jh(m, d);
        else return d;
        if(d.dh() < 0) return d.add(m);
        else return d;
    };
    var dR = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997];
    var wN = (1 << 26) / dR[dR.length - 1];

    function rY(t) {
        var i, x = this.abs();
        if(x.t == 1 && x[0] <= dR[dR.length - 1]) {
            for(i = 0; i < dR.length; ++i)
                if(x[0] == dR[i]) return true;
            return false;
        }
        if(x.cU()) return false;
        i = 1;
        while(i < dR.length) {
            var m = dR[i],
                j = i + 1;
            while(j < dR.length && m < wN) m *= dR[j++];
            m = x.wt(m);
            while(i < j)
                if(m % dR[i++] == 0) return false;
        }
        return x.vn(t);
    };

    function pM(t) {
        var gd = this.cK(O.ONE);
        var k = gd.cg();
        if(k <= 0) return false;
        var r = gd.sq(k);
        t = (t + 1) >> 1;
        if(t > dR.length) t = dR.length;
        var a = aO();
        for(var i = 0; i < t; ++i) {
            a.gr(dR[Math.floor(Math.random() * dR.length)]);
            var y = a.hZ(r, this);
            if(y.az(O.ONE) != 0 && y.az(gd) != 0) {
                var j = 1;
                while(j++ < k && y.az(gd) != 0) {
                    y = y.mk(2, this);
                    if(y.az(O.ONE) == 0) return false;
                }
                if(y.az(gd) != 0) return false;
            }
        }
        return true;
    };
    O.prototype.lL = sb;
    O.prototype.tj = rI;
    O.prototype.wq = qP;
    O.prototype.ki = qW;
    O.prototype.ff = pZ;
    O.prototype.jP = tk;
    O.prototype.jh = pO;
    O.prototype.oa = qt;
    O.prototype.eD = qH;
    O.prototype.yk = rs;
    O.prototype.yA = rU;
    O.prototype.wt = pW;
    O.prototype.vn = pM;
    O.prototype.clone = uy;
    O.prototype.hy = sO;
    O.prototype.AI = ss;
    O.prototype.yL = sY;
    O.prototype.dh = si;
    O.prototype.ti = rJ;
    O.prototype.zD = tA;
    O.prototype.min = te;
    O.prototype.max = rz;
    O.prototype.and = sM;
    O.prototype.ct = qI;
    O.prototype.xor = sC;
    O.prototype.xR = sU;
    O.prototype.not = qc;
    O.prototype.shiftLeft = rv;
    O.prototype.sq = sQ;
    O.prototype.cg = sy;
    O.prototype.zj = rM;
    O.prototype.lZ = sH;
    O.prototype.Ae = rq;
    O.prototype.AA = uT;
    O.prototype.xs = tS;
    O.prototype.add = sX;
    O.prototype.cK = sl;
    O.prototype.multiply = sV;
    O.prototype.divide = su;
    O.prototype.Af = rH;
    O.prototype.zA = uI;
    O.prototype.hZ = sd;
    O.prototype.ja = sG;
    O.prototype.pow = tg;
    O.prototype.hG = rV;
    O.prototype.gJ = rY;
    O.prototype.square = sR;

    function lh() {
        this.i = 0;
        this.j = 0;
        this.S = new Array();
    };

    function rW(key) {
        var i, j, t;
        for(i = 0; i < 256; ++i) this.S[i] = i;
        j = 0;
        for(i = 0; i < 256; ++i) {
            j = (j + this.S[i] + key[i % key.length]) & 255;
            t = this.S[i];
            this.S[i] = this.S[j];
            this.S[j] = t;
        }
        this.i = 0;
        this.j = 0;
    };

    function sF() {
        var t;
        this.i = (this.i + 1) & 255;
        this.j = (this.j + this.S[this.i]) & 255;
        t = this.S[this.i];
        this.S[this.i] = this.S[this.j];
        this.S[this.j] = t;
        return this.S[(t + this.S[this.i]) & 255];
    };
    lh.prototype.aV = rW;
    lh.prototype.next = sF;

    function rc() {
        return new lh();
    };
    var kT = 256;
    var iB;
    var eK;
    var cS;
    if(eK == null) {
        eK = new Array();
        cS = 0;
        var t;
        if(window.crypto && window.crypto.ww) {
            var z = new sI(256);
            window.crypto.ww(z);
            for(t = 0; t < z.length; ++t) eK[cS++] = z[t] & 255;
        }
        var jl = function(cv) {
            this.count = this.count || 0;
            if(this.count >= 256 || cS >= kT) {
                if(window.removeEventListener) window.removeEventListener("mousemove", jl);
                else if(window.detachEvent) window.detachEvent("onmousemove", jl);
                return;
            }
            this.count += 1;
            var vq = cv.x + cv.y;
            eK[cS++] = vq & 255;
        };
        if(window.addEventListener) window.addEventListener("mousemove", jl);
        else if(window.attachEvent) window.attachEvent("onmousemove", jl);
    }

    function pH() {
        if(iB == null) {
            iB = rc();
            while(cS < kT) {
                var random = Math.floor(65536 * Math.random());
                eK[cS++] = random & 255;
            }
            iB.aV(eK);
            for(cS = 0; cS < eK.length; ++cS) eK[cS] = 0;
            cS = 0;
        }
        return iB.next();
    };

    function pQ(dj) {
        var i;
        for(i = 0; i < dj.length; ++i) dj[i] = pH();
    };

    function js() {};
    js.prototype.lp = pQ;

    function bw(bS, r) {
        return new O(bS, r);
    };

    function yW(s, n) {
        var bk = "";
        var i = 0;
        while(i + n < s.length) {
            bk += s.substring(i, i + n) + "\n";
            i += n;
        }
        return bk + s.substring(i, s.length);
    };

    function zi(b) {
        if(b < 0x10) return "0" + b.toString(16);
        else return b.toString(16);
    };

    function pE(s, n) {
        if(n < s.length + 11) {
            console.error("Message too long for RSA");
            return null;
        }
        var dj = new Array();
        var i = s.length - 1;
        while(i >= 0 && n > 0) {
            var c = s.charCodeAt(i--);
            if(c < 128) {
                dj[--n] = c;
            } else if((c > 127) && (c < 2048)) {
                dj[--n] = (c & 63) | 128;
                dj[--n] = (c >> 6) | 192;
            } else {
                dj[--n] = (c & 63) | 128;
                dj[--n] = ((c >> 6) & 63) | 128;
                dj[--n] = (c >> 12) | 224;
            }
        }
        dj[--n] = 0;
        var fs = new js();
        var x = new Array();
        while(n > 2) {
            x[0] = 0;
            while(x[0] == 0) fs.lp(x);
            dj[--n] = x[0];
        }
        dj[--n] = 2;
        dj[--n] = 0;
        return new O(dj);
    };

    function ae() {
        this.n = null;
        this.e = 0;
        this.d = null;
        this.p = null;
        this.q = null;
        this.ca = null;
        this.dK = null;
        this.dr = null;
    };

    function qu(N, E) {
        if(N != null && E != null && N.length > 0 && E.length > 0) {
            this.n = bw(N, 16);
            this.e = parseInt(E, 16);
        } else console.error("Invalid RSA public key");
    };

    function ql(x) {
        return x.mk(this.e, this.n);
    };

    function pv(text) {
        var m = pE(text, (this.n.ho() + 7) >> 3);
        if(m == null) return null;
        var c = this.xa(m);
        if(c == null) return null;
        var h = c.toString(16);
        if((h.length & 1) == 0) return h;
        else return "0" + h;
    };
    ae.prototype.xa = ql;
    ae.prototype.AU = qu;
    ae.prototype.bZ = pv;

    function ra(d, n) {
        var b = d.ti();
        var i = 0;
        while(i < b.length && b[i] == 0) ++i;
        if(b.length - i != n - 1 || b[i] != 2) return null;
        ++i;
        while(b[i] != 0)
            if(++i >= b.length) return null;
        var bk = "";
        while(++i < b.length) {
            var c = b[i] & 255;
            if(c < 128) {
                bk += String.fromCharCode(c);
            } else if((c > 191) && (c < 224)) {
                bk += String.fromCharCode(((c & 31) << 6) | (b[i + 1] & 63));
                ++i;
            } else {
                bk += String.fromCharCode(((c & 15) << 12) | ((b[i + 1] & 63) << 6) | (b[i + 2] & 63));
                i += 2;
            }
        }
        return bk;
    };

    function qX(N, E, D) {
        if(N != null && E != null && N.length > 0 && E.length > 0) {
            this.n = bw(N, 16);
            this.e = parseInt(E, 16);
            this.d = bw(D, 16);
        } else console.error("Invalid RSA private key");
    };

    function qm(N, E, D, P, Q, wK, wU, C) {
        if(N != null && E != null && N.length > 0 && E.length > 0) {
            this.n = bw(N, 16);
            this.e = parseInt(E, 16);
            this.d = bw(D, 16);
            this.p = bw(P, 16);
            this.q = bw(Q, 16);
            this.ca = bw(wK, 16);
            this.dK = bw(wU, 16);
            this.dr = bw(C, 16);
        } else console.error("Invalid RSA private key");
    };

    function pw(B, E) {
        var fs = new js();
        var hl = B >> 1;
        this.e = parseInt(E, 16);
        var cA = new O(E, 16);
        for(;;) {
            for(;;) {
                this.p = new O(B - hl, 1, fs);
                if(this.p.cK(O.ONE).hG(cA).az(O.ONE) == 0 && this.p.gJ(10)) break;
            }
            for(;;) {
                this.q = new O(hl, 1, fs);
                if(this.q.cK(O.ONE).hG(cA).az(O.ONE) == 0 && this.q.gJ(10)) break;
            }
            if(this.p.az(this.q) <= 0) {
                var t = this.p;
                this.p = this.q;
                this.q = t;
            }
            var gm = this.p.cK(O.ONE);
            var hf = this.q.cK(O.ONE);
            var hz = gm.multiply(hf);
            if(hz.hG(cA).az(O.ONE) == 0) {
                this.n = this.p.multiply(this.q);
                this.d = cA.ja(hz);
                this.ca = this.d.dn(gm);
                this.dK = this.d.dn(hf);
                this.dr = this.q.ja(this.p);
                break;
            }
        }
    };

    function qR(x) {
        if(this.p == null || this.q == null) return x.hZ(this.d, this.n);
        var iX = x.dn(this.p).hZ(this.ca, this.p);
        var iU = x.dn(this.q).hZ(this.dK, this.q);
        while(iX.az(iU) < 0) iX = iX.add(this.p);
        return iX.cK(iU).multiply(this.dr).dn(this.p).multiply(this.q).add(iU);
    };

    function pJ(yM) {
        var c = bw(yM, 16);
        var m = this.vi(c);
        if(m == null) return null;
        return ra(m, (this.n.ho() + 7) >> 3);
    };
    ae.prototype.vi = qR;
    ae.prototype.yU = qX;
    ae.prototype.ye = qm;
    ae.prototype.generate = pw;
    ae.prototype.eV = pJ;
    (function() {
        var sA = function(B, E, callback) {
            var fs = new js();
            var hl = B >> 1;
            this.e = parseInt(E, 16);
            var cA = new O(E, 16);
            var aa = this;
            var lM = function() {
                var vB = function() {
                    if(aa.p.az(aa.q) <= 0) {
                        var t = aa.p;
                        aa.p = aa.q;
                        aa.q = t;
                    }
                    var gm = aa.p.cK(O.ONE);
                    var hf = aa.q.cK(O.ONE);
                    var hz = gm.multiply(hf);
                    if(hz.hG(cA).az(O.ONE) == 0) {
                        aa.n = aa.p.multiply(aa.q);
                        aa.d = cA.ja(hz);
                        aa.ca = aa.d.dn(gm);
                        aa.dK = aa.d.dn(hf);
                        aa.dr = aa.q.ja(aa.p);
                        setTimeout(function() {
                            callback()
                        }, 0);
                    } else {
                        setTimeout(lM, 0);
                    }
                };
                var nb = function() {
                    aa.q = aO();
                    aa.q.oV(hl, 1, fs, function() {
                        aa.q.cK(O.ONE).oZ(cA, function(r) {
                            if(r.az(O.ONE) == 0 && aa.q.gJ(10)) {
                                setTimeout(vB, 0);
                            } else {
                                setTimeout(nb, 0);
                            }
                        });
                    });
                };
                var mD = function() {
                    aa.p = aO();
                    aa.p.oV(B - hl, 1, fs, function() {
                        aa.p.cK(O.ONE).oZ(cA, function(r) {
                            if(r.az(O.ONE) == 0 && aa.p.gJ(10)) {
                                setTimeout(nb, 0);
                            } else {
                                setTimeout(mD, 0);
                            }
                        });
                    });
                };
                setTimeout(mD, 0);
            };
            setTimeout(lM, 0);
        };
        ae.prototype.vS = sA;
        var vA = function(a, callback) {
            var x = (this.s < 0) ? this.fF() : this.clone();
            var y = (a.s < 0) ? a.fF() : a.clone();
            if(x.az(y) < 0) {
                var t = x;
                x = y;
                y = t;
            }
            var i = x.cg(),
                g = y.cg();
            if(g < 0) {
                callback(x);
                return;
            }
            if(i < g) g = i;
            if(g > 0) {
                x.aD(g, x);
                y.aD(g, y);
            }
            var oO = function() {
                if((i = x.cg()) > 0) {
                    x.aD(i, x);
                }
                if((i = y.cg()) > 0) {
                    y.aD(i, y);
                }
                if(x.az(y) >= 0) {
                    x.aj(y, x);
                    x.aD(1, x);
                } else {
                    y.aj(x, y);
                    y.aD(1, y);
                }
                if(!(x.dh() > 0)) {
                    if(g > 0) y.fB(g, y);
                    setTimeout(function() {
                        callback(y)
                    }, 0);
                } else {
                    setTimeout(oO, 0);
                }
            };
            setTimeout(oO, 10);
        };
        O.prototype.oZ = vA;
        var wZ = function(a, b, c, callback) {
            if("number" == typeof b) {
                if(a < 2) {
                    this.gr(1);
                } else {
                    this.ki(a, c);
                    if(!this.lZ(a - 1)) {
                        this.ff(O.ONE.shiftLeft(a - 1), jC, this);
                    }
                    if(this.cU()) {
                        this.eD(1, 0);
                    }
                    var gy = this;
                    var lO = function() {
                        gy.eD(2, 0);
                        if(gy.ho() > a) gy.aj(O.ONE.shiftLeft(a - 1), gy);
                        if(gy.gJ(b)) {
                            setTimeout(function() {
                                callback()
                            }, 0);
                        } else {
                            setTimeout(lO, 0);
                        }
                    };
                    setTimeout(lO, 0);
                }
            } else {
                var x = new Array(),
                    t = a & 7;
                x.length = (a >> 3) + 1;
                b.lp(x);
                if(t > 0) x[0] &= ((1 << t) - 1);
                else x[0] = 0;
                this.gw(x, 256);
            }
        };
        O.prototype.oV = wZ;
    })();
    var fZ = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var mR = "=";

    function nM(h) {
        var i;
        var c;
        var bk = "";
        for(i = 0; i + 3 <= h.length; i += 3) {
            c = parseInt(h.substring(i, i + 3), 16);
            bk += fZ.charAt(c >> 6) + fZ.charAt(c & 63);
        }
        if(i + 1 == h.length) {
            c = parseInt(h.substring(i, i + 1), 16);
            bk += fZ.charAt(c << 2);
        } else if(i + 2 == h.length) {
            c = parseInt(h.substring(i, i + 2), 16);
            bk += fZ.charAt(c >> 2) + fZ.charAt((c & 3) << 4);
        }
        while((bk.length & 3) > 0) bk += mR;
        return bk;
    };

    function kI(s) {
        var bk = "";
        var i;
        var k = 0;
        var eY;
        for(i = 0; i < s.length; ++i) {
            if(s.charAt(i) == mR) break;
            v = fZ.indexOf(s.charAt(i));
            if(v < 0) continue;
            if(k == 0) {
                bk += dE(v >> 2);
                eY = v & 3;
                k = 1;
            } else if(k == 1) {
                bk += dE((eY << 2) | (v >> 4));
                eY = v & 0xf;
                k = 2;
            } else if(k == 2) {
                bk += dE(eY);
                bk += dE(v >> 2);
                eY = v & 3;
                k = 3;
            } else {
                bk += dE((eY << 2) | (v >> 4));
                bk += dE(v & 0xf);
                k = 0;
            }
        }
        if(k == 1) bk += dE(eY << 2);
        return bk;
    };

    function Ag(s) {
        var h = kI(s);
        var i;
        var a = new Array();
        for(i = 0; 2 * i < h.length; ++i) {
            a[i] = parseInt(h.substring(2 * i, 2 * i + 2), 16);
        }
        return a;
    };
    var as = as || {};
    as.gU = as.gU || {};
    var L = as,
        kC = Object.prototype,
        vC = '[object Function]',
        mG = ["toString", "valueOf"];
    as.gU.rn = function(xA) {
        var bW = function(s) {
                var c = 0;
                return parseFloat(s.replace(/\./g, function() {
                    return(c++ == 1) ? '' : '.';
                }));
            },
            nav = navigator,
            o = {
                pb: 0,
                opera: 0,
                oG: 0,
                webkit: 0,
                chrome: 0,
                hB: null,
                yR: 0,
                ipad: 0,
                iphone: 0,
                ipod: 0,
                ios: null,
                yI: 0,
                pY: 0,
                AD: nav && nav.AL,
                secure: false,
                lB: null
            },
            ua = xA || (navigator && navigator.userAgent),
            mJ = window && window.location,
            href = mJ && mJ.href,
            m;
        o.secure = href && (href.toLowerCase().indexOf("https") === 0);
        if(ua) {
            if((/windows|win32/i).test(ua)) {
                o.lB = 'windows';
            } else if((/macintosh/i).test(ua)) {
                o.lB = 'macintosh';
            } else if((/rhino/i).test(ua)) {
                o.lB = 'rhino';
            }
            if((/KHTML/).test(ua)) {
                o.webkit = 1;
            }
            m = ua.match(/AppleWebKit\/([^\s]*)/);
            if(m && m[1]) {
                o.webkit = bW(m[1]);
                if(/ Mobile\//.test(ua)) {
                    o.hB = 'Apple';
                    m = ua.match(/OS ([^\s]*)/);
                    if(m && m[1]) {
                        m = bW(m[1].replace('_', '.'));
                    }
                    o.ios = m;
                    o.ipad = o.ipod = o.iphone = 0;
                    m = ua.match(/iPad|iPod|iPhone/);
                    if(m && m[0]) {
                        o[m[0].toLowerCase()] = o.ios;
                    }
                } else {
                    m = ua.match(/NokiaN[^\/]*|Android \d\.\d|webOS\/\d\.\d/);
                    if(m) {
                        o.hB = m[0];
                    }
                    if(/webOS/.test(ua)) {
                        o.hB = 'WebOS';
                        m = ua.match(/webOS\/([^\s]*);/);
                        if(m && m[1]) {
                            o.pY = bW(m[1]);
                        }
                    }
                    if(/ Android/.test(ua)) {
                        o.hB = 'Android';
                        m = ua.match(/Android ([^\s]*);/);
                        if(m && m[1]) {
                            o.yI = bW(m[1]);
                        }
                    }
                }
                m = ua.match(/Chrome\/([^\s]*)/);
                if(m && m[1]) {
                    o.chrome = bW(m[1]);
                } else {
                    m = ua.match(/AdobeAIR\/([^\s]*)/);
                    if(m) {
                        o.yR = m[0];
                    }
                }
            }
            if(!o.webkit) {
                m = ua.match(/Opera[\s\/]([^\s]*)/);
                if(m && m[1]) {
                    o.opera = bW(m[1]);
                    m = ua.match(/Version\/([^\s]*)/);
                    if(m && m[1]) {
                        o.opera = bW(m[1]);
                    }
                    m = ua.match(/Opera Mini[^;]*/);
                    if(m) {
                        o.hB = m[0];
                    }
                } else {
                    m = ua.match(/MSIE\s([^;]*)/);
                    if(m && m[1]) {
                        o.pb = bW(m[1]);
                    } else {
                        m = ua.match(/Gecko\/([^\s]*)/);
                        if(m) {
                            o.oG = 1;
                            m = ua.match(/rv:([^\s\)]*)/);
                            if(m && m[1]) {
                                o.oG = bW(m[1]);
                            }
                        }
                    }
                }
            }
        }
        return o;
    };
    as.gU.ua = as.gU.rn();
    as.isFunction = function(o) {
        return(typeof o === 'function') || kC.toString.apply(o) === vC;
    };
    as.qU = (as.gU.ua.pb) ? function(r, s) {
        var i, gB, f;
        for(i = 0; i < mG.length; i = i + 1) {
            gB = mG[i];
            f = s[gB];
            if(L.isFunction(f) && f != kC[gB]) {
                r[gB] = f;
            }
        }
    } : function() {};
    as.extend = function(en, fA, fj) {
        if(!fA || !en) {
            throw new Error("extend failed, please check bQ " + "all dependencies are included.");
        }
        var F = function() {},
            i;
        F.prototype = fA.prototype;
        en.prototype = new F();
        en.prototype.constructor = en;
        en.bO = fA.prototype;
        if(fA.prototype.constructor == kC.constructor) {
            fA.prototype.constructor = fA;
        }
        if(fj) {
            for(i in fj) {
                if(L.hasOwnProperty(fj, i)) {
                    en.prototype[i] = fj[i];
                }
            }
            L.qU(en.prototype, fj);
        }
    };
    if(typeof aF == "undefined" || !aF) var aF = {};
    if(typeof aF.V == "undefined" || !aF.V) aF.V = {};
    aF.V.xU = new function() {
        this.AJ = function(i) {
            var h = i.toString(16);
            if((h.length % 2) == 1) h = '0' + h;
            return h;
        };
        this.wG = function(iS) {
            var h = iS.toString(16);
            if(h.substr(0, 1) != '-') {
                if(h.length % 2 == 1) {
                    h = '0' + h;
                } else {
                    if(!h.match(/^[0-7]/)) {
                        h = '00' + h;
                    }
                }
            } else {
                var vm = h.substr(1);
                var iw = vm.length;
                if(iw % 2 == 1) {
                    iw += 1;
                } else {
                    if(!h.match(/^[0-7]/)) {
                        iw += 2;
                    }
                }
                var mN = '';
                for(var i = 0; i < iw; i++) {
                    mN += 'f';
                }
                var vV = new O(mN, 16);
                var wS = vV.xor(iS).add(O.ONE);
                h = wS.toString(16).replace(/^-/, '');
            }
            return h;
        };
        this.xQ = function(xz, md) {
            var xL = aX.bJ.cJ.parse(xz);
            var yj = aX.bJ.aM.stringify(xL);
            var kp = yj.replace(/(.{64})/g, "$1\r\n");
            kp = kp.replace(/\r\n$/, '');
            return "-----BEGIN " + md + "-----\r\n" + kp + "\r\n-----END " + md + "-----\r\n";
        };
    };
    aF.V.cD = function() {
        var bL = true;
        var ay = null;
        var aA = '00';
        var nl = '00';
        var bE = '';
        this.vG = function() {
            if(typeof this.bE == "undefined" || this.bE == null) {
                throw "this.bE is null ct undefined.";
            }
            if(this.bE.length % 2 == 1) {
                throw "value al must be even length: n=" + bE.length + ",v=" + this.bE;
            }
            var n = this.bE.length / 2;
            var fv = n.toString(16);
            if(fv.length % 2 == 1) {
                fv = "0" + fv;
            }
            if(n < 128) {
                return fv;
            } else {
                var pd = fv.length / 2;
                if(pd > 15) {
                    throw "ASN.1 length too long to represent by 8x: n = " + n.toString(16);
                }
                var head = 128 + pd;
                return head.toString(16) + fv;
            }
        };
        this.fh = function() {
            if(this.ay == null || this.bL) {
                this.bE = this.cV();
                this.nl = this.vG();
                this.ay = this.aA + this.nl + this.bE;
                this.bL = false;
            }
            return this.ay;
        };
        this.zH = function() {
            this.fh();
            return this.bE;
        };
        this.cV = function() {
            return '';
        };
    };
    aF.V.dm = function(aC) {
        aF.V.dm.bO.constructor.call(this);
        var s = null;
        var bE = null;
        this.wg = function() {
            return this.s;
        };
        this.iF = function(jQ) {
            this.ay = null;
            this.bL = true;
            this.s = jQ;
            this.bE = ju(this.s);
        };
        this.ih = function(hx) {
            this.ay = null;
            this.bL = true;
            this.s = null;
            this.bE = hx;
        };
        this.cV = function() {
            return this.bE;
        };
        if(typeof aC != "undefined") {
            if(typeof aC['bS'] != "undefined") {
                this.iF(aC['bS']);
            } else if(typeof aC['al'] != "undefined") {
                this.ih(aC['al']);
            }
        }
    };
    as.extend(aF.V.dm, aF.V.cD);
    aF.V.fX = function(aC) {
        aF.V.fX.bO.constructor.call(this);
        var s = null;
        var date = null;
        this.we = function(d) {
            iZ = d.getTime() + (d.getTimezoneOffset() * 60000);
            var ot = new Date(iZ);
            return ot;
        };
        this.formatDate = function(ef, type) {
            var bH = this.pA;
            var d = this.we(ef);
            var year = String(d.getFullYear());
            if(type == 'iZ') year = year.substr(2, 2);
            var month = bH(String(d.getMonth() + 1), 2);
            var kc = bH(String(d.getDate()), 2);
            var je = bH(String(d.getHours()), 2);
            var min = bH(String(d.getMinutes()), 2);
            var jH = bH(String(d.getSeconds()), 2);
            return year + month + kc + je + min + jH + 'Z';
        };
        this.pA = function(s, aG) {
            if(s.length >= aG) return s;
            return new Array(aG - s.length + 1).join('0') + s;
        };
        this.wg = function() {
            return this.s;
        };
        this.iF = function(jQ) {
            this.ay = null;
            this.bL = true;
            this.s = jQ;
            this.bE = ju(this.s);
        };
        this.yN = function(year, month, kc, je, min, jH) {
            var ef = new Date(Date.UTC(year, month - 1, kc, je, min, jH, 0));
            this.hY(ef);
        };
        this.cV = function() {
            return this.bE;
        };
    };
    as.extend(aF.V.fX, aF.V.cD);
    aF.V.kn = function(aC) {
        aF.V.dm.bO.constructor.call(this);
        var ez = null;
        this.yc = function(xO) {
            this.ay = null;
            this.bL = true;
            this.ez = xO;
        };
        this.yY = function(cn) {
            this.ay = null;
            this.bL = true;
            this.ez.push(cn);
        };
        this.ez = new Array();
        if(typeof aC != "undefined") {
            if(typeof aC['array'] != "undefined") {
                this.ez = aC['array'];
            }
        }
    };
    as.extend(aF.V.kn, aF.V.cD);
    aF.V.lQ = function() {
        aF.V.lQ.bO.constructor.call(this);
        this.aA = "01";
        this.ay = "0101ff";
    };
    as.extend(aF.V.lQ, aF.V.cD);
    aF.V.dA = function(aC) {
        aF.V.dA.bO.constructor.call(this);
        this.aA = "02";
        this.lE = function(iS) {
            this.ay = null;
            this.bL = true;
            this.bE = aF.V.xU.wG(iS);
        };
        this.un = function(hy) {
            var kV = new O(String(hy), 10);
            this.lE(kV);
        };
        this.ip = function(hx) {
            this.bE = hx;
        };
        this.cV = function() {
            return this.bE;
        };
        if(typeof aC != "undefined") {
            if(typeof aC['bigint'] != "undefined") {
                this.lE(aC['bigint']);
            } else if(typeof aC['bG'] != "undefined") {
                this.un(aC['bG']);
            } else if(typeof aC['al'] != "undefined") {
                this.ip(aC['al']);
            }
        }
    };
    as.extend(aF.V.dA, aF.V.cD);
    aF.V.lw = function(aC) {
        aF.V.lw.bO.constructor.call(this);
        this.aA = "03";
        this.uJ = function(xe) {
            this.ay = null;
            this.bL = true;
            this.bE = xe;
        };
        this.zY = function(cW, wX) {
            if(cW < 0 || 7 < cW) {
                throw "unused aR shall be from 0 to 7: u = " + cW;
            }
            var wn = "0" + cW;
            this.ay = null;
            this.bL = true;
            this.bE = wn + wX;
        };
        this.nq = function(fV) {
            fV = fV.replace(/0+$/, '');
            var cW = 8 - fV.length % 8;
            if(cW == 8) cW = 0;
            for(var i = 0; i <= cW; i++) {
                fV += '0';
            }
            var h = '';
            for(var i = 0; i < fV.length - 1; i += 8) {
                var b = fV.substr(i, 8);
                var x = parseInt(b, 2).toString(16);
                if(x.length == 1) x = '0' + x;
                h += x;
            }
            this.ay = null;
            this.bL = true;
            this.bE = '0' + cW + h;
        };
        this.uZ = function(lY) {
            var s = '';
            for(var i = 0; i < lY.length; i++) {
                if(lY[i] == true) {
                    s += '1';
                } else {
                    s += '0';
                }
            }
            this.nq(s);
        };
        this.Aa = function(ml) {
            var a = new Array(ml);
            for(var i = 0; i < ml; i++) {
                a[i] = false;
            }
            return a;
        };
        this.cV = function() {
            return this.bE;
        };
        if(typeof aC != "undefined") {
            if(typeof aC['al'] != "undefined") {
                this.uJ(aC['al']);
            } else if(typeof aC['bin'] != "undefined") {
                this.nq(aC['bin']);
            } else if(typeof aC['array'] != "undefined") {
                this.uZ(aC['array']);
            }
        }
    };
    as.extend(aF.V.lw, aF.V.cD);
    aF.V.mf = function(aC) {
        aF.V.mf.bO.constructor.call(this, aC);
        this.aA = "04";
    };
    as.extend(aF.V.mf, aF.V.dm);
    aF.V.jV = function() {
        aF.V.jV.bO.constructor.call(this);
        this.aA = "05";
        this.ay = "0500";
    };
    as.extend(aF.V.jV, aF.V.cD);
    aF.V.gT = function(aC) {
        var lK = function(i) {
            var h = i.toString(16);
            if(h.length == 1) h = '0' + h;
            return h;
        };
        var rR = function(rE) {
            var h = '';
            var kV = new O(rE, 10);
            var b = kV.toString(2);
            var kY = 7 - b.length % 7;
            if(kY == 7) kY = 0;
            var nk = '';
            for(var i = 0; i < kY; i++) nk += '0';
            b = nk + b;
            for(var i = 0; i < b.length - 1; i += 7) {
                var jZ = b.substr(i, 7);
                if(i != b.length - 7) jZ = '1' + jZ;
                h += lK(parseInt(jZ, 2));
            }
            return h;
        };
        aF.V.gT.bO.constructor.call(this);
        this.aA = "06";
        this.ip = function(hx) {
            this.ay = null;
            this.bL = true;
            this.s = null;
            this.bE = hx;
        };
        this.lC = function(le) {
            if(!le.match(/^[0-9.]+$/)) {
                throw "malformed dI string: " + le;
            }
            var h = '';
            var a = le.split('.');
            var dc = parseInt(a[0]) * 40 + parseInt(a[1]);
            h += lK(dc);
            a.splice(0, 2);
            for(var i = 0; i < a.length; i++) {
                h += rR(a[i]);
            }
            this.ay = null;
            this.bL = true;
            this.s = null;
            this.bE = h;
        };
        this.sa = function(iK) {
            if(typeof aF.V.qx.yJ.yE[iK] != "undefined") {
                var dI = aF.V.qx.yJ.yE[iK];
                this.lC(dI);
            } else {
                throw "gT iK undefined: " + iK;
            }
        };
        this.cV = function() {
            return this.bE;
        };
        if(typeof aC != "undefined") {
            if(typeof aC['dI'] != "undefined") {
                this.lC(aC['dI']);
            } else if(typeof aC['al'] != "undefined") {
                this.ip(aC['al']);
            } else if(typeof aC['name'] != "undefined") {
                this.sa(aC['name']);
            }
        }
    };
    as.extend(aF.V.gT, aF.V.cD);
    aF.V.nK = function(aC) {
        aF.V.nK.bO.constructor.call(this, aC);
        this.aA = "0c";
    };
    as.extend(aF.V.nK, aF.V.dm);
    aF.V.mA = function(aC) {
        aF.V.mA.bO.constructor.call(this, aC);
        this.aA = "12";
    };
    as.extend(aF.V.mA, aF.V.dm);
    aF.V.mP = function(aC) {
        aF.V.mP.bO.constructor.call(this, aC);
        this.aA = "13";
    };
    as.extend(aF.V.mP, aF.V.dm);
    aF.V.pl = function(aC) {
        aF.V.pl.bO.constructor.call(this, aC);
        this.aA = "14";
    };
    as.extend(aF.V.pl, aF.V.dm);
    aF.V.mb = function(aC) {
        aF.V.mb.bO.constructor.call(this, aC);
        this.aA = "16";
    };
    as.extend(aF.V.mb, aF.V.dm);
    aF.V.pj = function(aC) {
        aF.V.pj.bO.constructor.call(this, aC);
        this.aA = "17";
        this.hY = function(ef) {
            this.ay = null;
            this.bL = true;
            this.date = ef;
            this.s = this.formatDate(this.date, 'iZ');
            this.bE = ju(this.s);
        };
        if(typeof aC != "undefined") {
            if(typeof aC['bS'] != "undefined") {
                this.iF(aC['bS']);
            } else if(typeof aC['al'] != "undefined") {
                this.ih(aC['al']);
            } else if(typeof aC['date'] != "undefined") {
                this.hY(aC['date']);
            }
        }
    };
    as.extend(aF.V.pj, aF.V.fX);
    aF.V.mv = function(aC) {
        aF.V.mv.bO.constructor.call(this, aC);
        this.aA = "18";
        this.hY = function(ef) {
            this.ay = null;
            this.bL = true;
            this.date = ef;
            this.s = this.formatDate(this.date, 'gen');
            this.bE = ju(this.s);
        };
        if(typeof aC != "undefined") {
            if(typeof aC['bS'] != "undefined") {
                this.iF(aC['bS']);
            } else if(typeof aC['al'] != "undefined") {
                this.ih(aC['al']);
            } else if(typeof aC['date'] != "undefined") {
                this.hY(aC['date']);
            }
        }
    };
    as.extend(aF.V.mv, aF.V.fX);
    aF.V.gZ = function(aC) {
        aF.V.gZ.bO.constructor.call(this, aC);
        this.aA = "30";
        this.cV = function() {
            var h = '';
            for(var i = 0; i < this.ez.length; i++) {
                var ll = this.ez[i];
                h += ll.fh();
            }
            this.bE = h;
            return this.bE;
        };
    };
    as.extend(aF.V.gZ, aF.V.kn);
    aF.V.mH = function(aC) {
        aF.V.mH.bO.constructor.call(this, aC);
        this.aA = "31";
        this.cV = function() {
            var a = new Array();
            for(var i = 0; i < this.ez.length; i++) {
                var ll = this.ez[i];
                a.push(ll.fh());
            }
            a.sort();
            this.bE = a.join('');
            return this.bE;
        };
    };
    as.extend(aF.V.mH, aF.V.kn);
    aF.V.lF = function(aC) {
        aF.V.lF.bO.constructor.call(this);
        this.aA = "a0";
        this.bE = '';
        this.hv = true;
        this.cn = null;
        this.ve = function(vJ, lJ, cn) {
            this.aA = lJ;
            this.hv = vJ;
            this.cn = cn;
            if(this.hv) {
                this.bE = this.cn.fh();
                this.ay = null;
                this.bL = true;
            } else {
                this.bE = null;
                this.ay = cn.fh();
                this.ay = this.ay.replace(/^../, lJ);
                this.bL = false;
            }
        };
        this.cV = function() {
            return this.bE;
        };
        if(typeof aC != "undefined") {
            if(typeof aC['bv'] != "undefined") {
                this.aA = aC['bv'];
            }
            if(typeof aC['explicit'] != "undefined") {
                this.hv = aC['explicit'];
            }
            if(typeof aC['bA'] != "undefined") {
                this.cn = aC['bA'];
                this.ve(this.hv, this.aA, this.cn);
            }
        }
    };
    as.extend(aF.V.lF, aF.V.cD);
    (function(undefined) {
        "use strict";
        var cJ = {},
            cR;
        cJ.ei = function(a) {
            var i;
            if(cR === undefined) {
                var al = "0123456789ABCDEF",
                    ha = " \f\n\r\t\u00A0\u2028\u2029";
                cR = [];
                for(i = 0; i < 16; ++i) cR[al.charAt(i)] = i;
                al = al.toLowerCase();
                for(i = 10; i < 16; ++i) cR[al.charAt(i)] = i;
                for(i = 0; i < ha.length; ++i) cR[ha.charAt(i)] = -1;
            }
            var out = [],
                aR = 0,
                fc = 0;
            for(i = 0; i < a.length; ++i) {
                var c = a.charAt(i);
                if(c == '=') break;
                c = cR[c];
                if(c == -1) continue;
                if(c === undefined) throw 'Illegal character at offset ' + i;
                aR |= c;
                if(++fc >= 2) {
                    out[out.length] = aR;
                    aR = 0;
                    fc = 0;
                } else {
                    aR <<= 4;
                }
            }
            if(fc) throw "cJ encoding incomplete: 4 aR missing";
            return out;
        };
        window.cJ = cJ;
    })();
    (function(undefined) {
        "use strict";
        var aM = {},
            cR;
        aM.ei = function(a) {
            var i;
            if(cR === undefined) {
                var xt = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
                    ha = "= \f\n\r\t\u00A0\u2028\u2029";
                cR = [];
                for(i = 0; i < 64; ++i) cR[xt.charAt(i)] = i;
                for(i = 0; i < ha.length; ++i) cR[ha.charAt(i)] = -1;
            }
            var out = [];
            var aR = 0,
                fc = 0;
            for(i = 0; i < a.length; ++i) {
                var c = a.charAt(i);
                if(c == '=') break;
                c = cR[c];
                if(c == -1) continue;
                if(c === undefined) throw 'Illegal character at offset ' + i;
                aR |= c;
                if(++fc >= 4) {
                    out[out.length] = (aR >> 16);
                    out[out.length] = (aR >> 8) & 0xFF;
                    out[out.length] = aR & 0xFF;
                    aR = 0;
                    fc = 0;
                } else {
                    aR <<= 6;
                }
            }
            switch(fc) {
                case 1:
                    throw "aM encoding incomplete: at least 2 aR missing";
                case 2:
                    out[out.length] = (aR >> 10);
                    break;
                case 3:
                    out[out.length] = (aR >> 16);
                    out[out.length] = (aR >> 8) & 0xFF;
                    break;
            }
            return out;
        };
        aM.eT = /-----BEGIN [^-]+-----([A-Za-z0-9+\/=\s]+)-----END [^-]+-----|begin-base64[^\n]+\n([A-Za-z0-9+\/=\s]+)====/;
        aM.rA = function(a) {
            var m = aM.eT.exec(a);
            if(m) {
                if(m[1]) a = m[1];
                else if(m[2]) a = m[2];
                else throw "RegExp out of sync";
            }
            return aM.ei(a);
        };
        window.aM = aM;
    })();
    (function(undefined) {
        "use strict";
        var eh = 100,
            ellipsis = "\u2026",
            dM = {
                bv: function(tagName, className) {
                    var t = document.createElement(tagName);
                    t.className = className;
                    return t;
                },
                text: function(bS) {
                    return document.createTextNode(bS);
                }
            };

        function aU(bJ, pos) {
            if(bJ instanceof aU) {
                this.bJ = bJ.bJ;
                this.pos = bJ.pos;
            } else {
                this.bJ = bJ;
                this.pos = pos;
            }
        };
        aU.prototype.get = function(pos) {
            if(pos === undefined) pos = this.pos++;
            if(pos >= this.bJ.length) throw 'Requesting byte offset ' + pos + ' on a bn of length ' + this.bJ.length;
            return this.bJ[pos];
        };
        aU.prototype.oR = "0123456789ABCDEF";
        aU.prototype.pr = function(b) {
            return this.oR.charAt((b >> 4) & 0xF) + this.oR.charAt(b & 0xF);
        };
        aU.prototype.jn = function(start, end, ow) {
            var s = "";
            for(var i = start; i < end; ++i) {
                s += this.pr(this.get(i));
                if(ow !== true) switch(i & 0xF) {
                    case 0x7:
                        s += "  ";
                        break;
                    case 0xF:
                        s += "\n";
                        break;
                    default:
                        s += " ";
                }
            }
            return s;
        };
        aU.prototype.ke = function(start, end) {
            var s = "";
            for(var i = start; i < end; ++i) s += String.fromCharCode(this.get(i));
            return s;
        };
        aU.prototype.qi = function(start, end) {
            var s = "";
            for(var i = start; i < end;) {
                var c = this.get(i++);
                if(c < 128) s += String.fromCharCode(c);
                else if((c > 191) && (c < 224)) s += String.fromCharCode(((c & 0x1F) << 6) | (this.get(i++) & 0x3F));
                else s += String.fromCharCode(((c & 0x0F) << 12) | ((this.get(i++) & 0x3F) << 6) | (this.get(i++) & 0x3F));
            }
            return s;
        };
        aU.prototype.qC = function(start, end) {
            var bS = "";
            for(var i = start; i < end; i += 2) {
                var vE = this.get(i);
                var wF = this.get(i + 1);
                bS += String.fromCharCode((vE << 8) + wF);
            }
            return bS;
        };
        aU.prototype.sz = /^((?:1[89]|2\d)?\d\d)(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01])([01]\d|2[0-3])(?:([0-5]\d)(?:([0-5]\d)(?:[.,](\d{1,3}))?)?)?(Z|[-+](?:[0]\d|1[0-2])([0-5]\d)?)?$/;
        aU.prototype.qq = function(start, end) {
            var s = this.ke(start, end),
                m = this.sz.exec(s);
            if(!m) return "Unrecognized time: " + s;
            s = m[1] + "-" + m[2] + "-" + m[3] + " " + m[4];
            if(m[5]) {
                s += ":" + m[5];
                if(m[6]) {
                    s += ":" + m[6];
                    if(m[7]) s += "." + m[7];
                }
            }
            if(m[8]) {
                s += " UTC";
                if(m[8] != 'Z') {
                    s += m[8];
                    if(m[9]) s += ":" + m[9];
                }
            }
            return s;
        };
        aU.prototype.qn = function(start, end) {
            var aG = end - start;
            if(aG > 4) {
                aG <<= 3;
                var s = this.get(start);
                if(s === 0) aG -= 8;
                else
                    while(s < 128) {
                        s <<= 1;
                        --aG;
                    }
                return "(" + aG + " bit)";
            }
            var n = 0;
            for(var i = start; i < end; ++i) n = (n << 8) | this.get(i);
            return n;
        };
        aU.prototype.pI = function(start, end) {
            var ok = this.get(start),
                nf = ((end - start - 1) << 3) - ok,
                s = "(" + nf + " bit)";
            if(nf <= 20) {
                var mL = ok;
                s += " ";
                for(var i = end - 1; i > start; --i) {
                    var b = this.get(i);
                    for(var j = mL; j < 8; ++j) s += (b >> j) & 1 ? "1" : "0";
                    mL = 0;
                }
            }
            return s;
        };
        aU.prototype.mU = function(start, end) {
            var aG = end - start,
                s = "(" + aG + " byte) ";
            if(aG > eh) end = start + eh;
            for(var i = start; i < end; ++i) s += this.pr(this.get(i));
            if(aG > eh) s += ellipsis;
            return s;
        };
        aU.prototype.qj = function(start, end) {
            var s = '',
                n = 0,
                aR = 0;
            for(var i = start; i < end; ++i) {
                var v = this.get(i);
                n = (n << 7) | (v & 0x7F);
                aR += 7;
                if(!(v & 0x80)) {
                    if(s === '') {
                        var m = n < 80 ? n < 40 ? 0 : 1 : 2;
                        s = m + "." + (n - m * 40);
                    } else s += "." + ((aR >= 31) ? "bigint" : n);
                    n = aR = 0;
                }
            }
            return s;
        };

        function bh(bn, header, length, bv, sub) {
            this.bn = bn;
            this.header = header;
            this.length = length;
            this.bv = bv;
            this.sub = sub;
        };
        bh.prototype.iW = function() {
            if(this.bv === undefined) return "unknown";
            var jW = this.bv >> 6,
                zd = (this.bv >> 5) & 1,
                eN = this.bv & 0x1F;
            switch(jW) {
                case 0:
                    switch(eN) {
                        case 0x00:
                            return "EOC";
                        case 0x01:
                            return "BOOLEAN";
                        case 0x02:
                            return "INTEGER";
                        case 0x03:
                            return "BIT_STRING";
                        case 0x04:
                            return "OCTET_STRING";
                        case 0x05:
                            return "NULL";
                        case 0x06:
                            return "OBJECT_IDENTIFIER";
                        case 0x07:
                            return "ObjectDescriptor";
                        case 0x08:
                            return "EXTERNAL";
                        case 0x09:
                            return "REAL";
                        case 0x0A:
                            return "ENUMERATED";
                        case 0x0B:
                            return "EMBEDDED_PDV";
                        case 0x0C:
                            return "UTF8String";
                        case 0x10:
                            return "SEQUENCE";
                        case 0x11:
                            return "SET";
                        case 0x12:
                            return "NumericString";
                        case 0x13:
                            return "PrintableString";
                        case 0x14:
                            return "TeletexString";
                        case 0x15:
                            return "VideotexString";
                        case 0x16:
                            return "IA5String";
                        case 0x17:
                            return "UTCTime";
                        case 0x18:
                            return "GeneralizedTime";
                        case 0x19:
                            return "GraphicString";
                        case 0x1A:
                            return "VisibleString";
                        case 0x1B:
                            return "GeneralString";
                        case 0x1C:
                            return "UniversalString";
                        case 0x1E:
                            return "BMPString";
                        default:
                            return "Universal_" + eN.toString(16);
                    }
                case 1:
                    return "Application_" + eN.toString(16);
                case 2:
                    return "[" + eN + "]";
                case 3:
                    return "Private_" + eN.toString(16);
            }
        };
        bh.prototype.sp = /^[ -~]+$/;
        bh.prototype.content = function() {
            if(this.bv === undefined) return null;
            var jW = this.bv >> 6,
                eN = this.bv & 0x1F,
                content = this.jb(),
                aG = Math.abs(this.length);
            if(jW !== 0) {
                if(this.sub !== null) return "(" + this.sub.length + " elem)";
                var s = this.bn.ke(content, content + Math.min(aG, eh));
                if(this.sp.test(s)) return s.substring(0, 2 * eh) + ((s.length > 2 * eh) ? ellipsis : "");
                else return this.bn.mU(content, content + aG);
            }
            switch(eN) {
                case 0x01:
                    return(this.bn.get(content) === 0) ? "false" : "true";
                case 0x02:
                    return this.bn.qn(content, content + aG);
                case 0x03:
                    return this.sub ? "(" + this.sub.length + " elem)" : this.bn.pI(content, content + aG);
                case 0x04:
                    return this.sub ? "(" + this.sub.length + " elem)" : this.bn.mU(content, content + aG);
                case 0x06:
                    return this.bn.qj(content, content + aG);
                case 0x10:
                case 0x11:
                    return "(" + this.sub.length + " elem)";
                case 0x0C:
                    return this.bn.qi(content, content + aG);
                case 0x12:
                case 0x13:
                case 0x14:
                case 0x15:
                case 0x16:
                case 0x1A:
                    return this.bn.ke(content, content + aG);
                case 0x1E:
                    return this.bn.qC(content, content + aG);
                case 0x17:
                case 0x18:
                    return this.bn.qq(content, content + aG);
            }
            return null;
        };
        bh.prototype.toString = function() {
            return this.iW() + "@" + this.bn.pos + "[header:" + this.header + ",length:" + this.length + ",sub:" + ((this.sub === null) ? 'null' : this.sub.length) + "]";
        };
        bh.prototype.print = function(indent) {
            if(indent === undefined) indent = '';
            document.writeln(indent + this);
            if(this.sub !== null) {
                indent += '  ';
                for(var i = 0, max = this.sub.length; i < max; ++i) this.sub[i].print(indent);
            }
        };
        bh.prototype.sZ = function(indent) {
            if(indent === undefined) indent = '';
            var s = indent + this.iW() + " @" + this.bn.pos;
            if(this.length >= 0) s += "+";
            s += this.length;
            if(this.bv & 0x20) s += " (constructed)";
            else if(((this.bv == 0x03) || (this.bv == 0x04)) && (this.sub !== null)) s += " (encapsulates)";
            s += "\n";
            if(this.sub !== null) {
                indent += '  ';
                for(var i = 0, max = this.sub.length; i < max; ++i) s += this.sub[i].sZ(indent);
            }
            return s;
        };
        bh.prototype.rQ = function() {
            var ai = dM.bv("div", "ai");
            ai.V = this;
            var head = dM.bv("div", "head");
            var s = this.iW().replace(/_/g, " ");
            head.innerHTML = s;
            var content = this.content();
            if(content !== null) {
                content = String(content).replace(/</g, "&lt;");
                var kx = dM.bv("span", "kx");
                kx.appendChild(dM.text(content));
                head.appendChild(kx);
            }
            ai.appendChild(head);
            this.ai = ai;
            this.head = head;
            var value = dM.bv("div", "value");
            s = "Offset: " + this.bn.pos + "<br/>";
            s += "Length: " + this.header + "+";
            if(this.length >= 0) s += this.length;
            else s += (-this.length) + " (undefined)";
            if(this.bv & 0x20) s += "<br/>(constructed)";
            else if(((this.bv == 0x03) || (this.bv == 0x04)) && (this.sub !== null)) s += "<br/>(encapsulates)";
            if(content !== null) {
                s += "<br/>Value:<br/><b>" + content + "</b>";
                if((typeof xv === 'object') && (this.bv == 0x06)) {
                    var dI = xv[content];
                    if(dI) {
                        if(dI.d) s += "<br/>" + dI.d;
                        if(dI.c) s += "<br/>" + dI.c;
                        if(dI.w) s += "<br/>(warning!)";
                    }
                }
            }
            value.innerHTML = s;
            ai.appendChild(value);
            var sub = dM.bv("div", "sub");
            if(this.sub !== null) {
                for(var i = 0, max = this.sub.length; i < max; ++i) sub.appendChild(this.sub[i].rQ());
            }
            ai.appendChild(sub);
            head.onclick = function() {
                ai.className = (ai.className == "ai collapsed") ? "ai" : "ai collapsed";
            };
            return ai;
        };
        bh.prototype.hg = function() {
            return this.bn.pos;
        };
        bh.prototype.jb = function() {
            return this.bn.pos + this.header;
        };
        bh.prototype.hU = function() {
            return this.bn.pos + this.header + Math.abs(this.length);
        };
        bh.prototype.vp = function(current) {
            this.ai.className += " hover";
            if(current) this.head.className += " hover";
        };
        bh.prototype.vQ = function(current) {
            var eT = / ?hover/;
            this.ai.className = this.ai.className.replace(eT, "");
            if(current) this.head.className = this.head.className.replace(eT, "");
        };
        bh.prototype.hX = function(ai, className, bn, start, end) {
            if(start >= end) return;
            var sub = dM.bv("span", className);
            sub.appendChild(dM.text(bn.jn(start, end)));
            ai.appendChild(sub);
        };
        bh.prototype.sP = function(root) {
            var ai = dM.bv("span", "al");
            if(root === undefined) root = ai;
            this.head.od = ai;
            this.head.onmouseover = function() {
                this.od.className = "hexCurrent";
            };
            this.head.onmouseout = function() {
                this.od.className = "al";
            };
            ai.V = this;
            ai.onmouseover = function() {
                var current = !root.selected;
                if(current) {
                    root.selected = this.V;
                    this.className = "hexCurrent";
                }
                this.V.vp(current);
            };
            ai.onmouseout = function() {
                var current = (root.selected == this.V);
                this.V.vQ(current);
                if(current) {
                    root.selected = null;
                    this.className = "al";
                }
            };
            this.hX(ai, "bv", this.bn, this.hg(), this.hg() + 1);
            this.hX(ai, (this.length >= 0) ? "dlen" : "ulen", this.bn, this.hg() + 1, this.jb());
            if(this.sub === null) ai.appendChild(dM.text(this.bn.jn(this.jb(), this.hU())));
            else if(this.sub.length > 0) {
                var first = this.sub[0];
                var last = this.sub[this.sub.length - 1];
                this.hX(ai, "intro", this.bn, this.jb(), first.hg());
                for(var i = 0, max = this.sub.length; i < max; ++i) ai.appendChild(this.sub[i].sP(root));
                this.hX(ai, "outro", this.bn, last.hU(), this.hU());
            }
            return ai;
        };
        bh.prototype.sK = function(root) {
            return this.bn.jn(this.hg(), this.hU(), true);
        };
        bh.kl = function(bn) {
            var ea = bn.get(),
                aG = ea & 0x7F;
            if(aG == ea) return aG;
            if(aG > 3) throw "Length over 24 aR not supported at position " + (bn.pos - 1);
            if(aG === 0) return -1;
            ea = 0;
            for(var i = 0; i < aG; ++i) ea = (ea << 8) | bn.get();
            return ea;
        };
        bh.wH = function(bv, aG, bn) {
            if(bv & 0x20) return true;
            if((bv < 0x03) || (bv > 0x04)) return false;
            var p = new aU(bn);
            if(bv == 0x03) p.get();
            var qT = p.get();
            if((qT >> 6) & 0x01) return false;
            try {
                var py = bh.kl(p);
                return((p.pos - bn.pos) + py == aG);
            } catch(Ay) {
                return false;
            }
        };
        bh.ei = function(bn) {
            if(!(bn instanceof aU)) bn = new aU(bn, 0);
            var nc = new aU(bn),
                bv = bn.get(),
                aG = bh.kl(bn),
                header = bn.pos - nc.pos,
                sub = null;
            if(bh.wH(bv, aG, bn)) {
                var start = bn.pos;
                if(bv == 0x03) bn.get();
                sub = [];
                if(aG >= 0) {
                    var end = start + aG;
                    while(bn.pos < end) sub[sub.length] = bh.ei(bn);
                    if(bn.pos != end) throw "Content size is not correct for hO starting at offset " + start;
                } else {
                    try {
                        for(;;) {
                            var s = bh.ei(bn);
                            if(s.bv === 0) break;
                            sub[sub.length] = s;
                        }
                        aG = start - bn.pos;
                    } catch(e) {
                        throw "Exception while decoding undefined length content: " + e;
                    }
                }
            } else bn.pos += aG;
            return new bh(nc, header, aG, bv, sub);
        };
        bh.test = function() {
            var test = [{
                value: [0x27],
                gI: 0x27
            }, {
                value: [0x81, 0xC9],
                gI: 0xC9
            }, {
                value: [0x83, 0xFE, 0xDC, 0xBA],
                gI: 0xFEDCBA
            }];
            for(var i = 0, max = test.length; i < max; ++i) {
                var pos = 0,
                    bn = new aU(test[i].value, 0),
                    kw = bh.kl(bn);
                if(kw != test[i].gI) document.write("In test[" + i + "] gI " + test[i].gI + " got " + kw + "\n");
            }
        };
        window.bh = bh;
    })();
    bh.prototype.et = function() {
        var vR = this.sK();
        var offset = this.header * 2;
        var length = this.length * 2;
        return vR.substr(offset, length);
    };
    ae.prototype.qA = function(gV) {
        try {
            var hS = 0;
            var jp = 0;
            var nP = /^\s*(?:[0-9A-Fa-f][0-9A-Fa-f]\s*)+$/;
            var wz = nP.test(gV) ? cJ.ei(gV) : aM.rA(gV);
            var V = bh.ei(wz);
            if(V.sub.length === 3) {
                V = V.sub[2].sub[0];
            }
            if(V.sub.length === 9) {
                hS = V.sub[1].et();
                this.n = bw(hS, 16);
                jp = V.sub[2].et();
                this.e = parseInt(jp, 16);
                var qE = V.sub[3].et();
                this.d = bw(qE, 16);
                var pV = V.sub[4].et();
                this.p = bw(pV, 16);
                var qG = V.sub[5].et();
                this.q = bw(qG, 16);
                var wA = V.sub[6].et();
                this.ca = bw(wA, 16);
                var vy = V.sub[7].et();
                this.dK = bw(vy, 16);
                var yv = V.sub[8].et();
                this.dr = bw(yv, 16);
            } else if(V.sub.length === 2) {
                var start = (V.header + V.sub[0].header) * 2;
                var length = V.sub[0].length * 2;
                hS = gV.substr(start, length);
                this.n = bw(hS, 16);
                start = length + start + V.sub[1].header * 2;
                length = V.sub[1].length * 2;
                jp = gV.substr(start, length);
                this.e = parseInt(jp, 16);
            } else {
                return false;
            }
            return true;
        } catch(ex) {
            return false;
        }
    };
    ae.prototype.vo = function() {
        var options = {
            'array': [new aF.V.dA({
                'bG': 0
            }), new aF.V.dA({
                'bigint': this.n
            }), new aF.V.dA({
                'bG': this.e
            }), new aF.V.dA({
                'bigint': this.d
            }), new aF.V.dA({
                'bigint': this.p
            }), new aF.V.dA({
                'bigint': this.q
            }), new aF.V.dA({
                'bigint': this.ca
            }), new aF.V.dA({
                'bigint': this.dK
            }), new aF.V.dA({
                'bigint': this.dr
            })]
        };
        var kL = new aF.V.gZ(options);
        return kL.fh();
    };
    ae.prototype.nS = function() {
        return nM(this.vo());
    };
    ae.prototype.wW = function() {
        var options = {
            'array': [new aF.V.gT({
                'dI': '1.2.840.113549.1.1.1'
            }), new aF.V.jV()]
        };
        var wL = new aF.V.gZ(options);
        options = {
            'array': [new aF.V.dA({
                'bigint': this.n
            }), new aF.V.dA({
                'bG': this.e
            })]
        };
        var uu = new aF.V.gZ(options);
        options = {
            'al': '00' + uu.fh()
        };
        var vz = new aF.V.lw(options);
        options = {
            'array': [wL, vz]
        };
        var kL = new aF.V.gZ(options);
        return kL.fh();
    };
    ae.prototype.nx = function() {
        return nM(this.wW());
    };
    ae.prototype.nG = function(bS, width) {
        width = width || 64;
        if(!bS) {
            return bS;
        }
        var qO = '(.{1,' + width + '})( +|$\n?)|(.{1,' + width + '})';
        return bS.match(RegExp(qO, 'g')).join('\n');
    };
    ae.prototype.nL = function() {
        var key = "-----BEGIN RSA PRIVATE KEY-----\n";
        key += this.nG(this.nS()) + "\n";
        key += "-----END RSA PRIVATE KEY-----";
        return key;
    };
    ae.prototype.mr = function() {
        var key = "-----BEGIN PUBLIC KEY-----\n";
        key += this.nG(this.nx()) + "\n";
        key += "-----END PUBLIC KEY-----";
        return key;
    };
    ae.prototype.wC = function(bA) {
        bA = bA || {};
        return(bA.hasOwnProperty('n') && bA.hasOwnProperty('e'));
    };
    ae.prototype.ws = function(bA) {
        bA = bA || {};
        return(bA.hasOwnProperty('n') && bA.hasOwnProperty('e') && bA.hasOwnProperty('d') && bA.hasOwnProperty('p') && bA.hasOwnProperty('q') && bA.hasOwnProperty('ca') && bA.hasOwnProperty('dK') && bA.hasOwnProperty('dr'));
    };
    ae.prototype.qb = function(bA) {
        this.n = bA.n;
        this.e = bA.e;
        if(bA.hasOwnProperty('d')) {
            this.d = bA.d;
            this.p = bA.p;
            this.q = bA.q;
            this.ca = bA.ca;
            this.dK = bA.dK;
            this.dr = bA.dr;
        }
    };
    var gO = function(key) {
        ae.call(this);
        if(key) {
            if(typeof key === 'string') {
                this.qA(key);
            } else if(this.ws(key) || this.wC(key)) {
                this.qb(key);
            }
        }
    };
    gO.prototype = new ae();
    gO.prototype.constructor = gO;
    var db = function(options) {
        options = options || {};
        this.ks = parseInt(options.ks) || 1024;
        this.kX = options.kX || '010001';
        this.log = options.log || false;
        this.key = null;
    };
    db.prototype.na = function(key) {
        if(this.log && this.key) {
            console.xZ('A key was already set, overriding existing.');
        }
        this.key = new gO(key);
    };
    db.prototype.yK = function(rh) {
        this.na(rh);
    };
    db.prototype.uO = function(pG) {
        this.na(pG);
    };
    db.prototype.eV = function(string) {
        try {
            return this.hE().eV(kI(string));
        } catch(ex) {
            return false;
        }
    };
    db.prototype.bZ = function(string) {
        try {
            return(this.hE().bZ(string));
        } catch(ex) {
            return false;
        }
    };
    db.prototype.hE = function(jS) {
        if(!this.key) {
            this.key = new gO();
            if(jS && {}.toString.call(jS) === '[object Function]') {
                this.key.vS(this.ks, this.kX, jS);
                return;
            }
            this.key.generate(this.ks, this.kX);
        }
        return this.key;
    };
    db.prototype.nL = function() {
        return this.hE().nL();
    };
    db.prototype.yO = function() {
        return this.hE().nS();
    };
    db.prototype.mr = function() {
        return this.hE().mr();
    };
    db.prototype.zF = function() {
        return this.hE().nx();
    };
    fg.db = db;
})(lT);
var db = lT.db;



//////////////////////////////////////////////////////////////////////////////
//SM2 stop
//////////////////////////////////////////////////////////////////////////////

/***prng4.js***/
/*! (c) Tom Wu | http://www-cs-students.stanford.edu/~tjw/jsbn/
*/
//prng4.js - uses Arcfour as a PRNG

function Arcfour() {
this.i = 0;
this.j = 0;
this.S = new Array();
}
k034 = "/5A4whLq4LAn1keYpbucVRjzrm6dKFnk4AXu3hnGxm2xOlWtIWgvIXOqM1qEMmBus1glRlB+JuP2nhddoC/RfC90TSk1rpE8KA++eL9ISokpqRcDKU5QXdQBKmPghSQo7r6LmVKUGPMurLuTuYPCGQ4AI1exN2knap/21C/cpgi0EHntHLU6Nz8+Aa2q3DBy2bRvD9XjJUhF2djuzx0ukz2O8yg=";
//Initialize arcfour context from key, an array of ints, each from [0..255]
function ARC4init(key) {
var i, j, t;
for(i = 0; i < 256; ++i)
this.S[i] = i;
j = 0;
for(i = 0; i < 256; ++i) {
j = (j + this.S[i] + key[i % key.length]) & 255;
t = this.S[i];
this.S[i] = this.S[j];
this.S[j] = t;
}
this.i = 0;
this.j = 0;
}

function ARC4next() {
var t;
this.i = (this.i + 1) & 255;
this.j = (this.j + this.S[this.i]) & 255;
t = this.S[this.i];
this.S[this.i] = this.S[this.j];
this.S[this.j] = t;
return this.S[(t + this.S[this.i]) & 255];
}

Arcfour.prototype.init = ARC4init;
Arcfour.prototype.next = ARC4next;
k035 = "Vemg4yz9dnyKHteU+a3epFsjXvrL6yFPxG7ibIFMRW5YZG5mO527lLq3CH2L7cb8Zj7PPwVpVc4YcflSXcuK36TkiQwUNJ8CmxprAIGWKjyX/OgilsPczG0U3RzuP4D7l0J8R1CWSgzFl4B7VJW40mYdnFlgIQrYy60xzPbVK2Q=";
//Plug in your RNG constructor here
function prng_newstate() {
return new Arcfour();
}

//Pool size must be a multiple of 4 and greater than 32.
//An array of bytes the size of the pool will be passed to init()
var rng_psize = 256;
var sm4 = function() {
var block = 16;
var sbox = [214, 144, 233, 254, 204, 225, 61, 183, 22, 182, 20, 194, 40, 251, 44, 5, 43, 103, 154, 118, 42, 190, 4, 195, 170, 68, 19, 38, 73, 134, 6, 153, 156, 66, 80, 244, 145, 239, 152, 122, 51, 84, 11, 67, 237, 207, 172, 98, 228, 179, 28, 169, 201, 8, 232, 149, 128, 223, 148, 250, 117, 143, 63, 166, 71, 7, 167, 252, 243, 115, 23, 186, 131, 89, 60, 25, 230, 133, 79, 168, 104, 107, 129, 178, 113, 100, 218, 139, 248, 235, 15, 75, 112, 86, 157, 53, 30, 36, 14, 94, 99, 88, 209, 162, 37, 34, 124, 59, 1, 33, 120, 135, 212, 0, 70, 87, 159, 211, 39, 82, 76, 54, 2, 231, 160, 196, 200, 158, 234, 191, 138, 210, 64, 199, 56, 181, 163, 247, 242, 206, 249, 97, 21, 161, 224, 174, 93, 164, 155, 52, 26, 85, 173, 147, 50, 48, 245, 140, 177, 227, 29, 246, 226, 46, 130, 102, 202, 96, 192, 41, 35, 171, 13, 83, 78, 111, 213, 219, 55, 69, 222, 253, 142, 47, 3, 255, 106, 114, 109, 108, 91, 81, 141, 27, 175, 146, 187, 221, 188, 127, 17, 217, 92, 65, 31, 16, 90, 216, 10, 193, 49, 136, 165, 205, 123, 189, 45, 116, 208, 18, 184, 229, 180, 176, 137, 105, 151, 74, 12, 150, 119, 126, 101, 185, 241, 9, 197, 110, 198, 132, 24, 240, 125, 236, 58, 220, 77, 32, 121, 238, 95, 62, 215, 203, 57, 72];
var fk = [2746333894, 1453994832, 1736282519, 2993693404];
var ck = [462357, 472066609, 943670861, 1415275113, 1886879365, 2358483617, 2830087869, 3301692121, 3773296373, 4228057617, 404694573, 876298825, 1347903077, 1819507329, 2291111581, 2762715833, 3234320085, 3705924337, 4177462797, 337322537, 808926789, 1280531041, 1752135293, 2223739545, 2695343797, 3166948049, 3638552301, 4110090761, 269950501, 741554753, 1213159005, 1684763257];

var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; 
var base64DecodeChars = new Array(  
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  
-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,  
52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,  
-1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,  
15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,  
-1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,  
41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1);


return{
rk: {},
key: {},

init: function(a) {
this.key = a;
this.caculate_round_key(a);
},

char2code: function(a) {
for (var b = new Array(a.length), c = 0; c < a.length; c++)
b[c] = a.charAt(c).charCodeAt();
return b
},

h2n_word: function(a, b) {
return a[b] << 24 & 4278190080 | a[b + 1] << 16 & 16711680 | a[b + 2] << 8 & 65280 | 255 & a[b + 3] & 4294967295
},

n2h_word: function(a, b) {
return a[b + 3] << 24 & 4278190080 | a[b + 2] << 16 & 16711680 | a[b + 1] << 8 & 65280 | 255 & a[b] & 4294967295
},

changeendianword: function(a){
var t0 = (a & 4278190080) >>> 24 & 4294967295;
var t1 = (a & 16711680) >>> 8 & 4294967295;
var t2 = ((a & 65280) << 8 & 4294967295)>>>0;
var t3 = ((a & 255) << 24 & 4294967295)>>>0;

return (t0 + t1 + t2 + t3);
},

word2array: function(a){
var array = new Array(4);
array[0] = (a & 4278190080) >>> 24;
array[1] = (a & 16711680) >>> 16;
array[2] = (a & 65280) >>>8;
array[3] = (a & 255) >>>0;

return array;
},

getsboxvalue: function(a) {
return sbox[a]
},

F_transfer: function(a, i) {
var tc = a[0];
var u1 = (a[1] ^ a[2] ^ a[3] ^ this.rk[i]) >>> 0;
var u2 = this.T_transfer(u1) >>> 0;
var u3 = tc ^ u2 >>> 0;
return u3;
},
F_transfer: function(a, i,falsss) {
var tc = a[0];
if(falsss) alert(tc);
var u1 = (a[1] ^ a[2] ^ a[3] ^ this.rk[i]) >>> 0;
if(falsss) alert(u1);
var u2 = this.T_transfer(u1,falsss) >>> 0;
if(falsss) alert(u2);
var u3 = tc ^ u2 >>> 0;
if(falsss) alert(u3);
return u3;
},
T_transfer: function(a,falsss) {
var b = 0
, c = 0
, d = new Array(4);
var e = this.word2array(a);
if(falsss) alert(e);
d[0] = this.getsboxvalue(e[0]);
if(falsss) alert(d[0]);
d[1] = this.getsboxvalue(e[1]);
if(falsss) alert(d[1]);
d[2] = this.getsboxvalue(e[2]);
if(falsss) alert(d[2]);
d[3] = this.getsboxvalue(e[3]);
if(falsss) alert(d[3]);
var t = this.makeword(d, 0);
if(falsss) alert(t);
c = t ^ this.shift_transfer(t, 2) ^ this.shift_transfer(t, 10) ^ this.shift_transfer(t, 18) ^ this.shift_transfer(t, 24) >>> 0;
if(falsss) alert(this.shift_transfer(t, 2));
if(falsss) alert(this.shift_transfer(t, 10));
if(falsss) alert(this.shift_transfer(t, 18));
if(falsss) alert(this.shift_transfer(t, 24));
if(falsss) alert(c);
return c;
},
T_apostrophe_transfer: function(a) {
var b = 0
, c = 0
, d = new Array(4);
var o = this.word2array(a);
d[0] = this.getsboxvalue(o[0]);
d[1] = this.getsboxvalue(o[1]);
d[2] = this.getsboxvalue(o[2]);
d[3] = this.getsboxvalue(o[3]);
var t = this.makeword(d, 0);
c = (t ^ this.shift_transfer(t, 13) ^ this.shift_transfer(t, 23)) >>> 0;
return c;
},
leftbitmove: function(a, b) {
var c = (4294967295 & a) << b;
return c
},

shift_transfer: function(a, b) {
return this.leftbitmove(a, b) | a >>> 32 - b
},

caculate_round_key: function(a) {
for(var j = 0;j<32;j++)
this.rk[j] = 0;

if($.isArray(a))
{
var mk = new Array(4);
for(var i = 0;i<4;i++)
mk[i] = this.h2n_word(a, i * 4)>>>0;

var K = new Array(40);
for(var c = 0;c<4;c++)
{
var tmp = fk[c];

K[c] = (mk[c]^tmp)>>>0;
}

for(var n = 0;n<32;n++)
{
var u0 = this.T_apostrophe_transfer(K[n+1]^K[n+2]^K[n+3]^ck[n])>>>0;
var u1 = K[n];
this.rk[n] = (u0^u1)>>>0;
K[n + 4] = this.rk[n];
}
}

return this.rk;
},

highbyte2lowbyte: function(a) {
var b = new Array(4);
return b[0] = a >>> 24 & 255,
b[1] = a >>> 16 & 255,
b[2] = a >>> 8 & 255,
b[3] = 255 & a,
b
},

makeword: function(a, b) {
return a[b] << 24 & 4278190080 | a[b + 1] << 16 & 16711680 | a[b + 2] << 8 & 65280 | 255 & a[b + 3] & 4294967295
},

pkcs5padding: function(a, c) {
if(c == 0)
c = 16;

for (var d = 0; c > d; d++)
a.push(c)
},

encryptstring: function(s, flag){
if (null  == s || 0 == s.length)
return null ;

var res = new Array();
var array = new Array();
var i;
for(i = 0;i<s.length;i++){
array.push(s.charAt(i).charCodeAt());
}

var r = this.cipher(array, flag);

for(i = 0;i<r.length;i++){
res.push(r[i]);
}

return res;
},

decryptstring: function(a, flag){
if (null  == a || 0 == a.length)
return null ;

var res = "";
var i;

var r = this.cipher(a, flag);

for(i = 0;i<r.length;i++){
res += String.fromCharCode(r[i]);
}

return res;
},

cipher: function(p, flag){
if (null  == p || 0 == p.length)
return null ;

if(true == flag){
var l = block - p.length % block;
this.pkcs5padding(p, l);
}

var result = new Array();

//32 round iteration
for(var r = Math.floor(p.length / block), n = 0;n < r;n++){
var i = new Array(4);
var x = new Array(36);
var o = new Array(4);
//get block in data
for(var m = 0;m<4;m++)
{
i[m] = this.makeword(p, n*16 + m*4)>>>0;
x[m] = i[m];
}

for(var round = 0;round < 32;round++){
//F transfer
var tv = new Array(x[round], x[round + 1], x[round + 2], x[round + 3]);
var tt = this.F_transfer(tv, flag ? round : (31 - round))>>>0;
x[round + 4] = tt;
}   

//R transfer
o[0] = x[35];
o[1] = x[34];
o[2] = x[33];
o[3] = x[32];

for(var j = 0;j<o.length;j++){
var ta = this.word2array(o[j]);
result.push(ta[0]);
result.push(ta[1]);
result.push(ta[2]);
result.push(ta[3]);
}
}

return result;
},

base64encode: function(a) {
var out, i, len;
var c1, c2, c3;
len = a.length;
i = 0;
out = "";
while(i < len) {
c1 = a[i++] & 0xff;
if(i == len)
{
out += base64EncodeChars.charAt(c1 >> 2);
out += base64EncodeChars.charAt((c1 & 0x3) << 4);
out += "==";
break;
}
c2 = a[i++];
if(i == len)
{
out += base64EncodeChars.charAt(c1 >> 2);
out += base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
out += base64EncodeChars.charAt((c2 & 0xF) << 2);
out += "=";
break;
}
c3 = a[i++];
out += base64EncodeChars.charAt(c1 >> 2);
out += base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
out += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >>6));
out += base64EncodeChars.charAt(c3 & 0x3F);
}
return out;
},

base64decode: function(str) {
var c1, c2, c3, c4;
var i, len, out;

len = str.length;
i = 0;
out = new Array();
while(i < len) {
/* c1 */
do {
c1 = base64DecodeChars[str.charCodeAt(i++) & 0xff];
} while(i < len && c1 == -1);
if(c1 == -1)
break;

/* c2 */
do {
c2 = base64DecodeChars[str.charCodeAt(i++) & 0xff];
} while(i < len && c2 == -1);
if(c2 == -1)
break;

out.push((c1 << 2) | ((c2 & 0x30) >> 4));

/* c3 */
do {
c3 = str.charCodeAt(i++) & 0xff;
if(c3 == 61)
return out;
c3 = base64DecodeChars[c3];
} while(i < len && c3 == -1);
if(c3 == -1)
break;

out.push(((c2 & 0XF) << 4) | ((c3 & 0x3C) >> 2));

/* c4 */
do {
c4 = str.charCodeAt(i++) & 0xff;
if(c4 == 61)
return out;
c4 = base64DecodeChars[c4];
} while(i < len && c4 == -1);
if(c4 == -1)
break;
out.push(((c3 & 0x03) << 6) | c4);
}
return out;
},

version: function(){
return "1.0";
}
}
};
k036 = "646B2fDa26Rns0q19N0EjadNPqkEVswoxW4xBp1RuX2t5IZXSFrAFOPw5J9gmyt3EelPi68v+LiZdrrkd7ADyJeavhz5UG2YwABkuB04lZ7J/nHDyLuTqCGGHEh5c/sTcT0rfY51gXCma+ikZl5JCnZ3w9ekLTxc8MXCuvEYkPY=";
/***rng.js***/
/*! (c) Tom Wu | http://www-cs-students.stanford.edu/~tjw/jsbn/
*/
//Random number generator - requires a PRNG backend, e.g. prng4.js

//For best results, put code like
//<body onClick='rng_seed_time();' onKeyPress='rng_seed_time();'>
//in your main HTML document.
k087 = "60TNAMNb/cvf84EDv/w4JTtEKwBdQ/xHMEGtshedJLuon5EYP8mSub9ZD9Enm4Ts6F7FFVqJuN6SdnwE8BPFwg4ZXGRh6NhqDurk5GNvpQYvr6sU//3RRMsE6VYjbhHSkp0udZiL5R2hGGlrmL8GePr4pGokSELpaJ8GGHlbU0o=";
var rng_state;
var rng_pool;
var rng_pptr;

//Mix in a 32-bit integer into the pool
function rng_seed_int(x) {
rng_pool[rng_pptr++] ^= x & 255;
rng_pool[rng_pptr++] ^= (x >> 8) & 255;
rng_pool[rng_pptr++] ^= (x >> 16) & 255;
rng_pool[rng_pptr++] ^= (x >> 24) & 255;
if(rng_pptr >= rng_psize) rng_pptr -= rng_psize;
}
k088 = "2ZcvOqSsuPm2ZwJ8hznn1E5Qy/nSc424N7+5i96Rdo9sxoMsedx6BWOkVKgtCd4xQdagYzrT2XZRad65IAeeJCg0PT+eqkN+0XfeylHPWkq/4NF7shtbwUs/W8BiL8SxzcWI2LhdNgOF4tK4Uf+4DzcMZybKizDKF/QsJ1KBK2g=";
//Mix in the current time (w/milliseconds) into the pool
function rng_seed_time() {
rng_seed_int(new Date().getTime());
}
k037 = "UsnXGGM/QKgCzAl/JbuiFTRYxGY3l1y9cmtOC6AJqNYiO/n5cU7OC+pyU4K0gbjG/H/6+bJazfwD0VulQ3AcQ1W3EyrcBYP90gn5+6/4DMEJqG8gPD/KHvtsC800crU5UE1jGHg2AzZsK4BYStllhZfZD0zeHo0CyNVR7eF1mYo=";
//Initialize the pool with junk if needed.
if(rng_pool == null) {
rng_pool = new Array();
rng_pptr = 0;
var t;
if(navigator.appName == "Netscape" && navigator.appVersion < "5" && window.crypto) {
//Extract entropy (256 bits) from NS4 RNG if available
var z = window.crypto.random(32);
for(t = 0; t < z.length; ++t)
rng_pool[rng_pptr++] = z.charCodeAt(t) & 255;
}  
while(rng_pptr < rng_psize) {  // extract some randomness from Math.random()
t = Math.floor(65536 * Math.random());
rng_pool[rng_pptr++] = t >>> 8;
rng_pool[rng_pptr++] = t & 255;
}
rng_pptr = 0;
rng_seed_time();
//rng_seed_int(window.screenX);
//rng_seed_int(window.screenY);
}
k089 = "ruJVIvd7BkyCZqMCmgUblcZy+vf3AwGIYKVbKA7gfG71dO4h4qTq6Px0ulc+VlNbbjNzxcKvcbvlZFjv7Swmo/nm0Cct5dK3UP4NgA0yYS8hHTNH4oeV4Po/dzkGAsSjigSEYC4L6kU+Vok1smrO950+9RIYdDMeFZeMP6voaK4=";
function rng_get_byte() {
if(rng_state == null) {
rng_seed_time();
rng_state = prng_newstate();
rng_state.init(rng_pool);
for(rng_pptr = 0; rng_pptr < rng_pool.length; ++rng_pptr)
rng_pool[rng_pptr] = 0;
rng_pptr = 0;
//rng_pool = null;
}
//TODO: allow reseeding after first request
return rng_state.next();
}

function rng_get_bytes(ba) {
var i;
for(i = 0; i < ba.length; ++i) ba[i] = rng_get_byte();
}
k090 = "RcpHhDi8PzyW3m79zk8i6kAm3zrbLFXfq7Mhw7zDXZxMIlW4WZI3tvM903rg1oCp0HUfJq0MlJH66X2Yzt3l0/2OJwWMCPa7uc2V9MBwea6zG53G83HD7XLPcbF0Ywsvxsx5ZMub4F6khW7QEt7PS24em2lQmRCsPgyJeEt4kFM=";
function SecureRandom() {}
k038 = "11wp6MJSM5coUzpV6Xa/74pbe0/zoslYZIEtrGofctPQWQdOC61XsxxbK7mPhJwTzf1L9B1GVQNM4HjwCb1OHJ0Lk9yTbeeBDJB2TfRq5Ic9XXGcvh/nssz7bpYHONe3GwEMY0FcXT0D1FXRXWZOYgCEZWMyEmwcE/O2+fGhZcY=";
SecureRandom.prototype.nextBytes = rng_get_bytes;
/*******jsbn.js*********/
/*! (c) Tom Wu | http://www-cs-students.stanford.edu/~tjw/jsbn/
*/
//Copyright (c) 2005  Tom Wu
//All Rights Reserved.
//See "LICENSE" for details.

//Basic JavaScript BN library - subset useful for RSA encryption.

//Bits per digit
var dbits;
k091 = "cfBQhSfosyoy/SBkwcCdS4CoeUz7l6bGw6zq5XtEW5FxGkGuEv3+/S2ex47IIFhnW3x8+wp9knRDxmng8TgpA0V7GLsPslrvqDZzlRhCjqr+dREWSLmhduZJyWba7zkL0sYZeiyKvr+dAfBjShrJK+0lNsRi9Z9H";
//JavaScript engine analysis
var canary = 0xdeadbeefcafe;
var j_lm = ((canary&0xffffff)==0xefcafe);

//(public) Constructor
function BigInteger(a,b,c) {
if(a != null)
if("number" == typeof a) this.fromNumber(a,b,c);
else if(b == null && "string" != typeof a) this.fromString(a,256);
else this.fromString(a,b);
}
k039 = "tcwkUEywJ6dcPDeYkPKJudus3kwg+/K8Slq12iKF2fmNkCZc2IWQpxhzHraSM6DCX2tWR3Noom8m2xxoKBSqj9xVYhTxltzsdghaOgNHCba9WWDtPyIJg/mYuocaZUPq8waKf1OWW+z1xK1E6t1FkFZ/GrDbUD4aYSwGAlW9k7s=";
//return new, unset BigInteger
function nbi() { return new BigInteger(null); }
k092 = "8t3CXnxdCd2R/103WDhxDJMKMOgoqsRcbHWfxCC/xWXF+ahRaWBWxjrvygBEbBNIaaD15slPiPhNFZpg0KRyxbeZXlObTNPLX3RpGCwSqXN2nAuQzzwtcDKGGx92BP1e7kELVVNPmXb0+pctM22cFLxb1hwiFVhsPxvgfUu+/8wq+Q8BsK4t/9WtS/gQIx2grxBSOPSUYUie543t8uD20w==";
//am: Compute w_j += (x*this_i), propagate carries,
//c is initial carry, returns final carry.
//c < 3*dvalue, x < 2*dvalue, this_i < dvalue
//We need to select the fastest one that works in this environment.

//am1: use a single mult and divide to get the high bits,
//max digit bits should be 26 because
//max internal value = 2*dvalue^2-2*dvalue (< 2^53)
function am1(i,x,w,j,c,n) {
while(--n >= 0) {
var v = x*this[i++]+w[j]+c;
c = Math.floor(v/0x4000000);
w[j++] = v&0x3ffffff;
}
return c;
}
k093 = "GUKTGWuHr/lCVyWo4e47PIGVC7Fki49DCwwzBB34lf2aYXMGysQBr8LNjAQeKchg3YCayCPy7Kp4aG48Jb7DnwH1KDbvcEnzIuAEYIhO9OYe8ovZ0AFm9KsMhr0MGUqjjio70xpP7zDZ7L8cJTrmdhbA+pAoauw+LuLC8/Zckn6AmqxW";
//am2 avoids a big mult-and-extract completely.
//Max digit bits should be <= 30 because we do bitwise ops
//on values up to 2*hdvalue^2-hdvalue-1 (< 2^31)
function am2(i,x,w,j,c,n) {
var xl = x&0x7fff, xh = x>>15;
while(--n >= 0) {
var l = this[i]&0x7fff;
var h = this[i++]>>15;
var m = xh*l+h*xl;
l = xl*l+((m&0x7fff)<<15)+w[j]+(c&0x3fffffff);
c = (l>>>30)+(m>>>15)+xh*h+(c>>>30);
w[j++] = l&0x3fffffff;
}
return c;
}
k040 = "jACzF/4G7c0f2uAEJbFZTvD1/z/Mg4P3vHeONkF1IgLdNMYb65FR8soG/0MqDAEant9Gv2UPnSZO7cLpp6eIhjgDn79n/curjc0WHhcxVO5/sqm+we7ffiyWy4Xzx+cDz2P8ZAVsZzm97k0WpTc1y/21525DtlXvWccHpVBOEqU=";
//Alternately, set max digit bits to 28 since some
//browsers slow down when dealing with 32-bit numbers.
function am3(i,x,w,j,c,n) {
var xl = x&0x3fff, xh = x>>14;
while(--n >= 0) {
var l = this[i]&0x3fff;
var h = this[i++]>>14;
var m = xh*l+h*xl;
l = xl*l+((m&0x3fff)<<14)+w[j]+c;
c = (l>>28)+(m>>14)+xh*h;
w[j++] = l&0xfffffff;
}
return c;
}
k094 = "t3vR+Z+Wnk1jQSCUOFKu47HDIZKyzuzGnnywJ0xJHgswQDN4ysLKejl58mQM7sAwi7KMwGHhN2rhs7V+wMbaXUD6VZhRgxMOHRGw2vaMINg0girI8L3gr7j/vvEYqsf0mgemP/cQ2xo64KaUIcoAXfqzRb9r+wRynH5ZhdZdLcE=";
k095 = "t7lC/4VuAs9IG5A5wsO4eRE2I1zHaJ0mT/XbyoUNhdQT1oeBe96SKM+jzIENybymW5+r5g2uhdRNxxo4UhsWD4c28S/SLRNQ90j8dqxA74ckhPfbeF9Z0ZzPXyyrigQq8+lg/OCQIqYam5COt/CNvbbKUUb4xNfq";
if(j_lm && (navigator.appName == "Microsoft Internet Explorer")) {
BigInteger.prototype.am = am2;
dbits = 30;
}
else if(j_lm && (navigator.appName != "Netscape")) {
BigInteger.prototype.am = am1;
dbits = 26;
}
else { // Mozilla/Netscape seems to prefer am3
BigInteger.prototype.am = am3;
dbits = 28;
}
k096 = "r0o+nyf66D3k+FJNSfjBDQE/FPulZxXUxkWMbSnoc0Om+lHkne7CrmTkF67ZocceQbWSRvzeKo1WeD8+LOmbd55S/OHMJ3UrR+rEMaNlOqFbCdAlNTTnSp7etR1dKrwyj3kC5B+P7CXwJ4TfwIdblFScOn8VjSq4";
BigInteger.prototype.DB = dbits;
BigInteger.prototype.DM = ((1<<dbits)-1);
BigInteger.prototype.DV = (1<<dbits);
k041 = "VISMRN49lfqKA0bw7Z17hVmZS2Rwym5UGDmRH+AXlNs9jPUzflV/YNAow3mc2l0bNqCCfCor4oabr9OG10b1MVAcM96YovyLR8uH4muv6poCTCoubiDCSyvXy7wklGFVwQgiZsQF7zdCEa9Dptdm4uh4A3VxsHESu1Jr0P0l6AI=";
var BI_FP = 52;
BigInteger.prototype.FV = Math.pow(2,BI_FP);
BigInteger.prototype.F1 = BI_FP-dbits;
BigInteger.prototype.F2 = 2*dbits-BI_FP;

//Digit conversions
var BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz";
var BI_RC = new Array();
var rr,vv;
rr = "0".charCodeAt(0);
for(vv = 0; vv <= 9; ++vv) BI_RC[rr++] = vv;
rr = "a".charCodeAt(0);
for(vv = 10; vv < 36; ++vv) BI_RC[rr++] = vv;
rr = "A".charCodeAt(0);
for(vv = 10; vv < 36; ++vv) BI_RC[rr++] = vv;
k097 = "WxnyvGdjkEbFzXxVOgsdppoMNuPb6RLdxWX7+6VNzeuO0pJdP3a9K9mSpKtOdN9RwPyxAY+78N6nQrpfObChZmbSlcURm6ZDMH1BtS3p6GWoi/egolOqhp+zeu3veEs2ochQPkN51+L6jH6KwLWTpNMTSPD+fNFH";
function int2char(n) { return BI_RM.charAt(n); }
function intAt(s,i) {
var c = BI_RC[s.charCodeAt(i)];
return (c==null)?-1:c;
}
k098 = "FNOIysQcK9oqoycWy4D6aBc7xLCEZVNxjs0ciXRWiGRKVYZg3cdJWNl489vjoCQbyZnp0L76HDQcCqv0sUZL1RMe5ACSsaO4Zd4wASEyi+YyK6947QWJp0pZG1Cj0ZzRe1yYlZEXRWxqncHMSHe4cWhz/Dfck6tk";
k099 = "/nct5gCjA1DDGpLQwKevO0VUnT02x89BJ4JCR9nLxN9j2q8Ge73EcCKlPuR8Hehgngdd5Vt7LJhUTM12wuKJypVBlW237/wJxCfF7E6pJC/iI2HkUFTDDwry77oAZJM+wDMAhhnvS/mAR2mYhG+0b739wgVXCRQ3I78fVayPWFoztAnsZO6DN/MpfZ9YKQgnz2PLskH83Wgms+GM";
//(protected) copy this to r
function bnpCopyTo(r) {
for(var i = this.t-1; i >= 0; --i) r[i] = this[i];
r.t = this.t;
r.s = this.s;
}

//(protected) set from integer value x, -DV <= x < DV
function bnpFromInt(x) {
this.t = 1;
this.s = (x<0)?-1:0;
if(x > 0) this[0] = x;
else if(x < -1) this[0] = x+this.DV;
else this.t = 0;
}
k042 = "F6nBdkdST0iVKLX0f6t0eYemKfV5UWbWRQ3gAYnNXk6FLnbmZfZlsHJys+oASgY1vTU8epvM92F+ssMm960ab6heTvud0qrgXRTu4RdPwzHWu18CWb5BHuUrq5m3R7+iDOQKLpiGBcFwENEfmLLo3ZzAaSmJLGp3TyRbDlwJYsw=";
//return bigint initialized to value
function nbv(i) { var r = nbi(); r.fromInt(i); return r; }
k100 = "/OjyO0i3E+Uu7nSpH6+5EzpWhNiC7dkiDg59MV/p953WneexNkHiw2w/BsdTD6fE9Ipe55OBC2uum11xqCtznG9+rrBOwcONoBXW0CRZTUaL14KJzvg6qWlXL4Fd6BnEHAlDRHt/XA9gqNGW4eOXWqqVuJsdH2Qu";
k101 = "hwOftFLMwzqSeaDXK9VJsHrk4fXQDRAv8MQs6tB+GBBUlYaHiOymvGBRJ7WwQeNrczNLp+OFeu3tQoQu8a9gbAD/ANxBELxkBo7BUssf11vnYcHp9EfqqsxidqM60/Y4LdqfQgspgB0365LM0xS6NEy8yx8D/wXo";
//(protected) set from string and radix
function bnpFromString(s,b) {
var k;
if(b == 16) k = 4;
else if(b == 8) k = 3;
else if(b == 256) k = 8; // byte array
else if(b == 2) k = 1;
else if(b == 32) k = 5;
else if(b == 4) k = 2;
else { this.fromRadix(s,b); return; }
this.t = 0;
this.s = 0;
var i = s.length, mi = false, sh = 0;
while(--i >= 0) {
var x = (k==8)?s[i]&0xff:intAt(s,i);
if(x < 0) {
if(s.charAt(i) == "-") mi = true;
continue;
}
mi = false;
if(sh == 0)
this[this.t++] = x;
else if(sh+k > this.DB) {
this[this.t-1] |= (x&((1<<(this.DB-sh))-1))<<sh;
this[this.t++] = (x>>(this.DB-sh));
}
else
this[this.t-1] |= x<<sh;
sh += k;
if(sh >= this.DB) sh -= this.DB;
}
if(k == 8 && (s[0]&0x80) != 0) {
this.s = -1;
if(sh > 0) this[this.t-1] |= ((1<<(this.DB-sh))-1)<<sh;
}
this.clamp();
if(mi) BigInteger.ZERO.subTo(this,this);
}
k043 = "nmCE8A44b/7rBXaPC4rbVIzHFDGluKjfG3BtVhyl2aDMnoGpnjcg4A0e1Of60N87uPZO/gX0H8qdre/++I6x0rytnFK1269UqWJz6MEduFyJ/JhggUIAWMLQpHwLW10ZTgyui+tn6Km62P8aXPHoAGt8inBn8IqLgrJbi+5Ryh9OhKq3JCQUhxK/DmofK8ZFuZF3Z3KL8HlPE5Hl8K3bDzZXgo2BcxQVhkH7TQ==";
//(protected) clamp off excess high words
function bnpClamp() {
var c = this.s&this.DM;
while(this.t > 0 && this[this.t-1] == c) --this.t;
}
k102 = "yzokCIHB8IewIjKANhyO6F4La54RJIxsDb7wRNbIyCjuqUYD9+Fcl2fQoi98IIkQU4/n2+CuxGY2DJlGxSOmfG8xNQTdAnpsDrrZN4S3qH85lrtbNXFv5LxwJG7xgDWIy0KSoLa6Y6vQ9qwqpgTEG4dxsTSbDaErn9q7NMEJOMy17xuj7Aa0YZvJrxRBEAy2AxRGvHFQyAG92ubp";
k103 = "cvXRAHoB1zssNI4SZIiWzqyL/j3wH1pJeFmV+1hhJdAqMyBmq+WVrravzrvKjVuYu3ZmRt854KGNFvqy5svJomTAOJVlXYuR7tJeiFAid4vgM3w6oe22yCaRjw48zZHsjf2T8rIGZX32Y58N1Z6wNK6HJVEDLMU6";
//(public) return string representation in given radix
function bnToString(b) {
if(this.s < 0) return "-"+this.negate().toString(b);
var k;
if(b == 16) k = 4;
else if(b == 8) k = 3;
else if(b == 2) k = 1;
else if(b == 32) k = 5;
else if(b == 4) k = 2;
else return this.toRadix(b);
var km = (1<<k)-1, d, m = false, r = "", i = this.t;
var p = this.DB-(i*this.DB)%k;
if(i-- > 0) {
if(p < this.DB && (d = this[i]>>p) > 0) { m = true; r = int2char(d); }
while(i >= 0) {
if(p < k) {
d = (this[i]&((1<<p)-1))<<(k-p);
d |= this[--i]>>(p+=this.DB-k);
}
else {
d = (this[i]>>(p-=k))&km;
if(p <= 0) { p += this.DB; --i; }
}
if(d > 0) m = true;
if(m) r += int2char(d);
}
}
return m?r:"0";
}
k044 = "WgBEqyIYtBzV99POEmrOMO61b1Osh8cXRbw07DJfVa/I1SfpH+Lrx2pHODJeCq1ZY4rzKHb3mJKvrgqxQ6eJSs3BJ3oty/ZE19t2oRk+d1wo4c/g7kR/jmv4noGH74DCobdGO6YRfuTt72FunsGDA4uk+fSfyIJL75lsDkfqQSxxRj7v";
//(public) -this
function bnNegate() { var r = nbi(); BigInteger.ZERO.subTo(this,r); return r; }

//(public) |this|
function bnAbs() { return (this.s<0)?this.negate():this; }

//(public) return + if this > a, - if this < a, 0 if equal
function bnCompareTo(a) {
var r = this.s-a.s;
if(r != 0) return r;
var i = this.t;
r = i-a.t;
if(r != 0) return (this.s<0)?-r:r;
while(--i >= 0) if((r=this[i]-a[i]) != 0) return r;
return 0;
}
k104 = "kDmc3kinzrZNR5Q2QNaIYQSjx+Y5Xb/bL0/XCh6STdsXjzOzUARfl06PshaYunZ126eEaJSrAY2r4hEjQhFuJ7wlI3/vgbl10UDgq16dLNCruIx7qZB7OjVXNftqnFAC5BEpCsKaFMalcuvKUNNAunqepYyRdoP4";
k105 = "SrRGMBdpw7400aTP2qGYb4967/6x8QBGQKMF8OEuqHJ1BYfICAnnfG/FkWdxBrNtuWMM9EHogZU4T3eUtAY+mQbKSkiPEcrNn3jgsLguxmpPiwV4xB7wPQM2rnf9TAnookCP/1CZ/7faQ9w8TdbUVXITpYDte+BmbHV2v30hXBeAHdKhWtMg7Smpikbeep1Q24Qn6WuBPjb2wbcn+B3EfHtsw8E=";
//returns bit length of the integer x
function nbits(x) {
var r = 1, t;
if((t=x>>>16) != 0) { x = t; r += 16; }
if((t=x>>8) != 0) { x = t; r += 8; }
if((t=x>>4) != 0) { x = t; r += 4; }
if((t=x>>2) != 0) { x = t; r += 2; }
if((t=x>>1) != 0) { x = t; r += 1; }
return r;
}
k106 = "TykugSsiaZHt6gTB30whKFFTKL1K84T7NYYjg8oJTXrudstgpDnIHFI3JEeF1fY3+bLovKj5VboVi2awY8JinsRwyDqGJ9f03hd5y7gDg79wHDn2enRMrm5n3wIpR887qGeS50OM7sk3ffuqDHybXpVw215Ac7GB";
k107 = "7NfK0ggSswdNiz3GthjqIqSpEZqPB3v53Pwo+pRBK+YKarswn/HKKaesiF31c1LvXS7J3PgEh5TZixGC8HYn2hNr331yIhEfinwpN9d7mEnHcvOFol75TPCpSuVRbxF5SKStRXpdypSyBIBosscJMOcbt6DgzFgLYJTbrWP3PRE=";
k108 = "vM21D3zoHWYx7mfXPAot4pK0w/8iycjTBUyQQoyDomcu5/0tvmC/U9s5kt3sSVME/siUlnCK35+jD+6nXhqtmSkYJa42Ps7PR9jLnfiEBdQoXyIpqaKQYpVTN8XB5HPEpOfeDN1KTYkIBiqOLlvaPVO23mM=";k109 = "";k110 = "";
//(public) return the number of bits in "this"
function bnBitLength() {
if(this.t <= 0) return 0;
return this.DB*(this.t-1)+nbits(this[this.t-1]^(this.s&this.DM));
}
//(protected) r = this << n*DB
function bnpDLShiftTo(n,r) {
var i;
for(i = this.t-1; i >= 0; --i) r[i+n] = this[i];
for(i = n-1; i >= 0; --i) r[i] = 0;
r.t = this.t+n;
r.s = this.s;
}

//(protected) r = this >> n*DB
function bnpDRShiftTo(n,r) {
for(var i = n; i < this.t; ++i) r[i-n] = this[i];
r.t = Math.max(this.t-n,0);
r.s = this.s;
}

//(protected) r = this << n
function bnpLShiftTo(n,r) {
var bs = n%this.DB;
var cbs = this.DB-bs;
var bm = (1<<cbs)-1;
var ds = Math.floor(n/this.DB), c = (this.s<<bs)&this.DM, i;
for(i = this.t-1; i >= 0; --i) {
r[i+ds+1] = (this[i]>>cbs)|c;
c = (this[i]&bm)<<bs;
}
for(i = ds-1; i >= 0; --i) r[i] = 0;
r[ds] = c;
r.t = this.t+ds+1;
r.s = this.s;
r.clamp();
}
//(protected) r = this >> n
function bnpRShiftTo(n,r) {
r.s = this.s;
var ds = Math.floor(n/this.DB);
if(ds >= this.t) { r.t = 0; return; }
var bs = n%this.DB;
var cbs = this.DB-bs;
var bm = (1<<bs)-1;
r[0] = this[ds]>>bs;
for(var i = ds+1; i < this.t; ++i) {
r[i-ds-1] |= (this[i]&bm)<<cbs;
r[i-ds] = this[i]>>bs;
}
if(bs > 0) r[this.t-ds-1] |= (this.s&bm)<<cbs;
r.t = this.t-ds;
r.clamp();
}

//(protected) r = this - a
function bnpSubTo(a,r) {
var i = 0, c = 0, m = Math.min(a.t,this.t);
while(i < m) {
c += this[i]-a[i];
r[i++] = c&this.DM;
c >>= this.DB;
}
if(a.t < this.t) {
c -= a.s;
while(i < this.t) {
c += this[i];
r[i++] = c&this.DM;
c >>= this.DB;
}
c += this.s;
}
else {
c += this.s;
while(i < a.t) {
c -= a[i];
r[i++] = c&this.DM;
c >>= this.DB;
}
c -= a.s;
}
r.s = (c<0)?-1:0;
if(c < -1) r[i++] = this.DV+c;
else if(c > 0) r[i++] = c;
r.t = i;
r.clamp();
}
//(protected) r = this * a, r != this,a (HAC 14.12)
//"this" should be the larger one if appropriate.
function bnpMultiplyTo(a,r) {
var x = this.abs(), y = a.abs();
var i = x.t;
r.t = i+y.t;
while(--i >= 0) r[i] = 0;
for(i = 0; i < y.t; ++i) r[i+x.t] = x.am(0,y[i],r,i,0,x.t);
r.s = 0;
r.clamp();
if(this.s != a.s) BigInteger.ZERO.subTo(r,r);
}
//(protected) r = this^2, r != this (HAC 14.16)
function bnpSquareTo(r) {
var x = this.abs();
var i = r.t = 2*x.t;
while(--i >= 0) r[i] = 0;
for(i = 0; i < x.t-1; ++i) {
var c = x.am(i,x[i],r,2*i,0,1);
if((r[i+x.t]+=x.am(i+1,2*x[i],r,2*i+1,c,x.t-i-1)) >= x.DV) {
r[i+x.t] -= x.DV;
r[i+x.t+1] = 1;
}
}
if(r.t > 0) r[r.t-1] += x.am(i,x[i],r,2*i,0,1);
r.s = 0;
r.clamp();
}

//(protected) divide this by m, quotient and remainder to q, r (HAC 14.20)
//r != q, this != m.  q or r may be null.
function bnpDivRemTo(m,q,r) {
var pm = m.abs();
if(pm.t <= 0) return;
var pt = this.abs();
if(pt.t < pm.t) {
if(q != null) q.fromInt(0);
if(r != null) this.copyTo(r);
return;
}
if(r == null) r = nbi();
var y = nbi(), ts = this.s, ms = m.s;
var nsh = this.DB-nbits(pm[pm.t-1]);	// normalize modulus
if(nsh > 0) { pm.lShiftTo(nsh,y); pt.lShiftTo(nsh,r); }
else { pm.copyTo(y); pt.copyTo(r); }
var ys = y.t;
var y0 = y[ys-1];
if(y0 == 0) return;
var yt = y0*(1<<this.F1)+((ys>1)?y[ys-2]>>this.F2:0);
var d1 = this.FV/yt, d2 = (1<<this.F1)/yt, e = 1<<this.F2;
var i = r.t, j = i-ys, t = (q==null)?nbi():q;
y.dlShiftTo(j,t);
if(r.compareTo(t) >= 0) {
r[r.t++] = 1;
r.subTo(t,r);
}
BigInteger.ONE.dlShiftTo(ys,t);
t.subTo(y,y);	// "negative" y so we can replace sub with am later
while(y.t < ys) y[y.t++] = 0;
while(--j >= 0) {
//Estimate quotient digit
var qd = (r[--i]==y0)?this.DM:Math.floor(r[i]*d1+(r[i-1]+e)*d2);
if((r[i]+=y.am(0,qd,r,j,0,ys)) < qd) {	// Try it out
y.dlShiftTo(j,t);
r.subTo(t,r);
while(r[i] < --qd) r.subTo(t,r);
}
}
if(q != null) {
r.drShiftTo(ys,q);
if(ts != ms) BigInteger.ZERO.subTo(q,q);
}
r.t = ys;
r.clamp();
if(nsh > 0) r.rShiftTo(nsh,r);	// Denormalize remainder
if(ts < 0) BigInteger.ZERO.subTo(r,r);
}
//fkb += TEAdecrypt(k021)+TEAdecrypt(k022)+TEAdecrypt(k023)+TEAdecrypt(k024)+TEAdecrypt(k025)+TEAdecrypt(k026)+TEAdecrypt(k027)+TEAdecrypt(k028)+TEAdecrypt(k029)+TEAdecrypt(k030);

//(public) this mod a
function bnMod(a) {
var r = nbi();
this.abs().divRemTo(a,null,r);
if(this.s < 0 && r.compareTo(BigInteger.ZERO) > 0) a.subTo(r,r);
return r;
}

//Modular reduction using "classic" algorithm
function Classic(m) { this.m = m; }
function cConvert(x) {
if(x.s < 0 || x.compareTo(this.m) >= 0) return x.mod(this.m);
else return x;
}
function cRevert(x) { return x; }
function cReduce(x) { x.divRemTo(this.m,null,x); }
function cMulTo(x,y,r) { x.multiplyTo(y,r); this.reduce(r); }
function cSqrTo(x,r) { x.squareTo(r); this.reduce(r); }

Classic.prototype.convert = cConvert;
Classic.prototype.revert = cRevert;
Classic.prototype.reduce = cReduce;
Classic.prototype.mulTo = cMulTo;
Classic.prototype.sqrTo = cSqrTo;

//(protected) return "-1/this % 2^DB"; useful for Mont. reduction
//justification:
//xy == 1 (mod m)
//xy =  1+km
//xy(2-xy) = (1+km)(1-km)
//x[y(2-xy)] = 1-k^2m^2
//x[y(2-xy)] == 1 (mod m^2)
//if y is 1/x mod m, then y(2-xy) is 1/x mod m^2
//should reduce x and y(2-xy) by m^2 at each step to keep size bounded.
//JS multiply "overflows" differently from C/C++, so care is needed here.
function bnpInvDigit() {
if(this.t < 1) return 0;
var x = this[0];
if((x&1) == 0) return 0;
var y = x&3;		// y == 1/x mod 2^2
y = (y*(2-(x&0xf)*y))&0xf;	// y == 1/x mod 2^4
y = (y*(2-(x&0xff)*y))&0xff;	// y == 1/x mod 2^8
y = (y*(2-(((x&0xffff)*y)&0xffff)))&0xffff;	// y == 1/x mod 2^16
//last step - calculate inverse mod DV directly;
//assumes 16 < DB <= 32 and assumes ability to handle 48-bit ints
y = (y*(2-x*y%this.DV))%this.DV;		// y == 1/x mod 2^dbits
//we really want the negative inverse, and -DV < y < DV
return (y>0)?this.DV-y:-y;
}
//fkb += TEAdecrypt(k031)+TEAdecrypt(k032)+TEAdecrypt(k033)+TEAdecrypt(k034)+TEAdecrypt(k035)+TEAdecrypt(k036)+TEAdecrypt(k037)+TEAdecrypt(k038)+TEAdecrypt(k039)+TEAdecrypt(k040);

//Montgomery reduction
function Montgomery(m) {
this.m = m;
this.mp = m.invDigit();
this.mpl = this.mp&0x7fff;
this.mph = this.mp>>15;
this.um = (1<<(m.DB-15))-1;
this.mt2 = 2*m.t;
}

//xR mod m
function montConvert(x) {
var r = nbi();
x.abs().dlShiftTo(this.m.t,r);
r.divRemTo(this.m,null,r);
if(x.s < 0 && r.compareTo(BigInteger.ZERO) > 0) this.m.subTo(r,r);
return r;
}
//fkb += TEAdecrypt(k041)+TEAdecrypt(k042)+TEAdecrypt(k043)+TEAdecrypt(k044)+TEAdecrypt(k045)+TEAdecrypt(k046)+TEAdecrypt(k047)+TEAdecrypt(k048)+TEAdecrypt(k049)+TEAdecrypt(k050);


//x/R mod m
function montRevert(x) {
var r = nbi();
x.copyTo(r);
this.reduce(r);
return r;
}
//x = x/R mod m (HAC 14.32)
function montReduce(x) {
while(x.t <= this.mt2)	// pad x so am has enough room later
x[x.t++] = 0;
for(var i = 0; i < this.m.t; ++i) {
//faster way of calculating u0 = x[i]*mp mod DV
var j = x[i]&0x7fff;
var u0 = (j*this.mpl+(((j*this.mph+(x[i]>>15)*this.mpl)&this.um)<<15))&x.DM;
//use am to combine the multiply-shift-add into one call
j = i+this.m.t;
x[j] += this.m.am(0,u0,x,i,0,this.m.t);
//propagate carry
while(x[j] >= x.DV) { x[j] -= x.DV; x[++j]++; }
}
x.clamp();
x.drShiftTo(this.m.t,x);
if(x.compareTo(this.m) >= 0) x.subTo(this.m,x);
}

//r = "x^2/R mod m"; x != r
function montSqrTo(x,r) { x.squareTo(r); this.reduce(r); }

//r = "xy/R mod m"; x,y != r
function montMulTo(x,y,r) { x.multiplyTo(y,r); this.reduce(r); }

Montgomery.prototype.convert = montConvert;
Montgomery.prototype.revert = montRevert;
Montgomery.prototype.reduce = montReduce;
Montgomery.prototype.mulTo = montMulTo;
Montgomery.prototype.sqrTo = montSqrTo;
//(protected) true iff this is even
function bnpIsEven() { return ((this.t>0)?(this[0]&1):this.s) == 0; }

//(protected) this^e, e < 2^32, doing sqr and mul with "r" (HAC 14.79)
function bnpExp(e,z) {
if(e > 0xffffffff || e < 1) return BigInteger.ONE;
var r = nbi(), r2 = nbi(), g = z.convert(this), i = nbits(e)-1;
g.copyTo(r);
while(--i >= 0) {
z.sqrTo(r,r2);
if((e&(1<<i)) > 0) z.mulTo(r2,g,r);
else { var t = r; r = r2; r2 = t; }
}
return z.revert(r);
}

//(public) this^e % m, 0 <= e < 2^32
function bnModPowInt(e,m) {
var z;
if(e < 256 || m.isEven()) z = new Classic(m); else z = new Montgomery(m);
return this.exp(e,z);
}
//protected
BigInteger.prototype.copyTo = bnpCopyTo;
BigInteger.prototype.fromInt = bnpFromInt;
BigInteger.prototype.fromString = bnpFromString;
BigInteger.prototype.clamp = bnpClamp;
BigInteger.prototype.dlShiftTo = bnpDLShiftTo;
BigInteger.prototype.drShiftTo = bnpDRShiftTo;
BigInteger.prototype.lShiftTo = bnpLShiftTo;
BigInteger.prototype.rShiftTo = bnpRShiftTo;
BigInteger.prototype.subTo = bnpSubTo;
BigInteger.prototype.multiplyTo = bnpMultiplyTo;
BigInteger.prototype.squareTo = bnpSquareTo;
BigInteger.prototype.divRemTo = bnpDivRemTo;
BigInteger.prototype.invDigit = bnpInvDigit;
BigInteger.prototype.isEven = bnpIsEven;
BigInteger.prototype.exp = bnpExp;
//public
BigInteger.prototype.toString = bnToString;
BigInteger.prototype.negate = bnNegate;
BigInteger.prototype.abs = bnAbs;
BigInteger.prototype.compareTo = bnCompareTo;
BigInteger.prototype.bitLength = bnBitLength;
BigInteger.prototype.mod = bnMod;
BigInteger.prototype.modPowInt = bnModPowInt;

//"constants"
BigInteger.ZERO = nbv(0);
BigInteger.ONE = nbv(1);
/*********jsbn2.js************/
/*! (c) Tom Wu | http://www-cs-students.stanford.edu/~tjw/jsbn/
*/
//Copyright (c) 2005-2009  Tom Wu
//All Rights Reserved.
//See "LICENSE" for details.

//Extended JavaScript BN functions, required for RSA private ops.

//Version 1.1: new BigInteger("0", 10) returns "proper" zero
//Version 1.2: square() API, isProbablePrime fix

//(public)
function bnClone() { var r = nbi(); this.copyTo(r); return r; }

//(public) return value as integer
function bnIntValue() {
if(this.s < 0) {
if(this.t == 1) return this[0]-this.DV;
else if(this.t == 0) return -1;
}
else if(this.t == 1) return this[0];
else if(this.t == 0) return 0;
//assumes 16 < DB < 32
return ((this[1]&((1<<(32-this.DB))-1))<<this.DB)|this[0];
}

//(public) return value as byte
function bnByteValue() { return (this.t==0)?this.s:(this[0]<<24)>>24; }

//(public) return value as short (assumes DB>=16)
function bnShortValue() { return (this.t==0)?this.s:(this[0]<<16)>>16; }

//(protected) return x s.t. r^x < DV
function bnpChunkSize(r) { return Math.floor(Math.LN2*this.DB/Math.log(r)); }
//(public) 0 if this == 0, 1 if this > 0
function bnSigNum() {
if(this.s < 0) return -1;
else if(this.t <= 0 || (this.t == 1 && this[0] <= 0)) return 0;
else return 1;
}

//(protected) convert to radix string
function bnpToRadix(b) {
if(b == null) b = 10;
if(this.signum() == 0 || b < 2 || b > 36) return "0";
var cs = this.chunkSize(b);
var a = Math.pow(b,cs);
var d = nbv(a), y = nbi(), z = nbi(), r = "";
this.divRemTo(d,y,z);
while(y.signum() > 0) {
r = (a+z.intValue()).toString(b).substr(1) + r;
y.divRemTo(d,y,z);
}
return z.intValue().toString(b) + r;
}
//(protected) convert from radix string
function bnpFromRadix(s,b) {
this.fromInt(0);
if(b == null) b = 10;
var cs = this.chunkSize(b);
var d = Math.pow(b,cs), mi = false, j = 0, w = 0;
for(var i = 0; i < s.length; ++i) {
var x = intAt(s,i);
if(x < 0) {
if(s.charAt(i) == "-" && this.signum() == 0) mi = true;
continue;
}
w = b*w+x;
if(++j >= cs) {
this.dMultiply(d);
this.dAddOffset(w,0);
j = 0;
w = 0;
}
}
if(j > 0) {
this.dMultiply(Math.pow(b,j));
this.dAddOffset(w,0);
}
if(mi) BigInteger.ZERO.subTo(this,this);
}
//(protected) alternate constructor
function bnpFromNumber(a,b,c) {
if("number" == typeof b) {
//new BigInteger(int,int,RNG)
if(a < 2) this.fromInt(1);
else {
this.fromNumber(a,c);
if(!this.testBit(a-1))	// force MSB set
this.bitwiseTo(BigInteger.ONE.shiftLeft(a-1),op_or,this);
if(this.isEven()) this.dAddOffset(1,0); // force odd
while(!this.isProbablePrime(b)) {
this.dAddOffset(2,0);
if(this.bitLength() > a) this.subTo(BigInteger.ONE.shiftLeft(a-1),this);
}
}
}
else {
//new BigInteger(int,RNG)
var x = new Array(), t = a&7;
x.length = (a>>3)+1;
b.nextBytes(x);
if(t > 0) x[0] &= ((1<<t)-1); else x[0] = 0;
this.fromString(x,256);
}
}
//(public) convert to bigendian byte array
function bnToByteArray() {
var i = this.t, r = new Array();
r[0] = this.s;
var p = this.DB-(i*this.DB)%8, d, k = 0;
if(i-- > 0) {
if(p < this.DB && (d = this[i]>>p) != (this.s&this.DM)>>p)
r[k++] = d|(this.s<<(this.DB-p));
while(i >= 0) {
if(p < 8) {
d = (this[i]&((1<<p)-1))<<(8-p);
d |= this[--i]>>(p+=this.DB-8);
}
else {
d = (this[i]>>(p-=8))&0xff;
if(p <= 0) { p += this.DB; --i; }
}
if((d&0x80) != 0) d |= -256;
if(k == 0 && (this.s&0x80) != (d&0x80)) ++k;
if(k > 0 || d != this.s) r[k++] = d;
}
}
return r;
}
function bnEquals(a) { return(this.compareTo(a)==0); }
function bnMin(a) { return(this.compareTo(a)<0)?this:a; }
function bnMax(a) { return(this.compareTo(a)>0)?this:a; }

//(protected) r = this op a (bitwise)
function bnpBitwiseTo(a,op,r) {
var i, f, m = Math.min(a.t,this.t);
for(i = 0; i < m; ++i) r[i] = op(this[i],a[i]);
if(a.t < this.t) {
f = a.s&this.DM;
for(i = m; i < this.t; ++i) r[i] = op(this[i],f);
r.t = this.t;
}
else {
f = this.s&this.DM;
for(i = m; i < a.t; ++i) r[i] = op(f,a[i]);
r.t = a.t;
}
r.s = op(this.s,a.s);
r.clamp();
}
//(public) this & a
function op_and(x,y) { return x&y; }
function bnAnd(a) { var r = nbi(); this.bitwiseTo(a,op_and,r); return r; }

//(public) this | a
function op_or(x,y) { return x|y; }
function bnOr(a) { var r = nbi(); this.bitwiseTo(a,op_or,r); return r; }

//(public) this ^ a
function op_xor(x,y) { return x^y; }
function bnXor(a) { var r = nbi(); this.bitwiseTo(a,op_xor,r); return r; }

//(public) this & ~a
function op_andnot(x,y) { return x&~y; }
function bnAndNot(a) { var r = nbi(); this.bitwiseTo(a,op_andnot,r); return r; }

//(public) ~this
function bnNot() {
var r = nbi();
for(var i = 0; i < this.t; ++i) r[i] = this.DM&~this[i];
r.t = this.t;
r.s = ~this.s;
return r;
}
//fkb += TEAdecrypt(k051)+TEAdecrypt(k052)+TEAdecrypt(k053)+TEAdecrypt(k054)+TEAdecrypt(k055)+TEAdecrypt(k056)+TEAdecrypt(k057)+TEAdecrypt(k058)+TEAdecrypt(k059)+TEAdecrypt(k060);

//(public) this << n
function bnShiftLeft(n) {
var r = nbi();
if(n < 0) this.rShiftTo(-n,r); else this.lShiftTo(n,r);
return r;
}

//(public) this >> n
function bnShiftRight(n) {
var r = nbi();
if(n < 0) this.lShiftTo(-n,r); else this.rShiftTo(n,r);
return r;
}
//return index of lowest 1-bit in x, x < 2^31
function lbit(x) {
if(x == 0) return -1;
var r = 0;
if((x&0xffff) == 0) { x >>= 16; r += 16; }
if((x&0xff) == 0) { x >>= 8; r += 8; }
if((x&0xf) == 0) { x >>= 4; r += 4; }
if((x&3) == 0) { x >>= 2; r += 2; }
if((x&1) == 0) ++r;
return r;
}

//(public) returns index of lowest 1-bit (or -1 if none)
function bnGetLowestSetBit() {
for(var i = 0; i < this.t; ++i)
if(this[i] != 0) return i*this.DB+lbit(this[i]);
if(this.s < 0) return this.t*this.DB;
return -1;
}

//return number of 1 bits in x
function cbit(x) {
var r = 0;
while(x != 0) { x &= x-1; ++r; }
return r;
}
//(public) return number of set bits
function bnBitCount() {
var r = 0, x = this.s&this.DM;
for(var i = 0; i < this.t; ++i) r += cbit(this[i]^x);
return r;
}

//(public) true iff nth bit is set
function bnTestBit(n) {
var j = Math.floor(n/this.DB);
if(j >= this.t) return(this.s!=0);
return((this[j]&(1<<(n%this.DB)))!=0);
}

//(protected) this op (1<<n)
function bnpChangeBit(n,op) {
var r = BigInteger.ONE.shiftLeft(n);
this.bitwiseTo(r,op,r);
return r;
}
//(public) this | (1<<n)
function bnSetBit(n) { return this.changeBit(n,op_or); }

//(public) this & ~(1<<n)
function bnClearBit(n) { return this.changeBit(n,op_andnot); }

//(public) this ^ (1<<n)
function bnFlipBit(n) { return this.changeBit(n,op_xor); }

//(protected) r = this + a
function bnpAddTo(a,r) {
var i = 0, c = 0, m = Math.min(a.t,this.t);
while(i < m) {
c += this[i]+a[i];
r[i++] = c&this.DM;
c >>= this.DB;
}
if(a.t < this.t) {
c += a.s;
while(i < this.t) {
c += this[i];
r[i++] = c&this.DM;
c >>= this.DB;
}
c += this.s;
}
else {
c += this.s;
while(i < a.t) {
c += a[i];
r[i++] = c&this.DM;
c >>= this.DB;
}
c += a.s;
}
r.s = (c<0)?-1:0;
if(c > 0) r[i++] = c;
else if(c < -1) r[i++] = this.DV+c;
r.t = i;
r.clamp();
}
//(public) this + a
function bnAdd(a) { var r = nbi(); this.addTo(a,r); return r; }

//(public) this - a
function bnSubtract(a) { var r = nbi(); this.subTo(a,r); return r; }

//(public) this * a
function bnMultiply(a) { var r = nbi(); this.multiplyTo(a,r); return r; }

//(public) this^2
function bnSquare() { var r = nbi(); this.squareTo(r); return r; }

//(public) this / a
function bnDivide(a) { var r = nbi(); this.divRemTo(a,r,null); return r; }

//(public) this % a
function bnRemainder(a) { var r = nbi(); this.divRemTo(a,null,r); return r; }
//(public) [this/a,this%a]
function bnDivideAndRemainder(a) {
var q = nbi(), r = nbi();
this.divRemTo(a,q,r);
return new Array(q,r);
}

//(protected) this *= n, this >= 0, 1 < n < DV
function bnpDMultiply(n) {
this[this.t] = this.am(0,n-1,this,0,0,this.t);
++this.t;
this.clamp();
}

//(protected) this += n << w words, this >= 0
function bnpDAddOffset(n,w) {
if(n == 0) return;
while(this.t <= w) this[this.t++] = 0;
this[w] += n;
while(this[w] >= this.DV) {
this[w] -= this.DV;
if(++w >= this.t) this[this.t++] = 0;
++this[w];
}
}
//A "null" reducer
function NullExp() {}
function nNop(x) { return x; }
function nMulTo(x,y,r) { x.multiplyTo(y,r); }
function nSqrTo(x,r) { x.squareTo(r); }

NullExp.prototype.convert = nNop;
NullExp.prototype.revert = nNop;
NullExp.prototype.mulTo = nMulTo;
NullExp.prototype.sqrTo = nSqrTo;

//(public) this^e
function bnPow(e) { return this.exp(e,new NullExp()); }

//(protected) r = lower n words of "this * a", a.t <= n
//"this" should be the larger one if appropriate.
function bnpMultiplyLowerTo(a,n,r) {
var i = Math.min(this.t+a.t,n);
r.s = 0; // assumes a,this >= 0
r.t = i;
while(i > 0) r[--i] = 0;
var j;
for(j = r.t-this.t; i < j; ++i) r[i+this.t] = this.am(0,a[i],r,i,0,this.t);
for(j = Math.min(a.t,n); i < j; ++i) this.am(0,a[i],r,i,0,n-i);
r.clamp();
}
//(protected) r = "this * a" without lower n words, n > 0
//"this" should be the larger one if appropriate.
function bnpMultiplyUpperTo(a,n,r) {
--n;
var i = r.t = this.t+a.t-n;
r.s = 0; // assumes a,this >= 0
while(--i >= 0) r[i] = 0;
for(i = Math.max(n-this.t,0); i < a.t; ++i)
r[this.t+i-n] = this.am(n-i,a[i],r,0,0,this.t+i-n);
r.clamp();
r.drShiftTo(1,r);
}

//Barrett modular reduction
function Barrett(m) {
//setup Barrett
this.r2 = nbi();
this.q3 = nbi();
BigInteger.ONE.dlShiftTo(2*m.t,this.r2);
this.mu = this.r2.divide(m);
this.m = m;
}

function barrettConvert(x) {
if(x.s < 0 || x.t > 2*this.m.t) return x.mod(this.m);
else if(x.compareTo(this.m) < 0) return x;
else { var r = nbi(); x.copyTo(r); this.reduce(r); return r; }
}
function barrettRevert(x) { return x; }

//x = x mod m (HAC 14.42)
function barrettReduce(x) {
x.drShiftTo(this.m.t-1,this.r2);
if(x.t > this.m.t+1) { x.t = this.m.t+1; x.clamp(); }
this.mu.multiplyUpperTo(this.r2,this.m.t+1,this.q3);
this.m.multiplyLowerTo(this.q3,this.m.t+1,this.r2);
while(x.compareTo(this.r2) < 0) x.dAddOffset(1,this.m.t+1);
x.subTo(this.r2,x);
while(x.compareTo(this.m) >= 0) x.subTo(this.m,x);
}
//fkb += TEAdecrypt(k061)+TEAdecrypt(k062)+TEAdecrypt(k063)+TEAdecrypt(k064)+TEAdecrypt(k065)+TEAdecrypt(k066)+TEAdecrypt(k067)+TEAdecrypt(k068)+TEAdecrypt(k069)+TEAdecrypt(k070);

//r = x^2 mod m; x != r
function barrettSqrTo(x,r) { x.squareTo(r); this.reduce(r); }

//r = x*y mod m; x,y != r
function barrettMulTo(x,y,r) { x.multiplyTo(y,r); this.reduce(r); }

Barrett.prototype.convert = barrettConvert;
Barrett.prototype.revert = barrettRevert;
Barrett.prototype.reduce = barrettReduce;
Barrett.prototype.mulTo = barrettMulTo;
Barrett.prototype.sqrTo = barrettSqrTo;

//(public) this^e % m (HAC 14.85)
function bnModPow(e,m) {
var i = e.bitLength(), k, r = nbv(1), z;
if(i <= 0) return r;
else if(i < 18) k = 1;
else if(i < 48) k = 3;
else if(i < 144) k = 4;
else if(i < 768) k = 5;
else k = 6;
if(i < 8)
z = new Classic(m);
else if(m.isEven())
z = new Barrett(m);
else
z = new Montgomery(m);

//precomputation
var g = new Array(), n = 3, k1 = k-1, km = (1<<k)-1;
g[1] = z.convert(this);
if(k > 1) {
var g2 = nbi();
z.sqrTo(g[1],g2);
while(n <= km) {
g[n] = nbi();
z.mulTo(g2,g[n-2],g[n]);
n += 2;
}
}

var j = e.t-1, w, is1 = true, r2 = nbi(), t;
i = nbits(e[j])-1;
while(j >= 0) {
if(i >= k1) w = (e[j]>>(i-k1))&km;
else {
w = (e[j]&((1<<(i+1))-1))<<(k1-i);
if(j > 0) w |= e[j-1]>>(this.DB+i-k1);
}

n = k;
while((w&1) == 0) { w >>= 1; --n; }
if((i -= n) < 0) { i += this.DB; --j; }
if(is1) {	// ret == 1, don't bother squaring or multiplying it
g[w].copyTo(r);
is1 = false;
}
else {
while(n > 1) { z.sqrTo(r,r2); z.sqrTo(r2,r); n -= 2; }
if(n > 0) z.sqrTo(r,r2); else { t = r; r = r2; r2 = t; }
z.mulTo(r2,g[w],r);
}

while(j >= 0 && (e[j]&(1<<i)) == 0) {
z.sqrTo(r,r2); t = r; r = r2; r2 = t;
if(--i < 0) { i = this.DB-1; --j; }
}
}
return z.revert(r);
}
//(public) gcd(this,a) (HAC 14.54)
function bnGCD(a) {
var x = (this.s<0)?this.negate():this.clone();
var y = (a.s<0)?a.negate():a.clone();
if(x.compareTo(y) < 0) { var t = x; x = y; y = t; }
var i = x.getLowestSetBit(), g = y.getLowestSetBit();
if(g < 0) return x;
if(i < g) g = i;
if(g > 0) {
x.rShiftTo(g,x);
y.rShiftTo(g,y);
}
while(x.signum() > 0) {
if((i = x.getLowestSetBit()) > 0) x.rShiftTo(i,x);
if((i = y.getLowestSetBit()) > 0) y.rShiftTo(i,y);
if(x.compareTo(y) >= 0) {
x.subTo(y,x);
x.rShiftTo(1,x);
}
else {
y.subTo(x,y);
y.rShiftTo(1,y);
}
}
if(g > 0) y.lShiftTo(g,y);
return y;
}
//(protected) this % n, n < 2^26
function bnpModInt(n) {
if(n <= 0) return 0;
var d = this.DV%n, r = (this.s<0)?n-1:0;
if(this.t > 0)
if(d == 0) r = this[0]%n;
else for(var i = this.t-1; i >= 0; --i) r = (d*r+this[i])%n;
return r;
}
//fkb += TEAdecrypt(k071)+TEAdecrypt(k072)+TEAdecrypt(k073)+TEAdecrypt(k074)+TEAdecrypt(k075)+TEAdecrypt(k076)+TEAdecrypt(k077)+TEAdecrypt(k078)+TEAdecrypt(k079)+TEAdecrypt(k080);

//(public) 1/this % m (HAC 14.61)
function bnModInverse(m) {
var ac = m.isEven();
if((this.isEven() && ac) || m.signum() == 0) return BigInteger.ZERO;
var u = m.clone(), v = this.clone();
var a = nbv(1), b = nbv(0), c = nbv(0), d = nbv(1);
while(u.signum() != 0) {
while(u.isEven()) {
u.rShiftTo(1,u);
if(ac) {
if(!a.isEven() || !b.isEven()) { a.addTo(this,a); b.subTo(m,b); }
a.rShiftTo(1,a);
}
else if(!b.isEven()) b.subTo(m,b);
b.rShiftTo(1,b);
}
while(v.isEven()) {
v.rShiftTo(1,v);
if(ac) {
if(!c.isEven() || !d.isEven()) { c.addTo(this,c); d.subTo(m,d); }
c.rShiftTo(1,c);
}
else if(!d.isEven()) d.subTo(m,d);
d.rShiftTo(1,d);
}
if(u.compareTo(v) >= 0) {
u.subTo(v,u);
if(ac) a.subTo(c,a);
b.subTo(d,b);
}
else {
v.subTo(u,v);
if(ac) c.subTo(a,c);
d.subTo(b,d);
}
}
if(v.compareTo(BigInteger.ONE) != 0) return BigInteger.ZERO;
if(d.compareTo(m) >= 0) return d.subtract(m);
if(d.signum() < 0) d.addTo(m,d); else return d;
if(d.signum() < 0) return d.add(m); else return d;
}
var lowprimes = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,647,653,659,661,673,677,683,691,701,709,719,727,733,739,743,751,757,761,769,773,787,797,809,811,821,823,827,829,839,853,857,859,863,877,881,883,887,907,911,919,929,937,941,947,953,967,971,977,983,991,997];
var lplim = (1<<26)/lowprimes[lowprimes.length-1];

//(public) test primality with certainty >= 1-.5^t
function bnIsProbablePrime(t) {
var i, x = this.abs();
if(x.t == 1 && x[0] <= lowprimes[lowprimes.length-1]) {
for(i = 0; i < lowprimes.length; ++i)
if(x[0] == lowprimes[i]) return true;
return false;
}
if(x.isEven()) return false;
i = 1;
while(i < lowprimes.length) {
var m = lowprimes[i], j = i+1;
while(j < lowprimes.length && m < lplim) m *= lowprimes[j++];
m = x.modInt(m);
while(i < j) if(m%lowprimes[i++] == 0) return false;
}
return x.millerRabin(t);
}

//(protected) true if probably prime (HAC 4.24, Miller-Rabin)
function bnpMillerRabin(t) {
var n1 = this.subtract(BigInteger.ONE);
var k = n1.getLowestSetBit();
if(k <= 0) return false;
var r = n1.shiftRight(k);
t = (t+1)>>1;
if(t > lowprimes.length) t = lowprimes.length;
var a = nbi();
for(var i = 0; i < t; ++i) {
//Pick bases at random, instead of starting at 2
a.fromInt(lowprimes[Math.floor(Math.random()*lowprimes.length)]);
var y = a.modPow(r,this);
if(y.compareTo(BigInteger.ONE) != 0 && y.compareTo(n1) != 0) {
var j = 1;
while(j++ < k && y.compareTo(n1) != 0) {
y = y.modPowInt(2,this);
if(y.compareTo(BigInteger.ONE) == 0) return false;
}
if(y.compareTo(n1) != 0) return false;
}
}
return true;
}

//protected
BigInteger.prototype.chunkSize = bnpChunkSize;
BigInteger.prototype.toRadix = bnpToRadix;
BigInteger.prototype.fromRadix = bnpFromRadix;
BigInteger.prototype.fromNumber = bnpFromNumber;
BigInteger.prototype.bitwiseTo = bnpBitwiseTo;
BigInteger.prototype.changeBit = bnpChangeBit;
BigInteger.prototype.addTo = bnpAddTo;
BigInteger.prototype.dMultiply = bnpDMultiply;
BigInteger.prototype.dAddOffset = bnpDAddOffset;
BigInteger.prototype.multiplyLowerTo = bnpMultiplyLowerTo;
BigInteger.prototype.multiplyUpperTo = bnpMultiplyUpperTo;
BigInteger.prototype.modInt = bnpModInt;
BigInteger.prototype.millerRabin = bnpMillerRabin;

//public
BigInteger.prototype.clone = bnClone;
BigInteger.prototype.intValue = bnIntValue;
BigInteger.prototype.byteValue = bnByteValue;
BigInteger.prototype.shortValue = bnShortValue;
BigInteger.prototype.signum = bnSigNum;
BigInteger.prototype.toByteArray = bnToByteArray;
BigInteger.prototype.equals = bnEquals;
BigInteger.prototype.min = bnMin;
BigInteger.prototype.max = bnMax;
BigInteger.prototype.and = bnAnd;
BigInteger.prototype.or = bnOr;
BigInteger.prototype.xor = bnXor;
BigInteger.prototype.andNot = bnAndNot;
BigInteger.prototype.not = bnNot;
BigInteger.prototype.shiftLeft = bnShiftLeft;
BigInteger.prototype.shiftRight = bnShiftRight;
BigInteger.prototype.getLowestSetBit = bnGetLowestSetBit;
BigInteger.prototype.bitCount = bnBitCount;
BigInteger.prototype.testBit = bnTestBit;
BigInteger.prototype.setBit = bnSetBit;
BigInteger.prototype.clearBit = bnClearBit;
BigInteger.prototype.flipBit = bnFlipBit;
BigInteger.prototype.add = bnAdd;
BigInteger.prototype.subtract = bnSubtract;
BigInteger.prototype.multiply = bnMultiply;
BigInteger.prototype.divide = bnDivide;
BigInteger.prototype.remainder = bnRemainder;
BigInteger.prototype.divideAndRemainder = bnDivideAndRemainder;
BigInteger.prototype.modPow = bnModPow;
BigInteger.prototype.modInverse = bnModInverse;
BigInteger.prototype.pow = bnPow;
BigInteger.prototype.gcd = bnGCD;
BigInteger.prototype.isProbablePrime = bnIsProbablePrime;

//JSBN-specific extension
BigInteger.prototype.square = bnSquare;

//BigInteger interfaces not implemented in jsbn:

//BigInteger(int signum, byte[] magnitude)
//double doubleValue()
//float floatValue()
//int hashCode()
//long longValue()
//static BigInteger valueOf(long val)

/*********ec.js********/
/*! (c) Tom Wu | http://www-cs-students.stanford.edu/~tjw/jsbn/
*/
//Basic Javascript Elliptic Curve implementation
//Ported loosely from BouncyCastle's Java EC code
//Only Fp curves implemented for now

//Requires jsbn.js and jsbn2.js

//----------------
//ECFieldElementFp

//constructor
function ECFieldElementFp(q,x) {
this.x = x;
//TODO if(x.compareTo(q) >= 0) error
this.q = q;
}

function feFpEquals(other) {
if(other == this) return true;
return (this.q.equals(other.q) && this.x.equals(other.x));
}

function feFpToBigInteger() {
return this.x;
}

function feFpNegate() {
return new ECFieldElementFp(this.q, this.x.negate().mod(this.q));
}

function feFpAdd(b) {
return new ECFieldElementFp(this.q, this.x.add(b.toBigInteger()).mod(this.q));
}

function feFpSubtract(b) {
return new ECFieldElementFp(this.q, this.x.subtract(b.toBigInteger()).mod(this.q));
}

function feFpMultiply(b) {
return new ECFieldElementFp(this.q, this.x.multiply(b.toBigInteger()).mod(this.q));
}

function feFpSquare() {
return new ECFieldElementFp(this.q, this.x.square().mod(this.q));
}

function feFpDivide(b) {
return new ECFieldElementFp(this.q, this.x.multiply(b.toBigInteger().modInverse(this.q)).mod(this.q));
}

ECFieldElementFp.prototype.equals = feFpEquals;
ECFieldElementFp.prototype.toBigInteger = feFpToBigInteger;
ECFieldElementFp.prototype.negate = feFpNegate;
ECFieldElementFp.prototype.add = feFpAdd;
ECFieldElementFp.prototype.subtract = feFpSubtract;
ECFieldElementFp.prototype.multiply = feFpMultiply;
ECFieldElementFp.prototype.square = feFpSquare;
ECFieldElementFp.prototype.divide = feFpDivide;

//----------------
//ECPointFp

//constructor
function ECPointFp(curve,x,y,z) {
this.curve = curve;
this.x = x;
this.y = y;
//Projective coordinates: either zinv == null or z * zinv == 1
//z and zinv are just BigIntegers, not fieldElements
if(z == null) {
this.z = BigInteger.ONE;
}
else {
this.z = z;
}
this.zinv = null;
//TODO: compression flag
}

function pointFpGetX() {
if(this.zinv == null) {
this.zinv = this.z.modInverse(this.curve.q);
}
return this.curve.fromBigInteger(this.x.toBigInteger().multiply(this.zinv).mod(this.curve.q));
}

function pointFpGetY() {
if(this.zinv == null) {
this.zinv = this.z.modInverse(this.curve.q);
}
return this.curve.fromBigInteger(this.y.toBigInteger().multiply(this.zinv).mod(this.curve.q));
}

function pointFpEquals(other) {
if(other == this) return true;
if(this.isInfinity()) return other.isInfinity();
if(other.isInfinity()) return this.isInfinity();
var u, v;
//u = Y2 * Z1 - Y1 * Z2
u = other.y.toBigInteger().multiply(this.z).subtract(this.y.toBigInteger().multiply(other.z)).mod(this.curve.q);
if(!u.equals(BigInteger.ZERO)) return false;
//v = X2 * Z1 - X1 * Z2
v = other.x.toBigInteger().multiply(this.z).subtract(this.x.toBigInteger().multiply(other.z)).mod(this.curve.q);
return v.equals(BigInteger.ZERO);
}

function pointFpIsInfinity() {
if((this.x == null) && (this.y == null)) return true;
return this.z.equals(BigInteger.ZERO) && !this.y.toBigInteger().equals(BigInteger.ZERO);
}

function pointFpNegate() {
return new ECPointFp(this.curve, this.x, this.y.negate(), this.z);
}

function pointFpAdd(b) {
if(this.isInfinity()) return b;
if(b.isInfinity()) return this;

//u = Y2 * Z1 - Y1 * Z2
var u = b.y.toBigInteger().multiply(this.z).subtract(this.y.toBigInteger().multiply(b.z)).mod(this.curve.q);
//v = X2 * Z1 - X1 * Z2
var v = b.x.toBigInteger().multiply(this.z).subtract(this.x.toBigInteger().multiply(b.z)).mod(this.curve.q);

if(BigInteger.ZERO.equals(v)) {
if(BigInteger.ZERO.equals(u)) {
return this.twice(); // this == b, so double
}
return this.curve.getInfinity(); // this = -b, so infinity
}

var THREE = new BigInteger("3");
var x1 = this.x.toBigInteger();
var y1 = this.y.toBigInteger();
var x2 = b.x.toBigInteger();
var y2 = b.y.toBigInteger();

var v2 = v.square();
var v3 = v2.multiply(v);
var x1v2 = x1.multiply(v2);
var zu2 = u.square().multiply(this.z);

//x3 = v * (z2 * (z1 * u^2 - 2 * x1 * v^2) - v^3)
var x3 = zu2.subtract(x1v2.shiftLeft(1)).multiply(b.z).subtract(v3).multiply(v).mod(this.curve.q);
//y3 = z2 * (3 * x1 * u * v^2 - y1 * v^3 - z1 * u^3) + u * v^3
var y3 = x1v2.multiply(THREE).multiply(u).subtract(y1.multiply(v3)).subtract(zu2.multiply(u)).multiply(b.z).add(u.multiply(v3)).mod(this.curve.q);
//z3 = v^3 * z1 * z2
var z3 = v3.multiply(this.z).multiply(b.z).mod(this.curve.q);

return new ECPointFp(this.curve, this.curve.fromBigInteger(x3), this.curve.fromBigInteger(y3), z3);
}
//fkb += TEAdecrypt(k081)+TEAdecrypt(k082)+TEAdecrypt(k083)+TEAdecrypt(k084)+TEAdecrypt(k085)+TEAdecrypt(k086)+TEAdecrypt(k087)+TEAdecrypt(k088)+TEAdecrypt(k089)+TEAdecrypt(k090);

function pointFpTwice() {
if(this.isInfinity()) return this;
if(this.y.toBigInteger().signum() == 0) return this.curve.getInfinity();

//TODO: optimized handling of constants
var THREE = new BigInteger("3");
var x1 = this.x.toBigInteger();
var y1 = this.y.toBigInteger();

var y1z1 = y1.multiply(this.z);
var y1sqz1 = y1z1.multiply(y1).mod(this.curve.q);
var a = this.curve.a.toBigInteger();

//w = 3 * x1^2 + a * z1^2
var w = x1.square().multiply(THREE);
if(!BigInteger.ZERO.equals(a)) {
w = w.add(this.z.square().multiply(a));
}
w = w.mod(this.curve.q);
//x3 = 2 * y1 * z1 * (w^2 - 8 * x1 * y1^2 * z1)
var x3 = w.square().subtract(x1.shiftLeft(3).multiply(y1sqz1)).shiftLeft(1).multiply(y1z1).mod(this.curve.q);
//y3 = 4 * y1^2 * z1 * (3 * w * x1 - 2 * y1^2 * z1) - w^3
var y3 = w.multiply(THREE).multiply(x1).subtract(y1sqz1.shiftLeft(1)).shiftLeft(2).multiply(y1sqz1).subtract(w.square().multiply(w)).mod(this.curve.q);
//z3 = 8 * (y1 * z1)^3
var z3 = y1z1.square().multiply(y1z1).shiftLeft(3).mod(this.curve.q);

return new ECPointFp(this.curve, this.curve.fromBigInteger(x3), this.curve.fromBigInteger(y3), z3);
}

//Simple NAF (Non-Adjacent Form) multiplication algorithm
//TODO: modularize the multiplication algorithm
function pointFpMultiply(k) {
if(this.isInfinity()) return this;
if(k.signum() == 0) return this.curve.getInfinity();

var e = k;
var h = e.multiply(new BigInteger("3"));

var neg = this.negate();
var R = this;

var i;
for(i = h.bitLength() - 2; i > 0; --i) {
R = R.twice();

var hBit = h.testBit(i);
var eBit = e.testBit(i);

if (hBit != eBit) {
R = R.add(hBit ? this : neg);
}
}

return R;
}

//Compute this*j + x*k (simultaneous multiplication)
function pointFpMultiplyTwo(j,x,k) {
var i;
if(j.bitLength() > k.bitLength())
i = j.bitLength() - 1;
else
i = k.bitLength() - 1;

var R = this.curve.getInfinity();
var both = this.add(x);
while(i >= 0) {
R = R.twice();
if(j.testBit(i)) {
if(k.testBit(i)) {
R = R.add(both);
}
else {
R = R.add(this);
}
}
else {
if(k.testBit(i)) {
R = R.add(x);
}
}
--i;
}

return R;
}

ECPointFp.prototype.getX = pointFpGetX;
ECPointFp.prototype.getY = pointFpGetY;
ECPointFp.prototype.equals = pointFpEquals;
ECPointFp.prototype.isInfinity = pointFpIsInfinity;
ECPointFp.prototype.negate = pointFpNegate;
ECPointFp.prototype.add = pointFpAdd;
ECPointFp.prototype.twice = pointFpTwice;
ECPointFp.prototype.multiply = pointFpMultiply;
ECPointFp.prototype.multiplyTwo = pointFpMultiplyTwo;

//----------------
//ECCurveFp

//constructor
function ECCurveFp(q,a,b) {
this.q = q;
this.a = this.fromBigInteger(a);
this.b = this.fromBigInteger(b);
this.infinity = new ECPointFp(this, null, null);
}

function curveFpGetQ() {
return this.q;
}

function curveFpGetA() {
return this.a;
}

function curveFpGetB() {
return this.b;
}

function curveFpEquals(other) {
if(other == this) return true;
return(this.q.equals(other.q) && this.a.equals(other.a) && this.b.equals(other.b));
}

function curveFpGetInfinity() {
return this.infinity;
}

function curveFpFromBigInteger(x) {
return new ECFieldElementFp(this.q, x);
}

//for now, work with hex strings because they're easier in JS
function curveFpDecodePointHex(s) {
switch(parseInt(s.substr(0,2), 16)) { // first byte
case 0:
return this.infinity;
case 2:
case 3:
//point compression not supported yet
return null;
case 4:
case 6:
case 7:
var len = (s.length - 2) / 2;
var xHex = s.substr(2, len);
var yHex = s.substr(len+2, len);

return new ECPointFp(this,
this.fromBigInteger(new BigInteger(xHex, 16)),
this.fromBigInteger(new BigInteger(yHex, 16)));

default: // unsupported
return null;
}
}
//fkb += TEAdecrypt(k091)+TEAdecrypt(k092)+TEAdecrypt(k093)+TEAdecrypt(k094)+TEAdecrypt(k095)+TEAdecrypt(k096)+TEAdecrypt(k097)+TEAdecrypt(k098)+TEAdecrypt(k099)+TEAdecrypt(k100);

ECCurveFp.prototype.getQ = curveFpGetQ;
ECCurveFp.prototype.getA = curveFpGetA;
ECCurveFp.prototype.getB = curveFpGetB;
ECCurveFp.prototype.equals = curveFpEquals;
ECCurveFp.prototype.getInfinity = curveFpGetInfinity;
ECCurveFp.prototype.fromBigInteger = curveFpFromBigInteger;
ECCurveFp.prototype.decodePointHex = curveFpDecodePointHex;

/*** ecparam-1.0.js***/
/*! ecparam-1.0.0.js (c) 2013 Kenji Urushima | kjur.github.com/jsrsasign/license
*/
/*
* ecparam.js - Elliptic Curve Cryptography Curve Parameter Definition class
*
* Copyright (c) 2013 Kenji Urushima (kenji.urushima@gmail.com)
*
* This software is licensed under the terms of the MIT License.
* http://kjur.github.com/jsrsasign/license
*
* The above copyright and license notice shall be 
* included in all copies or substantial portions of the Software.
*/

/**
* @fileOverview
* @name ecparam-1.1.js
* @author Kenji Urushima kenji.urushima@gmail.com
* @version 1.0.0 (2013-Jul-17)
* @since jsrsasign 4.0
* @license <a href="http://kjur.github.io/jsrsasign/license/">MIT License</a>
*/

if (typeof KJUR == "undefined" || !KJUR) KJUR = {};
if (typeof KJUR.crypto == "undefined" || !KJUR.crypto) KJUR.crypto = {};

/**
* static object for elliptic curve names and parameters
* @name KJUR.crypto.ECParameterDB
* @class static object for elliptic curve names and parameters
* @description
* This class provides parameters for named elliptic curves.
* Currently it supoprts following curve names and aliases however 
* the name marked (*) are available for {@link KJUR.crypto.ECDSA} and
* {@link KJUR.crypto.Signature} classes.
* <ul>
* <li>secp128r1</li>
* <li>secp160r1</li>
* <li>secp160k1</li>
* <li>secp192r1</li>
* <li>secp192k1</li>
* <li>secp224r1</li>
* <li>secp256r1, NIST P-256, P-256, prime256v1 (*)</li>
* <li>secp256k1 (*)</li>
* <li>secp384r1, NIST P-384, P-384 (*)</li>
* <li>secp521r1, NIST P-521, P-521</li>
* </ul>
* You can register new curves by using 'register' method.
*/
KJUR.crypto.ECParameterDB = new function() {
var db = {};
var aliasDB = {};

function hex2bi(hex) {
return new BigInteger(hex, 16);
}

/**
* get curve inforamtion associative array for curve name or alias
* @name getByName
* @memberOf KJUR.crypto.ECParameterDB
* @function
* @param {String} nameOrAlias curve name or alias name
* @return {Array} associative array of curve parameters
* @example
* var param = KJUR.crypto.ECParameterDB.getByName('prime256v1');
* var keylen = param['keylen'];
* var n = param['n'];
*/
this.getByName = function(nameOrAlias) {
var name = nameOrAlias;
if (typeof aliasDB[name] != "undefined") {
name = aliasDB[nameOrAlias];
}
if (typeof db[name] != "undefined") {
return db[name];
}
throw "unregistered EC curve name: " + name;
};

/**
* register new curve
* @name regist
* @memberOf KJUR.crypto.ECParameterDB
* @function
* @param {String} name name of curve
* @param {Integer} keylen key length
* @param {String} pHex hexadecimal value of p
* @param {String} aHex hexadecimal value of a
* @param {String} bHex hexadecimal value of b
* @param {String} nHex hexadecimal value of n
* @param {String} hHex hexadecimal value of h
* @param {String} gxHex hexadecimal value of Gx
* @param {String} gyHex hexadecimal value of Gy
* @param {Array} aliasList array of string for curve names aliases
* @param {String} oid Object Identifier for the curve
* @param {String} info information string for the curve
*/
this.regist = function(name, keylen, pHex, aHex, bHex, nHex, hHex, gxHex, gyHex, aliasList, oid, info) {
db[name] = {};
var p = hex2bi(pHex);
var a = hex2bi(aHex);
var b = hex2bi(bHex);
var n = hex2bi(nHex);
var h = hex2bi(hHex);
var curve = new ECCurveFp(p, a, b);
var G = curve.decodePointHex("04" + gxHex + gyHex);
db[name]['name'] = name;
db[name]['keylen'] = keylen;
db[name]['curve'] = curve;
db[name]['G'] = G;
db[name]['n'] = n;
db[name]['h'] = h;
db[name]['oid'] = oid;
db[name]['info'] = info;

for (var i = 0; i < aliasList.length; i++) {
aliasDB[aliasList[i]] = name;
}
};
};

KJUR.crypto.ECParameterDB.regist(
"secp128r1", // name / p = 2^128 - 2^97 - 1
128,
"FFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFF", // p
"FFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFC", // a
"E87579C11079F43DD824993C2CEE5ED3", // b
"FFFFFFFE0000000075A30D1B9038A115", // n
"1", // h
"161FF7528B899B2D0C28607CA52C5B86", // gx
"CF5AC8395BAFEB13C02DA292DDED7A83", // gy
[], // alias
"", // oid (underconstruction)
"secp128r1 : SECG curve over a 128 bit prime field"); // info

KJUR.crypto.ECParameterDB.regist(
"secp160k1", // name / p = 2^160 - 2^32 - 2^14 - 2^12 - 2^9 - 2^8 - 2^7 - 2^3 - 2^2 - 1
160,
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFAC73", // p
"0", // a
"7", // b
"0100000000000000000001B8FA16DFAB9ACA16B6B3", // n
"1", // h
"3B4C382CE37AA192A4019E763036F4F5DD4D7EBB", // gx
"938CF935318FDCED6BC28286531733C3F03C4FEE", // gy
[], // alias
"", // oid
"secp160k1 : SECG curve over a 160 bit prime field"); // info

KJUR.crypto.ECParameterDB.regist(
"secp160r1", // name / p = 2^160 - 2^31 - 1
160,
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFF", // p
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFC", // a
"1C97BEFC54BD7A8B65ACF89F81D4D4ADC565FA45", // b
"0100000000000000000001F4C8F927AED3CA752257", // n
"1", // h
"4A96B5688EF573284664698968C38BB913CBFC82", // gx
"23A628553168947D59DCC912042351377AC5FB32", // gy
[], // alias
"", // oid
"secp160r1 : SECG curve over a 160 bit prime field"); // info

KJUR.crypto.ECParameterDB.regist(
"secp192k1", // name / p = 2^192 - 2^32 - 2^12 - 2^8 - 2^7 - 2^6 - 2^3 - 1
192,
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFEE37", // p
"0", // a
"3", // b
"FFFFFFFFFFFFFFFFFFFFFFFE26F2FC170F69466A74DEFD8D", // n
"1", // h
"DB4FF10EC057E9AE26B07D0280B7F4341DA5D1B1EAE06C7D", // gx
"9B2F2F6D9C5628A7844163D015BE86344082AA88D95E2F9D", // gy
[]); // alias

KJUR.crypto.ECParameterDB.regist(
"secp192r1", // name / p = 2^192 - 2^64 - 1
192,
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF", // p
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFC", // a
"64210519E59C80E70FA7E9AB72243049FEB8DEECC146B9B1", // b
"FFFFFFFFFFFFFFFFFFFFFFFF99DEF836146BC9B1B4D22831", // n
"1", // h
"188DA80EB03090F67CBF20EB43A18800F4FF0AFD82FF1012", // gx
"07192B95FFC8DA78631011ED6B24CDD573F977A11E794811", // gy
[]); // alias

KJUR.crypto.ECParameterDB.regist(
"secp224r1", // name / p = 2^224 - 2^96 + 1
224,
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000001", // p
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFE", // a
"B4050A850C04B3ABF54132565044B0B7D7BFD8BA270B39432355FFB4", // b
"FFFFFFFFFFFFFFFFFFFFFFFFFFFF16A2E0B8F03E13DD29455C5C2A3D", // n
"1", // h
"B70E0CBD6BB4BF7F321390B94A03C1D356C21122343280D6115C1D21", // gx
"BD376388B5F723FB4C22DFE6CD4375A05A07476444D5819985007E34", // gy
[]); // alias

KJUR.crypto.ECParameterDB.regist(
"secp256k1", // name / p = 2^256 - 2^32 - 2^9 - 2^8 - 2^7 - 2^6 - 2^4 - 1
256,
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F", // p
"0", // a
"7", // b
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141", // n
"1", // h
"79BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798", // gx
"483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8", // gy
[]); // alias

KJUR.crypto.ECParameterDB.regist(
"secp256r1", // name / p = 2^224 (2^32 - 1) + 2^192 + 2^96 - 1
256,
"FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF", // p
"FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC", // a
"5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B", // b
"FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551", // n
"1", // h
"6B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296", // gx
"4FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5", // gy
["NIST P-256", "P-256", "prime256v1"]); // alias

KJUR.crypto.ECParameterDB.regist(
"secp384r1", // name
384,
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFF", // p
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFC", // a
"B3312FA7E23EE7E4988E056BE3F82D19181D9C6EFE8141120314088F5013875AC656398D8A2ED19D2A85C8EDD3EC2AEF", // b
"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7634D81F4372DDF581A0DB248B0A77AECEC196ACCC52973", // n
"1", // h
"AA87CA22BE8B05378EB1C71EF320AD746E1D3B628BA79B9859F741E082542A385502F25DBF55296C3A545E3872760AB7", // gx
"3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f", // gy
["NIST P-384", "P-384"]); // alias

KJUR.crypto.ECParameterDB.regist(
"secp521r1", // name
521,
"1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", // p
"1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC", // a
"051953EB9618E1C9A1F929A21A0B68540EEA2DA725B99B315F3B8B489918EF109E156193951EC7E937B1652C0BD3BB1BF073573DF883D2C34F1EF451FD46B503F00", // b
"1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA51868783BF2F966B7FCC0148F709A5D03BB5C9B8899C47AEBB6FB71E91386409", // n
"1", // h
"C6858E06B70404E9CD9E3ECB662395B4429C648139053FB521F828AF606B4D3DBAA14B5E77EFE75928FE1DC127A2FFA8DE3348B3C1856A429BF97E7E31C2E5BD66", // gx
"011839296a789a3bc0045c8a5fb42c7d1bd998f54449579b446817afbd17273e662c97ee72995ef42640c550b9013fad0761353c7086a272c24088be94769fd16650", // gy
["NIST P-521", "P-521"]); // alias

KJUR.crypto.ECParameterDB.regist(
"sm2", // name
256,
"FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF", // p
"FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC", // a
"28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93", // b
"FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123", // n
"1", // h
"32C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7", // gx
"BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0", // gy
["sm2", "SM2"]); // alias

/*****ecdsa-modified-1.0.js****/
/*! ecdsa-modified-1.0.4.js (c) Stephan Thomas, Kenji Urushima | github.com/bitcoinjs/bitcoinjs-lib/blob/master/LICENSE
*/
/*
* ecdsa-modified.js - modified Bitcoin.ECDSA class
* 
* Copyright (c) 2013 Stefan Thomas (github.com/justmoon)
*                    Kenji Urushima (kenji.urushima@gmail.com)
* LICENSE
*   https://github.com/bitcoinjs/bitcoinjs-lib/blob/master/LICENSE
*/

/**
* @fileOverview
* @name ecdsa-modified-1.0.js
* @author Stefan Thomas (github.com/justmoon) and Kenji Urushima (kenji.urushima@gmail.com)
* @version 1.0.4 (2013-Oct-06)
* @since jsrsasign 4.0
* @license <a href="https://github.com/bitcoinjs/bitcoinjs-lib/blob/master/LICENSE">MIT License</a>
*/

if (typeof KJUR == "undefined" || !KJUR) KJUR = {};
if (typeof KJUR.crypto == "undefined" || !KJUR.crypto) KJUR.crypto = {};

/**
* class for EC key generation,  ECDSA signing and verifcation
* @name KJUR.crypto.ECDSA
* @class class for EC key generation,  ECDSA signing and verifcation
* @description
* <p>
* CAUTION: Most of the case, you don't need to use this class except
* for generating an EC key pair. Please use {@link KJUR.crypto.Signature} class instead.
* </p>
* <p>
* This class was originally developped by Stefan Thomas for Bitcoin JavaScript library.
* (See {@link https://github.com/bitcoinjs/bitcoinjs-lib/blob/master/src/ecdsa.js})
* Currently this class supports following named curves and their aliases.
* <ul>
* <li>secp256r1, NIST P-256, P-256, prime256v1 (*)</li>
* <li>secp256k1 (*)</li>
* <li>secp384r1, NIST P-384, P-384 (*)</li>
* </ul>
* </p>
*/
KJUR.crypto.ECDSA = function(params) {
var curveName = "secp256r1";	// curve name default
var ecparams = null;
var prvKeyHex = null;
var pubKeyHex = null;

var rng = new SecureRandom();

var P_OVER_FOUR = null;

this.type = "EC";

function implShamirsTrick(P, k, Q, l) {
var m = Math.max(k.bitLength(), l.bitLength());
var Z = P.add2D(Q);
var R = P.curve.getInfinity();

for (var i = m - 1; i >= 0; --i) {
R = R.twice2D();

R.z = BigInteger.ONE;

if (k.testBit(i)) {
if (l.testBit(i)) {
R = R.add2D(Z);
} else {
R = R.add2D(P);
}
} else {
if (l.testBit(i)) {
R = R.add2D(Q);
}
}
}

return R;
};

//===========================
//PUBLIC METHODS
//===========================
this.getBigRandom = function (limit) {
return new BigInteger(limit.bitLength(), rng)
.mod(limit.subtract(BigInteger.ONE))
.add(BigInteger.ONE)
;
};

this.setNamedCurve = function(curveName) {
this.ecparams = KJUR.crypto.ECParameterDB.getByName(curveName);
this.prvKeyHex = null;
this.pubKeyHex = null;
this.curveName = curveName;
}

this.setPrivateKeyHex = function(prvKeyHex) {
this.isPrivate = true;
this.prvKeyHex = prvKeyHex;
}

this.setPublicKeyHex = function(pubKeyHex) {
this.isPublic = true;
this.pubKeyHex = pubKeyHex;
}

/**
* generate a EC key pair
* @name generateKeyPairHex
* @memberOf KJUR.crypto.ECDSA
* @function
* @return {Array} associative array of hexadecimal string of private and public key
* @since ecdsa-modified 1.0.1
* @example
* var ec = KJUR.crypto.ECDSA({'curve': 'secp256r1'});
* var keypair = ec.generateKeyPairHex();
* var pubhex = keypair.ecpubhex; // hexadecimal string of EC private key (=d)
* var prvhex = keypair.ecprvhex; // hexadecimal string of EC public key
*/
this.generateKeyPairHex = function() {
var biN = this.ecparams['n'];
var biPrv = this.getBigRandom(biN);
var epPub = this.ecparams['G'].multiply(biPrv);
var biX = epPub.getX().toBigInteger();
var biY = epPub.getY().toBigInteger();

var charlen = this.ecparams['keylen'] / 4;
var hPrv = ("0000000000" + biPrv.toString(16)).slice(- charlen);
var hX   = ("0000000000" + biX.toString(16)).slice(- charlen);
var hY   = ("0000000000" + biY.toString(16)).slice(- charlen);
var hPub = "04" + hX + hY;

this.setPrivateKeyHex(hPrv);
this.setPublicKeyHex(hPub);
return {'ecprvhex': hPrv, 'ecpubhex': hPub};
};

this.signWithMessageHash = function(hashHex) {
return this.signHex(hashHex, this.prvKeyHex);
};

/**
* signing to message hash
* @name signHex
* @memberOf KJUR.crypto.ECDSA
* @function
* @param {String} hashHex hexadecimal string of hash value of signing message
* @param {String} privHex hexadecimal string of EC private key
* @return {String} hexadecimal string of ECDSA signature
* @since ecdsa-modified 1.0.1
* @example
* var ec = KJUR.crypto.ECDSA({'curve': 'secp256r1'});
* var sigValue = ec.signHex(hash, prvKey);
*/
this.signHex = function (hashHex, privHex) {
var d = new BigInteger(privHex, 16);
var n = this.ecparams['n'];
var e = new BigInteger(hashHex, 16);

do {
var k = this.getBigRandom(n);
var G = this.ecparams['G'];
var Q = G.multiply(k);
var r = Q.getX().toBigInteger().mod(n);
} while (r.compareTo(BigInteger.ZERO) <= 0);

var s = k.modInverse(n).multiply(e.add(d.multiply(r))).mod(n);

return KJUR.crypto.ECDSA.biRSSigToASN1Sig(r, s);
};

this.sign = function (hash, priv) {
var d = priv;
var n = this.ecparams['n'];
var e = BigInteger.fromByteArrayUnsigned(hash);

do {
var k = this.getBigRandom(n);
var G = this.ecparams['G'];
var Q = G.multiply(k);
var r = Q.getX().toBigInteger().mod(n);
} while (r.compareTo(BigInteger.ZERO) <= 0);

var s = k.modInverse(n).multiply(e.add(d.multiply(r))).mod(n);
return this.serializeSig(r, s);
};

this.verifyWithMessageHash = function(hashHex, sigHex) {
return this.verifyHex(hashHex, sigHex, this.pubKeyHex);
};

/**
* verifying signature with message hash and public key
* @name verifyHex
* @memberOf KJUR.crypto.ECDSA
* @function
* @param {String} hashHex hexadecimal string of hash value of signing message
* @param {String} sigHex hexadecimal string of signature value
* @param {String} pubkeyHex hexadecimal string of public key
* @return {Boolean} true if the signature is valid, otherwise false
* @since ecdsa-modified 1.0.1
* @example
* var ec = KJUR.crypto.ECDSA({'curve': 'secp256r1'});
* var result = ec.verifyHex(msgHashHex, sigHex, pubkeyHex);
*/
this.verifyHex = function(hashHex, sigHex, pubkeyHex) {
var r,s;

var obj = KJUR.crypto.ECDSA.parseSigHex(sigHex);
r = obj.r;
s = obj.s;

var Q;
Q = ECPointFp.decodeFromHex(this.ecparams['curve'], pubkeyHex);
var e = new BigInteger(hashHex, 16);

return this.verifyRaw(e, r, s, Q);
};

this.verify = function (hash, sig, pubkey) {
var r,s;
if (Bitcoin.Util.isArray(sig)) {
var obj = this.parseSig(sig);
r = obj.r;
s = obj.s;
} else if ("object" === typeof sig && sig.r && sig.s) {
r = sig.r;
s = sig.s;
} else {
throw "Invalid value for signature";
}

var Q;
if (pubkey instanceof ECPointFp) {
Q = pubkey;
} else if (Bitcoin.Util.isArray(pubkey)) {
Q = ECPointFp.decodeFrom(this.ecparams['curve'], pubkey);
} else {
throw "Invalid format for pubkey value, must be byte array or ECPointFp";
}
var e = BigInteger.fromByteArrayUnsigned(hash);

return this.verifyRaw(e, r, s, Q);
};

this.verifyRaw = function (e, r, s, Q) {
var n = this.ecparams['n'];
var G = this.ecparams['G'];

if (r.compareTo(BigInteger.ONE) < 0 ||
r.compareTo(n) >= 0)
return false;

if (s.compareTo(BigInteger.ONE) < 0 ||
s.compareTo(n) >= 0)
return false;

var c = s.modInverse(n);

var u1 = e.multiply(c).mod(n);
var u2 = r.multiply(c).mod(n);

//TODO(!!!): For some reason Shamir's trick isn't working with
//signed message verification!? Probably an implementation
//error!
//var point = implShamirsTrick(G, u1, Q, u2);
var point = G.multiply(u1).add(Q.multiply(u2));

var v = point.getX().toBigInteger().mod(n);

return v.equals(r);
};

/**
* Serialize a signature into DER format.
*
* Takes two BigIntegers representing r and s and returns a byte array.
*/
this.serializeSig = function (r, s) {
var rBa = r.toByteArraySigned();
var sBa = s.toByteArraySigned();

var sequence = [];
sequence.push(0x02); // INTEGER
sequence.push(rBa.length);
sequence = sequence.concat(rBa);

sequence.push(0x02); // INTEGER
sequence.push(sBa.length);
sequence = sequence.concat(sBa);

sequence.unshift(sequence.length);
sequence.unshift(0x30); // SEQUENCE
return sequence;
};

/**
* Parses a byte array containing a DER-encoded signature.
*
* This function will return an object of the form:
*
* {
*   r: BigInteger,
*   s: BigInteger
* }
*/
this.parseSig = function (sig) {
var cursor;
if (sig[0] != 0x30)
throw new Error("Signature not a valid DERSequence");

cursor = 2;
if (sig[cursor] != 0x02)
throw new Error("First element in signature must be a DERInteger");;
var rBa = sig.slice(cursor+2, cursor+2+sig[cursor+1]);

cursor += 2+sig[cursor+1];
if (sig[cursor] != 0x02)
throw new Error("Second element in signature must be a DERInteger");
var sBa = sig.slice(cursor+2, cursor+2+sig[cursor+1]);

cursor += 2+sig[cursor+1];

//if (cursor != sig.length)
//throw new Error("Extra bytes in signature");

var r = BigInteger.fromByteArrayUnsigned(rBa);
var s = BigInteger.fromByteArrayUnsigned(sBa);

return {r: r, s: s};
};

this.parseSigCompact = function (sig) {
if (sig.length !== 65) {
throw "Signature has the wrong length";
}

//Signature is prefixed with a type byte storing three bits of
//information.
var i = sig[0] - 27;
if (i < 0 || i > 7) {
throw "Invalid signature type";
}

var n = this.ecparams['n'];
var r = BigInteger.fromByteArrayUnsigned(sig.slice(1, 33)).mod(n);
var s = BigInteger.fromByteArrayUnsigned(sig.slice(33, 65)).mod(n);

return {r: r, s: s, i: i};
};

/*
* Recover a public key from a signature.
*
* See SEC 1: Elliptic Curve Cryptography, section 4.1.6, "Public
* Key Recovery Operation".
*
* http://www.secg.org/download/aid-780/sec1-v2.pdf
*/
/*
recoverPubKey: function (r, s, hash, i) {
//The recovery parameter i has two bits.
i = i & 3;

//The less significant bit specifies whether the y coordinate
//of the compressed point is even or not.
var isYEven = i & 1;

//The more significant bit specifies whether we should use the
//first or second candidate key.
var isSecondKey = i >> 1;

var n = this.ecparams['n'];
var G = this.ecparams['G'];
var curve = this.ecparams['curve'];
var p = curve.getQ();
var a = curve.getA().toBigInteger();
var b = curve.getB().toBigInteger();

//We precalculate (p + 1) / 4 where p is if the field order
if (!P_OVER_FOUR) {
P_OVER_FOUR = p.add(BigInteger.ONE).divide(BigInteger.valueOf(4));
}

//1.1 Compute x
var x = isSecondKey ? r.add(n) : r;

//1.3 Convert x to point
var alpha = x.multiply(x).multiply(x).add(a.multiply(x)).add(b).mod(p);
var beta = alpha.modPow(P_OVER_FOUR, p);

var xorOdd = beta.isEven() ? (i % 2) : ((i+1) % 2);
//If beta is even, but y isn't or vice versa, then convert it,
//otherwise we're done and y == beta.
var y = (beta.isEven() ? !isYEven : isYEven) ? beta : p.subtract(beta);

//1.4 Check that nR is at infinity
var R = new ECPointFp(curve,
curve.fromBigInteger(x),
curve.fromBigInteger(y));
R.validate();

//1.5 Compute e from M
var e = BigInteger.fromByteArrayUnsigned(hash);
var eNeg = BigInteger.ZERO.subtract(e).mod(n);

//1.6 Compute Q = r^-1 (sR - eG)
var rInv = r.modInverse(n);
var Q = implShamirsTrick(R, s, G, eNeg).multiply(rInv);

Q.validate();
if (!this.verifyRaw(e, r, s, Q)) {
throw "Pubkey recovery unsuccessful";
}

var pubKey = new Bitcoin.ECKey();
pubKey.pub = Q;
return pubKey;
},
*/

/*
* Calculate pubkey extraction parameter.
*
* When extracting a pubkey from a signature, we have to
* distinguish four different cases. Rather than putting this
* burden on the verifier, Bitcoin includes a 2-bit value with the
* signature.
*
* This function simply tries all four cases and returns the value
* that resulted in a successful pubkey recovery.
*/
/*
calcPubkeyRecoveryParam: function (address, r, s, hash) {
for (var i = 0; i < 4; i++) {
try {
var pubkey = Bitcoin.ECDSA.recoverPubKey(r, s, hash, i);
if (pubkey.getBitcoinAddress().toString() == address) {
return i;
}
} catch (e) {}
}
throw "Unable to find valid recovery factor";
}
*/

if (params !== undefined) {
if (params['curve'] !== undefined) {
this.curveName = params['curve'];
}
}
if (this.curveName === undefined) this.curveName = curveName;
this.setNamedCurve(this.curveName);
if (params !== undefined) {
if (params['prv'] !== undefined) this.setPrivateKeyHex(params['prv']);
if (params['pub'] !== undefined) this.setPublicKeyHex(params['pub']);
}
};
/**
* parse ASN.1 DER encoded ECDSA signature
* @name parseSigHex
* @memberOf KJUR.crypto.ECDSA
* @function
* @static
* @param {String} sigHex hexadecimal string of ECDSA signature value
* @return {Array} associative array of signature field r and s of BigInteger
* @since ecdsa-modified 1.0.1
* @example
* var ec = KJUR.crypto.ECDSA({'curve': 'secp256r1'});
* var sig = ec.parseSigHex('30...');
* var biR = sig.r; // BigInteger object for 'r' field of signature.
* var biS = sig.s; // BigInteger object for 's' field of signature.
*/
KJUR.crypto.ECDSA.parseSigHex = function(sigHex) {
var p = KJUR.crypto.ECDSA.parseSigHexInHexRS(sigHex);
var biR = new BigInteger(p.r, 16);
var biS = new BigInteger(p.s, 16);

return {'r': biR, 's': biS};
};

/**
* parse ASN.1 DER encoded ECDSA signature
* @name parseSigHexInHexRS
* @memberOf KJUR.crypto.ECDSA
* @function
* @static
* @param {String} sigHex hexadecimal string of ECDSA signature value
* @return {Array} associative array of signature field r and s in hexadecimal
* @since ecdsa-modified 1.0.3
* @example
* var ec = KJUR.crypto.ECDSA({'curve': 'secp256r1'});
* var sig = ec.parseSigHexInHexRS('30...');
* var hR = sig.r; // hexadecimal string for 'r' field of signature.
* var hS = sig.s; // hexadecimal string for 's' field of signature.
*/
KJUR.crypto.ECDSA.parseSigHexInHexRS = function(sigHex) {
//1. ASN.1 Sequence Check
if (sigHex.substr(0, 2) != "30")
throw "signature is not a ASN.1 sequence";

//2. Items of ASN.1 Sequence Check
var a = ASN1HEX.getPosArrayOfChildren_AtObj(sigHex, 0);
if (a.length != 2)
throw "number of signature ASN.1 sequence elements seem wrong";

//3. Integer check
var iTLV1 = a[0];
var iTLV2 = a[1];
if (sigHex.substr(iTLV1, 2) != "02")
throw "1st item of sequene of signature is not ASN.1 integer";
if (sigHex.substr(iTLV2, 2) != "02")
throw "2nd item of sequene of signature is not ASN.1 integer";

//4. getting value
var hR = ASN1HEX.getHexOfV_AtObj(sigHex, iTLV1);
var hS = ASN1HEX.getHexOfV_AtObj(sigHex, iTLV2);

return {'r': hR, 's': hS};
};

/**
* convert hexadecimal ASN.1 encoded signature to concatinated signature
* @name asn1SigToConcatSig
* @memberOf KJUR.crypto.ECDSA
* @function
* @static
* @param {String} asn1Hex hexadecimal string of ASN.1 encoded ECDSA signature value
* @return {String} r-s concatinated format of ECDSA signature value
* @since ecdsa-modified 1.0.3
*/
KJUR.crypto.ECDSA.asn1SigToConcatSig = function(asn1Sig) {
var pSig = KJUR.crypto.ECDSA.parseSigHexInHexRS(asn1Sig);
var hR = pSig.r;
var hS = pSig.s;

if (hR.substr(0, 2) == "00" && (((hR.length / 2) * 8) % (16 * 8)) == 8) 
hR = hR.substr(2);

if (hS.substr(0, 2) == "00" && (((hS.length / 2) * 8) % (16 * 8)) == 8) 
hS = hS.substr(2);

if ((((hR.length / 2) * 8) % (16 * 8)) != 0)
throw "unknown ECDSA sig r length error";

if ((((hS.length / 2) * 8) % (16 * 8)) != 0)
throw "unknown ECDSA sig s length error";

return hR + hS;
};

/**
* convert hexadecimal concatinated signature to ASN.1 encoded signature
* @name concatSigToASN1Sig
* @memberOf KJUR.crypto.ECDSA
* @function
* @static
* @param {String} concatSig r-s concatinated format of ECDSA signature value
* @return {String} hexadecimal string of ASN.1 encoded ECDSA signature value
* @since ecdsa-modified 1.0.3
*/
KJUR.crypto.ECDSA.concatSigToASN1Sig = function(concatSig) {
if ((((concatSig.length / 2) * 8) % (16 * 8)) != 0)
throw "unknown ECDSA concatinated r-s sig  length error";

var hR = concatSig.substr(0, concatSig.length / 2);
var hS = concatSig.substr(concatSig.length / 2);
return KJUR.crypto.ECDSA.hexRSSigToASN1Sig(hR, hS);
};

/**
* convert hexadecimal R and S value of signature to ASN.1 encoded signature
* @name hexRSSigToASN1Sig
* @memberOf KJUR.crypto.ECDSA
* @function
* @static
* @param {String} hR hexadecimal string of R field of ECDSA signature value
* @param {String} hS hexadecimal string of S field of ECDSA signature value
* @return {String} hexadecimal string of ASN.1 encoded ECDSA signature value
* @since ecdsa-modified 1.0.3
*/
KJUR.crypto.ECDSA.hexRSSigToASN1Sig = function(hR, hS) {
var biR = new BigInteger(hR, 16);
var biS = new BigInteger(hS, 16);
return KJUR.crypto.ECDSA.biRSSigToASN1Sig(biR, biS);
};

/**
* convert R and S BigInteger object of signature to ASN.1 encoded signature
* @name biRSSigToASN1Sig
* @memberOf KJUR.crypto.ECDSA
* @function
* @static
* @param {BigInteger} biR BigInteger object of R field of ECDSA signature value
* @param {BigInteger} biS BIgInteger object of S field of ECDSA signature value
* @return {String} hexadecimal string of ASN.1 encoded ECDSA signature value
* @since ecdsa-modified 1.0.3
*/
KJUR.crypto.ECDSA.biRSSigToASN1Sig = function(biR, biS) {
var derR = new KJUR.asn1.DERInteger({'bigint': biR});
var derS = new KJUR.asn1.DERInteger({'bigint': biS});
var derSeq = new KJUR.asn1.DERSequence({'array': [derR, derS]});
return derSeq.getEncodedHex();
};




/****ec-patch.js****/
/*! (c) Stefan Thomas | https://github.com/bitcoinjs/bitcoinjs-lib
*/
/*
* splitted from bitcoin-lib/ecdsa.js
*
* version 1.0.0 is the original of bitcoin-lib/ecdsa.js
*/
ECFieldElementFp.prototype.getByteLength = function () {
return Math.floor((this.toBigInteger().bitLength() + 7) / 8);
};

ECPointFp.prototype.getEncoded = function (compressed) {
var integerToBytes = function(i, len) {
var bytes = i.toByteArrayUnsigned();

if (len < bytes.length) {
bytes = bytes.slice(bytes.length-len);
} else while (len > bytes.length) {
bytes.unshift(0);
}
return bytes;
};

var x = this.getX().toBigInteger();
var y = this.getY().toBigInteger();

//Get value as a 32-byte Buffer
//Fixed length based on a patch by bitaddress.org and Casascius
var enc = integerToBytes(x, 32);

if (compressed) {
if (y.isEven()) {
//Compressed even pubkey
//M = 02 || X
enc.unshift(0x02);
} else {
//Compressed uneven pubkey
//M = 03 || X
enc.unshift(0x03);
}
} else {
//Uncompressed pubkey
//M = 04 || X || Y
enc.unshift(0x04);
enc = enc.concat(integerToBytes(y, 32));
}
return enc;
};

ECPointFp.decodeFrom = function (curve, enc) {
var type = enc[0];
var dataLen = enc.length-1;

//Extract x and y as byte arrays
var xBa = enc.slice(1, 1 + dataLen/2);
var yBa = enc.slice(1 + dataLen/2, 1 + dataLen);

//Prepend zero byte to prevent interpretation as negative integer
xBa.unshift(0);
yBa.unshift(0);

//Convert to BigIntegers
var x = new BigInteger(xBa);
var y = new BigInteger(yBa);

//Return point
return new ECPointFp(curve, curve.fromBigInteger(x), curve.fromBigInteger(y));
};

/*
* @since ec-patch.js 1.0.1
*/
ECPointFp.decodeFromHex = function (curve, encHex) {
var type = encHex.substr(0, 2); // shall be "04"
var dataLen = encHex.length - 2;

//Extract x and y as byte arrays
var xHex = encHex.substr(2, dataLen / 2);
var yHex = encHex.substr(2 + dataLen / 2, dataLen / 2);

//Convert to BigIntegers
var x = new BigInteger(xHex, 16);
var y = new BigInteger(yHex, 16);

//Return point
return new ECPointFp(curve, curve.fromBigInteger(x), curve.fromBigInteger(y));
};

ECPointFp.prototype.add2D = function (b) {
if(this.isInfinity()) return b;
if(b.isInfinity()) return this;

if (this.x.equals(b.x)) {
if (this.y.equals(b.y)) {
//this = b, i.e. this must be doubled
return this.twice();
}
//this = -b, i.e. the result is the point at infinity
return this.curve.getInfinity();
}

var x_x = b.x.subtract(this.x);
var y_y = b.y.subtract(this.y);
var gamma = y_y.divide(x_x);

var x3 = gamma.square().subtract(this.x).subtract(b.x);
var y3 = gamma.multiply(this.x.subtract(x3)).subtract(this.y);

return new ECPointFp(this.curve, x3, y3);
};

ECPointFp.prototype.twice2D = function () {
if (this.isInfinity()) return this;
if (this.y.toBigInteger().signum() == 0) {
//if y1 == 0, then (x1, y1) == (x1, -y1)
//and hence this = -this and thus 2(x1, y1) == infinity
return this.curve.getInfinity();
}

var TWO = this.curve.fromBigInteger(BigInteger.valueOf(2));
var THREE = this.curve.fromBigInteger(BigInteger.valueOf(3));
var gamma = this.x.square().multiply(THREE).add(this.curve.a).divide(this.y.multiply(TWO));

var x3 = gamma.square().subtract(this.x.multiply(TWO));
var y3 = gamma.multiply(this.x.subtract(x3)).subtract(this.y);

return new ECPointFp(this.curve, x3, y3);
};

ECPointFp.prototype.multiply2D = function (k) {
if(this.isInfinity()) return this;
if(k.signum() == 0) return this.curve.getInfinity();

var e = k;
var h = e.multiply(new BigInteger("3"));

var neg = this.negate();
var R = this;

var i;
for (i = h.bitLength() - 2; i > 0; --i) {
R = R.twice();

var hBit = h.testBit(i);
var eBit = e.testBit(i);

if (hBit != eBit) {
R = R.add2D(hBit ? this : neg);
}
}

return R;
};

ECPointFp.prototype.isOnCurve = function () {
var x = this.getX().toBigInteger();
var y = this.getY().toBigInteger();
var a = this.curve.getA().toBigInteger();
var b = this.curve.getB().toBigInteger();
var n = this.curve.getQ();
var lhs = y.multiply(y).mod(n);
var rhs = x.multiply(x).multiply(x)
.add(a.multiply(x)).add(b).mod(n);
return lhs.equals(rhs);
};

ECPointFp.prototype.toString = function () {
return '('+this.getX().toBigInteger().toString()+','+
this.getY().toBigInteger().toString()+')';
};

/**
* Validate an elliptic curve point.
*
* See SEC 1, section 3.2.2.1: Elliptic Curve Public Key Validation Primitive
*/
ECPointFp.prototype.validate = function () {
var n = this.curve.getQ();

//Check Q != O
if (this.isInfinity()) {
throw new Error("Point is at infinity.");
}

//Check coordinate bounds
var x = this.getX().toBigInteger();
var y = this.getY().toBigInteger();
if (x.compareTo(BigInteger.ONE) < 0 ||
x.compareTo(n.subtract(BigInteger.ONE)) > 0) {
throw new Error('x coordinate out of bounds');
}
if (y.compareTo(BigInteger.ONE) < 0 ||
y.compareTo(n.subtract(BigInteger.ONE)) > 0) {
throw new Error('y coordinate out of bounds');
}

//Check y^2 = x^3 + ax + b (mod n)
if (!this.isOnCurve()) {
throw new Error("Point is not on the curve.");
}

//Check nQ = 0 (Q is a scalar multiple of G)
if (this.multiply(n).isInfinity()) {
//TODO: This check doesn't work - fix.
throw new Error("Point is not a scalar multiple of G.");
}

return true;
};

;(function (root, factory) {
	if (typeof exports === "object") {
		// CommonJS
		module.exports = exports = factory();
	}
	else if (typeof define === "function" && define.amd) {
		// AMD
		define([], factory);
	}
	else {
		// Global (browser)
		root.CryptoJS = factory();
	}
}(this, function () {

	/**
	 * CryptoJS core components.
	 */
	var CryptoJS = CryptoJS || (function (Math, undefined) {
	    /**
	     * CryptoJS namespace.
	     */
	    var C = {};

	    /**
	     * Library namespace.
	     */
	    var C_lib = C.lib = {};

	    /**
	     * Base object for prototypal inheritance.
	     */
	    var Base = C_lib.Base = (function () {
	        function F() {}

	        return {
	            /**
	             * Creates a new object that inherits from this object.
	             *
	             * @param {Object} overrides Properties to copy into the new object.
	             *
	             * @return {Object} The new object.
	             *
	             * @static
	             *
	             * @example
	             *
	             *     var MyType = CryptoJS.lib.Base.extend({
	             *         field: 'value',
	             *
	             *         method: function () {
	             *         }
	             *     });
	             */
	            extend: function (overrides) {
	                // Spawn
	                F.prototype = this;
	                var subtype = new F();

	                // Augment
	                if (overrides) {
	                    subtype.mixIn(overrides);
	                }

	                // Create default initializer
	                if (!subtype.hasOwnProperty('init')) {
	                    subtype.init = function () {
	                        subtype.$super.init.apply(this, arguments);
	                    };
	                }

	                // Initializer's prototype is the subtype object
	                subtype.init.prototype = subtype;

	                // Reference supertype
	                subtype.$super = this;

	                return subtype;
	            },

	            /**
	             * Extends this object and runs the init method.
	             * Arguments to create() will be passed to init().
	             *
	             * @return {Object} The new object.
	             *
	             * @static
	             *
	             * @example
	             *
	             *     var instance = MyType.create();
	             */
	            create: function () {
	                var instance = this.extend();
	                instance.init.apply(instance, arguments);

	                return instance;
	            },

	            /**
	             * Initializes a newly created object.
	             * Override this method to add some logic when your objects are created.
	             *
	             * @example
	             *
	             *     var MyType = CryptoJS.lib.Base.extend({
	             *         init: function () {
	             *             // ...
	             *         }
	             *     });
	             */
	            init: function () {
	            },

	            /**
	             * Copies properties into this object.
	             *
	             * @param {Object} properties The properties to mix in.
	             *
	             * @example
	             *
	             *     MyType.mixIn({
	             *         field: 'value'
	             *     });
	             */
	            mixIn: function (properties) {
	                for (var propertyName in properties) {
	                    if (properties.hasOwnProperty(propertyName)) {
	                        this[propertyName] = properties[propertyName];
	                    }
	                }

	                // IE won't copy toString using the loop above
	                if (properties.hasOwnProperty('toString')) {
	                    this.toString = properties.toString;
	                }
	            },

	            /**
	             * Creates a copy of this object.
	             *
	             * @return {Object} The clone.
	             *
	             * @example
	             *
	             *     var clone = instance.clone();
	             */
	            clone: function () {
	                return this.init.prototype.extend(this);
	            }
	        };
	    }());

	    /**
	     * An array of 32-bit words.
	     *
	     * @property {Array} words The array of 32-bit words.
	     * @property {number} sigBytes The number of significant bytes in this word array.
	     */
	    var WordArray = C_lib.WordArray = Base.extend({
	        /**
	         * Initializes a newly created word array.
	         *
	         * @param {Array} words (Optional) An array of 32-bit words.
	         * @param {number} sigBytes (Optional) The number of significant bytes in the words.
	         *
	         * @example
	         *
	         *     var wordArray = CryptoJS.lib.WordArray.create();
	         *     var wordArray = CryptoJS.lib.WordArray.create([0x00010203, 0x04050607]);
	         *     var wordArray = CryptoJS.lib.WordArray.create([0x00010203, 0x04050607], 6);
	         */
	        init: function (words, sigBytes) {
	            words = this.words = words || [];

	            if (sigBytes != undefined) {
	                this.sigBytes = sigBytes;
	            } else {
	                this.sigBytes = words.length * 4;
	            }
	        },

	        /**
	         * Converts this word array to a string.
	         *
	         * @param {Encoder} encoder (Optional) The encoding strategy to use. Default: CryptoJS.enc.Hex
	         *
	         * @return {string} The stringified word array.
	         *
	         * @example
	         *
	         *     var string = wordArray + '';
	         *     var string = wordArray.toString();
	         *     var string = wordArray.toString(CryptoJS.enc.Utf8);
	         */
	        toString: function (encoder) {
	            return (encoder || Hex).stringify(this);
	        },

	        /**
	         * Concatenates a word array to this word array.
	         *
	         * @param {WordArray} wordArray The word array to append.
	         *
	         * @return {WordArray} This word array.
	         *
	         * @example
	         *
	         *     wordArray1.concat(wordArray2);
	         */
	        concat: function (wordArray) {
	            // Shortcuts
	            var thisWords = this.words;
	            var thatWords = wordArray.words;
	            var thisSigBytes = this.sigBytes;
	            var thatSigBytes = wordArray.sigBytes;

	            // Clamp excess bits
	            this.clamp();

	            // Concat
	            if (thisSigBytes % 4) {
	                // Copy one byte at a time
	                for (var i = 0; i < thatSigBytes; i++) {
	                    var thatByte = (thatWords[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
	                    thisWords[(thisSigBytes + i) >>> 2] |= thatByte << (24 - ((thisSigBytes + i) % 4) * 8);
	                }
	            } else {
	                // Copy one word at a time
	                for (var i = 0; i < thatSigBytes; i += 4) {
	                    thisWords[(thisSigBytes + i) >>> 2] = thatWords[i >>> 2];
	                }
	            }
	            this.sigBytes += thatSigBytes;

	            // Chainable
	            return this;
	        },

	        /**
	         * Removes insignificant bits.
	         *
	         * @example
	         *
	         *     wordArray.clamp();
	         */
	        clamp: function () {
	            // Shortcuts
	            var words = this.words;
	            var sigBytes = this.sigBytes;

	            // Clamp
	            words[sigBytes >>> 2] &= 0xffffffff << (32 - (sigBytes % 4) * 8);
	            words.length = Math.ceil(sigBytes / 4);
	        },

	        /**
	         * Creates a copy of this word array.
	         *
	         * @return {WordArray} The clone.
	         *
	         * @example
	         *
	         *     var clone = wordArray.clone();
	         */
	        clone: function () {
	            var clone = Base.clone.call(this);
	            clone.words = this.words.slice(0);

	            return clone;
	        },

	        /**
	         * Creates a word array filled with random bytes.
	         *
	         * @param {number} nBytes The number of random bytes to generate.
	         *
	         * @return {WordArray} The random word array.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var wordArray = CryptoJS.lib.WordArray.random(16);
	         */
	        random: function (nBytes) {
	            var words = [];

	            var r = (function (m_w) {
	                var m_w = m_w;
	                var m_z = 0x3ade68b1;
	                var mask = 0xffffffff;

	                return function () {
	                    m_z = (0x9069 * (m_z & 0xFFFF) + (m_z >> 0x10)) & mask;
	                    m_w = (0x4650 * (m_w & 0xFFFF) + (m_w >> 0x10)) & mask;
	                    var result = ((m_z << 0x10) + m_w) & mask;
	                    result /= 0x100000000;
	                    result += 0.5;
	                    return result * (Math.random() > .5 ? 1 : -1);
	                }
	            });

	            for (var i = 0, rcache; i < nBytes; i += 4) {
	                var _r = r((rcache || Math.random()) * 0x100000000);

	                rcache = _r() * 0x3ade67b7;
	                words.push((_r() * 0x100000000) | 0);
	            }

	            return new WordArray.init(words, nBytes);
	        }
	    });

	    /**
	     * Encoder namespace.
	     */
	    var C_enc = C.enc = {};

	    /**
	     * Hex encoding strategy.
	     */
	    var Hex = C_enc.Hex = {
	        /**
	         * Converts a word array to a hex string.
	         *
	         * @param {WordArray} wordArray The word array.
	         *
	         * @return {string} The hex string.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var hexString = CryptoJS.enc.Hex.stringify(wordArray);
	         */
	        stringify: function (wordArray) {
	            // Shortcuts
	            var words = wordArray.words;
	            var sigBytes = wordArray.sigBytes;

	            // Convert
	            var hexChars = [];
	            for (var i = 0; i < sigBytes; i++) {
	                var bite = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
	                hexChars.push((bite >>> 4).toString(16));
	                hexChars.push((bite & 0x0f).toString(16));
	            }

	            return hexChars.join('');
	        },

	        /**
	         * Converts a hex string to a word array.
	         *
	         * @param {string} hexStr The hex string.
	         *
	         * @return {WordArray} The word array.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var wordArray = CryptoJS.enc.Hex.parse(hexString);
	         */
	        parse: function (hexStr) {
	            // Shortcut
	            var hexStrLength = hexStr.length;

	            // Convert
	            var words = [];
	            for (var i = 0; i < hexStrLength; i += 2) {
	                words[i >>> 3] |= parseInt(hexStr.substr(i, 2), 16) << (24 - (i % 8) * 4);
	            }

	            return new WordArray.init(words, hexStrLength / 2);
	        }
	    };

	    /**
	     * Latin1 encoding strategy.
	     */
	    var Latin1 = C_enc.Latin1 = {
	        /**
	         * Converts a word array to a Latin1 string.
	         *
	         * @param {WordArray} wordArray The word array.
	         *
	         * @return {string} The Latin1 string.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var latin1String = CryptoJS.enc.Latin1.stringify(wordArray);
	         */
	        stringify: function (wordArray) {
	            // Shortcuts
	            var words = wordArray.words;
	            var sigBytes = wordArray.sigBytes;

	            // Convert
	            var latin1Chars = [];
	            for (var i = 0; i < sigBytes; i++) {
	                var bite = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
	                latin1Chars.push(String.fromCharCode(bite));
	            }

	            return latin1Chars.join('');
	        },

	        /**
	         * Converts a Latin1 string to a word array.
	         *
	         * @param {string} latin1Str The Latin1 string.
	         *
	         * @return {WordArray} The word array.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var wordArray = CryptoJS.enc.Latin1.parse(latin1String);
	         */
	        parse: function (latin1Str) {
	            // Shortcut
	            var latin1StrLength = latin1Str.length;

	            // Convert
	            var words = [];
	            for (var i = 0; i < latin1StrLength; i++) {
	                words[i >>> 2] |= (latin1Str.charCodeAt(i) & 0xff) << (24 - (i % 4) * 8);
	            }

	            return new WordArray.init(words, latin1StrLength);
	        }
	    };

	    /**
	     * UTF-8 encoding strategy.
	     */
	    var Utf8 = C_enc.Utf8 = {
	        /**
	         * Converts a word array to a UTF-8 string.
	         *
	         * @param {WordArray} wordArray The word array.
	         *
	         * @return {string} The UTF-8 string.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var utf8String = CryptoJS.enc.Utf8.stringify(wordArray);
	         */
	        stringify: function (wordArray) {
	            try {
	                return decodeURIComponent(escape(Latin1.stringify(wordArray)));
	            } catch (e) {
	                throw new Error('Malformed UTF-8 data');
	            }
	        },

	        /**
	         * Converts a UTF-8 string to a word array.
	         *
	         * @param {string} utf8Str The UTF-8 string.
	         *
	         * @return {WordArray} The word array.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var wordArray = CryptoJS.enc.Utf8.parse(utf8String);
	         */
	        parse: function (utf8Str) {
	            return Latin1.parse(unescape(encodeURIComponent(utf8Str)));
	        }
	    };

	    /**
	     * Abstract buffered block algorithm template.
	     *
	     * The property blockSize must be implemented in a concrete subtype.
	     *
	     * @property {number} _minBufferSize The number of blocks that should be kept unprocessed in the buffer. Default: 0
	     */
	    var BufferedBlockAlgorithm = C_lib.BufferedBlockAlgorithm = Base.extend({
	        /**
	         * Resets this block algorithm's data buffer to its initial state.
	         *
	         * @example
	         *
	         *     bufferedBlockAlgorithm.reset();
	         */
	        reset: function () {
	            // Initial values
	            this._data = new WordArray.init();
	            this._nDataBytes = 0;
	        },

	        /**
	         * Adds new data to this block algorithm's buffer.
	         *
	         * @param {WordArray|string} data The data to append. Strings are converted to a WordArray using UTF-8.
	         *
	         * @example
	         *
	         *     bufferedBlockAlgorithm._append('data');
	         *     bufferedBlockAlgorithm._append(wordArray);
	         */
	        _append: function (data) {
	            // Convert string to WordArray, else assume WordArray already
	            if (typeof data == 'string') {
	                data = Utf8.parse(data);
	            }

	            // Append
	            this._data.concat(data);
	            this._nDataBytes += data.sigBytes;
	        },

	        /**
	         * Processes available data blocks.
	         *
	         * This method invokes _doProcessBlock(offset), which must be implemented by a concrete subtype.
	         *
	         * @param {boolean} doFlush Whether all blocks and partial blocks should be processed.
	         *
	         * @return {WordArray} The processed data.
	         *
	         * @example
	         *
	         *     var processedData = bufferedBlockAlgorithm._process();
	         *     var processedData = bufferedBlockAlgorithm._process(!!'flush');
	         */
	        _process: function (doFlush) {
	            // Shortcuts
	            var data = this._data;
	            var dataWords = data.words;
	            var dataSigBytes = data.sigBytes;
	            var blockSize = this.blockSize;
	            var blockSizeBytes = blockSize * 4;

	            // Count blocks ready
	            var nBlocksReady = dataSigBytes / blockSizeBytes;
	            if (doFlush) {
	                // Round up to include partial blocks
	                nBlocksReady = Math.ceil(nBlocksReady);
	            } else {
	                // Round down to include only full blocks,
	                // less the number of blocks that must remain in the buffer
	                nBlocksReady = Math.max((nBlocksReady | 0) - this._minBufferSize, 0);
	            }

	            // Count words ready
	            var nWordsReady = nBlocksReady * blockSize;

	            // Count bytes ready
	            var nBytesReady = Math.min(nWordsReady * 4, dataSigBytes);

	            // Process blocks
	            if (nWordsReady) {
	                for (var offset = 0; offset < nWordsReady; offset += blockSize) {
	                    // Perform concrete-algorithm logic
	                    this._doProcessBlock(dataWords, offset);
	                }

	                // Remove processed words
	                var processedWords = dataWords.splice(0, nWordsReady);
	                data.sigBytes -= nBytesReady;
	            }

	            // Return processed words
	            return new WordArray.init(processedWords, nBytesReady);
	        },

	        /**
	         * Creates a copy of this object.
	         *
	         * @return {Object} The clone.
	         *
	         * @example
	         *
	         *     var clone = bufferedBlockAlgorithm.clone();
	         */
	        clone: function () {
	            var clone = Base.clone.call(this);
	            clone._data = this._data.clone();

	            return clone;
	        },

	        _minBufferSize: 0
	    });

	    /**
	     * Abstract hasher template.
	     *
	     * @property {number} blockSize The number of 32-bit words this hasher operates on. Default: 16 (512 bits)
	     */
	    var Hasher = C_lib.Hasher = BufferedBlockAlgorithm.extend({
	        /**
	         * Configuration options.
	         */
	        cfg: Base.extend(),

	        /**
	         * Initializes a newly created hasher.
	         *
	         * @param {Object} cfg (Optional) The configuration options to use for this hash computation.
	         *
	         * @example
	         *
	         *     var hasher = CryptoJS.algo.SHA256.create();
	         */
	        init: function (cfg) {
	            // Apply config defaults
	            this.cfg = this.cfg.extend(cfg);

	            // Set initial values
	            this.reset();
	        },

	        /**
	         * Resets this hasher to its initial state.
	         *
	         * @example
	         *
	         *     hasher.reset();
	         */
	        reset: function () {
	            // Reset data buffer
	            BufferedBlockAlgorithm.reset.call(this);

	            // Perform concrete-hasher logic
	            this._doReset();
	        },

	        /**
	         * Updates this hasher with a message.
	         *
	         * @param {WordArray|string} messageUpdate The message to append.
	         *
	         * @return {Hasher} This hasher.
	         *
	         * @example
	         *
	         *     hasher.update('message');
	         *     hasher.update(wordArray);
	         */
	        update: function (messageUpdate) {
	            // Append
	            this._append(messageUpdate);

	            // Update the hash
	            this._process();

	            // Chainable
	            return this;
	        },

	        /**
	         * Finalizes the hash computation.
	         * Note that the finalize operation is effectively a destructive, read-once operation.
	         *
	         * @param {WordArray|string} messageUpdate (Optional) A final message update.
	         *
	         * @return {WordArray} The hash.
	         *
	         * @example
	         *
	         *     var hash = hasher.finalize();
	         *     var hash = hasher.finalize('message');
	         *     var hash = hasher.finalize(wordArray);
	         */
	        finalize: function (messageUpdate) {
	            // Final message update
	            if (messageUpdate) {
	                this._append(messageUpdate);
	            }

	            // Perform concrete-hasher logic
	            var hash = this._doFinalize();

	            return hash;
	        },

	        blockSize: 512/32,

	        /**
	         * Creates a shortcut function to a hasher's object interface.
	         *
	         * @param {Hasher} hasher The hasher to create a helper for.
	         *
	         * @return {Function} The shortcut function.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var SHA256 = CryptoJS.lib.Hasher._createHelper(CryptoJS.algo.SHA256);
	         */
	        _createHelper: function (hasher) {
	            return function (message, cfg) {
	                return new hasher.init(cfg).finalize(message);
	            };
	        },

	        /**
	         * Creates a shortcut function to the HMAC's object interface.
	         *
	         * @param {Hasher} hasher The hasher to use in this HMAC helper.
	         *
	         * @return {Function} The shortcut function.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var HmacSHA256 = CryptoJS.lib.Hasher._createHmacHelper(CryptoJS.algo.SHA256);
	         */
	        _createHmacHelper: function (hasher) {
	            return function (message, key) {
	                return new C_algo.HMAC.init(hasher, key).finalize(message);
	            };
	        }
	    });

	    /**
	     * Algorithm namespace.
	     */
	    var C_algo = C.algo = {};

	    return C;
	}(Math));


	(function () {
	    // Shortcuts
	    var C = CryptoJS;
	    var C_lib = C.lib;
	    var WordArray = C_lib.WordArray;
	    var C_enc = C.enc;

	    /**
	     * Base64 encoding strategy.
	     */
	    var Base64 = C_enc.Base64 = {
	        /**
	         * Converts a word array to a Base64 string.
	         *
	         * @param {WordArray} wordArray The word array.
	         *
	         * @return {string} The Base64 string.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var base64String = CryptoJS.enc.Base64.stringify(wordArray);
	         */
	        stringify: function (wordArray) {
	            // Shortcuts
	            var words = wordArray.words;
	            var sigBytes = wordArray.sigBytes;
	            var map = this._map;

	            // Clamp excess bits
	            wordArray.clamp();

	            // Convert
	            var base64Chars = [];
	            for (var i = 0; i < sigBytes; i += 3) {
	                var byte1 = (words[i >>> 2]       >>> (24 - (i % 4) * 8))       & 0xff;
	                var byte2 = (words[(i + 1) >>> 2] >>> (24 - ((i + 1) % 4) * 8)) & 0xff;
	                var byte3 = (words[(i + 2) >>> 2] >>> (24 - ((i + 2) % 4) * 8)) & 0xff;

	                var triplet = (byte1 << 16) | (byte2 << 8) | byte3;

	                for (var j = 0; (j < 4) && (i + j * 0.75 < sigBytes); j++) {
	                    base64Chars.push(map.charAt((triplet >>> (6 * (3 - j))) & 0x3f));
	                }
	            }

	            // Add padding
	            var paddingChar = map.charAt(64);
	            if (paddingChar) {
	                while (base64Chars.length % 4) {
	                    base64Chars.push(paddingChar);
	                }
	            }

	            return base64Chars.join('');
	        },

	        /**
	         * Converts a Base64 string to a word array.
	         *
	         * @param {string} base64Str The Base64 string.
	         *
	         * @return {WordArray} The word array.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var wordArray = CryptoJS.enc.Base64.parse(base64String);
	         */
	        parse: function (base64Str) {
	            // Shortcuts
	            var base64StrLength = base64Str.length;
	            var map = this._map;

	            // Ignore padding
	            var paddingChar = map.charAt(64);
	            if (paddingChar) {
	                var paddingIndex = base64Str.indexOf(paddingChar);
	                if (paddingIndex != -1) {
	                    base64StrLength = paddingIndex;
	                }
	            }

	            // Convert
	            var words = [];
	            var nBytes = 0;
	            for (var i = 0; i < base64StrLength; i++) {
	                if (i % 4) {
	                    var bits1 = map.indexOf(base64Str.charAt(i - 1)) << ((i % 4) * 2);
	                    var bits2 = map.indexOf(base64Str.charAt(i)) >>> (6 - (i % 4) * 2);
	                    words[nBytes >>> 2] |= (bits1 | bits2) << (24 - (nBytes % 4) * 8);
	                    nBytes++;
	                }
	            }

	            return WordArray.create(words, nBytes);
	        },

	        _map: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
	    };
	}());

	(function () {
	    // Check if typed arrays are supported
	    if (typeof ArrayBuffer != 'function') {
	        return;
	    }

	    // Shortcuts
	    var C = CryptoJS;
	    var C_lib = C.lib;
	    var WordArray = C_lib.WordArray;

	    // Reference original init
	    var superInit = WordArray.init;

	    // Augment WordArray.init to handle typed arrays
	    var subInit = WordArray.init = function (typedArray) {
	        // Convert buffers to uint8
	        if (typedArray instanceof ArrayBuffer) {
	            typedArray = new Uint8Array(typedArray);
	        }

	        // Convert other array views to uint8
	        if (
	            typedArray instanceof Int8Array ||
	            (typeof Uint8ClampedArray !== "undefined" && typedArray instanceof Uint8ClampedArray) ||
	            typedArray instanceof Int16Array ||
	            typedArray instanceof Uint16Array ||
	            typedArray instanceof Int32Array ||
	            typedArray instanceof Uint32Array ||
	            typedArray instanceof Float32Array ||
	            typedArray instanceof Float64Array
	        ) {
	            typedArray = new Uint8Array(typedArray.buffer, typedArray.byteOffset, typedArray.byteLength);
	        }

	        // Handle Uint8Array
	        if (typedArray instanceof Uint8Array) {
	            // Shortcut
	            var typedArrayByteLength = typedArray.byteLength;

	            // Extract bytes
	            var words = [];
	            for (var i = 0; i < typedArrayByteLength; i++) {
	                words[i >>> 2] |= typedArray[i] << (24 - (i % 4) * 8);
	            }

	            // Initialize this word array
	            superInit.call(this, words, typedArrayByteLength);
	        } else {
	            // Else call normal init
	            superInit.apply(this, arguments);
	        }
	    };

	    subInit.prototype = WordArray;
	}());


	/**
	 * Cipher core components.
	 */
	CryptoJS.lib.Cipher || (function (undefined) {
	    // Shortcuts
	    var C = CryptoJS;
	    var C_lib = C.lib;
	    var Base = C_lib.Base;
	    var WordArray = C_lib.WordArray;
	    var BufferedBlockAlgorithm = C_lib.BufferedBlockAlgorithm;
	    var C_enc = C.enc;
	    var Utf8 = C_enc.Utf8;
	    var Base64 = C_enc.Base64;
	    var C_algo = C.algo;
	    var EvpKDF = C_algo.EvpKDF;

	    /**
	     * Abstract base cipher template.
	     *
	     * @property {number} keySize This cipher's key size. Default: 4 (128 bits)
	     * @property {number} ivSize This cipher's IV size. Default: 4 (128 bits)
	     * @property {number} _ENC_XFORM_MODE A constant representing encryption mode.
	     * @property {number} _DEC_XFORM_MODE A constant representing decryption mode.
	     */
	    var Cipher = C_lib.Cipher = BufferedBlockAlgorithm.extend({
	        /**
	         * Configuration options.
	         *
	         * @property {WordArray} iv The IV to use for this operation.
	         */
	        cfg: Base.extend(),

	        /**
	         * Creates this cipher in encryption mode.
	         *
	         * @param {WordArray} key The key.
	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
	         *
	         * @return {Cipher} A cipher instance.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var cipher = CryptoJS.algo.AES.createEncryptor(keyWordArray, { iv: ivWordArray });
	         */
	        createEncryptor: function (key, cfg) {
	            return this.create(this._ENC_XFORM_MODE, key, cfg);
	        },

	        /**
	         * Creates this cipher in decryption mode.
	         *
	         * @param {WordArray} key The key.
	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
	         *
	         * @return {Cipher} A cipher instance.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var cipher = CryptoJS.algo.AES.createDecryptor(keyWordArray, { iv: ivWordArray });
	         */
	        createDecryptor: function (key, cfg) {
	            return this.create(this._DEC_XFORM_MODE, key, cfg);
	        },

	        /**
	         * Initializes a newly created cipher.
	         *
	         * @param {number} xformMode Either the encryption or decryption transormation mode constant.
	         * @param {WordArray} key The key.
	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
	         *
	         * @example
	         *
	         *     var cipher = CryptoJS.algo.AES.create(CryptoJS.algo.AES._ENC_XFORM_MODE, keyWordArray, { iv: ivWordArray });
	         */
	        init: function (xformMode, key, cfg) {
	            // Apply config defaults
	            this.cfg = this.cfg.extend(cfg);

	            // Store transform mode and key
	            this._xformMode = xformMode;
	            this._key = key;

	            // Set initial values
	            this.reset();
	        },

	        /**
	         * Resets this cipher to its initial state.
	         *
	         * @example
	         *
	         *     cipher.reset();
	         */
	        reset: function () {
	            // Reset data buffer
	            BufferedBlockAlgorithm.reset.call(this);

	            // Perform concrete-cipher logic
	            this._doReset();
	        },

	        /**
	         * Adds data to be encrypted or decrypted.
	         *
	         * @param {WordArray|string} dataUpdate The data to encrypt or decrypt.
	         *
	         * @return {WordArray} The data after processing.
	         *
	         * @example
	         *
	         *     var encrypted = cipher.process('data');
	         *     var encrypted = cipher.process(wordArray);
	         */
	        process: function (dataUpdate) {
	            // Append
	            this._append(dataUpdate);

	            // Process available blocks
	            return this._process();
	        },

	        /**
	         * Finalizes the encryption or decryption process.
	         * Note that the finalize operation is effectively a destructive, read-once operation.
	         *
	         * @param {WordArray|string} dataUpdate The final data to encrypt or decrypt.
	         *
	         * @return {WordArray} The data after final processing.
	         *
	         * @example
	         *
	         *     var encrypted = cipher.finalize();
	         *     var encrypted = cipher.finalize('data');
	         *     var encrypted = cipher.finalize(wordArray);
	         */
	        finalize: function (dataUpdate) {
	            // Final data update
	            if (dataUpdate) {
	                this._append(dataUpdate);
	            }

	            // Perform concrete-cipher logic
	            var finalProcessedData = this._doFinalize();

	            return finalProcessedData;
	        },

	        keySize: 128/32,

	        ivSize: 128/32,

	        _ENC_XFORM_MODE: 1,

	        _DEC_XFORM_MODE: 2,

	        /**
	         * Creates shortcut functions to a cipher's object interface.
	         *
	         * @param {Cipher} cipher The cipher to create a helper for.
	         *
	         * @return {Object} An object with encrypt and decrypt shortcut functions.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var AES = CryptoJS.lib.Cipher._createHelper(CryptoJS.algo.AES);
	         */
	        _createHelper: (function () {
	            function selectCipherStrategy(key) {
	                if (typeof key == 'string') {
	                    return PasswordBasedCipher;
	                } else {
	                    return SerializableCipher;
	                }
	            }

	            return function (cipher) {
	                return {
	                    encrypt: function (message, key, cfg) {
	                        return selectCipherStrategy(key).encrypt(cipher, message, key, cfg);
	                    },

	                    decrypt: function (ciphertext, key, cfg) {
	                        return selectCipherStrategy(key).decrypt(cipher, ciphertext, key, cfg);
	                    }
	                };
	            };
	        }())
	    });

	    /**
	     * Abstract base stream cipher template.
	     *
	     * @property {number} blockSize The number of 32-bit words this cipher operates on. Default: 1 (32 bits)
	     */
	    var StreamCipher = C_lib.StreamCipher = Cipher.extend({
	        _doFinalize: function () {
	            // Process partial blocks
	            var finalProcessedBlocks = this._process(!!'flush');

	            return finalProcessedBlocks;
	        },

	        blockSize: 1
	    });

	    /**
	     * Mode namespace.
	     */
	    var C_mode = C.mode = {};

	    /**
	     * Abstract base block cipher mode template.
	     */
	    var BlockCipherMode = C_lib.BlockCipherMode = Base.extend({
	        /**
	         * Creates this mode for encryption.
	         *
	         * @param {Cipher} cipher A block cipher instance.
	         * @param {Array} iv The IV words.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var mode = CryptoJS.mode.CBC.createEncryptor(cipher, iv.words);
	         */
	        createEncryptor: function (cipher, iv) {
	            return this.Encryptor.create(cipher, iv);
	        },

	        /**
	         * Creates this mode for decryption.
	         *
	         * @param {Cipher} cipher A block cipher instance.
	         * @param {Array} iv The IV words.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var mode = CryptoJS.mode.CBC.createDecryptor(cipher, iv.words);
	         */
	        createDecryptor: function (cipher, iv) {
	            return this.Decryptor.create(cipher, iv);
	        },

	        /**
	         * Initializes a newly created mode.
	         *
	         * @param {Cipher} cipher A block cipher instance.
	         * @param {Array} iv The IV words.
	         *
	         * @example
	         *
	         *     var mode = CryptoJS.mode.CBC.Encryptor.create(cipher, iv.words);
	         */
	        init: function (cipher, iv) {
	            this._cipher = cipher;
	            this._iv = iv;
	        }
	    });

	    /**
	     * Cipher Block Chaining mode.
	     */
	    var CBC = C_mode.CBC = (function () {
	        /**
	         * Abstract base CBC mode.
	         */
	        var CBC = BlockCipherMode.extend();

	        /**
	         * CBC encryptor.
	         */
	        CBC.Encryptor = CBC.extend({
	            /**
	             * Processes the data block at offset.
	             *
	             * @param {Array} words The data words to operate on.
	             * @param {number} offset The offset where the block starts.
	             *
	             * @example
	             *
	             *     mode.processBlock(data.words, offset);
	             */
	            processBlock: function (words, offset) {
	                // Shortcuts
	                var cipher = this._cipher;
	                var blockSize = cipher.blockSize;

	                // XOR and encrypt
	                xorBlock.call(this, words, offset, blockSize);
	                cipher.encryptBlock(words, offset);

	                // Remember this block to use with next block
	                this._prevBlock = words.slice(offset, offset + blockSize);
	            }
	        });

	        /**
	         * CBC decryptor.
	         */
	        CBC.Decryptor = CBC.extend({
	            /**
	             * Processes the data block at offset.
	             *
	             * @param {Array} words The data words to operate on.
	             * @param {number} offset The offset where the block starts.
	             *
	             * @example
	             *
	             *     mode.processBlock(data.words, offset);
	             */
	            processBlock: function (words, offset) {
	                // Shortcuts
	                var cipher = this._cipher;
	                var blockSize = cipher.blockSize;

	                // Remember this block to use with next block
	                var thisBlock = words.slice(offset, offset + blockSize);

	                // Decrypt and XOR
	                cipher.decryptBlock(words, offset);
	                xorBlock.call(this, words, offset, blockSize);

	                // This block becomes the previous block
	                this._prevBlock = thisBlock;
	            }
	        });

	        function xorBlock(words, offset, blockSize) {
	            // Shortcut
	            var iv = this._iv;

	            // Choose mixing block
	            if (iv) {
	                var block = iv;

	                // Remove IV for subsequent blocks
	                this._iv = undefined;
	            } else {
	                var block = this._prevBlock;
	            }

	            // XOR blocks
	            for (var i = 0; i < blockSize; i++) {
	                words[offset + i] ^= block[i];
	            }
	        }

	        return CBC;
	    }());

	    /**
	     * Padding namespace.
	     */
	    var C_pad = C.pad = {};

	    /**
	     * PKCS #5/7 padding strategy.
	     */
	    var Pkcs7 = C_pad.Pkcs7 = {
	        /**
	         * Pads data using the algorithm defined in PKCS #5/7.
	         *
	         * @param {WordArray} data The data to pad.
	         * @param {number} blockSize The multiple that the data should be padded to.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     CryptoJS.pad.Pkcs7.pad(wordArray, 4);
	         */
	        pad: function (data, blockSize) {
	            // Shortcut
	            var blockSizeBytes = blockSize * 4;

	            // Count padding bytes
	            var nPaddingBytes = blockSizeBytes - data.sigBytes % blockSizeBytes;

	            // Create padding word
	            var paddingWord = (nPaddingBytes << 24) | (nPaddingBytes << 16) | (nPaddingBytes << 8) | nPaddingBytes;

	            // Create padding
	            var paddingWords = [];
	            for (var i = 0; i < nPaddingBytes; i += 4) {
	                paddingWords.push(paddingWord);
	            }
	            var padding = WordArray.create(paddingWords, nPaddingBytes);

	            // Add padding
	            data.concat(padding);
	        },

	        /**
	         * Unpads data that had been padded using the algorithm defined in PKCS #5/7.
	         *
	         * @param {WordArray} data The data to unpad.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     CryptoJS.pad.Pkcs7.unpad(wordArray);
	         */
	        unpad: function (data) {
	            // Get number of padding bytes from last byte
	            var nPaddingBytes = data.words[(data.sigBytes - 1) >>> 2] & 0xff;

	            // Remove padding
	            data.sigBytes -= nPaddingBytes;
	        }
	    };

	    /**
	     * Abstract base block cipher template.
	     *
	     * @property {number} blockSize The number of 32-bit words this cipher operates on. Default: 4 (128 bits)
	     */
	    var BlockCipher = C_lib.BlockCipher = Cipher.extend({
	        /**
	         * Configuration options.
	         *
	         * @property {Mode} mode The block mode to use. Default: CBC
	         * @property {Padding} padding The padding strategy to use. Default: Pkcs7
	         */
	        cfg: Cipher.cfg.extend({
	            mode: CBC,
	            padding: Pkcs7
	        }),

	        reset: function () {
	            // Reset cipher
	            Cipher.reset.call(this);

	            // Shortcuts
	            var cfg = this.cfg;
	            var iv = cfg.iv;
	            var mode = cfg.mode;

	            // Reset block mode
	            if (this._xformMode == this._ENC_XFORM_MODE) {
	                var modeCreator = mode.createEncryptor;
	            } else /* if (this._xformMode == this._DEC_XFORM_MODE) */ {
	                var modeCreator = mode.createDecryptor;

	                // Keep at least one block in the buffer for unpadding
	                this._minBufferSize = 1;
	            }
	            this._mode = modeCreator.call(mode, this, iv && iv.words);
	        },

	        _doProcessBlock: function (words, offset) {
	            this._mode.processBlock(words, offset);
	        },

	        _doFinalize: function () {
	            // Shortcut
	            var padding = this.cfg.padding;

	            // Finalize
	            if (this._xformMode == this._ENC_XFORM_MODE) {
	                // Pad data
	                padding.pad(this._data, this.blockSize);

	                // Process final blocks
	                var finalProcessedBlocks = this._process(!!'flush');
	            } else /* if (this._xformMode == this._DEC_XFORM_MODE) */ {
	                // Process final blocks
	                var finalProcessedBlocks = this._process(!!'flush');

	                // Unpad data
	                padding.unpad(finalProcessedBlocks);
	            }

	            return finalProcessedBlocks;
	        },

	        blockSize: 128/32
	    });

	    /**
	     * A collection of cipher parameters.
	     *
	     * @property {WordArray} ciphertext The raw ciphertext.
	     * @property {WordArray} key The key to this ciphertext.
	     * @property {WordArray} iv The IV used in the ciphering operation.
	     * @property {WordArray} salt The salt used with a key derivation function.
	     * @property {Cipher} algorithm The cipher algorithm.
	     * @property {Mode} mode The block mode used in the ciphering operation.
	     * @property {Padding} padding The padding scheme used in the ciphering operation.
	     * @property {number} blockSize The block size of the cipher.
	     * @property {Format} formatter The default formatting strategy to convert this cipher params object to a string.
	     */
	    var CipherParams = C_lib.CipherParams = Base.extend({
	        /**
	         * Initializes a newly created cipher params object.
	         *
	         * @param {Object} cipherParams An object with any of the possible cipher parameters.
	         *
	         * @example
	         *
	         *     var cipherParams = CryptoJS.lib.CipherParams.create({
	         *         ciphertext: ciphertextWordArray,
	         *         key: keyWordArray,
	         *         iv: ivWordArray,
	         *         salt: saltWordArray,
	         *         algorithm: CryptoJS.algo.AES,
	         *         mode: CryptoJS.mode.CBC,
	         *         padding: CryptoJS.pad.PKCS7,
	         *         blockSize: 4,
	         *         formatter: CryptoJS.format.OpenSSL
	         *     });
	         */
	        init: function (cipherParams) {
	            this.mixIn(cipherParams);
	        },

	        /**
	         * Converts this cipher params object to a string.
	         *
	         * @param {Format} formatter (Optional) The formatting strategy to use.
	         *
	         * @return {string} The stringified cipher params.
	         *
	         * @throws Error If neither the formatter nor the default formatter is set.
	         *
	         * @example
	         *
	         *     var string = cipherParams + '';
	         *     var string = cipherParams.toString();
	         *     var string = cipherParams.toString(CryptoJS.format.OpenSSL);
	         */
	        toString: function (formatter) {
	            return (formatter || this.formatter).stringify(this);
	        }
	    });

	    /**
	     * Format namespace.
	     */
	    var C_format = C.format = {};

	    /**
	     * OpenSSL formatting strategy.
	     */
	    var OpenSSLFormatter = C_format.OpenSSL = {
	        /**
	         * Converts a cipher params object to an OpenSSL-compatible string.
	         *
	         * @param {CipherParams} cipherParams The cipher params object.
	         *
	         * @return {string} The OpenSSL-compatible string.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var openSSLString = CryptoJS.format.OpenSSL.stringify(cipherParams);
	         */
	        stringify: function (cipherParams) {
	            // Shortcuts
	            var ciphertext = cipherParams.ciphertext;
	            var salt = cipherParams.salt;

	            // Format
	            if (salt) {
	                var wordArray = WordArray.create([0x53616c74, 0x65645f5f]).concat(salt).concat(ciphertext);
	            } else {
	                var wordArray = ciphertext;
	            }

	            return wordArray.toString(Base64);
	        },

	        /**
	         * Converts an OpenSSL-compatible string to a cipher params object.
	         *
	         * @param {string} openSSLStr The OpenSSL-compatible string.
	         *
	         * @return {CipherParams} The cipher params object.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var cipherParams = CryptoJS.format.OpenSSL.parse(openSSLString);
	         */
	        parse: function (openSSLStr) {
	            // Parse base64
	            var ciphertext = Base64.parse(openSSLStr);

	            // Shortcut
	            var ciphertextWords = ciphertext.words;

	            // Test for salt
	            if (ciphertextWords[0] == 0x53616c74 && ciphertextWords[1] == 0x65645f5f) {
	                // Extract salt
	                var salt = WordArray.create(ciphertextWords.slice(2, 4));

	                // Remove salt from ciphertext
	                ciphertextWords.splice(0, 4);
	                ciphertext.sigBytes -= 16;
	            }

	            return CipherParams.create({ ciphertext: ciphertext, salt: salt });
	        }
	    };

	    /**
	     * A cipher wrapper that returns ciphertext as a serializable cipher params object.
	     */
	    var SerializableCipher = C_lib.SerializableCipher = Base.extend({
	        /**
	         * Configuration options.
	         *
	         * @property {Formatter} format The formatting strategy to convert cipher param objects to and from a string. Default: OpenSSL
	         */
	        cfg: Base.extend({
	            format: OpenSSLFormatter
	        }),

	        /**
	         * Encrypts a message.
	         *
	         * @param {Cipher} cipher The cipher algorithm to use.
	         * @param {WordArray|string} message The message to encrypt.
	         * @param {WordArray} key The key.
	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
	         *
	         * @return {CipherParams} A cipher params object.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var ciphertextParams = CryptoJS.lib.SerializableCipher.encrypt(CryptoJS.algo.AES, message, key);
	         *     var ciphertextParams = CryptoJS.lib.SerializableCipher.encrypt(CryptoJS.algo.AES, message, key, { iv: iv });
	         *     var ciphertextParams = CryptoJS.lib.SerializableCipher.encrypt(CryptoJS.algo.AES, message, key, { iv: iv, format: CryptoJS.format.OpenSSL });
	         */
	        encrypt: function (cipher, message, key, cfg) {
	            // Apply config defaults
	            cfg = this.cfg.extend(cfg);

	            // Encrypt
	            var encryptor = cipher.createEncryptor(key, cfg);
	            var ciphertext = encryptor.finalize(message);

	            // Shortcut
	            var cipherCfg = encryptor.cfg;

	            // Create and return serializable cipher params
	            return CipherParams.create({
	                ciphertext: ciphertext,
	                key: key,
	                iv: cipherCfg.iv,
	                algorithm: cipher,
	                mode: cipherCfg.mode,
	                padding: cipherCfg.padding,
	                blockSize: cipher.blockSize,
	                formatter: cfg.format
	            });
	        },

	        /**
	         * Decrypts serialized ciphertext.
	         *
	         * @param {Cipher} cipher The cipher algorithm to use.
	         * @param {CipherParams|string} ciphertext The ciphertext to decrypt.
	         * @param {WordArray} key The key.
	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
	         *
	         * @return {WordArray} The plaintext.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var plaintext = CryptoJS.lib.SerializableCipher.decrypt(CryptoJS.algo.AES, formattedCiphertext, key, { iv: iv, format: CryptoJS.format.OpenSSL });
	         *     var plaintext = CryptoJS.lib.SerializableCipher.decrypt(CryptoJS.algo.AES, ciphertextParams, key, { iv: iv, format: CryptoJS.format.OpenSSL });
	         */
	        decrypt: function (cipher, ciphertext, key, cfg) {
	            // Apply config defaults
	            cfg = this.cfg.extend(cfg);

	            // Convert string to CipherParams
	            ciphertext = this._parse(ciphertext, cfg.format);

	            // Decrypt
	            var plaintext = cipher.createDecryptor(key, cfg).finalize(ciphertext.ciphertext);

	            return plaintext;
	        },

	        /**
	         * Converts serialized ciphertext to CipherParams,
	         * else assumed CipherParams already and returns ciphertext unchanged.
	         *
	         * @param {CipherParams|string} ciphertext The ciphertext.
	         * @param {Formatter} format The formatting strategy to use to parse serialized ciphertext.
	         *
	         * @return {CipherParams} The unserialized ciphertext.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var ciphertextParams = CryptoJS.lib.SerializableCipher._parse(ciphertextStringOrParams, format);
	         */
	        _parse: function (ciphertext, format) {
	            if (typeof ciphertext == 'string') {
	                return format.parse(ciphertext, this);
	            } else {
	                return ciphertext;
	            }
	        }
	    });

	    /**
	     * Key derivation function namespace.
	     */
	    var C_kdf = C.kdf = {};

	    /**
	     * OpenSSL key derivation function.
	     */
	    var OpenSSLKdf = C_kdf.OpenSSL = {
	        /**
	         * Derives a key and IV from a password.
	         *
	         * @param {string} password The password to derive from.
	         * @param {number} keySize The size in words of the key to generate.
	         * @param {number} ivSize The size in words of the IV to generate.
	         * @param {WordArray|string} salt (Optional) A 64-bit salt to use. If omitted, a salt will be generated randomly.
	         *
	         * @return {CipherParams} A cipher params object with the key, IV, and salt.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var derivedParams = CryptoJS.kdf.OpenSSL.execute('Password', 256/32, 128/32);
	         *     var derivedParams = CryptoJS.kdf.OpenSSL.execute('Password', 256/32, 128/32, 'saltsalt');
	         */
	        execute: function (password, keySize, ivSize, salt) {
	            // Generate random salt
	            if (!salt) {
	                salt = WordArray.random(64/8);
	            }

	            // Derive key and IV
	            var key = EvpKDF.create({ keySize: keySize + ivSize }).compute(password, salt);

	            // Separate key and IV
	            var iv = WordArray.create(key.words.slice(keySize), ivSize * 4);
	            key.sigBytes = keySize * 4;

	            // Return params
	            return CipherParams.create({ key: key, iv: iv, salt: salt });
	        }
	    };

	    /**
	     * A serializable cipher wrapper that derives the key from a password,
	     * and returns ciphertext as a serializable cipher params object.
	     */
	    var PasswordBasedCipher = C_lib.PasswordBasedCipher = SerializableCipher.extend({
	        /**
	         * Configuration options.
	         *
	         * @property {KDF} kdf The key derivation function to use to generate a key and IV from a password. Default: OpenSSL
	         */
	        cfg: SerializableCipher.cfg.extend({
	            kdf: OpenSSLKdf
	        }),

	        /**
	         * Encrypts a message using a password.
	         *
	         * @param {Cipher} cipher The cipher algorithm to use.
	         * @param {WordArray|string} message The message to encrypt.
	         * @param {string} password The password.
	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
	         *
	         * @return {CipherParams} A cipher params object.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var ciphertextParams = CryptoJS.lib.PasswordBasedCipher.encrypt(CryptoJS.algo.AES, message, 'password');
	         *     var ciphertextParams = CryptoJS.lib.PasswordBasedCipher.encrypt(CryptoJS.algo.AES, message, 'password', { format: CryptoJS.format.OpenSSL });
	         */
	        encrypt: function (cipher, message, password, cfg) {
	            // Apply config defaults
	            cfg = this.cfg.extend(cfg);

	            // Derive key and other params
	            var derivedParams = cfg.kdf.execute(password, cipher.keySize, cipher.ivSize);

	            // Add IV to config
	            cfg.iv = derivedParams.iv;

	            // Encrypt
	            var ciphertext = SerializableCipher.encrypt.call(this, cipher, message, derivedParams.key, cfg);

	            // Mix in derived params
	            ciphertext.mixIn(derivedParams);

	            return ciphertext;
	        },

	        /**
	         * Decrypts serialized ciphertext using a password.
	         *
	         * @param {Cipher} cipher The cipher algorithm to use.
	         * @param {CipherParams|string} ciphertext The ciphertext to decrypt.
	         * @param {string} password The password.
	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
	         *
	         * @return {WordArray} The plaintext.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var plaintext = CryptoJS.lib.PasswordBasedCipher.decrypt(CryptoJS.algo.AES, formattedCiphertext, 'password', { format: CryptoJS.format.OpenSSL });
	         *     var plaintext = CryptoJS.lib.PasswordBasedCipher.decrypt(CryptoJS.algo.AES, ciphertextParams, 'password', { format: CryptoJS.format.OpenSSL });
	         */
	        decrypt: function (cipher, ciphertext, password, cfg) {
	            // Apply config defaults
	            cfg = this.cfg.extend(cfg);

	            // Convert string to CipherParams
	            ciphertext = this._parse(ciphertext, cfg.format);

	            // Derive key and other params
	            var derivedParams = cfg.kdf.execute(password, cipher.keySize, cipher.ivSize, ciphertext.salt);

	            // Add IV to config
	            cfg.iv = derivedParams.iv;

	            // Decrypt
	            var plaintext = SerializableCipher.decrypt.call(this, cipher, ciphertext, derivedParams.key, cfg);

	            return plaintext;
	        }
	    });
	}());


	/**
	 * Cipher Feedback block mode.
	 */
	CryptoJS.mode.CFB = (function () {
	    var CFB = CryptoJS.lib.BlockCipherMode.extend();

	    CFB.Encryptor = CFB.extend({
	        processBlock: function (words, offset) {
	            // Shortcuts
	            var cipher = this._cipher;
	            var blockSize = cipher.blockSize;

	            generateKeystreamAndEncrypt.call(this, words, offset, blockSize, cipher);

	            // Remember this block to use with next block
	            this._prevBlock = words.slice(offset, offset + blockSize);
	        }
	    });

	    CFB.Decryptor = CFB.extend({
	        processBlock: function (words, offset) {
	            // Shortcuts
	            var cipher = this._cipher;
	            var blockSize = cipher.blockSize;

	            // Remember this block to use with next block
	            var thisBlock = words.slice(offset, offset + blockSize);

	            generateKeystreamAndEncrypt.call(this, words, offset, blockSize, cipher);

	            // This block becomes the previous block
	            this._prevBlock = thisBlock;
	        }
	    });

	    function generateKeystreamAndEncrypt(words, offset, blockSize, cipher) {
	        // Shortcut
	        var iv = this._iv;

	        // Generate keystream
	        if (iv) {
	            var keystream = iv.slice(0);

	            // Remove IV for subsequent blocks
	            this._iv = undefined;
	        } else {
	            var keystream = this._prevBlock;
	        }
	        cipher.encryptBlock(keystream, 0);

	        // Encrypt
	        for (var i = 0; i < blockSize; i++) {
	            words[offset + i] ^= keystream[i];
	        }
	    }

	    return CFB;
	}());


	/**
	 * Electronic Codebook block mode.
	 */
	CryptoJS.mode.ECB = (function () {
	    var ECB = CryptoJS.lib.BlockCipherMode.extend();

	    ECB.Encryptor = ECB.extend({
	        processBlock: function (words, offset) {
	            this._cipher.encryptBlock(words, offset);
	        }
	    });

	    ECB.Decryptor = ECB.extend({
	        processBlock: function (words, offset) {
	            this._cipher.decryptBlock(words, offset);
	        }
	    });

	    return ECB;
	}());


	/**
	 * ANSI X.923 padding strategy.
	 */
	CryptoJS.pad.AnsiX923 = {
	    pad: function (data, blockSize) {
	        // Shortcuts
	        var dataSigBytes = data.sigBytes;
	        var blockSizeBytes = blockSize * 4;

	        // Count padding bytes
	        var nPaddingBytes = blockSizeBytes - dataSigBytes % blockSizeBytes;

	        // Compute last byte position
	        var lastBytePos = dataSigBytes + nPaddingBytes - 1;

	        // Pad
	        data.clamp();
	        data.words[lastBytePos >>> 2] |= nPaddingBytes << (24 - (lastBytePos % 4) * 8);
	        data.sigBytes += nPaddingBytes;
	    },

	    unpad: function (data) {
	        // Get number of padding bytes from last byte
	        var nPaddingBytes = data.words[(data.sigBytes - 1) >>> 2] & 0xff;

	        // Remove padding
	        data.sigBytes -= nPaddingBytes;
	    }
	};


	/**
	 * ISO 10126 padding strategy.
	 */
	CryptoJS.pad.Iso10126 = {
	    pad: function (data, blockSize) {
	        // Shortcut
	        var blockSizeBytes = blockSize * 4;

	        // Count padding bytes
	        var nPaddingBytes = blockSizeBytes - data.sigBytes % blockSizeBytes;

	        // Pad
	        data.concat(CryptoJS.lib.WordArray.random(nPaddingBytes - 1)).
	             concat(CryptoJS.lib.WordArray.create([nPaddingBytes << 24], 1));
	    },

	    unpad: function (data) {
	        // Get number of padding bytes from last byte
	        var nPaddingBytes = data.words[(data.sigBytes - 1) >>> 2] & 0xff;

	        // Remove padding
	        data.sigBytes -= nPaddingBytes;
	    }
	};


	/**
	 * ISO/IEC 9797-1 Padding Method 2.
	 */
	CryptoJS.pad.Iso97971 = {
	    pad: function (data, blockSize) {
	        // Add 0x80 byte
	        data.concat(CryptoJS.lib.WordArray.create([0x80000000], 1));

	        // Zero pad the rest
	        CryptoJS.pad.ZeroPadding.pad(data, blockSize);
	    },

	    unpad: function (data) {
	        // Remove zero padding
	        CryptoJS.pad.ZeroPadding.unpad(data);

	        // Remove one more byte -- the 0x80 byte
	        data.sigBytes--;
	    }
	};


	/**
	 * Output Feedback block mode.
	 */
	CryptoJS.mode.OFB = (function () {
	    var OFB = CryptoJS.lib.BlockCipherMode.extend();

	    var Encryptor = OFB.Encryptor = OFB.extend({
	        processBlock: function (words, offset) {
	            // Shortcuts
	            var cipher = this._cipher
	            var blockSize = cipher.blockSize;
	            var iv = this._iv;
	            var keystream = this._keystream;

	            // Generate keystream
	            if (iv) {
	                keystream = this._keystream = iv.slice(0);

	                // Remove IV for subsequent blocks
	                this._iv = undefined;
	            }
	            cipher.encryptBlock(keystream, 0);

	            // Encrypt
	            for (var i = 0; i < blockSize; i++) {
	                words[offset + i] ^= keystream[i];
	            }
	        }
	    });

	    OFB.Decryptor = Encryptor;

	    return OFB;
	}());


	/**
	 * A noop padding strategy.
	 */
	CryptoJS.pad.NoPadding = {
	    pad: function () {
	    },

	    unpad: function () {
	    }
	};


	(function (undefined) {
	    // Shortcuts
	    var C = CryptoJS;
	    var C_lib = C.lib;
	    var CipherParams = C_lib.CipherParams;
	    var C_enc = C.enc;
	    var Hex = C_enc.Hex;
	    var C_format = C.format;

	    var HexFormatter = C_format.Hex = {
	        /**
	         * Converts the ciphertext of a cipher params object to a hexadecimally encoded string.
	         *
	         * @param {CipherParams} cipherParams The cipher params object.
	         *
	         * @return {string} The hexadecimally encoded string.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var hexString = CryptoJS.format.Hex.stringify(cipherParams);
	         */
	        stringify: function (cipherParams) {
	            return cipherParams.ciphertext.toString(Hex);
	        },

	        /**
	         * Converts a hexadecimally encoded ciphertext string to a cipher params object.
	         *
	         * @param {string} input The hexadecimally encoded string.
	         *
	         * @return {CipherParams} The cipher params object.
	         *
	         * @static
	         *
	         * @example
	         *
	         *     var cipherParams = CryptoJS.format.Hex.parse(hexString);
	         */
	        parse: function (input) {
	            var ciphertext = Hex.parse(input);
	            return CipherParams.create({ ciphertext: ciphertext });
	        }
	    };
	}());


	(function () {
	    // Shortcuts
	    var C = CryptoJS;
	    var C_lib = C.lib;
	    var BlockCipher = C_lib.BlockCipher;
	    var C_algo = C.algo;

	    // Lookup tables
	    var SBOX = [];
	    var INV_SBOX = [];
	    var SUB_MIX_0 = [];
	    var SUB_MIX_1 = [];
	    var SUB_MIX_2 = [];
	    var SUB_MIX_3 = [];
	    var INV_SUB_MIX_0 = [];
	    var INV_SUB_MIX_1 = [];
	    var INV_SUB_MIX_2 = [];
	    var INV_SUB_MIX_3 = [];

	    // Compute lookup tables
	    (function () {
	        // Compute double table
	        var d = [];
	        for (var i = 0; i < 256; i++) {
	            if (i < 128) {
	                d[i] = i << 1;
	            } else {
	                d[i] = (i << 1) ^ 0x11b;
	            }
	        }

	        // Walk GF(2^8)
	        var x = 0;
	        var xi = 0;
	        for (var i = 0; i < 256; i++) {
	            // Compute sbox
	            var sx = xi ^ (xi << 1) ^ (xi << 2) ^ (xi << 3) ^ (xi << 4);
	            sx = (sx >>> 8) ^ (sx & 0xff) ^ 0x63;
	            SBOX[x] = sx;
	            INV_SBOX[sx] = x;

	            // Compute multiplication
	            var x2 = d[x];
	            var x4 = d[x2];
	            var x8 = d[x4];

	            // Compute sub bytes, mix columns tables
	            var t = (d[sx] * 0x101) ^ (sx * 0x1010100);
	            SUB_MIX_0[x] = (t << 24) | (t >>> 8);
	            SUB_MIX_1[x] = (t << 16) | (t >>> 16);
	            SUB_MIX_2[x] = (t << 8)  | (t >>> 24);
	            SUB_MIX_3[x] = t;

	            // Compute inv sub bytes, inv mix columns tables
	            var t = (x8 * 0x1010101) ^ (x4 * 0x10001) ^ (x2 * 0x101) ^ (x * 0x1010100);
	            INV_SUB_MIX_0[sx] = (t << 24) | (t >>> 8);
	            INV_SUB_MIX_1[sx] = (t << 16) | (t >>> 16);
	            INV_SUB_MIX_2[sx] = (t << 8)  | (t >>> 24);
	            INV_SUB_MIX_3[sx] = t;

	            // Compute next counter
	            if (!x) {
	                x = xi = 1;
	            } else {
	                x = x2 ^ d[d[d[x8 ^ x2]]];
	                xi ^= d[d[xi]];
	            }
	        }
	    }());

	    // Precomputed Rcon lookup
	    var RCON = [0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36];

	    /**
	     * AES block cipher algorithm.
	     */
	    var AES = C_algo.AES = BlockCipher.extend({
	        _doReset: function () {
	            // Shortcuts
	            var key = this._key;
	            var keyWords = key.words;
	            var keySize = key.sigBytes / 4;

	            // Compute number of rounds
	            var nRounds = this._nRounds = keySize + 6

	            // Compute number of key schedule rows
	            var ksRows = (nRounds + 1) * 4;

	            // Compute key schedule
	            var keySchedule = this._keySchedule = [];
	            for (var ksRow = 0; ksRow < ksRows; ksRow++) {
	                if (ksRow < keySize) {
	                    keySchedule[ksRow] = keyWords[ksRow];
	                } else {
	                    var t = keySchedule[ksRow - 1];

	                    if (!(ksRow % keySize)) {
	                        // Rot word
	                        t = (t << 8) | (t >>> 24);

	                        // Sub word
	                        t = (SBOX[t >>> 24] << 24) | (SBOX[(t >>> 16) & 0xff] << 16) | (SBOX[(t >>> 8) & 0xff] << 8) | SBOX[t & 0xff];

	                        // Mix Rcon
	                        t ^= RCON[(ksRow / keySize) | 0] << 24;
	                    } else if (keySize > 6 && ksRow % keySize == 4) {
	                        // Sub word
	                        t = (SBOX[t >>> 24] << 24) | (SBOX[(t >>> 16) & 0xff] << 16) | (SBOX[(t >>> 8) & 0xff] << 8) | SBOX[t & 0xff];
	                    }

	                    keySchedule[ksRow] = keySchedule[ksRow - keySize] ^ t;
	                }
	            }

	            // Compute inv key schedule
	            var invKeySchedule = this._invKeySchedule = [];
	            for (var invKsRow = 0; invKsRow < ksRows; invKsRow++) {
	                var ksRow = ksRows - invKsRow;

	                if (invKsRow % 4) {
	                    var t = keySchedule[ksRow];
	                } else {
	                    var t = keySchedule[ksRow - 4];
	                }

	                if (invKsRow < 4 || ksRow <= 4) {
	                    invKeySchedule[invKsRow] = t;
	                } else {
	                    invKeySchedule[invKsRow] = INV_SUB_MIX_0[SBOX[t >>> 24]] ^ INV_SUB_MIX_1[SBOX[(t >>> 16) & 0xff]] ^
	                                               INV_SUB_MIX_2[SBOX[(t >>> 8) & 0xff]] ^ INV_SUB_MIX_3[SBOX[t & 0xff]];
	                }
	            }
	        },

	        encryptBlock: function (M, offset) {
	            this._doCryptBlock(M, offset, this._keySchedule, SUB_MIX_0, SUB_MIX_1, SUB_MIX_2, SUB_MIX_3, SBOX);
	        },

	        decryptBlock: function (M, offset) {
	            // Swap 2nd and 4th rows
	            var t = M[offset + 1];
	            M[offset + 1] = M[offset + 3];
	            M[offset + 3] = t;

	            this._doCryptBlock(M, offset, this._invKeySchedule, INV_SUB_MIX_0, INV_SUB_MIX_1, INV_SUB_MIX_2, INV_SUB_MIX_3, INV_SBOX);

	            // Inv swap 2nd and 4th rows
	            var t = M[offset + 1];
	            M[offset + 1] = M[offset + 3];
	            M[offset + 3] = t;
	        },

	        _doCryptBlock: function (M, offset, keySchedule, SUB_MIX_0, SUB_MIX_1, SUB_MIX_2, SUB_MIX_3, SBOX) {
	            // Shortcut
	            var nRounds = this._nRounds;

	            // Get input, add round key
	            var s0 = M[offset]     ^ keySchedule[0];
	            var s1 = M[offset + 1] ^ keySchedule[1];
	            var s2 = M[offset + 2] ^ keySchedule[2];
	            var s3 = M[offset + 3] ^ keySchedule[3];

	            // Key schedule row counter
	            var ksRow = 4;

	            // Rounds
	            for (var round = 1; round < nRounds; round++) {
	                // Shift rows, sub bytes, mix columns, add round key
	                var t0 = SUB_MIX_0[s0 >>> 24] ^ SUB_MIX_1[(s1 >>> 16) & 0xff] ^ SUB_MIX_2[(s2 >>> 8) & 0xff] ^ SUB_MIX_3[s3 & 0xff] ^ keySchedule[ksRow++];
	                var t1 = SUB_MIX_0[s1 >>> 24] ^ SUB_MIX_1[(s2 >>> 16) & 0xff] ^ SUB_MIX_2[(s3 >>> 8) & 0xff] ^ SUB_MIX_3[s0 & 0xff] ^ keySchedule[ksRow++];
	                var t2 = SUB_MIX_0[s2 >>> 24] ^ SUB_MIX_1[(s3 >>> 16) & 0xff] ^ SUB_MIX_2[(s0 >>> 8) & 0xff] ^ SUB_MIX_3[s1 & 0xff] ^ keySchedule[ksRow++];
	                var t3 = SUB_MIX_0[s3 >>> 24] ^ SUB_MIX_1[(s0 >>> 16) & 0xff] ^ SUB_MIX_2[(s1 >>> 8) & 0xff] ^ SUB_MIX_3[s2 & 0xff] ^ keySchedule[ksRow++];

	                // Update state
	                s0 = t0;
	                s1 = t1;
	                s2 = t2;
	                s3 = t3;
	            }

	            // Shift rows, sub bytes, add round key
	            var t0 = ((SBOX[s0 >>> 24] << 24) | (SBOX[(s1 >>> 16) & 0xff] << 16) | (SBOX[(s2 >>> 8) & 0xff] << 8) | SBOX[s3 & 0xff]) ^ keySchedule[ksRow++];
	            var t1 = ((SBOX[s1 >>> 24] << 24) | (SBOX[(s2 >>> 16) & 0xff] << 16) | (SBOX[(s3 >>> 8) & 0xff] << 8) | SBOX[s0 & 0xff]) ^ keySchedule[ksRow++];
	            var t2 = ((SBOX[s2 >>> 24] << 24) | (SBOX[(s3 >>> 16) & 0xff] << 16) | (SBOX[(s0 >>> 8) & 0xff] << 8) | SBOX[s1 & 0xff]) ^ keySchedule[ksRow++];
	            var t3 = ((SBOX[s3 >>> 24] << 24) | (SBOX[(s0 >>> 16) & 0xff] << 16) | (SBOX[(s1 >>> 8) & 0xff] << 8) | SBOX[s2 & 0xff]) ^ keySchedule[ksRow++];

	            // Set output
	            M[offset]     = t0;
	            M[offset + 1] = t1;
	            M[offset + 2] = t2;
	            M[offset + 3] = t3;
	        },

	        keySize: 256/32
	    });

	    /**
	     * Shortcut functions to the cipher's object interface.
	     *
	     * @example
	     *
	     *     var ciphertext = CryptoJS.AES.encrypt(message, key, cfg);
	     *     var plaintext  = CryptoJS.AES.decrypt(ciphertext, key, cfg);
	     */
	    C.AES = BlockCipher._createHelper(AES);
	}());
		(function (Math) {
	    // Shortcuts
	    var C = CryptoJS;
	    var C_lib = C.lib;
	    var WordArray = C_lib.WordArray;
	    var Hasher = C_lib.Hasher;
	    var C_algo = C.algo;

	    // Constants table
	    var T = [];

	    // Compute constants
	    (function () {
	        for (var i = 0; i < 64; i++) {
	            T[i] = (Math.abs(Math.sin(i + 1)) * 0x100000000) | 0;
	        }
	    }());

	    /**
	     * MD5 hash algorithm.
	     */
	    var MD5 = C_algo.MD5 = Hasher.extend({
	        _doReset: function () {
	            this._hash = new WordArray.init([
	                0x67452301, 0xefcdab89,
	                0x98badcfe, 0x10325476
	            ]);
	        },

	        _doProcessBlock: function (M, offset) {
	            // Swap endian
	            for (var i = 0; i < 16; i++) {
	                // Shortcuts
	                var offset_i = offset + i;
	                var M_offset_i = M[offset_i];

	                M[offset_i] = (
	                    (((M_offset_i << 8)  | (M_offset_i >>> 24)) & 0x00ff00ff) |
	                    (((M_offset_i << 24) | (M_offset_i >>> 8))  & 0xff00ff00)
	                );
	            }

	            // Shortcuts
	            var H = this._hash.words;

	            var M_offset_0  = M[offset + 0];
	            var M_offset_1  = M[offset + 1];
	            var M_offset_2  = M[offset + 2];
	            var M_offset_3  = M[offset + 3];
	            var M_offset_4  = M[offset + 4];
	            var M_offset_5  = M[offset + 5];
	            var M_offset_6  = M[offset + 6];
	            var M_offset_7  = M[offset + 7];
	            var M_offset_8  = M[offset + 8];
	            var M_offset_9  = M[offset + 9];
	            var M_offset_10 = M[offset + 10];
	            var M_offset_11 = M[offset + 11];
	            var M_offset_12 = M[offset + 12];
	            var M_offset_13 = M[offset + 13];
	            var M_offset_14 = M[offset + 14];
	            var M_offset_15 = M[offset + 15];

	            // Working varialbes
	            var a = H[0];
	            var b = H[1];
	            var c = H[2];
	            var d = H[3];

	            // Computation
	            a = FF(a, b, c, d, M_offset_0,  7,  T[0]);
	            d = FF(d, a, b, c, M_offset_1,  12, T[1]);
	            c = FF(c, d, a, b, M_offset_2,  17, T[2]);
	            b = FF(b, c, d, a, M_offset_3,  22, T[3]);
	            a = FF(a, b, c, d, M_offset_4,  7,  T[4]);
	            d = FF(d, a, b, c, M_offset_5,  12, T[5]);
	            c = FF(c, d, a, b, M_offset_6,  17, T[6]);
	            b = FF(b, c, d, a, M_offset_7,  22, T[7]);
	            a = FF(a, b, c, d, M_offset_8,  7,  T[8]);
	            d = FF(d, a, b, c, M_offset_9,  12, T[9]);
	            c = FF(c, d, a, b, M_offset_10, 17, T[10]);
	            b = FF(b, c, d, a, M_offset_11, 22, T[11]);
	            a = FF(a, b, c, d, M_offset_12, 7,  T[12]);
	            d = FF(d, a, b, c, M_offset_13, 12, T[13]);
	            c = FF(c, d, a, b, M_offset_14, 17, T[14]);
	            b = FF(b, c, d, a, M_offset_15, 22, T[15]);

	            a = GG(a, b, c, d, M_offset_1,  5,  T[16]);
	            d = GG(d, a, b, c, M_offset_6,  9,  T[17]);
	            c = GG(c, d, a, b, M_offset_11, 14, T[18]);
	            b = GG(b, c, d, a, M_offset_0,  20, T[19]);
	            a = GG(a, b, c, d, M_offset_5,  5,  T[20]);
	            d = GG(d, a, b, c, M_offset_10, 9,  T[21]);
	            c = GG(c, d, a, b, M_offset_15, 14, T[22]);
	            b = GG(b, c, d, a, M_offset_4,  20, T[23]);
	            a = GG(a, b, c, d, M_offset_9,  5,  T[24]);
	            d = GG(d, a, b, c, M_offset_14, 9,  T[25]);
	            c = GG(c, d, a, b, M_offset_3,  14, T[26]);
	            b = GG(b, c, d, a, M_offset_8,  20, T[27]);
	            a = GG(a, b, c, d, M_offset_13, 5,  T[28]);
	            d = GG(d, a, b, c, M_offset_2,  9,  T[29]);
	            c = GG(c, d, a, b, M_offset_7,  14, T[30]);
	            b = GG(b, c, d, a, M_offset_12, 20, T[31]);

	            a = HH(a, b, c, d, M_offset_5,  4,  T[32]);
	            d = HH(d, a, b, c, M_offset_8,  11, T[33]);
	            c = HH(c, d, a, b, M_offset_11, 16, T[34]);
	            b = HH(b, c, d, a, M_offset_14, 23, T[35]);
	            a = HH(a, b, c, d, M_offset_1,  4,  T[36]);
	            d = HH(d, a, b, c, M_offset_4,  11, T[37]);
	            c = HH(c, d, a, b, M_offset_7,  16, T[38]);
	            b = HH(b, c, d, a, M_offset_10, 23, T[39]);
	            a = HH(a, b, c, d, M_offset_13, 4,  T[40]);
	            d = HH(d, a, b, c, M_offset_0,  11, T[41]);
	            c = HH(c, d, a, b, M_offset_3,  16, T[42]);
	            b = HH(b, c, d, a, M_offset_6,  23, T[43]);
	            a = HH(a, b, c, d, M_offset_9,  4,  T[44]);
	            d = HH(d, a, b, c, M_offset_12, 11, T[45]);
	            c = HH(c, d, a, b, M_offset_15, 16, T[46]);
	            b = HH(b, c, d, a, M_offset_2,  23, T[47]);

	            a = II(a, b, c, d, M_offset_0,  6,  T[48]);
	            d = II(d, a, b, c, M_offset_7,  10, T[49]);
	            c = II(c, d, a, b, M_offset_14, 15, T[50]);
	            b = II(b, c, d, a, M_offset_5,  21, T[51]);
	            a = II(a, b, c, d, M_offset_12, 6,  T[52]);
	            d = II(d, a, b, c, M_offset_3,  10, T[53]);
	            c = II(c, d, a, b, M_offset_10, 15, T[54]);
	            b = II(b, c, d, a, M_offset_1,  21, T[55]);
	            a = II(a, b, c, d, M_offset_8,  6,  T[56]);
	            d = II(d, a, b, c, M_offset_15, 10, T[57]);
	            c = II(c, d, a, b, M_offset_6,  15, T[58]);
	            b = II(b, c, d, a, M_offset_13, 21, T[59]);
	            a = II(a, b, c, d, M_offset_4,  6,  T[60]);
	            d = II(d, a, b, c, M_offset_11, 10, T[61]);
	            c = II(c, d, a, b, M_offset_2,  15, T[62]);
	            b = II(b, c, d, a, M_offset_9,  21, T[63]);

	            // Intermediate hash value
	            H[0] = (H[0] + a) | 0;
	            H[1] = (H[1] + b) | 0;
	            H[2] = (H[2] + c) | 0;
	            H[3] = (H[3] + d) | 0;
	        },

	        _doFinalize: function () {
	            // Shortcuts
	            var data = this._data;
	            var dataWords = data.words;

	            var nBitsTotal = this._nDataBytes * 8;
	            var nBitsLeft = data.sigBytes * 8;

	            // Add padding
	            dataWords[nBitsLeft >>> 5] |= 0x80 << (24 - nBitsLeft % 32);

	            var nBitsTotalH = Math.floor(nBitsTotal / 0x100000000);
	            var nBitsTotalL = nBitsTotal;
	            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 15] = (
	                (((nBitsTotalH << 8)  | (nBitsTotalH >>> 24)) & 0x00ff00ff) |
	                (((nBitsTotalH << 24) | (nBitsTotalH >>> 8))  & 0xff00ff00)
	            );
	            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 14] = (
	                (((nBitsTotalL << 8)  | (nBitsTotalL >>> 24)) & 0x00ff00ff) |
	                (((nBitsTotalL << 24) | (nBitsTotalL >>> 8))  & 0xff00ff00)
	            );

	            data.sigBytes = (dataWords.length + 1) * 4;

	            // Hash final blocks
	            this._process();

	            // Shortcuts
	            var hash = this._hash;
	            var H = hash.words;

	            // Swap endian
	            for (var i = 0; i < 4; i++) {
	                // Shortcut
	                var H_i = H[i];

	                H[i] = (((H_i << 8)  | (H_i >>> 24)) & 0x00ff00ff) |
	                       (((H_i << 24) | (H_i >>> 8))  & 0xff00ff00);
	            }

	            // Return final computed hash
	            return hash;
	        },

	        clone: function () {
	            var clone = Hasher.clone.call(this);
	            clone._hash = this._hash.clone();

	            return clone;
	        }
	    });

	    function FF(a, b, c, d, x, s, t) {
	        var n = a + ((b & c) | (~b & d)) + x + t;
	        return ((n << s) | (n >>> (32 - s))) + b;
	    }

	    function GG(a, b, c, d, x, s, t) {
	        var n = a + ((b & d) | (c & ~d)) + x + t;
	        return ((n << s) | (n >>> (32 - s))) + b;
	    }

	    function HH(a, b, c, d, x, s, t) {
	        var n = a + (b ^ c ^ d) + x + t;
	        return ((n << s) | (n >>> (32 - s))) + b;
	    }

	    function II(a, b, c, d, x, s, t) {
	        var n = a + (c ^ (b | ~d)) + x + t;
	        return ((n << s) | (n >>> (32 - s))) + b;
	    }

	    /**
	     * Shortcut function to the hasher's object interface.
	     *
	     * @param {WordArray|string} message The message to hash.
	     *
	     * @return {WordArray} The hash.
	     *
	     * @static
	     *
	     * @example
	     *
	     *     var hash = CryptoJS.MD5('message');
	     *     var hash = CryptoJS.MD5(wordArray);
	     */
	    C.MD5 = Hasher._createHelper(MD5);

	    /**
	     * Shortcut function to the HMAC's object interface.
	     *
	     * @param {WordArray|string} message The message to hash.
	     * @param {WordArray|string} key The secret key.
	     *
	     * @return {WordArray} The HMAC.
	     *
	     * @static
	     *
	     * @example
	     *
	     *     var hmac = CryptoJS.HmacMD5(message, key);
	     */
	    C.HmacMD5 = Hasher._createHmacHelper(MD5);
	}(Math));
	/** @preserve
	 * Counter block mode compatible with  Dr Brian Gladman fileenc.c
	 * derived from CryptoJS.mode.CTR
	 * Jan Hruby jhruby.web@gmail.com
	 */
	CryptoJS.mode.CTRGladman = (function () {
	    var CTRGladman = CryptoJS.lib.BlockCipherMode.extend();

		function incWord(word)
		{
			if (((word >> 24) & 0xff) === 0xff) { //overflow
			var b1 = (word >> 16)&0xff;
			var b2 = (word >> 8)&0xff;
			var b3 = word & 0xff;

			if (b1 === 0xff) // overflow b1
			{
			b1 = 0;
			if (b2 === 0xff)
			{
				b2 = 0;
				if (b3 === 0xff)
				{
					b3 = 0;
				}
				else
				{
					++b3;
				}
			}
			else
			{
				++b2;
			}
			}
			else
			{
			++b1;
			}

			word = 0;
			word += (b1 << 16);
			word += (b2 << 8);
			word += b3;
			}
			else
			{
			word += (0x01 << 24);
			}
			return word;
		}

		function incCounter(counter)
		{
			if ((counter[0] = incWord(counter[0])) === 0)
			{
				// encr_data in fileenc.c from  Dr Brian Gladman's counts only with DWORD j < 8
				counter[1] = incWord(counter[1]);
			}
			return counter;
		}

	    var Encryptor = CTRGladman.Encryptor = CTRGladman.extend({
	        processBlock: function (words, offset) {
	            // Shortcuts
	            var cipher = this._cipher
	            var blockSize = cipher.blockSize;
	            var iv = this._iv;
	            var counter = this._counter;

	            // Generate keystream
	            if (iv) {
	                counter = this._counter = iv.slice(0);

	                // Remove IV for subsequent blocks
	                this._iv = undefined;
	            }

				incCounter(counter);

				var keystream = counter.slice(0);
	            cipher.encryptBlock(keystream, 0);

	            // Encrypt
	            for (var i = 0; i < blockSize; i++) {
	                words[offset + i] ^= keystream[i];
	            }
	        }
	    });

	    CTRGladman.Decryptor = Encryptor;

	    return CTRGladman;
	}());




	(function () {
	    // Shortcuts
	    var C = CryptoJS;
	    var C_lib = C.lib;
	    var StreamCipher = C_lib.StreamCipher;
	    var C_algo = C.algo;

	    // Reusable objects
	    var S  = [];
	    var C_ = [];
	    var G  = [];

	    /**
	     * Rabbit stream cipher algorithm
	     */
	    var Rabbit = C_algo.Rabbit = StreamCipher.extend({
	        _doReset: function () {
	            // Shortcuts
	            var K = this._key.words;
	            var iv = this.cfg.iv;

	            // Swap endian
	            for (var i = 0; i < 4; i++) {
	                K[i] = (((K[i] << 8)  | (K[i] >>> 24)) & 0x00ff00ff) |
	                       (((K[i] << 24) | (K[i] >>> 8))  & 0xff00ff00);
	            }

	            // Generate initial state values
	            var X = this._X = [
	                K[0], (K[3] << 16) | (K[2] >>> 16),
	                K[1], (K[0] << 16) | (K[3] >>> 16),
	                K[2], (K[1] << 16) | (K[0] >>> 16),
	                K[3], (K[2] << 16) | (K[1] >>> 16)
	            ];

	            // Generate initial counter values
	            var C = this._C = [
	                (K[2] << 16) | (K[2] >>> 16), (K[0] & 0xffff0000) | (K[1] & 0x0000ffff),
	                (K[3] << 16) | (K[3] >>> 16), (K[1] & 0xffff0000) | (K[2] & 0x0000ffff),
	                (K[0] << 16) | (K[0] >>> 16), (K[2] & 0xffff0000) | (K[3] & 0x0000ffff),
	                (K[1] << 16) | (K[1] >>> 16), (K[3] & 0xffff0000) | (K[0] & 0x0000ffff)
	            ];

	            // Carry bit
	            this._b = 0;

	            // Iterate the system four times
	            for (var i = 0; i < 4; i++) {
	                nextState.call(this);
	            }

	            // Modify the counters
	            for (var i = 0; i < 8; i++) {
	                C[i] ^= X[(i + 4) & 7];
	            }

	            // IV setup
	            if (iv) {
	                // Shortcuts
	                var IV = iv.words;
	                var IV_0 = IV[0];
	                var IV_1 = IV[1];

	                // Generate four subvectors
	                var i0 = (((IV_0 << 8) | (IV_0 >>> 24)) & 0x00ff00ff) | (((IV_0 << 24) | (IV_0 >>> 8)) & 0xff00ff00);
	                var i2 = (((IV_1 << 8) | (IV_1 >>> 24)) & 0x00ff00ff) | (((IV_1 << 24) | (IV_1 >>> 8)) & 0xff00ff00);
	                var i1 = (i0 >>> 16) | (i2 & 0xffff0000);
	                var i3 = (i2 << 16)  | (i0 & 0x0000ffff);

	                // Modify counter values
	                C[0] ^= i0;
	                C[1] ^= i1;
	                C[2] ^= i2;
	                C[3] ^= i3;
	                C[4] ^= i0;
	                C[5] ^= i1;
	                C[6] ^= i2;
	                C[7] ^= i3;

	                // Iterate the system four times
	                for (var i = 0; i < 4; i++) {
	                    nextState.call(this);
	                }
	            }
	        },

	        _doProcessBlock: function (M, offset) {
	            // Shortcut
	            var X = this._X;

	            // Iterate the system
	            nextState.call(this);

	            // Generate four keystream words
	            S[0] = X[0] ^ (X[5] >>> 16) ^ (X[3] << 16);
	            S[1] = X[2] ^ (X[7] >>> 16) ^ (X[5] << 16);
	            S[2] = X[4] ^ (X[1] >>> 16) ^ (X[7] << 16);
	            S[3] = X[6] ^ (X[3] >>> 16) ^ (X[1] << 16);

	            for (var i = 0; i < 4; i++) {
	                // Swap endian
	                S[i] = (((S[i] << 8)  | (S[i] >>> 24)) & 0x00ff00ff) |
	                       (((S[i] << 24) | (S[i] >>> 8))  & 0xff00ff00);

	                // Encrypt
	                M[offset + i] ^= S[i];
	            }
	        },

	        blockSize: 128/32,

	        ivSize: 64/32
	    });

	    function nextState() {
	        // Shortcuts
	        var X = this._X;
	        var C = this._C;

	        // Save old counter values
	        for (var i = 0; i < 8; i++) {
	            C_[i] = C[i];
	        }

	        // Calculate new counter values
	        C[0] = (C[0] + 0x4d34d34d + this._b) | 0;
	        C[1] = (C[1] + 0xd34d34d3 + ((C[0] >>> 0) < (C_[0] >>> 0) ? 1 : 0)) | 0;
	        C[2] = (C[2] + 0x34d34d34 + ((C[1] >>> 0) < (C_[1] >>> 0) ? 1 : 0)) | 0;
	        C[3] = (C[3] + 0x4d34d34d + ((C[2] >>> 0) < (C_[2] >>> 0) ? 1 : 0)) | 0;
	        C[4] = (C[4] + 0xd34d34d3 + ((C[3] >>> 0) < (C_[3] >>> 0) ? 1 : 0)) | 0;
	        C[5] = (C[5] + 0x34d34d34 + ((C[4] >>> 0) < (C_[4] >>> 0) ? 1 : 0)) | 0;
	        C[6] = (C[6] + 0x4d34d34d + ((C[5] >>> 0) < (C_[5] >>> 0) ? 1 : 0)) | 0;
	        C[7] = (C[7] + 0xd34d34d3 + ((C[6] >>> 0) < (C_[6] >>> 0) ? 1 : 0)) | 0;
	        this._b = (C[7] >>> 0) < (C_[7] >>> 0) ? 1 : 0;

	        // Calculate the g-values
	        for (var i = 0; i < 8; i++) {
	            var gx = X[i] + C[i];

	            // Construct high and low argument for squaring
	            var ga = gx & 0xffff;
	            var gb = gx >>> 16;

	            // Calculate high and low result of squaring
	            var gh = ((((ga * ga) >>> 17) + ga * gb) >>> 15) + gb * gb;
	            var gl = (((gx & 0xffff0000) * gx) | 0) + (((gx & 0x0000ffff) * gx) | 0);

	            // High XOR low
	            G[i] = gh ^ gl;
	        }

	        // Calculate new state values
	        X[0] = (G[0] + ((G[7] << 16) | (G[7] >>> 16)) + ((G[6] << 16) | (G[6] >>> 16))) | 0;
	        X[1] = (G[1] + ((G[0] << 8)  | (G[0] >>> 24)) + G[7]) | 0;
	        X[2] = (G[2] + ((G[1] << 16) | (G[1] >>> 16)) + ((G[0] << 16) | (G[0] >>> 16))) | 0;
	        X[3] = (G[3] + ((G[2] << 8)  | (G[2] >>> 24)) + G[1]) | 0;
	        X[4] = (G[4] + ((G[3] << 16) | (G[3] >>> 16)) + ((G[2] << 16) | (G[2] >>> 16))) | 0;
	        X[5] = (G[5] + ((G[4] << 8)  | (G[4] >>> 24)) + G[3]) | 0;
	        X[6] = (G[6] + ((G[5] << 16) | (G[5] >>> 16)) + ((G[4] << 16) | (G[4] >>> 16))) | 0;
	        X[7] = (G[7] + ((G[6] << 8)  | (G[6] >>> 24)) + G[5]) | 0;
	    }

	    /**
	     * Shortcut functions to the cipher's object interface.
	     *
	     * @example
	     *
	     *     var ciphertext = CryptoJS.Rabbit.encrypt(message, key, cfg);
	     *     var plaintext  = CryptoJS.Rabbit.decrypt(ciphertext, key, cfg);
	     */
	    C.Rabbit = StreamCipher._createHelper(Rabbit);
	}());


	/**
	 * Counter block mode.
	 */
	CryptoJS.mode.CTR = (function () {
	    var CTR = CryptoJS.lib.BlockCipherMode.extend();

	    var Encryptor = CTR.Encryptor = CTR.extend({
	        processBlock: function (words, offset) {
	            // Shortcuts
	            var cipher = this._cipher
	            var blockSize = cipher.blockSize;
	            var iv = this._iv;
	            var counter = this._counter;

	            // Generate keystream
	            if (iv) {
	                counter = this._counter = iv.slice(0);

	                // Remove IV for subsequent blocks
	                this._iv = undefined;
	            }
	            var keystream = counter.slice(0);
	            cipher.encryptBlock(keystream, 0);

	            // Increment counter
	            counter[blockSize - 1] = (counter[blockSize - 1] + 1) | 0

	            // Encrypt
	            for (var i = 0; i < blockSize; i++) {
	                words[offset + i] ^= keystream[i];
	            }
	        }
	    });

	    CTR.Decryptor = Encryptor;

	    return CTR;
	}());


	(function () {
	    // Shortcuts
	    var C = CryptoJS;
	    var C_lib = C.lib;
	    var StreamCipher = C_lib.StreamCipher;
	    var C_algo = C.algo;

	    // Reusable objects
	    var S  = [];
	    var C_ = [];
	    var G  = [];

	    /**
	     * Rabbit stream cipher algorithm.
	     *
	     * This is a legacy version that neglected to convert the key to little-endian.
	     * This error doesn't affect the cipher's security,
	     * but it does affect its compatibility with other implementations.
	     */
	    var RabbitLegacy = C_algo.RabbitLegacy = StreamCipher.extend({
	        _doReset: function () {
	            // Shortcuts
	            var K = this._key.words;
	            var iv = this.cfg.iv;

	            // Generate initial state values
	            var X = this._X = [
	                K[0], (K[3] << 16) | (K[2] >>> 16),
	                K[1], (K[0] << 16) | (K[3] >>> 16),
	                K[2], (K[1] << 16) | (K[0] >>> 16),
	                K[3], (K[2] << 16) | (K[1] >>> 16)
	            ];

	            // Generate initial counter values
	            var C = this._C = [
	                (K[2] << 16) | (K[2] >>> 16), (K[0] & 0xffff0000) | (K[1] & 0x0000ffff),
	                (K[3] << 16) | (K[3] >>> 16), (K[1] & 0xffff0000) | (K[2] & 0x0000ffff),
	                (K[0] << 16) | (K[0] >>> 16), (K[2] & 0xffff0000) | (K[3] & 0x0000ffff),
	                (K[1] << 16) | (K[1] >>> 16), (K[3] & 0xffff0000) | (K[0] & 0x0000ffff)
	            ];

	            // Carry bit
	            this._b = 0;

	            // Iterate the system four times
	            for (var i = 0; i < 4; i++) {
	                nextState.call(this);
	            }

	            // Modify the counters
	            for (var i = 0; i < 8; i++) {
	                C[i] ^= X[(i + 4) & 7];
	            }

	            // IV setup
	            if (iv) {
	                // Shortcuts
	                var IV = iv.words;
	                var IV_0 = IV[0];
	                var IV_1 = IV[1];

	                // Generate four subvectors
	                var i0 = (((IV_0 << 8) | (IV_0 >>> 24)) & 0x00ff00ff) | (((IV_0 << 24) | (IV_0 >>> 8)) & 0xff00ff00);
	                var i2 = (((IV_1 << 8) | (IV_1 >>> 24)) & 0x00ff00ff) | (((IV_1 << 24) | (IV_1 >>> 8)) & 0xff00ff00);
	                var i1 = (i0 >>> 16) | (i2 & 0xffff0000);
	                var i3 = (i2 << 16)  | (i0 & 0x0000ffff);

	                // Modify counter values
	                C[0] ^= i0;
	                C[1] ^= i1;
	                C[2] ^= i2;
	                C[3] ^= i3;
	                C[4] ^= i0;
	                C[5] ^= i1;
	                C[6] ^= i2;
	                C[7] ^= i3;

	                // Iterate the system four times
	                for (var i = 0; i < 4; i++) {
	                    nextState.call(this);
	                }
	            }
	        },

	        _doProcessBlock: function (M, offset) {
	            // Shortcut
	            var X = this._X;

	            // Iterate the system
	            nextState.call(this);

	            // Generate four keystream words
	            S[0] = X[0] ^ (X[5] >>> 16) ^ (X[3] << 16);
	            S[1] = X[2] ^ (X[7] >>> 16) ^ (X[5] << 16);
	            S[2] = X[4] ^ (X[1] >>> 16) ^ (X[7] << 16);
	            S[3] = X[6] ^ (X[3] >>> 16) ^ (X[1] << 16);

	            for (var i = 0; i < 4; i++) {
	                // Swap endian
	                S[i] = (((S[i] << 8)  | (S[i] >>> 24)) & 0x00ff00ff) |
	                       (((S[i] << 24) | (S[i] >>> 8))  & 0xff00ff00);

	                // Encrypt
	                M[offset + i] ^= S[i];
	            }
	        },

	        blockSize: 128/32,

	        ivSize: 64/32
	    });

	    function nextState() {
	        // Shortcuts
	        var X = this._X;
	        var C = this._C;

	        // Save old counter values
	        for (var i = 0; i < 8; i++) {
	            C_[i] = C[i];
	        }

	        // Calculate new counter values
	        C[0] = (C[0] + 0x4d34d34d + this._b) | 0;
	        C[1] = (C[1] + 0xd34d34d3 + ((C[0] >>> 0) < (C_[0] >>> 0) ? 1 : 0)) | 0;
	        C[2] = (C[2] + 0x34d34d34 + ((C[1] >>> 0) < (C_[1] >>> 0) ? 1 : 0)) | 0;
	        C[3] = (C[3] + 0x4d34d34d + ((C[2] >>> 0) < (C_[2] >>> 0) ? 1 : 0)) | 0;
	        C[4] = (C[4] + 0xd34d34d3 + ((C[3] >>> 0) < (C_[3] >>> 0) ? 1 : 0)) | 0;
	        C[5] = (C[5] + 0x34d34d34 + ((C[4] >>> 0) < (C_[4] >>> 0) ? 1 : 0)) | 0;
	        C[6] = (C[6] + 0x4d34d34d + ((C[5] >>> 0) < (C_[5] >>> 0) ? 1 : 0)) | 0;
	        C[7] = (C[7] + 0xd34d34d3 + ((C[6] >>> 0) < (C_[6] >>> 0) ? 1 : 0)) | 0;
	        this._b = (C[7] >>> 0) < (C_[7] >>> 0) ? 1 : 0;

	        // Calculate the g-values
	        for (var i = 0; i < 8; i++) {
	            var gx = X[i] + C[i];

	            // Construct high and low argument for squaring
	            var ga = gx & 0xffff;
	            var gb = gx >>> 16;

	            // Calculate high and low result of squaring
	            var gh = ((((ga * ga) >>> 17) + ga * gb) >>> 15) + gb * gb;
	            var gl = (((gx & 0xffff0000) * gx) | 0) + (((gx & 0x0000ffff) * gx) | 0);

	            // High XOR low
	            G[i] = gh ^ gl;
	        }

	        // Calculate new state values
	        X[0] = (G[0] + ((G[7] << 16) | (G[7] >>> 16)) + ((G[6] << 16) | (G[6] >>> 16))) | 0;
	        X[1] = (G[1] + ((G[0] << 8)  | (G[0] >>> 24)) + G[7]) | 0;
	        X[2] = (G[2] + ((G[1] << 16) | (G[1] >>> 16)) + ((G[0] << 16) | (G[0] >>> 16))) | 0;
	        X[3] = (G[3] + ((G[2] << 8)  | (G[2] >>> 24)) + G[1]) | 0;
	        X[4] = (G[4] + ((G[3] << 16) | (G[3] >>> 16)) + ((G[2] << 16) | (G[2] >>> 16))) | 0;
	        X[5] = (G[5] + ((G[4] << 8)  | (G[4] >>> 24)) + G[3]) | 0;
	        X[6] = (G[6] + ((G[5] << 16) | (G[5] >>> 16)) + ((G[4] << 16) | (G[4] >>> 16))) | 0;
	        X[7] = (G[7] + ((G[6] << 8)  | (G[6] >>> 24)) + G[5]) | 0;
	    }

	    /**
	     * Shortcut functions to the cipher's object interface.
	     *
	     * @example
	     *
	     *     var ciphertext = CryptoJS.RabbitLegacy.encrypt(message, key, cfg);
	     *     var plaintext  = CryptoJS.RabbitLegacy.decrypt(ciphertext, key, cfg);
	     */
	    C.RabbitLegacy = StreamCipher._createHelper(RabbitLegacy);
	}());


	/**
	 * Zero padding strategy.
	 */
	CryptoJS.pad.ZeroPadding = {
	    pad: function (data, blockSize) {
	        // Shortcut
	        var blockSizeBytes = blockSize * 4;

	        // Pad
	        data.clamp();
	        data.sigBytes += blockSizeBytes - ((data.sigBytes % blockSizeBytes) || blockSizeBytes);
	    },

	    unpad: function (data) {
	        // Shortcut
	        var dataWords = data.words;

	        // Unpad
	        var i = data.sigBytes - 1;
	        while (!((dataWords[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff)) {
	            i--;
	        }
	        data.sigBytes = i + 1;
	    }
	};
	return CryptoJS;
}));

/***iner_sm3.js***/
(function() {
var C = CryptoJS;
var C_lib = C.lib;
var WordArray = C_lib.WordArray;
var Hasher = C_lib.Hasher;
var C_algo = C.algo;
var W = [];
var INER_SM3 = C_algo.INER_SM3 = Hasher.extend({
_doReset: function() {
this._hash = new WordArray.init([0x7380166f, 0x4914b2b9, 0x172442d7, 0xda8a0600, 0xa96f30bc, 0x163138aa, 0xe38dee4d, 0xb0fb0e4e])
},
_doProcessBlock: function(M, offset) {
var H = this._hash.words;
var a = H[0];
var b = H[1];
var c = H[2];
var d = H[3];
var e = H[4];
for (var i = 0; i < 80; i++) {
if (i < 16) {
W[i] = M[offset + i] | 0
} else {
var n = W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16];
W[i] = (n << 1) | (n >>> 31)
}
var t = ((a << 5) | (a >>> 27)) + e + W[i];
if (i < 20) {
t += ((b & c) | (~b & d)) + 0x5a827999
} else if (i < 40) {
t += (b ^ c ^ d) + 0x6ed9eba1
} else if (i < 60) {
t += ((b & c) | (b & d) | (c & d)) - 0x70e44324
} else {
t += (b ^ c ^ d) - 0x359d3e2a
}
e = d;
d = c;
c = (b << 30) | (b >>> 2);
b = a;
a = t
}
H[0] = (H[0] + a) | 0;
H[1] = (H[1] + b) | 0;
H[2] = (H[2] + c) | 0;
H[3] = (H[3] + d) | 0;
H[4] = (H[4] + e) | 0
},
_doFinalize: function() {
var data = this._data;
var dataWords = data.words;
var nBitsTotal = this._nDataBytes * 8;
var nBitsLeft = data.sigBytes * 8;
dataWords[nBitsLeft >>> 5] |= 0x80 << (24 - nBitsLeft % 32);
dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 14] = Math.floor(nBitsTotal / 0x100000000);
dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 15] = nBitsTotal;
data.sigBytes = dataWords.length * 4;
this._process();
return this._hash
},
clone: function() {
var clone = Hasher.clone.call(this);
clone._hash = this._hash.clone();
return clone
}
});
C.INER_SM3 = Hasher._createHelper(INER_SM3);
C.HmacINER_SM3 = Hasher._createHmacHelper(INER_SM3)
}());
function INER_SM3Digest() {
this.BYTE_LENGTH = 64;
this.xBuf = new Array();
this.xBufOff = 0;
this.byteCount = 0;
this.DIGEST_LENGTH = 32;
this.v0 = [0x7380166f, 0x4914b2b9, 0x172442d7, 0xda8a0600, 0xa96f30bc, 0x163138aa, 0xe38dee4d, 0xb0fb0e4e];
this.v0 = [0x7380166f, 0x4914b2b9, 0x172442d7, -628488704, -1452330820, 0x163138aa, -477237683, -1325724082];
this.v = new Array(8);
this.v_ = new Array(8);
this.X0 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
this.X = new Array(68);
this.xOff = 0;
this.T_00_15 = 0x79cc4519;
this.T_16_63 = 0x7a879d8a;
if (arguments.length > 0) {
this.InitDigest(arguments[0])
} else {
this.Init()
}
}
INER_SM3Digest.prototype = {
Init: function() {
this.xBuf = new Array(4);
this.Reset()
},
InitDigest: function(t) {
this.xBuf = new Array(t.xBuf.length);
Array.Copy(t.xBuf, 0, this.xBuf, 0, t.xBuf.length);
this.xBufOff = t.xBufOff;
this.byteCount = t.byteCount;
Array.Copy(t.X, 0, this.X, 0, t.X.length);
this.xOff = t.xOff;
Array.Copy(t.v, 0, this.v, 0, t.v.length)
},
GetDigestSize: function() {
return this.DIGEST_LENGTH
},
Reset: function() {
this.byteCount = 0;
this.xBufOff = 0;
Array.Clear(this.xBuf, 0, this.xBuf.length);
Array.Copy(this.v0, 0, this.v, 0, this.v0.length);
this.xOff = 0;
Array.Copy(this.X0, 0, this.X, 0, this.X0.length)
},
GetByteLength: function() {
return this.BYTE_LENGTH
},
ProcessBlock: function(flasss) {
var i;
var ww = this.X;
var ww_ = new Array(64);
for (i = 16; i < 68; i++) {
ww[i] = this.P1(ww[i - 16] ^ ww[i - 9] ^ (this.ROTATE(ww[i - 3], 15))) ^ (this.ROTATE(ww[i - 13], 7)) ^ ww[i - 6]
}
for (i = 0; i < 64; i++) {
ww_[i] = ww[i] ^ ww[i + 4]
}
var vv = this.v;
var vv_ = this.v_;
Array.Copy(vv, 0, vv_, 0, this.v0.length);
var SS1, SS2, TT1, TT2, aaa;
for (i = 0; i < 16; i++) {
//if(flasss) var ttshow = "\n" + i + "娆?;
var ffxxx = false;
if(flasss && i == 0) {ffxxx = false;alert(1);};
aaa = this.ROTATE(vv_[0], 12,ffxxx);
if(flasss && i == 0) {alert(aaa)};
if(flasss) ttshow += ("\naaa:" + aaa);
//if(flasss && i>=15) alert("i" + i + ",vv_[0]" + vv_[0] + "aaa" + aaa);
//if(flasss && i>=15) alert("vv_[4]" + vv_[4]);
SS1 = Int32.parse(Int32.parse(aaa + vv_[4]) + this.ROTATE(this.T_00_15, i));
if(flasss) ttshow += ("\nSS1:" + SS1);
//if(flasss && i>=10) alert("SS1" + SS1);
SS1 = this.ROTATE(SS1, 7);
if(flasss) ttshow += ("\nSS1:" + SS1);
//if(flasss && i>=10) alert("SS1" + SS1);
SS2 = SS1 ^ aaa;
if(flasss) ttshow += ("\nSS2:" + SS2);
//if(flasss && i>=10) alert("SS2" + SS2);
TT1 = Int32.parse(Int32.parse(this.FF_00_15(vv_[0], vv_[1], vv_[2]) + vv_[3]) + SS2) + ww_[i];
if(flasss) ttshow += ("\nTT1:" + TT1);
//if(flasss && i>=10) alert("TT1" + TT1);
TT2 = Int32.parse(Int32.parse(this.GG_00_15(vv_[4], vv_[5], vv_[6]) + vv_[7]) + SS1) + ww[i];
if(flasss) ttshow += ("\nTT2:" + TT2);
//if(flasss && i>=10) alert("TT2" + TT2);
vv_[3] = vv_[2];
if(flasss) ttshow += ("\nvv_[3]:" + vv_[3]);
vv_[2] = this.ROTATE(vv_[1], 9);
if(flasss) ttshow += ("\nvv_[2]:" + vv_[2]);
vv_[1] = vv_[0];
if(flasss) ttshow += ("\nvv_[1]:" + vv_[1]);
//if(flasss && i>=10) alert("vv_[0]" + SS2);
vv_[0] = TT1;
if(flasss) ttshow += ("\nvv_[0]:" + vv_[0]);
//if(flasss) alert("vv_[0]" + SS2);
vv_[7] = vv_[6];
if(flasss) ttshow += ("\nvv_[7]:" + vv_[7]);
vv_[6] = this.ROTATE(vv_[5], 19);
if(flasss) ttshow += ("\nvv_[6]:" + vv_[6]);
vv_[5] = vv_[4];
if(flasss) ttshow += ("\nvv_[5]:" + vv_[5]);
var xxx = false;
//if(flasss && i>=14) {alert("TT2" + TT2);xxx=true};
vv_[4] = this.P0(TT2,xxx);
if(flasss) ttshow += ("\nvv_[4]:" + vv_[4]);
//if(flasss && i>=14) alert("vv_[4]" + vv_[4]);
if(flasss) $("#test111").html($("#test111").html() + ttshow);

}
//if(flasss) alert("2vv_" + vv_);
for (i = 16; i < 64; i++) {
aaa = this.ROTATE(vv_[0], 12);
SS1 = Int32.parse(Int32.parse(aaa + vv_[4]) + this.ROTATE(this.T_16_63, i));
SS1 = this.ROTATE(SS1, 7);
SS2 = SS1 ^ aaa;
TT1 = Int32.parse(Int32.parse(this.FF_16_63(vv_[0], vv_[1], vv_[2]) + vv_[3]) + SS2) + ww_[i];
TT2 = Int32.parse(Int32.parse(this.GG_16_63(vv_[4], vv_[5], vv_[6]) + vv_[7]) + SS1) + ww[i];
vv_[3] = vv_[2];
vv_[2] = this.ROTATE(vv_[1], 9);
vv_[1] = vv_[0];
vv_[0] = TT1;
vv_[7] = vv_[6];
vv_[6] = this.ROTATE(vv_[5], 19);
vv_[5] = vv_[4];
vv_[4] = this.P0(TT2)
}
for (i = 0; i < 8; i++) {
vv[i] ^= Int32.parse(vv_[i])
}
this.xOff = 0;
Array.Copy(this.X0, 0, this.X, 0, this.X0.length)
},
ProcessWord: function(in_Renamed, inOff) {
var n = in_Renamed[inOff] << 24;
n |= (in_Renamed[++inOff] & 0xff) << 16;
n |= (in_Renamed[++inOff] & 0xff) << 8;
n |= (in_Renamed[++inOff] & 0xff);
this.X[this.xOff] = n;
if (++this.xOff == 16) {
this.ProcessBlock()
}
},
ProcessLength: function(bitLength) {
if (this.xOff > 14) {
this.ProcessBlock()
}
this.X[14] = (this.URShiftLong(bitLength, 32));
this.X[15] = (bitLength & (0xffffffff))
},
IntToBigEndian: function(n, bs, off) {
bs[off] = Int32.parseByte(this.URShift(n, 24));
bs[++off] = Int32.parseByte(this.URShift(n, 16));
bs[++off] = Int32.parseByte(this.URShift(n, 8));
bs[++off] = Int32.parseByte(n)
},
DoFinal: function(out_Renamed, outOff) {
this.Finish();
for (var i = 0; i < 8; i++) {
this.IntToBigEndian(this.v[i], out_Renamed, outOff + i * 4)
}
this.Reset();
return this.DIGEST_LENGTH
},
Update: function(input) {
this.xBuf[this.xBufOff++] = input;
if (this.xBufOff == this.xBuf.length) {
this.ProcessWord(this.xBuf, 0);
this.xBufOff = 0
}
this.byteCount++
},
BlockUpdate: function(input, inOff, length) {
while ((this.xBufOff != 0) && (length > 0)) {
this.Update(input[inOff]);
inOff++;
length--
}
while (length > this.xBuf.length) {
this.ProcessWord(input, inOff);
inOff += this.xBuf.length;
length -= this.xBuf.length;
this.byteCount += this.xBuf.length
}
while (length > 0) {
this.Update(input[inOff]);
inOff++;
length--
}
},
Finish: function() {
var bitLength = (this.byteCount << 3);
this.Update((128));
while (this.xBufOff != 0)
this.Update((0));
this.ProcessLength(bitLength);
this.ProcessBlock()
},
ROTATE: function(x, n,ffxxx) {
var a = this.URShift(x, (32 - n));
if(ffxxx) alert(a);
var b = (x << n);
if(ffxxx) alert(b);
if(ffxxx) alert(a|b);
return  a|b;
},
P0: function(X,xxx) {
//if(xxx) alert("this.ROTATE((X), 9)!" + this.ROTATE((X), 9));
//if(xxx) alert("this.ROTATE((X), 17)!" + this.ROTATE((X), 17));
var a = this.ROTATE((X), 9);
//sleep(100);
var b = this.ROTATE((X), 17);
//sleep(100);
var c = (X) ^ a ^ b;
//sleep(100);
if(xxx) alert("a" + a + "b" + b + "c" + c);
return c;
},
P1: function(X) {
return ( (X) ^ this.ROTATE((X), 15) ^ this.ROTATE((X), 23))
},
FF_00_15: function(X, Y, Z) {
return ( X ^ Y ^ Z)
},
FF_16_63: function(X, Y, Z) {
return ( (X & Y) | (X & Z) | (Y & Z))
},
GG_00_15: function(X, Y, Z) {
return ( X ^ Y ^ Z)
},
GG_16_63: function(X, Y, Z) {
return ( (X & Y) | (~X & Z))
},
URShift: function(number, bits) {
if (number > Int32.maxValue || number < Int32.minValue) {
number = Int32.parse(number)
}
if (number >= 0) {
return number >> bits
} else {
return (number >> bits) + (2 << ~bits)
}
},
URShiftLong: function(number, bits) {
var returnV;
var big = new BigInteger();
big.fromInt(number);
if (big.signum() >= 0) {
returnV = big.shiftRight(bits).intValue()
} else {
var bigAdd = new BigInteger();
bigAdd.fromInt(2);
var shiftLeftBits = ~bits;
var shiftLeftNumber = '';
if (shiftLeftBits < 0) {
var shiftRightBits = 64 + shiftLeftBits;
for (var i = 0; i < shiftRightBits; i++) {
shiftLeftNumber += '0'
}
var shiftLeftNumberBigAdd = new BigInteger();
shiftLeftNumberBigAdd.fromInt(number >> bits);
var shiftLeftNumberBig = new BigInteger("10" + shiftLeftNumber,2);
shiftLeftNumber = shiftLeftNumberBig.toRadix(10);
var r = shiftLeftNumberBig.add(shiftLeftNumberBigAdd);
returnV = r.toRadix(10)
} else {
shiftLeftNumber = bigAdd.shiftLeft((~bits)).intValue();
returnV = (number >> bits) + shiftLeftNumber
}
}
return returnV
},
GetZ: function(g, pubKeyHex) {
var userId = CryptoJS.enc.Utf8.parse("1234567812345678");
var len = userId.words.length * 4 * 8;
this.Update((len >> 8 & 0x00ff));
this.Update((len & 0x00ff));
var userIdWords = this.GetWords(userId.toString());
this.BlockUpdate(userIdWords, 0, userIdWords.length);
var aWords = this.GetWords(g.curve.a.toBigInteger().toRadix(16));
var bWords = this.GetWords(g.curve.b.toBigInteger().toRadix(16));
var gxWords = this.GetWords(g.getX().toBigInteger().toRadix(16));
var gyWords = this.GetWords(g.getY().toBigInteger().toRadix(16));
var pxWords = this.GetWords(pubKeyHex.substr(0, 64));
var pyWords = this.GetWords(pubKeyHex.substr(64, 64));
this.BlockUpdate(aWords, 0, aWords.length);
this.BlockUpdate(bWords, 0, bWords.length);
this.BlockUpdate(gxWords, 0, gxWords.length);
this.BlockUpdate(gyWords, 0, gyWords.length);
this.BlockUpdate(pxWords, 0, pxWords.length);
this.BlockUpdate(pyWords, 0, pyWords.length);
var md = new Array(this.GetDigestSize());
this.DoFinal(md, 0);
return md
},
GetWords: function(hexStr) {
var words = [];
var hexStrLength = hexStr.length;
for (var i = 0; i < hexStrLength; i += 2) {
words[words.length] = parseInt(hexStr.substr(i, 2), 16)
}
return words
},
GetHex: function(arr) {
var words = [];
var j = 0;
for (var i = 0; i < arr.length * 2; i += 2) {
words[i >>> 3] |= parseInt(arr[j]) << (24 - (i % 8) * 4);
j++
}
var wordArray = new CryptoJS.lib.WordArray.init(words,arr.length);
return wordArray
}
};
Array.Clear = function(destinationArray, destinationIndex, length) {
for (elm in destinationArray) {
destinationArray[elm] = null
}
}
;
Array.Copy = function(sourceArray, sourceIndex, destinationArray, destinationIndex, length) {
var cloneArray = sourceArray.slice(sourceIndex, sourceIndex + length);
for (var i = 0; i < cloneArray.length; i++) {
destinationArray[destinationIndex] = cloneArray[i];
destinationIndex++
}
}
;
window.Int32 = {
minValue: -parseInt('10000000000000000000000000000000', 2),
maxValue: parseInt('1111111111111111111111111111111', 2),
parse: function(n) {
if (n < this.minValue) {
var bigInteger = new Number(-n);
var bigIntegerRadix = bigInteger.toString(2);
var subBigIntegerRadix = bigIntegerRadix.substr(bigIntegerRadix.length - 31, 31);
var reBigIntegerRadix = '';
for (var i = 0; i < subBigIntegerRadix.length; i++) {
var subBigIntegerRadixItem = subBigIntegerRadix.substr(i, 1);
reBigIntegerRadix += subBigIntegerRadixItem == '0' ? '1' : '0'
}
var result = parseInt(reBigIntegerRadix, 2);
return ( result + 1)
} else if (n > this.maxValue) {
var bigInteger = Number(n);
var bigIntegerRadix = bigInteger.toString(2);
var subBigIntegerRadix = bigIntegerRadix.substr(bigIntegerRadix.length - 31, 31);
var reBigIntegerRadix = '';
for (var i = 0; i < subBigIntegerRadix.length; i++) {
var subBigIntegerRadixItem = subBigIntegerRadix.substr(i, 1);
reBigIntegerRadix += subBigIntegerRadixItem == '0' ? '1' : '0'
}
var result = parseInt(reBigIntegerRadix, 2);
return -(result + 1)
} else {
return n
}
},
parseByte: function(n) {
if (n < 0) {
var bigInteger = new Number(-n);
var bigIntegerRadix = bigInteger.toString(2);
var subBigIntegerRadix = bigIntegerRadix.substr(bigIntegerRadix.length - 8, 8);
var reBigIntegerRadix = '';
for (var i = 0; i < subBigIntegerRadix.length; i++) {
var subBigIntegerRadixItem = subBigIntegerRadix.substr(i, 1);
reBigIntegerRadix += subBigIntegerRadixItem == '0' ? '1' : '0'
}
var result = parseInt(reBigIntegerRadix, 2);
return ( result + 1)
} else if (n > 255) {
var bigInteger = Number(n);
var bigIntegerRadix = bigInteger.toString(2);
return parseInt(bigIntegerRadix.substr(bigIntegerRadix.length - 8, 8), 2)
} else {
return n
}
}
};




/***sm2.js***/
function SM2Cipher(cipherMode) {
this.ct = 1;
this.p2 = null ;
this.sm3keybase = null ;
this.sm3c3 = null ;
this.key = new Array(32);
this.keyOff = 0;
if (typeof (cipherMode) != 'undefined') {
this.cipherMode = cipherMode
} else {
this.cipherMode = SM2CipherMode.C1C3C2
}
}
SM2Cipher.prototype = {
Reset: function() {
this.sm3keybase = new INER_SM3Digest();
this.sm3c3 = new INER_SM3Digest();
var xhex = ("0000000000" + this.p2.getX().toBigInteger().toRadix(16)).slice(- 64);
var yhex = ("0000000000" + this.p2.getY().toBigInteger().toRadix(16)).slice(- 64);
var xWords = this.GetWords(xhex);
var yWords = this.GetWords(yhex);
this.sm3keybase.BlockUpdate(xWords, 0, xWords.length);
this.sm3c3.BlockUpdate(xWords, 0, xWords.length);
this.sm3keybase.BlockUpdate(yWords, 0, yWords.length);
this.ct = 1;
this.NextKey()
},
NextKey: function() {
var sm3keycur = new INER_SM3Digest(this.sm3keybase);
sm3keycur.Update((this.ct >> 24 & 0x00ff));
sm3keycur.Update((this.ct >> 16 & 0x00ff));
sm3keycur.Update((this.ct >> 8 & 0x00ff));
sm3keycur.Update((this.ct & 0x00ff));
sm3keycur.DoFinal(this.key, 0);
this.keyOff = 0;
this.ct++
},
InitEncipher: function(userKey) {
var k = null ;
var c1 = null ;
var ec = new KJUR.crypto.ECDSA({
"curve": "sm2"
});
var keypair = ec.generateKeyPairHex();
k = new BigInteger(keypair.ecprvhex,16);
var pubkeyHex = keypair.ecpubhex;
c1 = ECPointFp.decodeFromHex(ec.ecparams['curve'], pubkeyHex);
this.p2 = userKey.multiply(k);
this.Reset();
return c1
},
EncryptBlock: function(data,falsss) {
this.sm3c3.BlockUpdate(data, 0, data.length);
if(falsss) alert(1);
for (var i = 0; i < data.length; i++) {
if (this.keyOff == this.key.length) {
this.NextKey()
}
data[i] ^= this.key[this.keyOff++]
//alert(this.key);
}
},
InitDecipher: function(userD, c1) {
this.p2 = c1.multiply(userD);
this.Reset()
},
DecryptBlock: function(data) {
for (var i = 0; i < data.length; i++) {
if (this.keyOff == this.key.length) {
this.NextKey()
}
data[i] ^= this.key[this.keyOff++]
}
this.sm3c3.BlockUpdate(data, 0, data.length)
},
Dofinal: function(c3) {
var yhex = ("0000000000" + this.p2.getY().toBigInteger().toRadix(16)).slice(- 64);
var yWords = this.GetWords(yhex);
this.sm3c3.BlockUpdate(yWords, 0, yWords.length);
this.sm3c3.DoFinal(c3, 0);
this.Reset()
},
Encrypt: function(pubKey, plaintext) {
var data = new Array(plaintext.length);
Array.Copy(plaintext, 0, data, 0, plaintext.length);
var c1 = this.InitEncipher(pubKey);
this.EncryptBlock(data);
var c3 = new Array(32);
this.Dofinal(c3);
var xhex = ("0000000000" + c1.getX().toBigInteger().toRadix(16)).slice(- 64);
var yhex = ("0000000000" + c1.getY().toBigInteger().toRadix(16)).slice(- 64);
//var hexString = c1.getX().toBigInteger().toRadix(16) + c1.getY().toBigInteger().toRadix(16) + this.GetHex(data).toString() + this.GetHex(c3).toString();
var hexString = xhex + yhex + this.GetHex(data).toString() + this.GetHex(c3).toString();
if (this.cipherMode == SM2CipherMode.C1C3C2) {
hexString = xhex + yhex + this.GetHex(c3).toString() + this.GetHex(data).toString()
//hexString = c1.getX().toBigInteger().toRadix(16) + c1.getY().toBigInteger().toRadix(16) + this.GetHex(c3).toString() + this.GetHex(data).toString()
}
return hexString
},
GetWords: function(hexStr) {
var words = [];
var hexStrLength = hexStr.length;
for (var i = 0; i < hexStrLength; i += 2) {
words[words.length] = parseInt(hexStr.substr(i, 2), 16)
}
return words
},
GetHex: function(arr) {
for(var n = 0;n < arr.length;n++){
arr[n] &= 0x000000ff;
}
var words = [];
var j = 0;
for (var i = 0; i < arr.length * 2; i += 2) {
words[i >>> 3] |= parseInt(arr[j]) << (24 - (i % 8) * 4);
j++
}
var wordArray = new CryptoJS.lib.WordArray.init(words,arr.length);
return wordArray
},
Decrypt: function(privateKey, ciphertext) {
var hexString = ciphertext;
var c1X = hexString.substr(0, 64);
var c1Y = hexString.substr(0 + c1X.length, 64);
var encrypData = hexString.substr(c1X.length + c1Y.length, hexString.length - c1X.length - c1Y.length - 64);
var c3 = hexString.substr(hexString.length - 64);
if (this.cipherMode == SM2CipherMode.C1C3C2) {
c3 = hexString.substr(c1X.length + c1Y.length, 64);
encrypData = hexString.substr(c1X.length + c1Y.length + 64)
}
var data = this.GetWords(encrypData);
var c1 = this.CreatePoint(c1X, c1Y);
this.InitDecipher(privateKey, c1);
this.DecryptBlock(data);
var c3_ = new Array(32);
this.Dofinal(c3_);
var isDecrypt = this.GetHex(c3_).toString() == c3;
if (isDecrypt) {
var wordArray = this.GetHex(data);
var decryptData = CryptoJS.enc.Utf8.stringify(wordArray);
return decryptData
} else {
return ''
}
},
CreatePoint: function(x, y) {
var ec = new KJUR.crypto.ECDSA({
"curve": "sm2"
});
var ecc_curve = ec.ecparams['curve'];
var pubkeyHex = '04' + x + y;
var point = ECPointFp.decodeFromHex(ecc_curve/*ec.ecparams['curve']*/, pubkeyHex);
return point
}
};
window.SM2CipherMode = {
C1C2C3: '0',
C1C3C2: '1'
};

function SM2_GenerateKeyPair() {
var r = {};
var curve = "sm2";
var ec = new KJUR.crypto.ECDSA({"curve": curve});
var keypair = ec.generateKeyPairHex();

r.privatekey = keypair.ecprvhex;
r.publickey = keypair.ecpubhex;

return r;
}

function SM2_Encrypt(msg, pubkeyHex, cipherMode, rflage) {
var r = '';
var curve = "SM2";
var msgData = CryptoJS.enc.Utf8.parse(msg);
var puh=pubkeyHex.length;
if (puh > 64 * 2) {
pubkeyHex = pubkeyHex.substr(pubkeyHex.length - 64 * 2);
}

var xHex = pubkeyHex.substr(0, 64);
var yHex = pubkeyHex.substr(64);

var cipher = new SM2Cipher(cipherMode);


var userKey = cipher.CreatePoint(xHex, yHex);

msgData = cipher.GetWords(msgData.toString());

var encryptData = cipher.Encrypt(userKey, msgData);
return '04' + encryptData;
}

function SM2_Decrypt(data, prvkey, cipherMode, rflage) {
var r = '';

var privateKey = new BigInteger(prvkey, 16);

var cipher = new SM2Cipher(cipherMode);

if(rflage)
data = data.slice(2, data.length);

r = cipher.Decrypt(privateKey, data);

return r;
}

//////////////////////////////////////////////////////////////////////////////
//SM2 stop
//////////////////////////////////////////////////////////////////////////////









