import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import CopyPlugin from 'vite-plugin-files-copy'
const rootPath = path.join(__dirname, './')
console.log(" process.env.VITE_USER_NODE_ENV==", process.env.VITE_USER_NODE_ENV);
const EnvVar = process.env.VITE_USER_NODE_ENV;
// https://vitejs.dev/config/
export default defineConfig({
  publicDir: path.resolve('static'), //静态资源文件夹
  server: {
    host: true,
  },
  build: {
    outDir: path.join(rootPath, EnvVar === 'testc1'? 'distcs1':  (EnvVar === 'testc2' ? 'distcs2' : (EnvVar === 'testz' ? 'distzsc' :'dist'
    ))),
    assetsDir: 'assets', //指定静态资源放置处
    minify: 'terser', // 混淆器,terser构建后文件体积更小
    terserOptions: {
      // 清除console和debugger(minify: 'terser',)设置后这个terserOptions才有
      compress: {
        drop_console: true,
        drop_debugger: true,
      },
      keep_classnames: true,
    },
    rollupOptions: {
      input: path.join(rootPath, '/index.html'),
      output: {
        //自动分割包名输出 chunkSizeWarningLimit 配置大小
        chunkFileNames: 'js/[name]-[hash].js', //入口文件名
        entryFileNames: 'js/[name]-[hash].js', //出口文件名位置
        // assetFileNames: '[ext]/[name]-[hash].[ext]', //静态文件名位置
        assetFileNames: 'assets/static/[name]-[hash].[ext]',//静态文件名位置
        manualChunks(id) {
          if (id.includes('node_modules')) {
            return id
              .toString()
              .split('node_modules/')[1]
              .split('/')[0]
              .toString()
          }
        },
      },
    },
  },
  plugins: [
    vue(),
    CopyPlugin({
      patterns: [
        {
          from: path.join(rootPath, 'static'),
          ignore: ['*.md'],
        },
      ],
    }),
    CopyPlugin({
      patterns: [
        {
          from: path.join(rootPath, 'src/assets/svg'), // string 相对项目下相对路径
          to: path.join(rootPath, EnvVar === 'testc1'? 'distcs1/assets/svg':  (EnvVar === 'testc2' ? 'distcs2/assets/svg' : (EnvVar === 'testz' ? 'distzsc/assets/svg' :'dist/assets/svg'))),
        },
      ],
    }),
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      ASSET: path.resolve(__dirname, './src/assets'),
      UTIL: path.resolve(__dirname, './src/utils'),
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "@/assets/mixins.scss";`,
      },
    },
  },
})
